package com.gbs.grandmaster.common.entity;

import com.gbs.grandmaster.entity.ExamSchedule;
import com.gbs.grandmaster.entity.cassandra.ExamAttendance;
import com.gbs.grandmaster.entity.cassandra.ExamLog;

public class ExamInfoWrapper {
	
	private ExamSchedule examSchedule;
	private ExamAttendance examAttendance;
	private ExamLog examLog;
	
	public ExamSchedule getExamSchedule() {
		return examSchedule;
	}
	public void setExamSchedule(ExamSchedule examSchedule) {
		this.examSchedule = examSchedule;
	}
	public ExamAttendance getExamAttendance() {
		return examAttendance;
	}
	public void setExamAttendance(ExamAttendance examAttendance) {
		this.examAttendance = examAttendance;
	}
	public ExamLog getExamLog() {
		return examLog;
	}
	public void setExamLog(ExamLog examLog) {
		this.examLog = examLog;
	}
	
	

}
