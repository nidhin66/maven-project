package com.gbs.grandmaster.common.entity;

public enum JOB_STATUS {
	SCHEDULED,STARTED,FAILED,COMPLETED;
	private  JOB_STATUS() {
		
	}

}
