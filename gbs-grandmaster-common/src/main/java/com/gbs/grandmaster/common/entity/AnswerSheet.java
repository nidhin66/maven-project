package com.gbs.grandmaster.common.entity;

import com.gbs.grandmaster.entity.ExamSchedule;
import com.gbs.grandmaster.entity.cassandra.ExamAttendance;

public class AnswerSheet {
	
	private ExamSchedule exam;
	private ExamAttendance attendance;
	public ExamSchedule getExam() {
		return exam;
	}
	public void setExam(ExamSchedule exam) {
		this.exam = exam;
	}
	public ExamAttendance getAttendance() {
		return attendance;
	}
	public void setAttendance(ExamAttendance attendance) {
		this.attendance = attendance;
	}
	
	

}
