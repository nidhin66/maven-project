package com.gbs.grandmaster.common.entity;

public enum QUESTION_PAPER_STATUS {
	ALLOCATED("A"),DOWNLOADED("D");
	private final String code;
	private  QUESTION_PAPER_STATUS(String code) {
		this.code=code;
	}
	public String getCode() {
		return code;
	}
	
}
