package com.gbs.grandmaster.common.entity;

public enum DURATION_TYPE {	
		HOURS("HOURS"),
		DAYS("DAYS"),
	    MONTHS("MONTHS"),
	    YEARS("YEARS");
		private final String type;

	 	private DURATION_TYPE(String type) {
	        this.type = type;
	    }

		public String getType() {
			return type;
		}
	 	
	 	
}
