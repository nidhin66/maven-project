package com.gbs.grandmaster.common.entity;




public enum OfficeLevel{
	
	GLOBAL("Global Office"),
	REGION("Regional Office"),
	COUNTRY("Country"),
	PROVINCE("State or Province"),
	BRANCH("Branch"),
	LOCATION("Location");
	
	private String value;
	
	private OfficeLevel(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	
}
