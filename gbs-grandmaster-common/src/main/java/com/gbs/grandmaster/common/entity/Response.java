package com.gbs.grandmaster.common.entity;

import java.util.List;

public class Response {
	
	private boolean success;
	private Object data;
	private List<ApplicationError> errors = null;
	
	public Response() {
		
	}
	
	public Response(boolean success,Object data,List<ApplicationError> errorList) {
		this.success = success;
		this.data = data;
		this.errors = errorList;
	}
	
	public Response(List<ApplicationError> errorList) {
		this.success = false;
		this.data = null;
		this.errors = errorList;
	}
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public List<ApplicationError> getErrors() {
		return errors;
	}
	public void setErrors(List<ApplicationError> errors) {
		this.errors = errors;
	}
	
	
	
}
