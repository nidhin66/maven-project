
/*************************************************************************************

Organization:  GBS Plus Pvt Ltd.
	                  WINDSOR apartment,
	                  Ground floor lower level,
	                  TC no. 4/1256(38),
	                  Kuravankonam, Kowdiar Village,
	                  Trivandrum - 695 003
 
Copyright 2016 (C) GBS Plus Pvt Ltd. (www.gbs-plus.com)
All rights reserved.

**************************************************************************************/
package com.gbs.grandmaster.common.entity;

import java.io.Serializable;

public interface Identifiable<T> extends Serializable {
	public T getId(); 
	public int getVersion();
}
