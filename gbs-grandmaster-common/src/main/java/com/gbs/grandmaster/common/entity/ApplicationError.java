package com.gbs.grandmaster.common.entity;

public class ApplicationError {
	
	private String errorCode;
	private String errorMessage;
	private ERROR_LEVEL level;
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public ERROR_LEVEL getLevel() {
		return level;
	}
	public void setLevel(ERROR_LEVEL level) {
		this.level = level;
	}
	
	

}
