package com.gbs.grandmaster.common.entity;

public enum GENDER {
	MALE("M"),
	FEMALE("F"),
	OTHERS("O");
	private final String gender;
	private GENDER(String gender) {
		this.gender = gender;
	}
	public String getGender() {
		return gender;
	}

}
