package com.gbs.grandmaster.common.entity;

public enum QUESTION_TYPE {
	OBJECTIVE("O"),
	DESCRIPTIVE("D"),
	FILL_IN_BLANK("F"),
	MATCHING("M");
	private final String code ;
	private QUESTION_TYPE(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	
}
