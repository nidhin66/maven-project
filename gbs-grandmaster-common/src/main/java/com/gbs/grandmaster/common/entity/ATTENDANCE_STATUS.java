package com.gbs.grandmaster.common.entity;

public enum ATTENDANCE_STATUS {
	STARTED("S"),FINISHED("F"),SUBMITTED("D"),NOT_ATTEMPTED("N");
	private final String code ;
	private ATTENDANCE_STATUS(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	
}
