package com.gbs.grandmaster.common;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

public class CommonUtilitties {

	public static boolean isNullorEmpty(String str) {
		if (null != str && !"".equals(str) && !"NULL".equalsIgnoreCase(str)) {
			return false;
		} else {
			return true;
		}
	}

	public static <T> LinkedList<T> convertALtoLL(List<T> aL) {

		// Create an empty LinkedList
		LinkedList<T> lL = new LinkedList<>();

		// Iterate through the aL
		for (T t : aL) {

			// Add each element into the lL
			lL.add(t);
		}

		// Return the converted LinkedList
		return lL;
	}

	public static void writeFile(String path, String content) {
		try (FileWriter file = new FileWriter(path)) {

			file.write(content);
			file.flush();

		} catch (IOException e) {

		}
	}

	public static String readFile(String fileName) {
		StringBuilder sb = new StringBuilder();
		try {
			FileInputStream fis = new FileInputStream(fileName);
			byte[] buffer = new byte[10];

			while (fis.read(buffer) != -1) {
				sb.append(new String(buffer));
				buffer = new byte[10];
			}
			fis.close();
		} catch (IOException e) {

		}

		String content = sb.toString();
		return content;
	}

	public static String getMacId() {
		String macId = null;
		try {
			InetAddress ip = InetAddress.getLocalHost();

			Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();
			while (networks.hasMoreElements()) {
				NetworkInterface network = networks.nextElement();
				byte[] mac = network.getHardwareAddress();

				if (mac != null) {

					StringBuilder sb = new StringBuilder();
					for (int mac_length = 0; mac_length < mac.length; mac_length++) {
						sb.append(String.format("%02X%s", mac[mac_length], (mac_length < mac.length - 1) ? "-" : ""));
					}
					macId = sb.toString();
				}
			}
		} catch (UnknownHostException e) {

		} catch (SocketException e) {

		}
		return macId;
	}

	public static String getIpAddress() {
		// Find public IP address
		String systemipaddress = "";
		try {
			URL url_name = new URL("http://bot.whatismyipaddress.com");

			BufferedReader sc = new BufferedReader(new InputStreamReader(url_name.openStream()));

			// reads system IPAddress
			systemipaddress = sc.readLine().trim();
		} catch (Exception e) {

		}
		if (isNullorEmpty(systemipaddress)) {
			try {
				InetAddress localhost = InetAddress.getLocalHost();
				systemipaddress = localhost.getHostAddress().trim();
			} catch (UnknownHostException e) {

			}
		}
		return systemipaddress;
	}

}
