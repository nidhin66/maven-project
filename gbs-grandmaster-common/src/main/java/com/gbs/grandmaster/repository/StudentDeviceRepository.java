package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.StudentDevice;

@Repository
public interface StudentDeviceRepository extends JpaRepository<StudentDevice, Long>,PagingAndSortingRepository<StudentDevice, Long>{

}
