package com.gbs.grandmaster.repository.cassandra;

import org.springframework.data.cassandra.repository.CassandraRepository;

import com.gbs.grandmaster.entity.cassandra.FinalAnswer;

public interface FinalAnswerRepository  extends CassandraRepository<FinalAnswer> {
	FinalAnswer findByKeyStudentIdAndKeyExamScheduleIdAndKeyAttempt(final long studentId,final long examScheduleId,int attempt);
}
