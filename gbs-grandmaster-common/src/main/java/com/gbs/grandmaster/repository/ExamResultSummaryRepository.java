package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.gbs.grandmaster.entity.ExamResultSummary;
import com.gbs.grandmaster.entity.ExamResultID;

public interface ExamResultSummaryRepository extends JpaRepository<ExamResultSummary, ExamResultID>,PagingAndSortingRepository<ExamResultSummary, ExamResultID>{

}
