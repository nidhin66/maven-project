package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.gbs.grandmaster.common.entity.JOB_STATUS;
import com.gbs.grandmaster.entity.JobRequest;
import java.util.List;

public interface JobRequestRepository extends JpaRepository<JobRequest, Long>,PagingAndSortingRepository<JobRequest, Long>{
		
	public List<JobRequest> findByJobStatus(JOB_STATUS jobStatus);

}
