package com.gbs.grandmaster.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.gbs.grandmaster.entity.User;



public interface UserRepository extends PagingAndSortingRepository<User,Long>{

	User findByUserName(String userId);

}
