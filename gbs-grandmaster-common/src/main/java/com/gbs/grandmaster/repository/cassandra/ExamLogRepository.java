package com.gbs.grandmaster.repository.cassandra;

import org.springframework.data.cassandra.repository.CassandraRepository;

import com.gbs.grandmaster.entity.cassandra.ExamLog;

public interface ExamLogRepository extends CassandraRepository<ExamLog> {
	
	ExamLog findByKeyStudentIdAndKeyExamScheduleIdAndKeyAttempt(final long studentId,final long examScheduleId,int attempt);


}
