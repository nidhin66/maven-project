package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.ExamDefinition;

@Repository
public interface ExamDefinitionRepository extends JpaRepository<ExamDefinition, Long>,PagingAndSortingRepository<ExamDefinition, Long> {

}
