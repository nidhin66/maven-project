package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.Notification;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long>,PagingAndSortingRepository<Notification, Long>{

	
}
