package com.gbs.grandmaster.repository.cassandra;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import com.gbs.grandmaster.entity.cassandra.QuestionAttemptTime;

public interface QuestionAttemptTimeRepository extends CassandraRepository<QuestionAttemptTime> {
	
	@Query("select * from QUESTION_ATTEMPT_TIME where student_id=?0 and exam_schedule_id=?1 and attempt=?2 ALLOW FILTERING")
	public List<QuestionAttemptTime> findQuestionAttemptTimeByAttendance(long studentId,long examScheduleId,int attempt);
	
	@Query("select * from QUESTION_ATTEMPT_TIME where student_id=?0 and exam_schedule_id=?1 and attempt=?2 and question_id=?3 ALLOW FILTERING")
	public List<QuestionAttemptTime> findQuestionAttemptTimeByQuestionId(long studentId,long examScheduleId,int attempt,long questionId);

}
