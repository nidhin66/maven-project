package com.gbs.grandmaster.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.ExamSchedule;

@Repository
public interface ExamScheduleRepository extends JpaRepository<ExamSchedule, Long>,PagingAndSortingRepository<ExamSchedule, Long>{

	@Query("SELECT e FROM ExamSchedule e WHERE e.batch_id = :batch_id and e.resultImmeadiate = false order by e.startDateTime desc")
	public List<ExamSchedule> findExamByBatch(@Param("batch_id") long batch_id);
	
	@Query("SELECT e FROM ExamSchedule e WHERE e.batch_id = :batch_id and e.resultImmeadiate = true order by e.startDateTime desc")
	public List<ExamSchedule> findMockExamByBatch(@Param("batch_id") long batch_id);
	
	@Query("SELECT e FROM ExamSchedule e WHERE e.batch_id = :batch_id and e.resultImmeadiate = false and e.status = 5 order by e.startDateTime desc")
	public List<ExamSchedule> findResultPublishedExamByBatch(@Param("batch_id") long batch_id);
	
	@Query("SELECT e FROM ExamSchedule e WHERE e.batch_id = :batch_id and e.resultImmeadiate = true and e.status != 6 order by e.startDateTime desc")
	public List<ExamSchedule> findResultPublishedMockExamByBatch(@Param("batch_id") long batch_id);
	
}
