package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.Language;

@Repository
public interface LanguageRepository extends JpaRepository<Language, Long>,PagingAndSortingRepository<Language, Long>{

}
