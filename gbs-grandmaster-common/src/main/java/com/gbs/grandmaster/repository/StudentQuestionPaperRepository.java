package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gbs.grandmaster.entity.StudentQuestionPaper;


public interface StudentQuestionPaperRepository extends  JpaRepository<StudentQuestionPaper, Long>{
	public StudentQuestionPaper findByStudentIdAndExamScheduleId(long studentId,long examScheduleId);
}
