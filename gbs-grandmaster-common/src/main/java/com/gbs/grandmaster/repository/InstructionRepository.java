package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.Instruction;

@Repository
public interface InstructionRepository extends JpaRepository<Instruction, Long>,PagingAndSortingRepository<Instruction, Long>{

}
