package com.gbs.grandmaster.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.ExamSchedule;


@Repository
public interface MenuGroupRepository extends JpaRepository<ExamSchedule, Long>{
		
}
