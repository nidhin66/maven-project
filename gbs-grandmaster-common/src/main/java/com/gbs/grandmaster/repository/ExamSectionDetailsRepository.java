package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.ExamSectionDetails;

@Repository
public interface ExamSectionDetailsRepository extends JpaRepository<ExamSectionDetails, Long>,PagingAndSortingRepository<ExamSectionDetails, Long>{

}
