package com.gbs.grandmaster.repository.cassandra;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import com.gbs.grandmaster.entity.cassandra.Answer;


public interface AnswerRepository extends CassandraRepository<Answer> {
	
	@Query("select * from Answer where student_id=?0 and exam_schedule_id=?1 ALLOW FILTERING")
	public List<Answer> findByKeyStudentIdAndKeyExamScheduleId(final long studentId,final long examScheduleId);
	@Query("select * from Answer where student_id=?0 and exam_schedule_id=?1 and attempt=?2 ALLOW FILTERING")
	public List<Answer> findByKeyStudentIdAndKeyExamScheduleIdAndKeyAttempt(long studentId,long examScheduleId,int attempt);

}
