package com.gbs.grandmaster.repository.cassandra;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import com.gbs.grandmaster.entity.cassandra.ActiveUserLog;


public interface ActiveUserLogRepository extends CassandraRepository<ActiveUserLog> {
	
	@Query("select * from ACTIVE_USER_LOG where username=?0")
	ActiveUserLog findByUserName(String userName);

}
