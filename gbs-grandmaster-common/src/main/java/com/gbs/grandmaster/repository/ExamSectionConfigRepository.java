package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.ExamSectionConfig;

@Repository
public interface ExamSectionConfigRepository extends JpaRepository<ExamSectionConfig, Long>,PagingAndSortingRepository<ExamSectionConfig, Long>{

}
