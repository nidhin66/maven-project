package com.gbs.grandmaster.repository.cassandra;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.cassandra.ExamAttendance;

@Repository
public interface ExamAttendanceRespository extends CassandraRepository<ExamAttendance> {
    
	@Query("select * from EXAM_ATTENDANCE where student_id=?0 ALLOW FILTERING")
	List<ExamAttendance> findByKeyStudentId(final long studentId);
	
	@Query("select * from EXAM_ATTENDANCE where student_id=?0 and exam_schedule_id=?1 ALLOW FILTERING")
	List<ExamAttendance> findByKeyStudentIdAndKeyExamScheduleId(final long studentId,final long examScheduleId);
	
	ExamAttendance findByKeyStudentIdAndKeyExamScheduleIdAndKeyAttempt(final long studentId,final long examScheduleId,int attempt);
	
	@Query("select * from EXAM_ATTENDANCE where student_id=?0 and exam_schedule_id=?1 limit 1 ALLOW FILTERING")
	ExamAttendance findLastAttemptAttendance(final long studentId,final long examScheduleId);
}
