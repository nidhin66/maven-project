package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.StudentLocation;

@Repository
public interface StudentLocationRepository extends JpaRepository<StudentLocation, Long>,PagingAndSortingRepository<StudentLocation, Long>{

}
