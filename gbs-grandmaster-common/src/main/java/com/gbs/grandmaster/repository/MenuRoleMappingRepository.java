package com.gbs.grandmaster.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.ExamSchedule;
import com.gbs.grandmaster.entity.MenuRoleMapping;
@Repository
public interface MenuRoleMappingRepository extends JpaRepository<ExamSchedule, Long>{		
	
	@Query("SELECT m FROM MenuRoleMapping m WHERE m.role.id = :roleId and m.office.id=:officeId")
    public MenuRoleMapping findByRoleIdAndOfficeId(@Param("roleId") Long roleId,@Param("officeId") Long officeId);
}
