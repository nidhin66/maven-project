package com.gbs.grandmaster.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.gbs.grandmaster.entity.ExamSchedule;
public interface RoleRepository extends JpaRepository<ExamSchedule, Long>{

}
