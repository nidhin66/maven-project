package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.gbs.grandmaster.entity.ExamResult;
import com.gbs.grandmaster.entity.ExamResultID;

public interface ExamResultRepository extends JpaRepository<ExamResult, ExamResultID>,PagingAndSortingRepository<ExamResult, ExamResultID>{


}
