package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.ApplicationConfig;

@Repository
public interface AppConfigRepository extends JpaRepository<ApplicationConfig, String>{
	
	public ApplicationConfig findByProperty(String property);

}
