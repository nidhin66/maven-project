package com.gbs.grandmaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.Choice;

@Repository
public interface ChoiceRepository extends JpaRepository<Choice, Long>,PagingAndSortingRepository<Choice, Long>{

}
