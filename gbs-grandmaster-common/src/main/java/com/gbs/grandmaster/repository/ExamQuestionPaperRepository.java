package com.gbs.grandmaster.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gbs.grandmaster.entity.ExamQuestionPaper;

@Repository
public interface ExamQuestionPaperRepository extends  JpaRepository<ExamQuestionPaper, Long>{
	
	@Query("SELECT e FROM ExamQuestionPaper e WHERE e.exam_schedule_id = :exam_schedule_id")
	public List<ExamQuestionPaper> findByExam_schedule_id(@Param("exam_schedule_id") long exam_schedule_id);

}
