package com.gbs.grandmaster.entity.cassandra;

import java.util.Date;

import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

@Table("ANSWER")
public class Answer {

	@PrimaryKey
	private AnswerKey key;
	
	@Column("section_id")
	private long sectionId;

	@Column("content")
	private String content;
	@Column("correct")
	private boolean correct;
	@Column("marked")
	private boolean marked;

	@Column("mark")
	private double mark;

	@Column("negative_mark")
	private double negativeMark;

	@Column("update_time")
	private Date updateTime;

	public Answer() {

	}

	public Answer(AnswerKey key) {
		this.key = key;
	}

	public AnswerKey getKey() {
		return key;
	}

	public void setKey(AnswerKey key) {
		this.key = key;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}

	public double getNegativeMark() {
		return negativeMark;
	}

	public void setNegativeMark(double negativeMark) {
		this.negativeMark = negativeMark;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public boolean isMarked() {
		return marked;
	}

	public void setMarked(boolean marked) {
		this.marked = marked;
	}

	public long getSectionId() {
		return sectionId;
	}

	public void setSectionId(long sectionId) {
		this.sectionId = sectionId;
	}

}
