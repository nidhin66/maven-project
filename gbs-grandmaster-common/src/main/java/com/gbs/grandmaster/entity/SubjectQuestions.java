package com.gbs.grandmaster.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;

//@Entity
//@Table(name = "GM_SUBJECT_QUESTIONS")
public class SubjectQuestions implements Identifiable<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	@Version
	private int version;
	
	private Subject subject;
	
	@OneToMany(mappedBy="id", fetch=FetchType.EAGER)
	private List<Question> questions;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
	

}
