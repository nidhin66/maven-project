package com.gbs.grandmaster.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;

@Embeddable
public class ExamResultSummaryID implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long examId;

	private Long studentId;
	

	private int attempt;
	
	 

	public ExamResultSummaryID() {

	}

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public int getAttempt() {
		return attempt;
	}

	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof ExamResultSummaryID))
			return false;
		ExamResultSummaryID that = (ExamResultSummaryID) o;
		return Objects.equals(getExamId(), that.getExamId()) && Objects.equals(getStudentId(), that.getStudentId())
				&& Objects.equals(getAttempt(), that.getAttempt());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getExamId(), getStudentId(), getAttempt());
	}

	

}
