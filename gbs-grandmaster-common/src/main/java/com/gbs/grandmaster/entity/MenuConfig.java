package com.gbs.grandmaster.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.gbs.grandmaster.common.entity.Identifiable;


@Entity
@Table(name="GM_MENU_CONFIG")
public class MenuConfig implements Identifiable<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Version
	private int version;
	
	@OneToMany	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("ordinal asc")
	private Collection<MenuGroup> menuGroups;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Collection<MenuGroup> getMenuGroups() {
		return menuGroups;
	}

	public void setMenuGroups(Collection<MenuGroup> menuGroups) {
		this.menuGroups = menuGroups;
	}

		

}
