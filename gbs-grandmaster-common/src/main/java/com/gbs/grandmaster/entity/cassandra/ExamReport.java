package com.gbs.grandmaster.entity.cassandra;

import java.util.List;
import java.util.Map;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;


public class ExamReport {
	
	
	private Map<Long, List<QuestionAttemptTime>> timeDetails;
	
	public Map<Long, List<QuestionAttemptTime>> getTimeDetails() {
		return timeDetails;
	}

	public void setTimeDetails(Map<Long, List<QuestionAttemptTime>> timeDetails) {
		this.timeDetails = timeDetails;
	}
	
}
