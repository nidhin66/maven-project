package com.gbs.grandmaster.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;

//@Entity
//@Table(name = "GM_BATCH")
public class Batch implements Identifiable<Long> {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	@Id
//	@Column(nullable = false, length = 25)
//	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	
	@Version
	private int version;
	
	@ManyToOne
	private Course course;
	
	private Date startDate;
	
	private Date endDate;
	
	private int noOfSeats;
	
	private Branch branch;

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getNoOfSeats() {
		return noOfSeats;
	}

	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	
}
