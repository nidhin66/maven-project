package com.gbs.grandmaster.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;
import com.gbs.grandmaster.common.entity.QUESTION_TYPE;

@Entity
@Table(name = "GM_EXAM_SECTION_DETAILS")
public class ExamSectionDetails implements Identifiable<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	@Version
	private int version;
	
	private int noOfQuestions;
	
	private long subject_id;
	
	private QUESTION_TYPE questionType;
	
	private double markPerQuestion;
	
	private double negativeMarkPerQuestion;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getNoOfQuestions() {
		return noOfQuestions;
	}

	public void setNoOfQuestions(int noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}

	public long getSubject_id() {
		return subject_id;
	}

	public void setSubject_id(long subject_id) {
		this.subject_id = subject_id;
	}

	public QUESTION_TYPE getQuestionType() {
		return questionType;
	}

	public void setQuestionType(QUESTION_TYPE questionType) {
		this.questionType = questionType;
	}

	public double getMarkPerQuestion() {
		return markPerQuestion;
	}

	public void setMarkPerQuestion(double markPerQuestion) {
		this.markPerQuestion = markPerQuestion;
	}

	public double getNegativeMarkPerQuestion() {
		return negativeMarkPerQuestion;
	}

	public void setNegativeMarkPerQuestion(double negativeMarkPerQuestion) {
		this.negativeMarkPerQuestion = negativeMarkPerQuestion;
	}


}
