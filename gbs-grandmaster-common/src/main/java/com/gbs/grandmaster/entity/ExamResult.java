package com.gbs.grandmaster.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GM_EXAM_RESULT")
public class ExamResult {

	@EmbeddedId
	private ExamResultID id;
	
	private Long sectonId;
	
	private boolean correct;
	
	private double mark;
	
	private double negativeMark;
	
	private long timeTaken;
	
	private String selectedChoices;

	public ExamResultID getId() {
		return id;
	}

	public void setId(ExamResultID id) {
		this.id = id;
	}

	public Long getSectonId() {
		return sectonId;
	}

	public void setSectonId(Long sectonId) {
		this.sectonId = sectonId;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}

	public double getNegativeMark() {
		return negativeMark;
	}

	public void setNegativeMark(double negativeMark) {
		this.negativeMark = negativeMark;
	}

	public long getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(long timeTaken) {
		this.timeTaken = timeTaken;
	}

	public String getSelectedChoices() {
		return selectedChoices;
	}

	public void setSelectedChoices(String selectedChoices) {
		this.selectedChoices = selectedChoices;
	}

	
	
	

}
