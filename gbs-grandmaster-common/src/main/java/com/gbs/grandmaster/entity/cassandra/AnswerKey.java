package com.gbs.grandmaster.entity.cassandra;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;

@PrimaryKeyClass
public class AnswerKey {
	
	  @PrimaryKeyColumn(name = "student_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
	  private long studentId;
	 
	  @PrimaryKeyColumn(name = "exam_schedule_id", ordinal = 1, type = PrimaryKeyType.PARTITIONED)
	  private long examScheduleId;
	  
	  @PrimaryKeyColumn(name = "attempt", ordinal = 2, type = PrimaryKeyType.PARTITIONED)
	  private int attempt;
	  
	  @PrimaryKeyColumn(name = "question_paper_id", ordinal = 3, type = PrimaryKeyType.PARTITIONED)
	  private long questionPaperId;
	  
	  @PrimaryKeyColumn(name = "question_id", ordinal = 4, type = PrimaryKeyType.PARTITIONED)
	  private long questionId;
	  public AnswerKey() {
		  
	  }
	public AnswerKey(long studentId,long examScheduleId,int attempt,long questionPaperId, long questionId) {
		this.studentId = studentId;
		this.examScheduleId = examScheduleId;
		this.attempt = attempt;
		this.questionPaperId = questionPaperId;
		this.questionId = questionId;
	}

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}

	public long getExamScheduleId() {
		return examScheduleId;
	}

	public void setExamScheduleId(long examScheduleId) {
		this.examScheduleId = examScheduleId;
	}

	public int getAttempt() {
		return attempt;
	}

	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}

	public long getQuestionPaperId() {
		return questionPaperId;
	}

	public void setQuestionPaperId(long questionPaperId) {
		this.questionPaperId = questionPaperId;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}
	  
	  

}
