package com.gbs.grandmaster.entity.cassandra;

import java.util.Date;

import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gbs.grandmaster.common.entity.ATTENDANCE_STATUS;



@Table("EXAM_ATTENDANCE")
public class ExamAttendance {

	@PrimaryKey
	private AttendanceKey key;

	@Column("status")
	private ATTENDANCE_STATUS status;
	
	@Column("start_time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date startedDateTime;

	@Column("end_time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date endDateTime;
	
	@Column("submit_time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date submittedTime;
	
	public ExamAttendance() {
		
	}
	
	public ExamAttendance(AttendanceKey key) {
		this.key = key;
	}

	public AttendanceKey getKey() {
		return key;
	}

	public void setKey(AttendanceKey key) {
		this.key = key;
	}

	

	public ATTENDANCE_STATUS getStatus() {
		return status;
	}

	public void setStatus(ATTENDANCE_STATUS status) {
		this.status = status;
	}

	public Date getStartedDateTime() {
		return startedDateTime;
	}

	public void setStartedDateTime(Date startedDateTime) {
		this.startedDateTime = startedDateTime;
	}

	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}
	
	 @Override
	  public String toString() {
	    return "ExamAttendance{" + "key=" + key + ", status='" + status + '\'' + ", start_time=" + startedDateTime + '}';
	  }

	public Date getSubmittedTime() {
		return submittedTime;
	}

	public void setSubmittedTime(Date submittedTime) {
		this.submittedTime = submittedTime;
	}

}
