package com.gbs.grandmaster.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gbs.grandmaster.common.entity.ATTENDANCE_STATUS;
import com.gbs.grandmaster.common.entity.EXAM_STATUS;

@Entity
@Table(name = "GM_EXAM_RESULT_SUMMARY")
public class ExamResultSummary {
	
	@EmbeddedId
	private ExamResultSummaryID id;
	
	private int correctAnswers;
	
	private int wrongAnswers;
	
	private double markObtained;
	
	private double negativeMark;
	
	private double totalMark;
	
	private ATTENDANCE_STATUS attendanceStatus;

	

	public int getCorrectAnswers() {
		return correctAnswers;
	}

	public void setCorrectAnswers(int correctAnswers) {
		this.correctAnswers = correctAnswers;
	}

	public int getWrongAnswers() {
		return wrongAnswers;
	}

	public void setWrongAnswers(int wrongAnswers) {
		this.wrongAnswers = wrongAnswers;
	}

	public double getMarkObtained() {
		return markObtained;
	}

	public void setMarkObtained(double markObtained) {
		this.markObtained = markObtained;
	}

	public double getNegativeMark() {
		return negativeMark;
	}

	public void setNegativeMark(double negativeMark) {
		this.negativeMark = negativeMark;
	}

	public double getTotalMark() {
		return totalMark;
	}

	public void setTotalMark(double totalMark) {
		this.totalMark = totalMark;
	}

	public ATTENDANCE_STATUS getAttendanceStatus() {
		return attendanceStatus;
	}

	public void setAttendanceStatus(ATTENDANCE_STATUS attendanceStatus) {
		this.attendanceStatus = attendanceStatus;
	}

	public ExamResultSummaryID getId() {
		return id;
	}

	public void setId(ExamResultSummaryID id) {
		this.id = id;
	}
	
	

}
