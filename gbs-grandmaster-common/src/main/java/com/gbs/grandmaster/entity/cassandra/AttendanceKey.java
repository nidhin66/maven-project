package com.gbs.grandmaster.entity.cassandra;

import org.springframework.cassandra.core.Ordering;
import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;

@PrimaryKeyClass
public class AttendanceKey {	 
	 
	  @PrimaryKeyColumn(name = "student_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
	  private long studentId;
	 
	  @PrimaryKeyColumn(name = "exam_schedule_id", ordinal = 1, type = PrimaryKeyType.PARTITIONED)
	  private long examScheduleId;
	  
	  @PrimaryKeyColumn(name = "attempt", ordinal = 2, type = PrimaryKeyType.CLUSTERED, ordering = Ordering.DESCENDING)
	  private int attempt;
	  
	  
	  public AttendanceKey(){
		  
	  }

	public AttendanceKey(long studentId,long examScheduleId,int attempt) {
		this.studentId = studentId;
		this.examScheduleId = examScheduleId;
		this.attempt = attempt;
	}

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}

	public long getExamScheduleId() {
		return examScheduleId;
	}

	public void setExamScheduleId(long examScheduleId) {
		this.examScheduleId = examScheduleId;
	}

	public int getAttempt() {
		return attempt;
	}

	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}
	
	 @Override
	  public String toString() {
	    return "AttendanceKey{"
	        + "studentId='"
	        + studentId
	        + '\''
	        + ", examScheduleId="
	        + examScheduleId
	        + ", attempt="
	        + attempt
	        + '}';
	  }
	

}
