package com.gbs.grandmaster.entity.cassandra;

import java.util.Date;

import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

@Table("QUESTION_ATTEMPT_TIME")
public class QuestionAttemptTime {
	
	 @PrimaryKey private QuestionAttemptKey key;
	 
	 @Column("time_taken")
	 private long timeTaken;
	 
	 @Column("update_time")
	 private Date updateTime;
	 
	 public QuestionAttemptTime() {
		 
	 }
	 
	 public QuestionAttemptTime(QuestionAttemptKey key) {
		 this.key = key;
	 }

	public QuestionAttemptKey getKey() {
		return key;
	}

	public void setKey(QuestionAttemptKey key) {
		this.key = key;
	}

	public long getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(long timeTaken) {
		this.timeTaken = timeTaken;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	 
	 
	 

}
