package com.gbs.grandmaster.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;
import com.gbs.grandmaster.common.entity.QUESTION_PAPER_STATUS;

@Entity
@Table(name = "GM_STUDENT_QUESTION_PAPER")
public class StudentQuestionPaper implements Identifiable<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	@Version
	private int version;
	
	private long studentId;
	
	private long examScheduleId;
	
	private long questionPaperId;
	
	private QUESTION_PAPER_STATUS status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}

	public long getExamScheduleId() {
		return examScheduleId;
	}

	public void setExamScheduleId(long examScheduleId) {
		this.examScheduleId = examScheduleId;
	}

	public long getQuestionPaperId() {
		return questionPaperId;
	}

	public void setQuestionPaperId(long questionPaperId) {
		this.questionPaperId = questionPaperId;
	}

	public QUESTION_PAPER_STATUS getStatus() {
		return status;
	}

	public void setStatus(QUESTION_PAPER_STATUS status) {
		this.status = status;
	}

	
	
}
