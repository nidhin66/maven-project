package com.gbs.grandmaster.entity.cassandra;

import java.util.Date;

import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import com.gbs.grandmaster.common.entity.ATTENDANCE_STATUS;

@Table("EXAM_LOG")
public class ExamLog {

	@PrimaryKey
	private AttendanceKey key;
	
	@Column("section_status_str")
	private String sectionStatusStr;
	
	@Column("elapsed_time_sec")
	private long elapsedTimeInSec;
	
	@Column("status")
	private ATTENDANCE_STATUS status;
	
	@Column("update_time")
	private Date updateTime;

	public ExamLog() {

	}
	
	public ExamLog(AttendanceKey key) {
		this.key = key;
	}

	public AttendanceKey getKey() {
		return key;
	}

	public void setKey(AttendanceKey key) {
		this.key = key;
	}

	public String getSectionStatusStr() {
		return sectionStatusStr;
	}

	public void setSectionStatusStr(String sectionStatusStr) {
		this.sectionStatusStr = sectionStatusStr;
	}

	public long getElapsedTimeInSec() {
		return elapsedTimeInSec;
	}

	public void setElapsedTimeInSec(long elapsedTimeInSec) {
		this.elapsedTimeInSec = elapsedTimeInSec;
	}

	public ATTENDANCE_STATUS getStatus() {
		return status;
	}

	public void setStatus(ATTENDANCE_STATUS status) {
		this.status = status;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	

}
