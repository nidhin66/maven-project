package com.gbs.grandmaster.entity;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;

//@Entity
//@Table(name = "GM_QUESTION")
public class Question implements Identifiable<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	@Version
	private int version;
	
	@OneToMany(mappedBy="id", fetch=FetchType.EAGER)
	private Set<QuestionText> questionTexts;
	
	private String diagramPath;
	
	@OneToMany(mappedBy="id", fetch=FetchType.EAGER)
	private Set<Choice> choices;
	
	private double mark;
	
	private double negativeMark;
	
	private long paragraphId;
	
	private long questionSeq;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Set<QuestionText> getQuestionTexts() {
		return questionTexts;
	}

	public void setQuestionTexts(Set<QuestionText> questionTexts) {
		this.questionTexts = questionTexts;
	}

	public String getDiagramPath() {
		return diagramPath;
	}

	public void setDiagramPath(String diagramPath) {
		this.diagramPath = diagramPath;
	}

	public Set<Choice> getChoices() {
		return choices;
	}

	public void setChoices(Set<Choice> choices) {
		this.choices = choices;
	}

	public void addChoice(Choice choice) {
		if(null == choices) {
			choices = new HashSet<>();
		}
		choices.add(choice);
	}
	
	public void addQuestionText(QuestionText questionText) {
		if(null == questionTexts) {
			questionTexts = new HashSet<>();
		}
		questionTexts.add(questionText);
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}

	public double getNegativeMark() {
		return negativeMark;
	}

	public void setNegativeMark(double negativeMark) {
		this.negativeMark = negativeMark;
	}

	public long getParagraphId() {
		return paragraphId;
	}

	public void setParagraphId(long paragraphId) {
		this.paragraphId = paragraphId;
	}

	public long getQuestionSeq() {
		return questionSeq;
	}

	public void setQuestionSeq(long questionSeq) {
		this.questionSeq = questionSeq;
	}


}
