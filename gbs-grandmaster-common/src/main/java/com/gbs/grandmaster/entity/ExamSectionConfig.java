package com.gbs.grandmaster.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;

@Entity
@Table(name = "GM_EXAM_SECTION_CONFIG")
public class ExamSectionConfig implements Identifiable<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	@Version
	private int version;
		
	private String sectionName;
	
	private int durationInMin;
	
	private int sequnce;
	
	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	private Set<ExamSectionDetails> details ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public int getDurationInMin() {
		return durationInMin;
	}

	public void setDurationInMin(int durationInMin) {
		this.durationInMin = durationInMin;
	}

	public int getSequnce() {
		return sequnce;
	}

	public void setSequnce(int sequnce) {
		this.sequnce = sequnce;
	}

	public Set<ExamSectionDetails> getDetails() {
		return details;
	}

	public void setDetails(Set<ExamSectionDetails> details) {
		this.details = details;
	}

	public ExamSectionDetails getSectionDetailsById(long sectionId) {
		if(null != details) {
			return details.stream().filter(detail -> detail.getId() == sectionId).findAny()
			  .orElse(null);
		}else {
			return null;
		}
	}

}
