package com.gbs.grandmaster.entity.cassandra;

import java.util.Date;

import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;



@Table("FINAL_ANSWER")
public class FinalAnswer {
	
	@PrimaryKey
	private AttendanceKey key;
	
	@Column("content")
	private String content;
	
	@Column("update_time")
	private Date updateTime;

	public AttendanceKey getKey() {
		return key;
	}

	public void setKey(AttendanceKey key) {
		this.key = key;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	

}
