package com.gbs.grandmaster.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;

@Entity
@Table(name = "GM_EXAM_QUESTION_PAPER")
public class ExamQuestionPaper  implements Identifiable<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	@Version
	private int version;
	
	 private long exam_schedule_id;
	 
	 private long question_paper_id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public long getExam_schedule_id() {
		return exam_schedule_id;
	}

	public void setExam_schedule_id(long exam_schedule_id) {
		this.exam_schedule_id = exam_schedule_id;
	}

	public long getQuestion_paper_id() {
		return question_paper_id;
	}

	public void setQuestion_paper_id(long question_paper_id) {
		this.question_paper_id = question_paper_id;
	}
	 
	 

}
