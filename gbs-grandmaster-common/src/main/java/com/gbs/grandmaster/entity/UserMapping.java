package com.gbs.grandmaster.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;


@Entity
@Table(name = "GM_USER_MAPPING")
public class UserMapping implements Identifiable<Long> {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Version
	private int version;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}
	
	private String description;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "officeId")
	private Office office;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "roleId")
	private Role role;
	
	private boolean defaultRole;

	public boolean isDefaultRole() {
		return defaultRole;
	}

	public void setDefaultRole(boolean defaultRole) {
		this.defaultRole = defaultRole;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}	
	
	public UserMapping getDefaultRole(List<UserMapping> mappings) {		
		if (mappings!=null) {
			for (UserMapping userMapping : mappings) {
				if (userMapping.isDefaultRole()) {
					return userMapping;
				}
			}
		}
		return null;
	}
}
