package com.gbs.grandmaster.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;

//@Entity
//@Table(name = "GM_QUESTION_PAPER")
public class QuestionPaper implements Identifiable<Long> {
	
	private static final long serialVersionUID = 1L;
//	@Id
//	@Column(nullable = false, length = 25)
//	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	
	@Version
	private int version;
	
	private long examScheduleId;
	
	private String name;
	
//	@OneToMany(mappedBy="id", fetch=FetchType.EAGER)
	private List<ExamSection> sections;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public long getExamScheduleId() {
		return examScheduleId;
	}

	public void setExamScheduleId(long examScheduleId) {
		this.examScheduleId = examScheduleId;
	}

	public List<ExamSection> getSections() {
		return sections;
	}

	public void setSections(List<ExamSection> sections) {
		this.sections = sections;
	}
	
	public void addSection(ExamSection section) {
		if(null == sections) {
			sections = new ArrayList<>();
		}
		sections.add(section);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	
}
