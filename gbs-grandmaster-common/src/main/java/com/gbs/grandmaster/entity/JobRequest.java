package com.gbs.grandmaster.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gbs.grandmaster.common.entity.JOB_STATUS;
import com.gbs.grandmaster.common.entity.JOB_TYPE;

@Entity
@Table(name = "GM_JOB_REQUEST")
public class JobRequest {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	private Long examId;
	
	private JOB_TYPE jobType;
	
	private JOB_STATUS jobStatus;
	
	private Date startedTime;
	
	private Date finishedTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}

	public JOB_TYPE getJobType() {
		return jobType;
	}

	public void setJobType(JOB_TYPE jobType) {
		this.jobType = jobType;
	}

	public JOB_STATUS getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(JOB_STATUS jobStatus) {
		this.jobStatus = jobStatus;
	}

	public Date getStartedTime() {
		return startedTime;
	}

	public void setStartedTime(Date startedTime) {
		this.startedTime = startedTime;
	}

	public Date getFinishedTime() {
		return finishedTime;
	}

	public void setFinishedTime(Date finishedTime) {
		this.finishedTime = finishedTime;
	}
	
	

}
