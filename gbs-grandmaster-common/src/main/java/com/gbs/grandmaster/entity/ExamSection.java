package com.gbs.grandmaster.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;

//@Entity
//@Table(name = "GM_EXAM_SECTION")
public class ExamSection implements Identifiable<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	@Version
	private int version;
	
	private String sectionName;
	
	private int durationInMin;
	
	private int sequnce;
	
	private Map<Long, byte[]> pragraphContent;
	
//	@OneToMany(mappedBy="id", fetch=FetchType.EAGER)
	private List<Question> questions;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public int getDurationInMin() {
		return durationInMin;
	}

	public void setDurationInMin(int durationInMin) {
		this.durationInMin = durationInMin;
	}

	public int getSequnce() {
		return sequnce;
	}

	public void setSequnce(int sequnce) {
		this.sequnce = sequnce;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
	public void addQuestion(Question question) {
		if(null == questions) {
			questions = new ArrayList<>();
		}
		questions.add(question);
	}

	public Map<Long, byte[]> getPragraphContent() {
		return pragraphContent;
	}

	public void setPragraphContent(Map<Long, byte[]> pragraphContent) {
		this.pragraphContent = pragraphContent;
	}
	
	public void putParagraphContent(long id,byte[] content) {
		if(null == pragraphContent) {
			pragraphContent = new HashMap<>();
		}
		pragraphContent.put(id, content);
	}
	
	public byte[] getParagraphContentById(long id) {
		if(null != pragraphContent) {
			return pragraphContent.get(id);
		}else {
			return null;
		}
	}

}
