package com.gbs.grandmaster.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.gbs.grandmaster.common.entity.Identifiable;



@Entity
@Table(name = "GM_USER")
@Inheritance(
	    strategy = InheritanceType.JOINED
	)
public class User implements Identifiable<Long> {
	
	public Date getLastPasswordUpdated() {
		return lastPasswordUpdated;
	}

	public void setLastPasswordUpdated(Date lastPasswordUpdated) {
		this.lastPasswordUpdated = lastPasswordUpdated;
	}

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(nullable = false, length = 25)
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Version
	private int version;
	
	private String userName;
	private String password;
	private String firstName;
	private String middleName;
	private String lastName;
	private String email;
	private String mobileNumber;
	private String alterNativeMobNumber;
	@Column(columnDefinition = "boolean default true")
	private boolean activated ;
	@Column(columnDefinition = "boolean default false")
	private boolean banned ;
	private Date DOB;
	private Date lastLoggedInDate;
	private Date lastPasswordUpdated;
	
	@ManyToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<UserMapping> userMappings;	
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	
	
	@Column(length=50)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(length=50)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	public boolean isActivated() {
		return activated;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAlterNativeMobNumber() {
		return alterNativeMobNumber;
	}

	public void setAlterNativeMobNumber(String alterNativeMobNumber) {
		this.alterNativeMobNumber = alterNativeMobNumber;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public boolean isBanned() {
		return banned;
	}

	public void setBanned(boolean banned) {
		this.banned = banned;
	}
	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getLastLoggedInDate() {
		return lastLoggedInDate;
	}

	public void setLastLoggedInDate(Date lastLoggedInDate) {
		this.lastLoggedInDate = lastLoggedInDate;
	}

	public List<UserMapping> getUserMappings() {
		return userMappings;
	}

	public void setUserMappings(List<UserMapping> userMappings) {
		this.userMappings = userMappings;
	}

	public Date getDOB() {
		return DOB;
	}

	public void setDOB(Date dOB) {
		DOB = dOB;
	}

}
