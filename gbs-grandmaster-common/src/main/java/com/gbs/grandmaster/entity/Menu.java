package com.gbs.grandmaster.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;



@Entity
@Table(name="GM_MENU")
public class Menu implements Identifiable<Long> {

	private static final long serialVersionUID = 2666620683473897060L;
	
	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Version
	private int version;
	
	private String name;
	private String labelId;
	private String labelText;
	private int ordinal;
	private String actionName;
	private String displayName;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabelId() {
		return labelId;
	}

	public void setLabelId(String labelId) {
		this.labelId = labelId;
	}

	public String getLabelText() {
		return labelText;
	}

	public void setLabelText(String labelText) {
		this.labelText = labelText;
	}

	public int getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(int ordinal) {
		this.ordinal = ordinal;
	}
	
	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	
//	@ManyToOne
//	@LazyCollection(LazyCollectionOption.FALSE)
//	@JoinColumn(name="menu_group_id",referencedColumnName="ID")
//    @OrderBy("ordinal asc")
//	public MenuGroup getMenuGroup() {
//		return menuGroup;
//	}
//
//	public void setMenuGroup(MenuGroup menuGroup) {
//		this.menuGroup = menuGroup;
//	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Transient
	public String getDisplayName() {
		return displayName;
	}

	
}
