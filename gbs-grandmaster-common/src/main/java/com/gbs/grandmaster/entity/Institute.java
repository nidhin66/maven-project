package com.gbs.grandmaster.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;

//@Entity
//@Table(name = "GM_INSTITUTE")
public class Institute implements Identifiable<Long> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	@Id
//	@Column(nullable = false, length = 25)
//	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	@Version
	private int version;
	
	private String name;	

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
