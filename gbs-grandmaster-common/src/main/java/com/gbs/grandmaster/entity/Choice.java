package com.gbs.grandmaster.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.grandmaster.common.entity.Identifiable;

@Entity
@Table(name = "GM_CHOISE")
public class Choice implements Identifiable<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	@Version
	private int version;
	
	private int sequence;
	private boolean correct;
	
	@OneToMany(mappedBy="id", fetch=FetchType.EAGER)
	private Set<ChoiceText> choiceTexts;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public Set<ChoiceText> getChoiceTexts() {
		return choiceTexts;
	}

	public void setChoiceTexts(Set<ChoiceText> choiceTexts) {
		this.choiceTexts = choiceTexts;
	}
	public void addChoiceText(ChoiceText choiseText) {
		if(null == choiceTexts) {
			choiceTexts = new HashSet<>();
		}
		choiceTexts.add(choiseText);
	}


}
