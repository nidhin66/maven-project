package com.gbs.grandmaster.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gbs.grandmaster.common.entity.EXAM_STATUS;
import com.gbs.grandmaster.common.entity.Identifiable;

@Entity
@Table(name = "GM_EXAM_SCHEDULE")
public class ExamSchedule implements Identifiable<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	@Version
	private int version;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "exam_definition_id", referencedColumnName = "id", insertable = false, updatable = false)
	private ExamDefinition examDefinition;
	
    private long batch_id;
	
	private String name;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")	
	private Date startDateTime;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")	
	private Date endDateTime;
		
	private int noOfAttempts;
	
	private boolean resultImmeadiate;
	
	
	private int noOfQuestionPapers;

	private EXAM_STATUS status;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public ExamDefinition getExamDefinition() {
		return examDefinition;
	}

	public void setExamDefinition(ExamDefinition examDefinition) {
		this.examDefinition = examDefinition;
	}



	public long getBatch_id() {
		return batch_id;
	}

	public void setBatch_id(long batch_id) {
		this.batch_id = batch_id;
	}

	public Date getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}

	public int getNoOfAttempts() {
		return noOfAttempts;
	}

	public void setNoOfAttempts(int noOfAttempts) {
		this.noOfAttempts = noOfAttempts;
	}

	public boolean isResultImmeadiate() {
		return resultImmeadiate;
	}

	public void setResultImmeadiate(boolean resultImmeadiate) {
		this.resultImmeadiate = resultImmeadiate;
	}

//	public QuestionBank getQuestionBank() {
//		return questionBank;
//	}
//
//	public void setQuestionBank(QuestionBank questionBank) {
//		this.questionBank = questionBank;
//	}

	public int getNoOfQuestionPapers() {
		return noOfQuestionPapers;
	}

	public void setNoOfQuestionPapers(int noOfQuestionPapers) {
		this.noOfQuestionPapers = noOfQuestionPapers;
	}

	public EXAM_STATUS getStatus() {
		return status;
	}

	public void setStatus(EXAM_STATUS status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
