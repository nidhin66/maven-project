package com.gbs.grandmaster.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.gbs.grandmaster.common.entity.Identifiable;
import com.gbs.grandmaster.common.entity.OfficeLevel;


@Entity
@Table(name = "GM_OFFICE")
public class Office implements Identifiable<Long> {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Version
	private int version;
	
	private String name;
	
	private Office parentOffice;
	
	private OfficeLevel officeLevel;
	
	@OneToOne
	@JoinColumn(name="language_id",referencedColumnName="ID")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Language language;
	
	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}
	
	@Column(length=25)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Office getParentOffice() {
		return parentOffice;
	}

	public void setParentOffice(Office parentOffice) {
		this.parentOffice = parentOffice;
	}

	public OfficeLevel getOfficeLevel() {
		return officeLevel;
	}

	public void setOfficeLevel(OfficeLevel officeLevel) {
		this.officeLevel = officeLevel;
	}
}
