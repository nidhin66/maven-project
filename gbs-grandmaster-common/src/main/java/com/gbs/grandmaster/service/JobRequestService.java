package com.gbs.grandmaster.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.common.entity.JOB_STATUS;
import com.gbs.grandmaster.entity.JobRequest;
import com.gbs.grandmaster.repository.JobRequestRepository;

import java.util.List;


@Service
public class JobRequestService {
	
	@Autowired
	private JobRequestRepository jobRequestPropository;
	
	
	public List<JobRequest> findJobByStatus(JOB_STATUS jobStatus){
		return jobRequestPropository.findByJobStatus(jobStatus);
	}
	
	public List<JobRequest> findAllRequests(){
		return jobRequestPropository.findAll();
	}
	
	public JobRequest save(JobRequest jobRequest) {
		return jobRequestPropository.save(jobRequest);
	}

}
