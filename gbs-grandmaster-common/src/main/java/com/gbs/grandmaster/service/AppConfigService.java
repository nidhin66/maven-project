package com.gbs.grandmaster.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.common.Constants;
import com.gbs.grandmaster.common.entity.ApplicationError;
import com.gbs.grandmaster.common.entity.ERROR_LEVEL;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.repository.AppConfigRepository;

@Service
public class AppConfigService {
	
	@Autowired
	AppConfigRepository appConfigRepository;
	
	public Response getApplicationConfigs(){
		Response response=new Response();
		try {			
			response.setData(appConfigRepository.findAll());
			response.setSuccess(true);
		}catch(Exception e) {
			e.printStackTrace();
			List<ApplicationError> exceptionlist=new ArrayList<>();
			ApplicationError applicationError=new ApplicationError();			
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
			exceptionlist.add(applicationError);
			response.setErrors(exceptionlist);
		}
		return response;
	}

}
