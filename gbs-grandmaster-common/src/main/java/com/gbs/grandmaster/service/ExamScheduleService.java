package com.gbs.grandmaster.service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.common.Constants;
import com.gbs.grandmaster.common.entity.ApplicationError;
import com.gbs.grandmaster.common.entity.ERROR_LEVEL;
import com.gbs.grandmaster.common.entity.EXAM_STATUS;
import com.gbs.grandmaster.common.entity.ExamInfoWrapper;
import com.gbs.grandmaster.common.entity.QUESTION_PAPER_STATUS;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.Choice;
import com.gbs.grandmaster.entity.ChoiceText;
import com.gbs.grandmaster.entity.ExamDefinition;
import com.gbs.grandmaster.entity.ExamQuestionPaper;
import com.gbs.grandmaster.entity.ExamSchedule;
import com.gbs.grandmaster.entity.ExamSection;
import com.gbs.grandmaster.entity.ExamSectionConfig;
import com.gbs.grandmaster.entity.ExamSectionDetails;
import com.gbs.grandmaster.entity.Language;
import com.gbs.grandmaster.entity.Question;
import com.gbs.grandmaster.entity.QuestionPaper;
import com.gbs.grandmaster.entity.QuestionText;
import com.gbs.grandmaster.entity.StudentQuestionPaper;
import com.gbs.grandmaster.entity.cassandra.ExamAttendance;
import com.gbs.grandmaster.entity.cassandra.ExamLog;
import com.gbs.grandmaster.repository.ExamQuestionPaperRepository;
import com.gbs.grandmaster.repository.ExamScheduleRepository;
import com.gbs.grandmaster.repository.StudentQuestionPaperRepository;
import com.gbs.grandmaster.repository.cassandra.ExamAttendanceRespository;
import com.gbs.grandmaster.repository.cassandra.ExamLogRepository;

@Service
public class ExamScheduleService {

	@Autowired
	private ExamScheduleRepository examScheduleRepository;
	
	@Autowired
	private ExamQuestionPaperRepository examQuestionPaperRepository;
	
	@Autowired
	private StudentQuestionPaperRepository studentQuestionPaperRepository;
	
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private ExamAttendanceRespository examAttendanceRepository;
	
	@Autowired
	private ExamLogRepository examLogRepository;
	
	@Transactional
	public Response Save(ExamSchedule examSchedule) {
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
	
		response.setData(examScheduleRepository.save(examSchedule));
		if(response.getData()!=null)
			response.setSuccess(true);
			else
				response.setSuccess(false);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
				
	}
	
	public ExamSchedule findById(long examScheduleId) {
		return examScheduleRepository.findOne(examScheduleId);
	}

	@Transactional
	public Response Update(ExamSchedule examSchedule) {
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
	
		response.setData(examScheduleRepository.save(examSchedule));
		if(response.getData()!=null)
			response.setSuccess(true);
			else
				response.setSuccess(false);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
				
	}
	
	@Transactional
	public Response deleteAllSchedules() {

		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
			examScheduleRepository.deleteAll();
		response.setSuccess(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
	
	}

	@Transactional
	public Response Delete(Long id) {

		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
			examScheduleRepository.delete(id);
		response.setSuccess(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
	
	}

	@Transactional
	public Response FindByID(Long id) {

		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
	
		response.setData(examScheduleRepository.findOne(id));
		if(response.getData()!=null)
		response.setSuccess(true);
		else
			response.setSuccess(false);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
	
	}

	@Transactional
	public Page<ExamSchedule> getAll(Pageable pageable) {
		return examScheduleRepository.findAll(pageable);
	}
	
	@Transactional
	public Response listStudentExams(long student_id,long batch_id) {	
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
			List<ExamSchedule> examSchedules = examScheduleRepository.findExamByBatch(batch_id);
			List<ExamInfoWrapper> examDetails = new ArrayList<>();
			for(Iterator<ExamSchedule> it = examSchedules.iterator(); it.hasNext();) {
				ExamSchedule examSchedule = it.next();
				if(null != examSchedule.getStatus() && (examSchedule.getStatus() == EXAM_STATUS.SCHEDULED ||examSchedule.getStatus() == EXAM_STATUS.ENABLED || examSchedule.getStatus() == EXAM_STATUS.FINISHED)) {
					ExamInfoWrapper examDetail = new ExamInfoWrapper();
					Date currentDateTime = new Date();
					if(currentDateTime.after(examSchedule.getStartDateTime()) && currentDateTime.before(examSchedule.getEndDateTime())) {
						examSchedule.setStatus(EXAM_STATUS.ENABLED);
					}else if(currentDateTime.after(examSchedule.getEndDateTime())) {
						examSchedule.setStatus(EXAM_STATUS.FINISHED);
					}else if(examSchedule.getStartDateTime().after(currentDateTime)) {
						examSchedule.setStatus(EXAM_STATUS.SCHEDULED);
					}					
					examDetail.setExamSchedule(examSchedule);
					ExamAttendance examAttendance = examAttendanceRepository.findLastAttemptAttendance(student_id, examSchedule.getId());
					if(null != examAttendance) {
						examDetail.setExamAttendance(examAttendance);
						ExamLog examLog = examLogRepository.findByKeyStudentIdAndKeyExamScheduleIdAndKeyAttempt(student_id, examSchedule.getId(), examAttendance.getKey().getAttempt());
						examDetail.setExamLog(examLog);
					}	
					examDetails.add(examDetail);
				}
			}
			response.setData(examDetails);
			response.setSuccess(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
	}
	
	@Transactional
	public Response listStudentMockExams(long student_id,long batch_id) {	
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
			List<ExamSchedule> examSchedules = examScheduleRepository.findMockExamByBatch(batch_id);
			List<ExamInfoWrapper> examDetails = new ArrayList<>();
			for(Iterator<ExamSchedule> it = examSchedules.iterator(); it.hasNext();) {
				ExamSchedule examSchedule = it.next();
				if(null != examSchedule.getStatus() && (examSchedule.getStatus() == EXAM_STATUS.SCHEDULED || examSchedule.getStatus() == EXAM_STATUS.ENABLED || examSchedule.getStatus() == EXAM_STATUS.FINISHED)) {
					ExamInfoWrapper examDetail = new ExamInfoWrapper();
					Date currentDateTime = new Date();
					if(currentDateTime.after(examSchedule.getStartDateTime()) && currentDateTime.before(examSchedule.getEndDateTime())) {
						examSchedule.setStatus(EXAM_STATUS.ENABLED);
					}else if(currentDateTime.after(examSchedule.getEndDateTime())) {
						examSchedule.setStatus(EXAM_STATUS.FINISHED);
					}else if(examSchedule.getStartDateTime().after(currentDateTime)) {
						examSchedule.setStatus(EXAM_STATUS.SCHEDULED);
					}
					examDetail.setExamSchedule(examSchedule);
					ExamAttendance examAttendance = examAttendanceRepository.findLastAttemptAttendance(student_id, examSchedule.getId());
					if(null != examAttendance) {
						examDetail.setExamAttendance(examAttendance);
						ExamLog examLog = examLogRepository.findByKeyStudentIdAndKeyExamScheduleIdAndKeyAttempt(student_id, examSchedule.getId(), examAttendance.getKey().getAttempt());
						examDetail.setExamLog(examLog);
					}	
					examDetails.add(examDetail);
				}
			}
			response.setData(examDetails);
			response.setSuccess(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
	}
	
	@Transactional
	public Response getQuestionPaper(long exam_schedule_id,long student_id) {
		Response response = new Response();	
		try {
		ExamSchedule examSchedule = examScheduleRepository.findOne(exam_schedule_id);
		if(null != examSchedule) {
			ExamQuestionPaper examQuestionPaper = null;
			StudentQuestionPaper studentQuestionPaper = studentQuestionPaperRepository.findByStudentIdAndExamScheduleId(student_id, exam_schedule_id);
			if(null == studentQuestionPaper) {
				List<ExamQuestionPaper> examQuestionPapers = examQuestionPaperRepository.findByExam_schedule_id(exam_schedule_id);
				if(null != examQuestionPapers && examQuestionPapers.size()>0) {
					Random random =   new Random();
					int index = random.nextInt(examQuestionPapers.size());
					examQuestionPaper = examQuestionPapers.get(index);
					if(null != examQuestionPaper ) {
						studentQuestionPaper = new StudentQuestionPaper();
						studentQuestionPaper.setExamScheduleId(exam_schedule_id);
						studentQuestionPaper.setStudentId(student_id);
						studentQuestionPaper.setQuestionPaperId(examQuestionPaper.getQuestion_paper_id());
						studentQuestionPaper.setStatus(QUESTION_PAPER_STATUS.DOWNLOADED);
						studentQuestionPaperRepository.save(studentQuestionPaper);
					}					
				}				
			}
			if(null != studentQuestionPaper) {
				long questionPaperId = studentQuestionPaper.getQuestionPaperId();
				response.setData(getQuestionPaper(examSchedule, questionPaperId));
				response.setSuccess(true);
			}

		}
		
		
	}catch(Exception e) {
		ApplicationError applicationError = new ApplicationError();
		applicationError.setErrorCode("");
		applicationError.setErrorMessage(Constants.ERROR_MSG);
		applicationError.setLevel(ERROR_LEVEL.ERROR);
		response.setSuccess(false);
		List<ApplicationError> exceptionlist = new ArrayList<>();
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
	}
	return response;
	}
	
	@Transactional
	private QuestionPaper getQuestionPaper(ExamSchedule examSchedule,long questionPaperId) {
		QuestionPaper questionPaper = new QuestionPaper();
		questionPaper.setId(questionPaperId);
		questionPaper.setExamScheduleId(examSchedule.getId());
		ExamDefinition examDefinition = examSchedule.getExamDefinition();
		Query questionWithParaQuery = em.createNativeQuery("select q.question_id,mq.question_content,mp.paragraph_id,mp.paragraph_content,mq.question_solution,q.exam_section_details_id,q.question_number from "
				+ "gm_exam_paper_questions q,mm_question mq,mm_question_paragraph mp where mq.question_id = q.question_id and "
				+ "mp.paragraph_id = mq.paragraph_id and q.exam_paper_id=:exam_paper_id and q.status = 1 and q.exam_section_details_id in (:exam_section_details_id) order by q.question_number");
		

		Query questionWithOutParaQuery = em.createNativeQuery("select q.question_id,mq.question_content,mq.paragraph_id,cast('' as BINARY) as paragraph_content,mq.question_solution,q.exam_section_details_id,q.question_number "
				+ "from gm_exam_paper_questions q,mm_question mq where exam_paper_id = :exam_paper_id and q.status = 1 and q.exam_section_details_id in (:exam_section_details_id)"
				+ " and mq.question_id = q.question_id and mq.paragraph_id = 0 order by q.question_number");
		
		Query optionQuery = em.createNativeQuery("select option_id,option_number,option_content,option_answer from mm_question_option where question_id=? order by option_number ");
		for(ExamSectionConfig examSectionConfig : examDefinition.getExamSections()) {
			ExamSection examSection = new ExamSection();
			examSection.setId(examSectionConfig.getId());
			examSection.setSectionName(examSectionConfig.getSectionName());
			examSection.setDurationInMin(examSectionConfig.getDurationInMin());
			examSection.setSequnce(examSectionConfig.getSequnce());
			questionPaper.addSection(examSection);
			List<Long> examSectionDetailIds = new ArrayList<>();
			for(ExamSectionDetails sectionDetails : examSectionConfig.getDetails()) {
				
			examSectionDetailIds.add(sectionDetails.getId());}

			questionWithParaQuery.setParameter("exam_paper_id", questionPaperId);
			questionWithParaQuery.setParameter("exam_section_details_id", examSectionDetailIds);
			
			questionWithOutParaQuery.setParameter("exam_paper_id", questionPaperId);
			questionWithOutParaQuery.setParameter("exam_section_details_id", examSectionDetailIds);
			
			List<Object[]> questionResult = questionWithParaQuery.getResultList();
			List<Object[]> questionWithOutParaResult = questionWithOutParaQuery.getResultList();			
			
			questionResult.addAll(questionWithOutParaResult);
			for (Object[] record : questionResult) {
				Question question = new Question();
				long question_id = Long.valueOf((Integer)record[0]);
				byte[] questionContent = (byte[]) record[1];
				long paragraphId = 0;
				if(null != record[2]) {
					paragraphId = Long.valueOf((Integer)record[2]);
				}							
				byte[] paragraphContent = null;
				if(null != record[3]) {
					paragraphContent = 
							(byte[]) record[3];
				}
				byte[] solutionContent = null;
				if(null != record[4]) {
					solutionContent = 
							(byte[]) record[4];
				}
				if(null != paragraphContent && paragraphContent.length > 0) {
					byte[] existingParagraph = examSection.getParagraphContentById(paragraphId);
					if(null  == existingParagraph) {
						examSection.putParagraphContent(paragraphId, paragraphContent);
					}
				}
				long sectionDetailId = 0;
				if(null != record[5]) {
					sectionDetailId = Long.valueOf((Integer)record[5]);
				}
				long questionNumber = 0;
				if(null != record[6]) {
					questionNumber = Long.valueOf((Integer)record[6]);
				}
				ExamSectionDetails sectionDetails = examSectionConfig.getSectionDetailsById(sectionDetailId);
				question.setParagraphId(paragraphId);
				question.setId(question_id);
				question.setMark(sectionDetails.getMarkPerQuestion());
				question.setNegativeMark(sectionDetails.getNegativeMarkPerQuestion());
				question.setQuestionSeq(questionNumber);
				QuestionText questionText = new QuestionText();
				questionText.setContent(questionContent);
				questionText.setSolution(solutionContent);
				questionText.setId((long) 1);
				questionText.setLanguage(new Language());
				question.addQuestionText(questionText);					
				optionQuery.setParameter(1, question_id);
				List<Object[]> optionResult = optionQuery.getResultList();
				int i=1;
				for (Object[] optionRecord : optionResult) {
					Choice choice = new Choice();
					long optionId = Long.valueOf((Integer)optionRecord[0]);
					choice.setId(optionId);
					choice.setSequence(i);
					byte[] content = (byte[]) optionRecord[2];
					boolean correct = (boolean) optionRecord[3];
					
					choice.setCorrect(correct);
					
				    ChoiceText choiceText = new ChoiceText();
				    choiceText.setContent(content);
				    choiceText.setId(optionId);
				    choiceText.setLanguage(new Language());
				    choice.addChoiceText(choiceText);
					question.addChoice(choice);
					i++;
				}
				examSection.addQuestion(question);
			}
		
		}
		return questionPaper;
		
	}


}
