package com.gbs.grandmaster.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.entity.ExamResultSummary;
import com.gbs.grandmaster.repository.ExamResultSummaryRepository;

@Service
public class ExamResultSummaryService {
	
	@Autowired
	private ExamResultSummaryRepository summaryRepository;
	
	public ExamResultSummary save(ExamResultSummary resultSummary) {
		return summaryRepository.save(resultSummary);
	}

}
