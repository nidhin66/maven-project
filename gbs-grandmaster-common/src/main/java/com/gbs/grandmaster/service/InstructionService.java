package com.gbs.grandmaster.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.common.Constants;
import com.gbs.grandmaster.common.entity.ApplicationError;
import com.gbs.grandmaster.common.entity.ERROR_LEVEL;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.Instruction;
import com.gbs.grandmaster.repository.InstructionRepository;

@Service
public class InstructionService {

	@Autowired
	private InstructionRepository instructionRepository;

	@Transactional
	public Response Save(Instruction instruction) {
		List<ApplicationError> exceptionList = new ArrayList<>();
		ApplicationError applicationError = new ApplicationError();
		Response response = new Response();
		try {
			response.setData(instructionRepository.save(instruction));
			if (response.getData() != null)
				response.setSuccess(true);
			else
				response.setSuccess(false);
		} catch (Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionList.add(applicationError);
		response.setErrors(exceptionList);
		return response;
	}

	@Transactional
	public Response Update(Instruction instruction) {
		List<ApplicationError> exceptionList = new ArrayList<>();
		ApplicationError applicationError = new ApplicationError();
		Response response = new Response();

		try {
			response.setData(instructionRepository.save(instruction));
			if (response.getData() != null)
				response.setSuccess(true);
			else
				response.setSuccess(false);

		} catch (Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}

		exceptionList.add(applicationError);
		response.setErrors(exceptionList);
		return response;

	}

	@Transactional
	public Response Delete(Long id) {
		List<ApplicationError> exceptionList = new ArrayList<>();
		ApplicationError applicationError = new ApplicationError();
		Response response = new Response();

		try {
			instructionRepository.delete(id);
			response.setSuccess(true);

		} catch (Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionList.add(applicationError);
		response.setErrors(exceptionList);
		return response;

	}

	@Transactional
	public Response FindByID(Long id) {

		List<ApplicationError> exceptionlist = new ArrayList<>();
		ApplicationError applicationError = new ApplicationError();
		Response response = new Response();
		try {

			response.setData(instructionRepository.findOne(id));
			if (response.getData() != null)
				response.setSuccess(true);
			else
				response.setSuccess(false);
		} catch (Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return response;

	}

	@Transactional
	public Instruction getInstruction() {
        List<Instruction> instructions = instructionRepository.findAll();
		return instructions.get(0);
	}

}
