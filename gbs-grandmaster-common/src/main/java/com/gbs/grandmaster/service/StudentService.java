package com.gbs.grandmaster.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.common.Constants;
import com.gbs.grandmaster.common.entity.ApplicationError;
import com.gbs.grandmaster.common.entity.ERROR_LEVEL;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.Student;
import com.gbs.grandmaster.entity.StudentCourse;
import com.gbs.grandmaster.entity.User;
import com.gbs.grandmaster.exception.GrandMasterServiceException;
import com.gbs.grandmaster.repository.UserRepository;

@Service
public class StudentService {

	@Autowired
	UserRepository userRepository;
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	@PersistenceContext
	private EntityManager em;

	public Response findStudentByUserName(String userName) {
		User user = userRepository.findByUserName(userName);
		Response response = new Response();	
		try {
		if (user != null) {
			Query studentQuery = em.createNativeQuery(
					"SELECT s.name,gender,address,street,c.name district,st.name state,contact_number,whatsapp_number,mobile_number,email,date_of_birth,student_image,s.registration_number "
							+ "FROM am_students s,cities c,states st where s.student_id = ? and c.id = s.district and st.id=s.state;");
			studentQuery.setParameter(1, user.getId());
			List<Object[]> studentResult = studentQuery.getResultList();
			Student student = new Student();
			for (Object[] record : studentResult) {
				String name = String.valueOf(record[0]);
				String gender = String.valueOf(record[1]);
				String address = String.valueOf(record[2]);
				String street = String.valueOf(record[3]);
				String district = String.valueOf(record[4]);
				String state = String.valueOf(record[5]);
				String contactNumber = String.valueOf(record[6]);
				String whatsupNumber = String.valueOf(record[7]);
				String mobileNumber = String.valueOf((record)[8]);
				String email = String.valueOf(record[9]);
				String dob = String.valueOf(record[10]);
				String studentImage = String.valueOf(record[11]);
				String regsitrationno = String.valueOf(record[12]);
				
				student.setId(user.getId());
				student.setName(name);
				student.setGender(gender);
				student.setAddress(address);
				student.setStreet(street);
				student.setDistrict(district);
				student.setState(state);
				student.setContactNumber(contactNumber);
				student.setWhatsupNumber(whatsupNumber);
				student.setMobileNumber(mobileNumber);
				student.setEmail(email);
				student.setDob(dob);
				student.setStudentImage(studentImage);
				student.setRegsitrationno(regsitrationno);
				break;
			}
			response.setData(student);
			response.setSuccess(true);
		}
		}catch(Exception e) {
			ApplicationError applicationError = new ApplicationError();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
			List<ApplicationError> exceptionlist = new ArrayList<>();
			exceptionlist.add(applicationError);
			response.setErrors(exceptionlist);
		}
		return response;
	}
	
	public List<Long> getStudentIdsByBatch(long batchId) throws GrandMasterServiceException{
		List<Long> studentIds = new ArrayList<>();
		try {
			Query studentQuery = em.createNativeQuery("select student_id from  am_student_course_mapping where batch_id = ?");
			studentQuery.setParameter(1, batchId);
			List<Integer> studentResult = studentQuery.getResultList();
			for (Integer record : studentResult) {
				long student_id = Long.valueOf(record);
				studentIds.add(student_id);
			}
		}catch(Exception e) {
			logger.error("Failed to fetch student ids for the batch : " + batchId , e);
			throw new GrandMasterServiceException("Failed to fetch student ids for the batch : " + batchId);
		}
		return studentIds;
	}
	
	public Response getStudentBatchDetails(long studentId) {
		Response response = new Response();	
		try {
			Query studentCourseQuery = em.createNativeQuery("select student_id,course_id,c.class_name course,sc.batch_id,ba.batch_name,center_id,i."
					+ "institute_name center,sc.branch_id,br.institute_name branch from am_student_course_mapping sc, "
					+ "am_batch_center_mapping ba,am_classes c,am_institute_master i,am_institute_master br where sc.student_id=? and sc.status=1 and "
					+ "ba.batch_id = sc.batch_id and c.class_id = sc.course_id and i.institute_master_id = sc.center_id and "
					+ "br.institute_master_id = sc.branch_id");
			studentCourseQuery.setParameter(1, studentId);
			List<Object[]> studentCourseRslt = studentCourseQuery.getResultList();
			StudentCourse studentCourse = new StudentCourse();
			for (Object[] record : studentCourseRslt) {
				long student_id = Long.valueOf((Integer) record[0]);
				long course_id = Long.valueOf((Integer)record[1]);
				String course_name = String.valueOf(record[2]);
				long batch_id = Long.valueOf((Integer)record[3]);
				String batch_name = String.valueOf(record[4]);
				long center_id = Long.valueOf((Integer)record[5]);
				String center_name = String.valueOf( record[6]);
				long branch_id = Long.valueOf((Integer)record[7]);
				String branch_name = String.valueOf( record[8]);
				
				studentCourse.setId(student_id);
				studentCourse.setCourse_id(course_id);
				studentCourse.setCourse_name(course_name);
				studentCourse.setBatch_id(batch_id);
				studentCourse.setBatch_name(batch_name);
				studentCourse.setCenter_id(center_id);
				studentCourse.setCenter_name(center_name);
				studentCourse.setBranch_id(branch_id);
				studentCourse.setBranch_name(branch_name);
				break;
			}
			
			response.setData(studentCourse);
			response.setSuccess(true);
			
		}catch(Exception e) {
			ApplicationError applicationError = new ApplicationError();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
			List<ApplicationError> exceptionlist = new ArrayList<>();
			exceptionlist.add(applicationError);
			response.setErrors(exceptionlist);
		}
		return response;
		
	}

}
