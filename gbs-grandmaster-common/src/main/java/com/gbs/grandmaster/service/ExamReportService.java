package com.gbs.grandmaster.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.cassandra.ExamAttendance;
import com.gbs.grandmaster.entity.cassandra.ExamReport;
import com.gbs.grandmaster.entity.cassandra.QuestionAttemptTime;

@Service
public class ExamReportService {
	
	
	@Autowired
	ExamLogService examLogService;
	public Response getExamReport(ExamAttendance attendance)
	{
		Response response = new Response();
		try {
			 Map<Long, List<QuestionAttemptTime>>  questionTimeDetails =  examLogService.getQuestionTimeDetails(attendance);
			 ExamReport examReport = new ExamReport();
			 examReport.setTimeDetails(questionTimeDetails);
			response.setData(examReport);
			response.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
		
	}
	

}
