package com.gbs.grandmaster.service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.common.Constants;
import com.gbs.grandmaster.common.entity.ApplicationError;
import com.gbs.grandmaster.common.entity.ERROR_LEVEL;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.common.entity.UserAbstract;
import com.gbs.grandmaster.entity.User;
import com.gbs.grandmaster.repository.UserRepository;






@Service(value = "userService")
public class UserService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Transactional
	public Response createOrUpdateUser(UserAbstract userAbstract) {
		Response response = new Response();		
		try {
			User user = userRepository.findOne(userAbstract.getId());
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode(userAbstract.getPassword());
			if(null == user) {
				user = new User();	
				user.setId(userAbstract.getId());
			}
			user.setUserName(userAbstract.getUserName());
			user.setPassword(hashedPassword);
			userRepository.save(user);
			response.setSuccess(true);
		}catch(Exception e) {
			e.printStackTrace();
			ApplicationError applicationError = new ApplicationError();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
			List<ApplicationError> exceptionlist = new ArrayList<>();
			exceptionlist.add(applicationError);
			response.setErrors(exceptionlist);
		}
		return response;

	}
	

	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		User user = userRepository.findByUserName(userId);
		if(user == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), getAuthority());
	}

	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	public List<User> findAll() {
		List<User> list = new ArrayList<>();
		userRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	
	public void delete(long id) {

		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
			userRepository.delete(id);
		response.setSuccess(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
	
	}

	
    public Response save(User user) {
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
	
		response.setData(userRepository.save(user));
		if(response.getData()!=null)
			response.setSuccess(true);
			else
				response.setSuccess(false);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
				
	}
}
