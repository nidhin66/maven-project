package com.gbs.grandmaster.service;


import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.common.Constants;
import com.gbs.grandmaster.common.entity.ApplicationError;
import com.gbs.grandmaster.common.entity.ERROR_LEVEL;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.ExamDefinition;
import com.gbs.grandmaster.repository.ExamDefinitionRepository;

@Service
public class ExamDefinitionService {


	@Autowired
	private ExamDefinitionRepository examDefinitionRepository;
	
	@Transactional
	public Response Save(ExamDefinition examDefinition) {
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
	
		response.setData(examDefinitionRepository.save(examDefinition));
		if(response.getData()!=null)
			response.setSuccess(true);
			else
				response.setSuccess(false);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
				
	}

	@Transactional
	public Response Update(ExamDefinition examDefinition) {
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
	
		response.setData(examDefinitionRepository.save(examDefinition));
		if(response.getData()!=null)
			response.setSuccess(true);
			else
				response.setSuccess(false);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
				
	}
	
	@Transactional
	public Response deleteExamDefinitions() {
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
			examDefinitionRepository.deleteAll();
		}catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
	
	}

	@Transactional
	public Response Delete(Long id) {

		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
			examDefinitionRepository.delete(id);
		response.setSuccess(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
	
	}

	@Transactional
	public Response FindByID(Long id) {

		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
	
		response.setData(examDefinitionRepository.findOne(id));
		if(response.getData()!=null)
		response.setSuccess(true);
		else
			response.setSuccess(false);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
	
	}

	@Transactional
	public Page<ExamDefinition> getAll(Pageable pageable) {
		return examDefinitionRepository.findAll(pageable);
	}














}
