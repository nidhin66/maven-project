package com.gbs.grandmaster.service;


import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.common.Constants;
import com.gbs.grandmaster.common.entity.ApplicationError;
import com.gbs.grandmaster.common.entity.ERROR_LEVEL;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.ExamSectionDetails;
import com.gbs.grandmaster.repository.ExamSectionDetailsRepository;

@Service
public class ExamSectionDetailsService {

	@Autowired
	private ExamSectionDetailsRepository examSectionDetailsRepository;
	
	@Transactional
	public Response Save(ExamSectionDetails examSectionDetails) {
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
	
		response.setData(examSectionDetailsRepository.save(examSectionDetails));
		if(response.getData()!=null)
			response.setSuccess(true);
			else
				response.setSuccess(false);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
				
	}

	@Transactional
	public Response Update(ExamSectionDetails examSectionDetails) {
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {	
		response.setData(examSectionDetailsRepository.save(examSectionDetails));
		if(response.getData()!=null)
			response.setSuccess(true);
			else
				response.setSuccess(false);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
				
	}

	@Transactional
	public Response Delete(Long id) {

		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
			examSectionDetailsRepository.delete(id);
		response.setSuccess(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
	
	}

	@Transactional
	public Response FindByID(Long id) {

		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
	
		response.setData(examSectionDetailsRepository.findOne(id));
		if(response.getData()!=null)
		response.setSuccess(true);
		else
			response.setSuccess(false);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;
	
	}

	@Transactional
	public Page<ExamSectionDetails> getAll(Pageable pageable) {
		return examSectionDetailsRepository.findAll(pageable);
	}




}
