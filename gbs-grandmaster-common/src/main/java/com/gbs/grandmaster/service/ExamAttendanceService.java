package com.gbs.grandmaster.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.common.Constants;
import com.gbs.grandmaster.common.entity.ATTENDANCE_STATUS;
import com.gbs.grandmaster.common.entity.ApplicationError;
import com.gbs.grandmaster.common.entity.ERROR_LEVEL;
import com.gbs.grandmaster.common.entity.EXAM_STATUS;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.ExamSchedule;
import com.gbs.grandmaster.entity.cassandra.AttendanceKey;
import com.gbs.grandmaster.entity.cassandra.ExamAttendance;
import com.gbs.grandmaster.entity.cassandra.ExamLog;
import com.gbs.grandmaster.repository.ExamScheduleRepository;
import com.gbs.grandmaster.repository.cassandra.ExamAttendanceRespository;
import com.gbs.grandmaster.repository.cassandra.ExamLogRepository;

@Service
public class ExamAttendanceService {

	@Autowired
	private ExamAttendanceRespository examAttendanceRepository;
	
	@Autowired
	private ExamLogRepository examLogRepository;
	
	@Autowired
	private ExamScheduleRepository examScheduleRepository;
	
	@Transactional
	public Response startExam(ExamAttendance examAttendance) {
		
		Response response=new Response();
		List<ApplicationError> exceptionlist=new ArrayList<>();
		try {
			ExamSchedule examSchedule = examScheduleRepository.findOne(examAttendance.getKey().getExamScheduleId());
			if(null != examSchedule) {
				Date currentDateTime = new Date();
				if(currentDateTime.before(examSchedule.getStartDateTime())) {
					ApplicationError applicationError=new ApplicationError();
					applicationError.setErrorCode("");
					applicationError.setErrorMessage("Exam not started yet.");
					applicationError.setLevel(ERROR_LEVEL.ERROR);
					response.setSuccess(false);
					exceptionlist.add(applicationError);
					response.setErrors(exceptionlist);
				}else if(currentDateTime.after(examSchedule.getEndDateTime())) {
					ApplicationError applicationError=new ApplicationError();
					applicationError.setErrorCode("");
					applicationError.setErrorMessage("Exam is already finished");
					applicationError.setLevel(ERROR_LEVEL.ERROR);
					response.setSuccess(false);
					exceptionlist.add(applicationError);
					response.setErrors(exceptionlist);
				}else {
					ExamAttendance lastAttendance =	examAttendanceRepository.findLastAttemptAttendance(examAttendance.getKey().getStudentId(), examAttendance.getKey().getExamScheduleId());
					int attempt = 1;
					if(null != lastAttendance) {
						if(lastAttendance.getStatus() == ATTENDANCE_STATUS.SUBMITTED) {
							attempt = lastAttendance.getKey().getAttempt() + 1;
						}else {
							attempt = lastAttendance.getKey().getAttempt();
						}
					}			
//					List<ExamAttendance> attendances = examAttendanceRepository.findByKeyStudentIdAndKeyExamScheduleId(examAttendance.getKey().getStudentId(), examAttendance.getKey().getExamScheduleId());
//					int attempt = 0;
//					if(null != attendances) {
//						attempt = attendances.size();
//					}
//					attempt = attempt + 1;
					examAttendance.getKey().setAttempt(attempt);
					examAttendance.setStatus(ATTENDANCE_STATUS.STARTED);
					examAttendance.setStartedDateTime(new Date());
					response.setSuccess(true);
					response.setData(examAttendanceRepository.save(examAttendance));		
				}
			}					
		}
		catch(Exception e) {
			e.printStackTrace();
			
			ApplicationError applicationError=new ApplicationError();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
			exceptionlist.add(applicationError);
			response.setErrors(exceptionlist);
		}
		
		return	response;
				
	}

	@Transactional
	public Response finishExam(ExamAttendance examAttendance) {
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
			AttendanceKey attendanceKey = examAttendance.getKey();
			ExamAttendance existingAttendance = examAttendanceRepository.
					findByKeyStudentIdAndKeyExamScheduleIdAndKeyAttempt(attendanceKey.getStudentId(), attendanceKey.getExamScheduleId(), attendanceKey.getAttempt());
			if(null != existingAttendance) {
				examAttendance = existingAttendance;
			}
			examAttendance.setStatus(ATTENDANCE_STATUS.FINISHED);
			examAttendance.setEndDateTime(new Date());			
			response.setData(examAttendanceRepository.save(examAttendance));	
			response.setSuccess(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;				
	}
	
	@Transactional
	public Response UploadExam(ExamAttendance examAttendance) {
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
			AttendanceKey attendanceKey = examAttendance.getKey();
			ExamAttendance existingAttendance = examAttendanceRepository.
					findByKeyStudentIdAndKeyExamScheduleIdAndKeyAttempt(attendanceKey.getStudentId(), attendanceKey.getExamScheduleId(), attendanceKey.getAttempt());

			ExamLog examLog = examLogRepository.findByKeyStudentIdAndKeyExamScheduleIdAndKeyAttempt(attendanceKey.getStudentId(), attendanceKey.getExamScheduleId(), attendanceKey.getAttempt());
			if(null != existingAttendance) {
				examAttendance = existingAttendance;
			}
			examAttendance.setStatus(ATTENDANCE_STATUS.SUBMITTED);
			examAttendance.setSubmittedTime(new Date());
			response.setData(examAttendanceRepository.save(examAttendance));	
			if(null != examLog) {
				examLog.setStatus(ATTENDANCE_STATUS.SUBMITTED);
				examLog.setUpdateTime(examAttendance.getSubmittedTime());
				examLogRepository.save(examLog);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;				
	}
	
	@Transactional
	public Response submitExam(ExamAttendance examAttendance) {
		List<ApplicationError> exceptionlist=new ArrayList<>();
		ApplicationError applicationError=new ApplicationError();
		Response response=new Response();
		try {
			AttendanceKey attendanceKey = examAttendance.getKey();
			ExamAttendance existingAttendance = examAttendanceRepository.
					findByKeyStudentIdAndKeyExamScheduleIdAndKeyAttempt(attendanceKey.getStudentId(), attendanceKey.getExamScheduleId(), attendanceKey.getAttempt());
			if(null != existingAttendance) {
				examAttendance = existingAttendance;
			}
			examAttendance.setStatus(ATTENDANCE_STATUS.SUBMITTED);
			examAttendance.setSubmittedTime(new Date());
			response.setData(examAttendanceRepository.save(examAttendance));	
    		response.setSuccess(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
		}
		exceptionlist.add(applicationError);
		response.setErrors(exceptionlist);
		return	response;				
	}
	
	public List<ExamAttendance> getAttendance(long examScheduleId,long studentId) {
		return examAttendanceRepository.findByKeyStudentIdAndKeyExamScheduleId(studentId, examScheduleId);
	}
	
	

	












}
