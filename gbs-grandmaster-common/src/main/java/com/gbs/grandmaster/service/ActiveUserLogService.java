package com.gbs.grandmaster.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.common.Constants;
import com.gbs.grandmaster.common.entity.ApplicationError;
import com.gbs.grandmaster.common.entity.ERROR_LEVEL;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.ApplicationConfig;
import com.gbs.grandmaster.entity.cassandra.ActiveUserLog;
import com.gbs.grandmaster.entity.cassandra.Answer;
import com.gbs.grandmaster.repository.AppConfigRepository;
import com.gbs.grandmaster.repository.cassandra.ActiveUserLogRepository;

@Service
public class ActiveUserLogService {
	
	@Autowired
	private ActiveUserLogRepository activeUserLogRepository;
	
	@Autowired
	private AppConfigRepository appConfigRepository;
	
	
	public Response logActiveUserLog(ActiveUserLog activeUserLog) {
		Response response=new Response();
		try {
			activeUserLogRepository.save(activeUserLog);
			response.setSuccess(true);
		}catch(Exception e) {				
				List<ApplicationError> exceptionlist=new ArrayList<>();
				ApplicationError applicationError=new ApplicationError();			
				applicationError.setErrorCode("");
				applicationError.setErrorMessage(Constants.ERROR_MSG);
				applicationError.setLevel(ERROR_LEVEL.ERROR);
				response.setSuccess(false);
				exceptionlist.add(applicationError);
				response.setErrors(exceptionlist);
			}
			return response;
	}
	
	public Response isUserActive(ActiveUserLog activeUserLog) {
		Response response=new Response();
		try {
			ActiveUserLog userLog  = activeUserLogRepository.findByUserName(activeUserLog.getUserName());
			if(null == userLog) {
				response.setData(false);
			}else {
				Date updateTime = userLog.getUpdateTime();
				Date currentTime = new Date();
				long diff = currentTime.getTime() - updateTime.getTime();
				int diffmin = (int) (diff / (60 * 1000));
				ApplicationConfig appconfig = appConfigRepository.findByProperty(Constants.MULTIPLE_LOGIN_ALLOWED);
				if(null != appconfig) {
					if(activeUserLog.getMacId().equalsIgnoreCase(userLog.getMacId())) {
						response.setData(false);
					}else {
						if("No".equalsIgnoreCase(appconfig.getValue())) {
							ApplicationConfig timeCheckConfig = appConfigRepository.findByProperty(Constants.ACTIVE_USER_CHECK_TIME);
							int timeInterval = 1;
							if(null != timeCheckConfig) {
								timeInterval = Integer.valueOf(timeCheckConfig.getValue());
							}
							if(diffmin > timeInterval) {
								response.setData(false);
							}else {
								response.setData(true);
							}
						}else {
							response.setData(false);
						}
					}					
				}else {
					response.setData(false);
				}
			}
			response.setSuccess(true);
		}catch(Exception e) {
			List<ApplicationError> exceptionlist=new ArrayList<>();
			ApplicationError applicationError=new ApplicationError();			
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
			exceptionlist.add(applicationError);
			response.setErrors(exceptionlist);
		}
		return response;
	}

}
