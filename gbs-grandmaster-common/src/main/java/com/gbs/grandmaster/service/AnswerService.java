package com.gbs.grandmaster.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.common.Constants;
import com.gbs.grandmaster.common.entity.ATTENDANCE_STATUS;
import com.gbs.grandmaster.common.entity.AnswerSheet;
import com.gbs.grandmaster.common.entity.ApplicationError;
import com.gbs.grandmaster.common.entity.ERROR_LEVEL;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.ExamSchedule;
import com.gbs.grandmaster.entity.cassandra.Answer;
import com.gbs.grandmaster.entity.cassandra.ExamAttendance;
import com.gbs.grandmaster.repository.ExamScheduleRepository;
import com.gbs.grandmaster.repository.cassandra.AnswerRepository;
import com.gbs.grandmaster.repository.cassandra.ExamAttendanceRespository;


@Service
public class AnswerService {
	
	@Autowired
	AnswerRepository answerRepository;
	
	@Autowired
	ExamScheduleRepository examScheduleRepository;
	
	@Autowired
	ExamAttendanceRespository examAttendanceRespository;
	
	public Response saveAswers(List<Answer> answers) {
		Response response=new Response();
		try {
			for(Answer answer : answers) {
				answer.setUpdateTime(new Date());
			}
			answerRepository.save(answers);
			response.setSuccess(true);
		}catch(Exception e) {
			e.printStackTrace();
			List<ApplicationError> exceptionlist=new ArrayList<>();
			ApplicationError applicationError=new ApplicationError();			
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
			exceptionlist.add(applicationError);
			response.setErrors(exceptionlist);
		}
		return response;
	}
	
	public Response getAnswers(long studentId,long examScheduleId,int attempt) {
		Response response=new Response();
		try {			
			response.setData(answerRepository.findByKeyStudentIdAndKeyExamScheduleIdAndKeyAttempt(studentId, examScheduleId, attempt));
			response.setSuccess(true);
		}catch(Exception e) {
			e.printStackTrace();
			List<ApplicationError> exceptionlist=new ArrayList<>();
			ApplicationError applicationError=new ApplicationError();			
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
			exceptionlist.add(applicationError);
			response.setErrors(exceptionlist);
		}
		return response;
	}

	public Response getExamAnswerSheet(long student_id, long batch_id) {
		Response response=new Response();
		try {		
			List<AnswerSheet> answerSheets = new ArrayList<>();
			List<ExamSchedule> examSchedules = examScheduleRepository.findResultPublishedExamByBatch(batch_id);
			if(null != examSchedules || examSchedules.size()>0) {	
				for(ExamSchedule examSchedule : examSchedules) {
					List<ExamAttendance> attempts = examAttendanceRespository.findByKeyStudentIdAndKeyExamScheduleId(student_id, examSchedule.getId());
					for(ExamAttendance attendance : attempts) {
						if(attendance.getStatus() != ATTENDANCE_STATUS.STARTED) {
							AnswerSheet answerSheet = new AnswerSheet();
							answerSheet.setAttendance(attendance);
							answerSheet.setExam(examSchedule);
							answerSheets.add(answerSheet);
						}
					}
				}				
			}
			response.setData(answerSheets);
			response.setSuccess(true);
		}catch(Exception e) {
			e.printStackTrace();
			List<ApplicationError> exceptionlist=new ArrayList<>();
			ApplicationError applicationError=new ApplicationError();			
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
			exceptionlist.add(applicationError);
			response.setErrors(exceptionlist);
		}
		return response;
	}
	
	public Response getMockExamAnswerSheet(long student_id, long batch_id) {
		Response response=new Response();
		try {		
			List<AnswerSheet> answerSheets = new ArrayList<>();
			List<ExamSchedule> examSchedules = examScheduleRepository.findResultPublishedMockExamByBatch(batch_id);
			if(null != examSchedules || examSchedules.size()>0) {	
				for(ExamSchedule examSchedule : examSchedules) {
					List<ExamAttendance> attempts = examAttendanceRespository.findByKeyStudentIdAndKeyExamScheduleId(student_id, examSchedule.getId());
					for(ExamAttendance attendance : attempts) {
						if(attendance.getStatus() != ATTENDANCE_STATUS.STARTED) {
							AnswerSheet answerSheet = new AnswerSheet();
							answerSheet.setAttendance(attendance);
							answerSheet.setExam(examSchedule);
							answerSheets.add(answerSheet);
						}
					}
				}				
			}
			response.setData(answerSheets);
			response.setSuccess(true);
		}catch(Exception e) {
			e.printStackTrace();
			List<ApplicationError> exceptionlist=new ArrayList<>();
			ApplicationError applicationError=new ApplicationError();			
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
			exceptionlist.add(applicationError);
			response.setErrors(exceptionlist);
		}
		return response;
	}
			
	

}
