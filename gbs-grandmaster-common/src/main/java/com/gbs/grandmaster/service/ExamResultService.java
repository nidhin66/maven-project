package com.gbs.grandmaster.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.grandmaster.entity.ExamResult;
import com.gbs.grandmaster.repository.ExamResultRepository;

@Service
public class ExamResultService {
	
	@Autowired
	private ExamResultRepository resultRepository;
	
	public void save(List<ExamResult> examResults) {
		resultRepository.save(examResults);
	}


}
