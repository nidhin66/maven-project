package com.gbs.grandmaster.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datastax.driver.core.utils.UUIDs;
import com.gbs.grandmaster.common.Constants;
import com.gbs.grandmaster.common.entity.ApplicationError;
import com.gbs.grandmaster.common.entity.ERROR_LEVEL;
import com.gbs.grandmaster.common.entity.ExamStatusLog;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.cassandra.Answer;
import com.gbs.grandmaster.entity.cassandra.ExamAttendance;
import com.gbs.grandmaster.entity.cassandra.QuestionAttemptKey;
import com.gbs.grandmaster.entity.cassandra.QuestionAttemptTime;
import com.gbs.grandmaster.repository.cassandra.AnswerRepository;
import com.gbs.grandmaster.repository.cassandra.ExamLogRepository;
import com.gbs.grandmaster.repository.cassandra.QuestionAttemptTimeRepository;

@Service
public class ExamLogService {

	@Autowired
	private ExamLogRepository examLogRepository;

	@Autowired
	private AnswerRepository answerRepository;

	@Autowired
	private QuestionAttemptTimeRepository questionAttemptTimeRepository;
	
	public List<QuestionAttemptTime> findQuestionAttemptTimeByQuestionId(long studentId,long examScheduleId,int attempt,long questionId){
		return questionAttemptTimeRepository.findQuestionAttemptTimeByQuestionId(studentId, examScheduleId, attempt, questionId);
	}
	
	public Map<Long, List<QuestionAttemptTime>> getQuestionTimeDetails(ExamAttendance attendance){		
		Map<Long, List<QuestionAttemptTime>> timeDetails = new HashMap<>();
		List<QuestionAttemptTime> questionAttempts = questionAttemptTimeRepository
				.findQuestionAttemptTimeByAttendance(attendance.getKey().getStudentId(), attendance.getKey().getExamScheduleId(), attendance.getKey().getAttempt());
		for(QuestionAttemptTime questionAttemptTime : questionAttempts) {
			long questionId = questionAttemptTime.getKey().getQuestionId();
			List<QuestionAttemptTime> questionTimeDetails = timeDetails.get(questionId);
			if(null == questionTimeDetails) {
				questionTimeDetails = new ArrayList<>();
				timeDetails.put(questionId, questionTimeDetails);
			}
			questionTimeDetails.add(questionAttemptTime);
		}
		return timeDetails;		
	}

	public Response saveExamLog(ExamStatusLog statusLog) {
		Response response = new Response();
		try {
			for (Answer answer : statusLog.getAnswers()) {
				answer.setUpdateTime(new Date());
			}
			for (QuestionAttemptTime questionAttemptTime : statusLog.getQuestionAttemptTime()) {
				questionAttemptTime.setUpdateTime(new Date());
				questionAttemptTime.getKey().setId(UUIDs.timeBased());
			}
			answerRepository.save(statusLog.getAnswers());
			statusLog.getExamLog().setUpdateTime(new Date());
			questionAttemptTimeRepository.save(statusLog.getQuestionAttemptTime());
			statusLog.getExamLog().setUpdateTime(new Date());
			examLogRepository.save(statusLog.getExamLog());
			response.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
			List<ApplicationError> exceptionlist = new ArrayList<>();
			ApplicationError applicationError = new ApplicationError();
			applicationError.setErrorCode("");
			applicationError.setErrorMessage(Constants.ERROR_MSG);
			applicationError.setLevel(ERROR_LEVEL.ERROR);
			response.setSuccess(false);
			exceptionlist.add(applicationError);
			response.setErrors(exceptionlist);
		}
		return response;
	}
	
	
	
}
