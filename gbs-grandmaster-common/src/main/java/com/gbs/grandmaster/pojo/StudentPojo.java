package com.gbs.grandmaster.pojo;

import java.util.Date;

import com.gbs.grandmaster.common.entity.EXAM_STATUS;

public class StudentPojo {

	private Long id;
	private EXAM_STATUS status;
	private boolean resultImmeadiate;
	private int noOfAttempts;
	private Date startDateTime;
	private Date endDateTime;
	
	public Date getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EXAM_STATUS getStatus() {
		return status;
	}

	public void setStatus(EXAM_STATUS exam_STATUS) {
		this.status = exam_STATUS;
	}

	public boolean isResultImmeadiate() {
		return resultImmeadiate;
	}

	public void setResultImmeadiate(boolean resultImmeadiate) {
		this.resultImmeadiate = resultImmeadiate;
	}

	public int getNoOfAttempts() {
		return noOfAttempts;
	}

	public void setNoOfAttempts(int noOfAttempts) {
		this.noOfAttempts = noOfAttempts;
	}

}
