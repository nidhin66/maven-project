package com.gbs.grandmaster.exception;

public class GrandMasterApplicationException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public GrandMasterApplicationException(String message) {
		super(message);
	}

}
