package com.gbs.grandmaster.ui.model;

public class Instruction {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private int version;
	private byte[] content;

	//ID
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	//VERSION
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	
	//CONTENT
	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}
	
	public String getContentAsString()
	{
		if(null != content) {
			return new String(content);
		}else {
			return "";
		}
		
	}
	

}



