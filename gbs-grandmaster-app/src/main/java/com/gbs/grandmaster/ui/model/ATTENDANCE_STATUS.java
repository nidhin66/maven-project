package com.gbs.grandmaster.ui.model;

public enum ATTENDANCE_STATUS {
	STARTED("S"),FINISHED("F"),SUBMITTED("D");
	private final String code ;
	ATTENDANCE_STATUS(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	
	public String toString() {
		return code;
	}
	
}
