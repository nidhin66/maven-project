package com.gbs.grandmaster.ui.page;

import com.gbs.grandmaster.ui.AppUtils;
import com.gbs.grandmaster.ui.Constants;
import com.gbs.grandmaster.ui.LabelAndMessageUtil;
import com.gbs.grandmaster.ui.page.controller.LoginController;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;

public class LoginWindow extends BorderPane {

	TextField userField = new TextField();
	PasswordField passwordField = new PasswordField();
	Button loginButton;
	
	public LoginWindow() {

		SplitPane contentPane = new SplitPane();
		contentPane.setOrientation(Orientation.HORIZONTAL);
		contentPane.setDividerPositions(0.60);
		GMImageSlider imageSlider = new GMImageSlider();
		SplitPane.setResizableWithParent(imageSlider, true);
		contentPane.getItems().addAll(imageSlider, createLoginPane());
		setCenter(contentPane);
		LoginController controller = new LoginController(this);
	}

	private VBox createLoginPane() {
		
		VBox loginPane = new VBox();
		ImageView imageView = new ImageView("images/grand_master.png"); 
		imageView.setFitWidth(100);
		imageView.setFitHeight(75);
		imageView.setPreserveRatio(true);
		
		Label versionLbl = new Label("Version - "+AppUtils.getAppVersion());
		versionLbl.getStyleClass().add("versionLbl");
		
		userField.setPromptText(LabelAndMessageUtil.getLabel(Constants.LBL_USER_NAME));
		passwordField.setPromptText(LabelAndMessageUtil.getLabel(Constants.LBL_PASSWORD));
		loginButton = new Button(LabelAndMessageUtil.getLabel(Constants.LBL_SUBMIT));
		
		loginPane.getChildren().addAll(imageView,versionLbl,userField,passwordField,loginButton);
		loginPane.setAlignment(Pos.CENTER_LEFT);
		loginPane.setSpacing(20);
		loginPane.setPadding(new Insets(0, 50, 0, 75));
		return loginPane;
	}
	
	public String getUserName() {
		return userField.getText();
	}

	public String getPassword() {
		return passwordField.getText();
	}

	public Button getSubmitButton() {
		loginButton.setDefaultButton(true);
		loginButton.getStyleClass().add("loginButton");
		loginButton.setPrefSize(85, 30);
		return loginButton;
	}

}
