package com.gbs.grandmaster.ui.model;

import java.util.Date;

public class ExamLog {

	private AttendanceKey key;
	
	private String sectionStatusStr;
	
	private long elapsedTimeInSec;
	
	private ATTENDANCE_STATUS status;
	
	private Date updateTime;

	public ExamLog() {

	}
	
	public ExamLog(AttendanceKey key) {
		this.key = key;
	}

	public AttendanceKey getKey() {
		return key;
	}

	public void setKey(AttendanceKey key) {
		this.key = key;
	}

	public String getSectionStatusStr() {
		return sectionStatusStr;
	}

	public void setSectionStatusStr(String sectionStatusStr) {
		this.sectionStatusStr = sectionStatusStr;
	}

	public long getElapsedTimeInSec() {
		return elapsedTimeInSec;
	}

	public void setElapsedTimeInSec(long elapsedTimeInSec) {
		this.elapsedTimeInSec = elapsedTimeInSec;
	}

	public ATTENDANCE_STATUS getStatus() {
		return status;
	}

	public void setStatus(ATTENDANCE_STATUS status) {
		this.status = status;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	

}
