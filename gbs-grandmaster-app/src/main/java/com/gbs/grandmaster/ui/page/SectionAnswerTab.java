package com.gbs.grandmaster.ui.page;

import java.util.List;

import org.apache.log4j.Logger;

import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.model.Question;
import com.gbs.grandmaster.ui.model.QuestionAttemptTime;
import com.gbs.grandmaster.ui.page.controller.AppController;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Tab;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class SectionAnswerTab extends Tab{
	static Logger logger = Logger.getLogger(SectionAnswerTab.class);
	
	public SectionAnswerTab(AnswerSheetPage answerSheetPage,ExamSection examSection) {
		setText(examSection.getSectionName().toUpperCase());	
	    getStyleClass().add("answer-tab");
		setClosable(false);
			
		VBox answerVBox = new VBox();
		answerVBox.setPadding(new Insets(20,25,0,20));
		answerVBox.setSpacing(5);
		answerVBox.setAlignment(Pos.TOP_CENTER);
		answerVBox.setStyle("-fx-background-color: white;");
		
	
		
		HBox totalQuestionsBox = new HBox();
		totalQuestionsBox.setSpacing(20);
		totalQuestionsBox.setStyle("-fx-background-color:#e9f1f4;");		
		Label totalQuestionsText = new Label("Total Questions");
		totalQuestionsText.setPadding(new Insets(10,0,10,10));
		totalQuestionsText.setStyle("-fx-font-weight: bold;");
		Region totalQuestionMidRegion = new Region();
		HBox.setHgrow(totalQuestionMidRegion, Priority.ALWAYS);
		Label totalQuestionsNumber = new Label("0");
		totalQuestionsNumber.setPadding(new Insets(5,10,5,10));
		HBox.setMargin(totalQuestionsNumber, new Insets(5,10,5,0));
		totalQuestionsNumber.setStyle("-fx-background-color:#00a99d;-fx-text-fill: #7bdae1;-fx-font-weight: bold;");
		totalQuestionsBox.getChildren().addAll(totalQuestionsText,totalQuestionMidRegion,totalQuestionsNumber);
		HBox.setHgrow(totalQuestionsBox, Priority.ALWAYS);
		
		HBox totalAnsweredBox = new HBox();
		totalAnsweredBox.setSpacing(20);
		totalAnsweredBox.setStyle("-fx-background-color:#e4e8ea");	
		Label totalAnsweredText = new Label("Total Answered");
		totalAnsweredText.setPadding(new Insets(10,0,10,10));
		totalAnsweredText.setStyle("-fx-font-weight: bold;");
		Region totalAnsweredMidRegion = new Region();
		HBox.setHgrow(totalAnsweredMidRegion, Priority.ALWAYS);
		Label totalAnsweredNumber = new Label("0");
		totalAnsweredNumber.setPadding(new Insets(5,10,5,10));
		HBox.setMargin(totalAnsweredNumber, new Insets(5,10,5,0));
		totalAnsweredNumber.setStyle("-fx-background-color:#00a99d;-fx-text-fill: #7bdae1;-fx-font-weight: bold;");
		totalAnsweredBox.getChildren().addAll(totalAnsweredText,totalAnsweredMidRegion,totalAnsweredNumber);
		HBox.setHgrow(totalAnsweredBox, Priority.ALWAYS);
		
		HBox totalCorrectBox = new HBox();
		totalCorrectBox.setSpacing(20);
		totalCorrectBox.setStyle("-fx-background-color:#e9f1f4");			
		Label totalCorrectText = new Label("Total Correct Answers");
		totalCorrectText.setPadding(new Insets(10,10,10,10));
		totalCorrectText.setStyle("-fx-font-weight: bold;");
		Region totalCorrectMidRegion = new Region();		
		HBox.setHgrow(totalCorrectMidRegion, Priority.ALWAYS);
		Label totalCorrectNumber = new Label("0");
		totalCorrectNumber.setPadding(new Insets(5,10,5,10));
		HBox.setMargin(totalCorrectNumber, new Insets(5,10,5,0));
		totalCorrectNumber.setStyle("-fx-background-color:#00a99d;-fx-text-fill: #7bdae1;-fx-font-weight: bold;");
		totalCorrectBox.getChildren().addAll(totalCorrectText,totalCorrectMidRegion,totalCorrectNumber);
		HBox.setHgrow(totalCorrectBox, Priority.ALWAYS);
		
		HBox totalWrongBox = new HBox();
		totalWrongBox.setSpacing(20);
		totalWrongBox.setStyle("-fx-background-color:#e4e8ea");	
		Label totalWrongText = new Label("Total Wrong Answers");
		totalWrongText.setPadding(new Insets(10,10,10,10));
		totalWrongText.setStyle("-fx-font-weight: bold;");
		Region totalWrongMidRegion = new Region();
		HBox.setHgrow(totalWrongMidRegion, Priority.ALWAYS);
		Label totalWrongNumber = new Label("0");
		totalWrongNumber.setPadding(new Insets(5,10,5,10));
		HBox.setMargin(totalWrongNumber, new Insets(5,10,5,0));
		totalWrongNumber.setStyle("-fx-background-color:#00a99d;-fx-text-fill: #7bdae1;-fx-font-weight: bold;");
		totalWrongBox.getChildren().addAll(totalWrongText,totalWrongMidRegion,totalWrongNumber);
		HBox.setHgrow(totalWrongBox, Priority.ALWAYS);

		
		HBox questionStsBox = new HBox();
		questionStsBox.getChildren().addAll(totalQuestionsBox,totalAnsweredBox,totalCorrectBox,totalWrongBox);
		answerVBox.getChildren().add(questionStsBox);
		
		HBox obtainedMarkBox = new HBox();
		obtainedMarkBox.setSpacing(10);
		obtainedMarkBox.setStyle("-fx-background-color:#1e88e5");	
		Label obtainedMarkText = new Label("Marks Obtained");	
		obtainedMarkText.setStyle("-fx-text-fill:white;-fx-font-weight: bold;");
		obtainedMarkText.setPadding(new Insets(10,10,10,10));
		Region  obtainedMarkMidRegion = new Region();
		HBox.setHgrow(obtainedMarkMidRegion, Priority.ALWAYS);
		Label obtainedMarkSep = new Label(":");
		obtainedMarkSep.setStyle("-fx-text-fill:white;-fx-font-weight: bold;");
		obtainedMarkSep.setAlignment(Pos.CENTER_RIGHT);
		obtainedMarkSep.setPadding(new Insets(10,0,10,10));
		Label obtainedMarkBoxLbl = new Label("0");
		obtainedMarkBoxLbl.setPadding(new Insets(10,10,10,0));
		obtainedMarkBoxLbl.setStyle("-fx-text-fill:white;-fx-font-weight: bold;");
		obtainedMarkBox.getChildren().addAll(obtainedMarkText,obtainedMarkMidRegion,obtainedMarkSep,obtainedMarkBoxLbl);
		HBox.setHgrow(obtainedMarkBox, Priority.ALWAYS);
		
		
		HBox negativeMarkBox = new HBox();
		negativeMarkBox.setSpacing(10);
		negativeMarkBox.setStyle("-fx-background-color:#0e6aaf");
		Label negativeMarkText = new Label("Negative Marks");
		negativeMarkText.setStyle("-fx-text-fill:white;-fx-font-weight: bold;");
		negativeMarkText.setPadding(new Insets(10,10,10,10));
		Region  negativeMarkMidRegion = new Region();
		HBox.setHgrow(negativeMarkMidRegion, Priority.ALWAYS);
		Label negativeMarkSep = new Label(":");
		negativeMarkSep.setStyle("-fx-text-fill:white;-fx-font-weight: bold;");
		negativeMarkSep.setPadding(new Insets(10,0,10,10));
		negativeMarkSep.setAlignment(Pos.CENTER_RIGHT);
		Label negativeMarkBoxLbl = new Label("0");
		negativeMarkBoxLbl.setPadding(new Insets(10,10,10,0));
		negativeMarkBoxLbl.setStyle("-fx-text-fill:white;-fx-font-weight: bold;");
		negativeMarkBox.getChildren().addAll(negativeMarkText,negativeMarkMidRegion,negativeMarkSep,negativeMarkBoxLbl);
		HBox.setHgrow(negativeMarkBox, Priority.ALWAYS);
		
		HBox grandTotalBox = new HBox();
		grandTotalBox.setSpacing(10);
		grandTotalBox.setStyle("-fx-background-color:#1e88e5");
		Label grandTotalText = new Label("Grand Total");		
		grandTotalText.setStyle("-fx-text-fill:white;-fx-font-weight: bold;");
		grandTotalText.setPadding(new Insets(10,10,10,10));
		Region  grandTotalMidRegion = new Region();
		HBox.setHgrow(grandTotalMidRegion, Priority.ALWAYS);
		Label grandTotalSep = new Label(":");
		grandTotalSep.setStyle("-fx-text-fill:white;-fx-font-weight: bold;");
		grandTotalSep.setAlignment(Pos.CENTER_RIGHT);
		grandTotalSep.setPadding(new Insets(10,0,10,10));
		HBox.setHgrow(grandTotalSep, Priority.ALWAYS);
		Label grandTotalBoxLbl = new Label("0");
		grandTotalBoxLbl.setStyle("-fx-text-fill:white;-fx-font-weight: bold;");
		grandTotalBoxLbl.setPadding(new Insets(10,10,10,0));
		
		grandTotalBox.getChildren().addAll(grandTotalText,grandTotalMidRegion,grandTotalSep,grandTotalBoxLbl);
		HBox.setHgrow(grandTotalBox, Priority.ALWAYS);
		
		
		HBox marksBox = new HBox();
		marksBox.getChildren().addAll(obtainedMarkBox,negativeMarkBox,grandTotalBox);
		answerVBox.getChildren().add(marksBox);
		
		
		
		GridPane answerGrid = new GridPane();
		answerGrid.setPadding(new Insets(20,0,0,0));
		answerGrid.setStyle("-fx-background-color: white;");
		answerGrid.setHgap(15);
		answerGrid.setVgap(20);
		
		int i =0;	
		int itemCount = getItemCount();

		int row = -1;
		int col = 0;
		int attemptedAnswers = 0;
		int correctAnswers = 0;
		double marksObtained = 0;
		double negativeMarks = 0;
		double grandTotal = 0;
//		logger.debug("Time taken for questions in section :" + examSection.getSectionName());
//		logger.debug("**********************************************************************");
		for(Question question : examSection.getQuestions()) {
			Answer answer = answerSheetPage.getAnswer(question.getId());	
			if(null != answer) {
				if(!CommonUtilitties.isNullorEmpty(answer.getContent())) {
					attemptedAnswers++;
				}
				if(answer.isCorrect()) {
					correctAnswers++;
				}
				marksObtained = marksObtained + answer.getMark();
				negativeMarks = negativeMarks+ answer.getNegativeMark();
			}
			AnswerButton ansBtn = new AnswerButton(examSection,question, answer, i);
			if(i%itemCount == 0) {
				row++;
				col=0;
			}
			answerGrid.add(ansBtn, col,row );
//			logger.debug("Question Number : " + i);
//			logger.debug("Time Taken ");
//			List<QuestionAttemptTime> attemptTimes = answerSheetPage.getQuestionAttemptTime(question.getId());
//			if(null != attemptTimes) {
//				for(QuestionAttemptTime attemptTime : attemptTimes) {
//					logger.debug("Time taken :" + attemptTime.getTimeTaken() +  " Time : " + attemptTime.getUpdateTime());
//				}
//			}
			
//			logger.debug("#####################################################################");
			i++;
			col++;
			
			
		}
		int wrongAnswers = attemptedAnswers - correctAnswers;
		grandTotal = marksObtained - negativeMarks;
		
		totalQuestionsNumber.setText(examSection.getQuestionCount()+"");
		totalAnsweredNumber.setText(attemptedAnswers + "");
		totalCorrectNumber.setText(correctAnswers + "");
		totalWrongNumber.setText(wrongAnswers + "");
		
		obtainedMarkBoxLbl.setText(marksObtained+"");
		negativeMarkBoxLbl.setText(negativeMarks + "");
		grandTotalBoxLbl.setText(grandTotal + "");
		
		answerVBox.getChildren().add(answerGrid);
		
		setContent(answerVBox);
	
	}
	
	
	private int getItemCount() {
		int count = (int) (UIUtilities.getScreenWidth() / 65);
		return  count;
	}

}
