package com.gbs.grandmaster.ui.page;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.gbs.grandmaster.ui.page.action.ExamTimerListner;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.util.Duration;

public class ExamTimer extends Label implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int timeInMin = 0;
	
	Timeline digitalTime;
	
	private ExamTimerListner listner ;
	
	private Date startTime;
	
	private boolean countDown = true;
	
	private long elapsedTime = 0;

	public ExamTimer(int timeInMin,boolean countDown,ExamTimerListner listner) {
		this.timeInMin = timeInMin;
		this.listner = listner;
		this.countDown = countDown;		
		int timeInSeconds = timeInMin * 60;
//		int timeInSeconds = 60;
		setTimeLine(timeInSeconds);		
	}	
	
	private void setTimeLine(long timeInSeconds) {
		digitalTime = new Timeline(new KeyFrame(Duration.minutes(0), new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				Date currentTime = new Date();
				long seconds = (currentTime.getTime() - startTime.getTime()) / 1000;
				long millis = 0;				
				if(countDown) {
					millis = (timeInSeconds - seconds) * 1000;
				}else {
					millis = seconds *1000;
				}	
				long totalTimeInMills = timeInSeconds * 1000;
				String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
						TimeUnit.MILLISECONDS.toMinutes(millis)
								- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
						TimeUnit.MILLISECONDS.toSeconds(millis)
								- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
				
				String totoalHMS = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(totalTimeInMills),
						TimeUnit.MILLISECONDS.toMinutes(totalTimeInMills)
								- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(totalTimeInMills)),
						TimeUnit.MILLISECONDS.toSeconds(totalTimeInMills)
								- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTimeInMills)));
				
				if (seconds <= timeInSeconds) {
					setText("TIME - "+hms + " / " + totoalHMS);
				}else {
					if(null != listner) {
						listner.timerFinished();
					}					
					digitalTime.stop();
				}
			}
		}), new KeyFrame(Duration.seconds(1)));

		digitalTime.setCycleCount(Animation.INDEFINITE);
	}
	
	
	public void startClock(Date startTime) {
		this.startTime = startTime;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startTime);
		calendar.add(Calendar.SECOND, (int) (elapsedTime * -1));
		startTime = calendar.getTime();
		digitalTime.play();
	}
	
	public void startClock() {		
		startClock(new Date());			
	}
	public void stopClock() {
		if(null != listner) {
			listner.timerFinished();
		}		
		digitalTime.stop();
	}
	
	public long elapsedTimeinSecond() {
		Date currentTime = new Date();
		long seconds = (currentTime.getTime() - startTime.getTime()) / 1000;
		return seconds;
	}

	public void setElapsedTime(long elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	
}
