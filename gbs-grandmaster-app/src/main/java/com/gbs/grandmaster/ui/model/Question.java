package com.gbs.grandmaster.ui.model;


import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;



//@Entity
//@Table(name = "GM_QUESTION")
public class Question {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private Long id;
	
	
	private int version;
	
	
	private Set<QuestionText> questionTexts;
	
	private String diagramPath;
	
	
	private Set<Choice> choices;
	
	private double mark;
	
	private double negativeMark;
	
	private long paragraphId;
	
	private long questionSeq;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Set<QuestionText> getQuestionTexts() {
		return questionTexts;
	}

	public void setQuestionTexts(Set<QuestionText> questionTexts) {
		this.questionTexts = questionTexts;
	}

	public String getDiagramPath() {
		return diagramPath;
	}

	public void setDiagramPath(String diagramPath) {
		this.diagramPath = diagramPath;
	}

	public Set<Choice> getChoices() {
		return choices;
	}

	public void setChoices(Set<Choice> choices) {
		this.choices = choices;
	}

	
	public void addQuestionText(QuestionText questionText) {
		if(null == questionTexts) {
			questionTexts = new HashSet<>();
		}
		questionTexts.add(questionText);
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}

	public double getNegativeMark() {
		return negativeMark;
	}

	public void setNegativeMark(double negativeMark) {
		this.negativeMark = negativeMark;
	}

	public String getDefaultQuestionText() {
		if(null != questionTexts) {
			Iterator<QuestionText> iter = questionTexts.iterator();
			QuestionText first = iter.next();
			byte[] cotent = first.getContent();
			return new String(cotent);
		}else {
			return "";
		}
	}
	
	public String getDefaultSolutionText() {
		if(null != questionTexts) {
			Iterator<QuestionText> iter = questionTexts.iterator();
			QuestionText first = iter.next();
			byte[] cotent = first.getSolution();
			if(null != cotent) {
				return new String(cotent);
			}else {
				return "";
			}
			
		}else {
			return "";
		}
	}

	public long getParagraphId() {
		return paragraphId;
	}

	public void setParagraphId(long paragraphId) {
		this.paragraphId = paragraphId;
	}

	public long getQuestionSeq() {
		return questionSeq;
	}

	public void setQuestionSeq(long questionSeq) {
		this.questionSeq = questionSeq;
	}


}
