package com.gbs.grandmaster.ui.common;

import com.gbs.grandmaster.ui.UIManager;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class MainWindow extends BorderPane{
	
	private static class MainWindowHelper {
		private static final MainWindow INSTANCE = new MainWindow();
	}
	
	private MainWindow() {
		createHeader();
		createFooter();
	}
	
	public static MainWindow getInstance() {
		
		return MainWindowHelper.INSTANCE;
	} 
	
	private void createHeader() {
		HBox headerPane = new HBox();
		headerPane.getStyleClass().add("common-header");
		Label username = new Label("User :");
		username.getStyleClass().add("userName");
		headerPane.getChildren().add(username);
		Label userLabel = new Label(UIManager.getInstance().getStudent().getName());
		userLabel.getStyleClass().add("userLabel");
		headerPane.getChildren().add(userLabel);		
		headerPane.setSpacing(10);
		Button logOutBtn = new Button();
		logOutBtn.getStyleClass().add("logOutButton");
		logOutBtn.setPrefSize(25, 25);
		headerPane.getChildren().add(logOutBtn);
		logOutBtn.setOnAction(e->{
			UIManager.exitApplication(true);
		});		
		headerPane.setAlignment(Pos.CENTER_RIGHT);
		setTop(headerPane);
		HBox.setHgrow(logOutBtn, Priority.ALWAYS);
		BorderPane.setAlignment(headerPane, Pos.CENTER_RIGHT);
		headerPane.setPadding(new Insets(5, 5, 5, 0));
	}
	
	private void createFooter() {
		HBox footerPane = new HBox();
		footerPane.getStyleClass().add("common-footer");	
		
		ImageView footerImage = new ImageView("images/powered.png");
		footerImage.setPreserveRatio(true);
		footerImage.setFitHeight(20);
		footerImage.setFitWidth(125);
		footerPane.getChildren().add(footerImage);
		footerPane.setAlignment(Pos.CENTER_LEFT);
		
		
		
		setBottom(footerPane);
		BorderPane.setAlignment(footerImage, Pos.BASELINE_LEFT);
		footerPane.setPadding(new Insets(15, 0, 15, 15));
	}
	
	public void setContent(Parent parent) {	
		BorderPane.setAlignment(parent, Pos.CENTER);
		setCenter(parent);		
//		setMargin(parent, new Insets(200,25, 25, 25));
	}

}
