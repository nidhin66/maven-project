package com.gbs.grandmaster.ui.model;

public enum EXAM_STATUS {
	CREATED("C"),SCHEDULED("S"),ENABLED("E"),FINISHED("F"),RESULT_PUBLISHED("R"),CLOSED("D");
	private final String code ;
	private EXAM_STATUS(String code) {
		this.code =code;
	}

}
