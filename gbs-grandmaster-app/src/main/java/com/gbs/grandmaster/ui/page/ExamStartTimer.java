package com.gbs.grandmaster.ui.page;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.gbs.grandmaster.ui.page.action.ExamTimerListner;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.util.Duration;

public class ExamStartTimer extends Label {

	Timeline digitalTime;
	
	private Date startTime;
	
	private ExamTimerListner listner ;
	
	public ExamStartTimer(Date startTime,ExamTimerListner listner) {
		getStyleClass().add("examClock");
		this.listner = listner;
		this.startTime = startTime;
		setTimeLine();
		startClock();
	}
	
	
	private void setTimeLine() {
		digitalTime = new Timeline(new KeyFrame(Duration.minutes(0), new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				Date currentTime = new Date();
				long seconds = (startTime.getTime() - currentTime.getTime()) / 1000;
				long millis = 0;				
				
				millis = seconds * 1000;
				
				String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
						TimeUnit.MILLISECONDS.toMinutes(millis)
								- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
						TimeUnit.MILLISECONDS.toSeconds(millis)
								- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
				
//				String totoalHMS = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(totalTimeInMills),
//						TimeUnit.MILLISECONDS.toMinutes(totalTimeInMills)
//								- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(totalTimeInMills)),
//						TimeUnit.MILLISECONDS.toSeconds(totalTimeInMills)
//								- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTimeInMills)));
				
				if (seconds > 0) {
					setText("Exam Starts in - "+hms );
				}else {
					setText("");
					listner.timerFinished();
					digitalTime.stop();
				}
			}
		}), new KeyFrame(Duration.seconds(1)));

		digitalTime.setCycleCount(Animation.INDEFINITE);
	}

	public void startClock() {		
		digitalTime.play();
	}

	public void stopClock() {
		digitalTime.stop();
	}

}
