package com.gbs.grandmaster.ui.page;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.Constants;
import com.gbs.grandmaster.ui.ExamLogWorker;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.common.GrandMasterServiceException;
import com.gbs.grandmaster.ui.common.GrandmasterPage;
import com.gbs.grandmaster.ui.common.MainWindow;
import com.gbs.grandmaster.ui.model.ATTENDANCE_STATUS;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.EXAM_STATUS;
import com.gbs.grandmaster.ui.model.Exam;
import com.gbs.grandmaster.ui.model.ExamAttendance;
import com.gbs.grandmaster.ui.model.ExamLog;
import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.model.QuestionPaper;
import com.gbs.grandmaster.ui.page.action.ExamTimerListner;
import com.gbs.grandmaster.ui.page.controller.ExamController;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ExamPage extends GrandmasterPage implements ExamTimerListner {

	private Exam exam = null;

	private Map<Integer, ExamSectionBox> sectionBoxes = null;

	private int currentSectionIndex = 0;
	private QuestionPaper questionPaper;
	private ExamAttendance examAttendance;
	private ExamController examController;
	private ExamLogWorker logWorker;
	private ATTENDANCE_STATUS status;
	private boolean recovered = false;
	private boolean mockExam;
	private Map<Long, Answer> recoveredAnswers = null;
	private long student_id;
	private long examScheduleId;
	private Date examStartTime;
	/**
	 * For the scenario where the exam is not following sequnce
	 */
	private ExamTimer examClock;
	boolean followSequnce = true;
	public ExamPage(String name, Exam exam, QuestionPaper questionPaper, boolean mockExam) {
		super(name, false);
		this.exam = exam;
		this.mockExam = mockExam;
		this.questionPaper = questionPaper;
		sectionBoxes = new TreeMap<>();
		followSequnce = exam.getExamDefinition().isFollowSectionSequnce();
		if(!followSequnce) {
			examClock = new ExamTimer(exam.getExamDefinition().getDurationInMin(), true, this);
			examClock.getStyleClass().add("examClock");
		}
		for (ExamSection section : questionPaper.getSections()) {
			ExamSectionBox sectionBox = new ExamSectionBox(section,followSequnce);			
			sectionBoxes.put(section.getSequnce(), sectionBox);
		}
		UIManager.getInstance().setCurrentExamPage(this);
		createContentPane();
		logWorker = new ExamLogWorker(this);
	}

	@Override
	public void createContentPane() {
		ScrollPane sectionScroll = new ScrollPane();
		VBox sectionContainer = new VBox();
		sectionContainer.setAlignment(Pos.BOTTOM_CENTER);
		sectionContainer.setMaxWidth(290);
		sectionContainer.setPadding(new Insets(25, 0, 0, 10));
		sectionScroll.setMaxWidth(300);
		
		int i = 0;
		for (Iterator<Integer> it = sectionBoxes.keySet().iterator(); it.hasNext();) {
			if (i > 0) {
				sectionContainer.getChildren().add(createDivider());
			}
			int index = it.next();
			ExamSectionBox sectionBox = sectionBoxes.get(index);
			sectionContainer.getChildren().add(sectionBox);

			i++;
		}
		if (sectionBoxes.size() > 1) {
			sectionScroll.setContent(sectionContainer);
			setLeft(sectionScroll);
		}
	}

	private HBox createDivider() {
		HBox dividerContainer = new HBox();
		// dividerContainer.setStyle("-fx-border-width: 1 1 1 1; -fx-border-color:
		// white;");
		dividerContainer.setAlignment(Pos.TOP_LEFT);
		Label divider = new Label();
		divider.setPrefSize(2, 20);
		divider.setMinSize(2, 20);
		dividerContainer.setPadding(new Insets(0, 0, 0, 25));
		divider.setStyle("-fx-background-color:black");
		dividerContainer.getChildren().add(divider);
		return dividerContainer;
	}

	public void startExam(ExamAttendance examAttendance) {
		status = ATTENDANCE_STATUS.STARTED;
		this.examAttendance = examAttendance;
		examStartTime = new Date();
		if(!followSequnce) {			 
			examClock.startClock(examStartTime);
		}
		startNextSection();
		logWorker.start();
	}

	public void startNextSection() {
		if (currentSectionIndex > 0 && currentSectionIndex <= sectionBoxes.size()) {
			sectionBoxes.get(currentSectionIndex).finishExamSection();
		}
		currentSectionIndex = currentSectionIndex + 1;
		if (currentSectionIndex > 0 && currentSectionIndex <= sectionBoxes.size()) {
			sectionBoxes.get(currentSectionIndex).showExamSection();
		} else {
			status = ATTENDANCE_STATUS.FINISHED;
			logWorker.stop();
			finishExam(examAttendance);
		}
	}

	public void finishExam(ExamAttendance examAttendance) {
		this.examAttendance = examAttendance;
		if(null == examAttendance.getEndDateTime()) {
			try {
				this.examAttendance = ExamController.finishExam(examAttendance);
			   } catch (GrandMasterServiceException e) {
			}	
		}		
		MainWindow.getInstance().setCenter(new ExamFinishPage("EXAM FINISHED", this.examAttendance, mockExam, true));
		UIManager.getInstance().setCurrentExamPage(null);
	}

	public ExamAttendance getExamAttendance() {
		return examAttendance;
	}

	public QuestionPaper getQuestionPaper() {
		return questionPaper;
	}

	public int remainingSectionCount() {
		return sectionBoxes.size() - currentSectionIndex;
	}

	public ExamLog getExamLog() {
		ExamLog examLog = new ExamLog(examAttendance.getKey());
		StringBuffer statusBfr = new StringBuffer();
		for (Iterator<Integer> it = sectionBoxes.keySet().iterator(); it.hasNext();) {
			ExamSectionBox sectionBox = sectionBoxes.get(it.next());
			if (!CommonUtilitties.isNullorEmpty(statusBfr.toString())) {
				statusBfr.append(Constants.RECOD_DELIMITER);
			}
			statusBfr.append(sectionBox.getStatusString());
		}
		examLog.setStatus(status);
		if (status == ATTENDANCE_STATUS.STARTED) {
			ExamSectionBox sectionBox = sectionBoxes.get(currentSectionIndex);
			if(followSequnce) {
				examLog.setElapsedTimeInSec(sectionBox.getElapsedTime());
			}else {
				examLog.setElapsedTimeInSec(examClock.elapsedTimeinSecond());
			}
			
		}
		examLog.setSectionStatusStr(statusBfr.toString());
		return examLog;
	}

	public ATTENDANCE_STATUS getStatus() {
		return status;
	}

	public void setStatus(ATTENDANCE_STATUS status) {
		this.status = status;
	}

	public void logAnswer(Answer answer) {
		logWorker.logAnswer(answer);
	}

	public ExamLogWorker getLogWorker() {
		return logWorker;
	}

	public void recoverExam(ExamAttendance previousAttempt, List<Answer> answers, ExamLog previousLog) {
		recovered = true;
		recoveredAnswers = new TreeMap<>();
		this.examAttendance = previousAttempt;
		if (null != answers) {
			for (Answer answer : answers) {
				recoveredAnswers.put(answer.getKey().getQuestionId(), answer);
			}
		}
		if (null != previousLog) {

			String[] statusStrs = previousLog.getSectionStatusStr().split(Constants.RECOD_DELIMITER);
			Map<Long, String> sectionStatus = new TreeMap<>();
			for (String statusStr : statusStrs) {
				if (!CommonUtilitties.isNullorEmpty(statusStr)) {
					int delIndex = statusStr.indexOf("$");
					sectionStatus.put(Long.valueOf(statusStr.substring(0, delIndex)), statusStr);
				}
			}
			for (Iterator<Integer> it = sectionBoxes.keySet().iterator(); it.hasNext();) {
				int index = it.next();
				ExamSectionBox sectionBox = sectionBoxes.get(index);
				String statusStr = sectionStatus.get(sectionBox.getSection().getId());
				if (!CommonUtilitties.isNullorEmpty(statusStr)) {
					int delIndex = statusStr.indexOf("$");
					long sectionId = Long.valueOf(statusStr.substring(0, delIndex));
					String status = statusStr.substring(delIndex + 1);
					ATTENDANCE_STATUS attendanceStatus = null;
					for (ATTENDANCE_STATUS as : ATTENDANCE_STATUS.values()) {
						if (as.getCode().equalsIgnoreCase(status)) {
							attendanceStatus = as;
							break;
						}
					}
					if (attendanceStatus != ATTENDANCE_STATUS.SUBMITTED) {
						currentSectionIndex = index - 1;
						sectionBox.recoverSection(previousLog.getElapsedTimeInSec(), attendanceStatus);
					} else if (attendanceStatus == ATTENDANCE_STATUS.SUBMITTED) {
						sectionBox.recoverSection(sectionBox.getSection().getDurationInMin() * 60, attendanceStatus);
					}
				}
			}
		}

	}

	public boolean isRecovered() {
		return recovered;
	}

	public Answer getRecoveredAnswer(long questionId) {
		return recoveredAnswers.get(questionId);
	}

	public void refreshPage() {
		sectionBoxes.get(currentSectionIndex).refresh();
	}

	@Override
	public void timerFinished() {
		status = ATTENDANCE_STATUS.FINISHED;
		logWorker.stop();
		for (Iterator<Integer> it = sectionBoxes.keySet().iterator(); it.hasNext();) {
			ExamSectionBox sectionBox = sectionBoxes.get(it.next());
			sectionBox.finishExamSection();
		}
		finishExam(examAttendance);		
	}


	
	public void setAllSectionInactive() {
		for (Iterator<Integer> it = sectionBoxes.keySet().iterator(); it.hasNext();) {
			ExamSectionBox sectionBox = sectionBoxes.get(it.next());
			sectionBox.setAsInative();
		}
	}
	
	public int getExamDuration() {
		return exam.getExamDefinition().getDurationInMin();
	}

	public Date getExamStartTime() {
		return examStartTime;
	}
	
	

}
