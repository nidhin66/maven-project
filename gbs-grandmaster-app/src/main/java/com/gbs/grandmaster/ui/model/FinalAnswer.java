package com.gbs.grandmaster.ui.model;

import java.util.Date;





public class FinalAnswer {
	
	
	private AttendanceKey key;
	
	private String content;
	
	private Date updateTime;

	public AttendanceKey getKey() {
		return key;
	}

	public void setKey(AttendanceKey key) {
		this.key = key;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	

}
