package com.gbs.grandmaster.ui.model;


public class AttendanceKey {	 
	
	  private long studentId;
	 
	  private long examScheduleId;
	  
	  private int attempt;
	  
  public AttendanceKey(){
		  
	  }

	public AttendanceKey(long studentId,long examScheduleId,int attempt) {
		this.studentId = studentId;
		this.examScheduleId = examScheduleId;
		this.attempt = attempt;
	}

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}

	public long getExamScheduleId() {
		return examScheduleId;
	}

	public void setExamScheduleId(long examScheduleId) {
		this.examScheduleId = examScheduleId;
	}

	public int getAttempt() {
		return attempt;
	}

	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}
	
	 @Override
	  public String toString() {
	    return "AttendanceKey{"
	        + "studentId='"
	        + studentId
	        + '\''
	        + ", examScheduleId="
	        + examScheduleId
	        + ", attempt="
	        + attempt
	        + '}';
	  }
	

}
