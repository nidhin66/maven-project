package com.gbs.grandmaster.ui.common;

import com.gbs.grandmaster.ui.UIUtilities;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class PageIconButton extends VBox{
	
	String name = "";
	String pageId = "";
	
	
	public PageIconButton(String pageName,String pageId) {
		this.name = pageName;
		this.pageId = pageId;
		getStyleClass().add("pageIcon");
		
		this.setPrefWidth(getIconWidth());
		this.setPrefHeight(getIconHeight());

		Label pageLabel = new Label();
		pageLabel.getStyleClass().add("pageLabel");
		pageLabel.setText(pageName);
		HBox topBox = new HBox();
		topBox.setAlignment(Pos.TOP_LEFT);
		topBox.getChildren().add(pageLabel);
		topBox.setPadding(new Insets(10, 0, 0, 10));

		
		Region midRegion = new Region();
		VBox.setVgrow(midRegion, Priority.ALWAYS);
		
		Button pageBtn = new Button();
		pageBtn.setOnAction(new PageIconButtonHandler(pageId));
		pageBtn.getStyleClass().add("pageIconButton");		
		pageBtn.setPrefSize(40, 40);
		HBox bottomBox = new HBox();
		bottomBox.setAlignment(Pos.BASELINE_RIGHT);
		bottomBox.getChildren().add(pageBtn);
		bottomBox.setPadding(new Insets(0, 10, 10, 0));
		
		getChildren().addAll(topBox,midRegion,bottomBox);
		
	}
	
	private double getIconHeight() {
		return UIUtilities.getScreenHeight() * .20;
	}
	
	private double getIconWidth() {
		return UIUtilities.getScreenWidth() * .25;
	}
}
