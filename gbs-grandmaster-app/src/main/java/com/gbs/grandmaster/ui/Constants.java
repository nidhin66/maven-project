package com.gbs.grandmaster.ui;

public interface Constants {
	
	public static final String RECOD_DELIMITER = "#";

	/**
	 * Page Labels
	 */
	public static final String PAGE_PROFILE = "PAGE_PROFILE";
	public static final String PAGE_INSTRUCTION = "PAGE_INSTRUCTION";
	public static final String PAGE_CALENDAR = "PAGE_CALENDAR";
	public static final String PAGE_EXAM = "PAGE_EXAM";
	public static final String PAGE_NOTIFICATION = "PAGE_NOTIFICATION";
	public static final String PAGE_MOCKTEST = "PAGE_MOCKTEST";
	public static final String EXAM_RESULT = "EXAM_RESULT";
	

	/**
	 * Common Labels
	 */
	public static final String LBL_APPLICATION_EXIT = "LBL_APPLICATION_EXIT";
	public static final String LBL_DELETE_NOTIFICATION = "LBL_DELETE_NOTIFICATION";
	public static final String LBL_BACK_HOME = "LBL_BACK_HOME";
	public static final String LBL_QUESTION_LIST = "LBL_QUESTION_LIST";
	public static final String LBL_PREVIOUS = "LBL_PREVIOUS";
	public static final String LBL_NEXT = "LBL_NEXT";
	public static final String APP_VERSION = "APP_VERSION";
	public static final String IMAGE_BASE_URL = "IMAGE_BASE_URL";
	public static final String MULTIPLE_LOGIN_ALLOWED="MULTIPLE_LOGIN_ALLOWED";
	public static final String ACTIVE_USER_CHECK_TIME="ACTIVE_USER_CHECK_TIME";

	/**
	 * Common Messages
	 */
	public static final String MSG_APPLICATION_EXIT = "MSG_APPLICATION_EXIT";
	public static final String MSG_DELETE_NOTIFICATION = "MSG_DELETE_NOTIFICATION";

	/**
	 * Login Page Labels
	 */
	public static final String LBL_LOGIN_TITLE = "LBL_LOGIN_TITLE";
	public static final String LBL_USER_NAME = "LBL_USER_NAME";
	public static final String LBL_PASSWORD = "LBL_PASSWORD";
	public static final String LBL_SUBMIT = "LBL_SUBMIT";
	public static final String LBL_LOGIN_ERROR = "LBL_LOGIN_ERROR";

	/**
	 * Login Page Messages
	 */
	public static final String MSG_LOGIN_BLK_USER = "MSG_LOGIN_BLK_USER";
	public static final String MSG_LOGIN_BLK_PWD = "MSG_LOGIN_BLK_PWD";
	public static final String MSG_LOGIN_INVALID = "MSG_LOGIN_INVALID";
	

}
