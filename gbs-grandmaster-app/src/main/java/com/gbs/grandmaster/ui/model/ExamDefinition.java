package com.gbs.grandmaster.ui.model;

import java.util.List;
import java.util.Set;



public class ExamDefinition {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	
	private int version;
	
	private int durationInMin; 
	
//	private Course course;
	
	private String exam_name;
	
	
	private Set<ExamSectionConfig> examSections;
	
	private boolean followSectionSequnce;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getDurationInMin() {
		return durationInMin;
	}

	public void setDurationInMin(int durationInMin) {
		this.durationInMin = durationInMin;
	}

//	public Course getCourse() {
//		return course;
//	}
//
//	public void setCourse(Course course) {
//		this.course = course;
//	}

	

	public boolean isFollowSectionSequnce() {
		return followSectionSequnce;
	}

	public Set<ExamSectionConfig> getExamSections() {
		return examSections;
	}

	public void setExamSections(Set<ExamSectionConfig> examSections) {
		this.examSections = examSections;
	}

	public void setFollowSectionSequnce(boolean followSectionSequnce) {
		this.followSectionSequnce = followSectionSequnce;
	}

	public String getExam_name() {
		return exam_name;
	}

	public void setExam_name(String exam_name) {
		this.exam_name = exam_name;
	}
	

}
