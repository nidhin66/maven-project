package com.gbs.grandmaster.ui.page;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Scanner;

import javax.swing.plaf.basic.BasicComboBoxUI.ComboBoxLayoutManager;

import java.net.URL;

import com.gbs.grandmaster.ui.AppUtils;
import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.Constants;
import com.gbs.grandmaster.ui.LabelAndMessageUtil;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.common.GrandmasterPage;
import com.gbs.grandmaster.ui.common.MainWindow;
import com.gbs.grandmaster.ui.common.WorkIndicatorDialog;
import com.gbs.grandmaster.ui.model.AnswerSheet;
import com.gbs.grandmaster.ui.model.ApplicationConfig;
import com.gbs.grandmaster.ui.model.Student;
import com.gbs.grandmaster.ui.model.StudentCourse;
import com.gbs.grandmaster.ui.oauth.OAuthUtils;
import com.gbs.grandmaster.ui.page.controller.ExamController;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class ProfilePage extends GrandmasterPage {

	private ImageView profileimage;
	private VBox vbox;
	private WorkIndicatorDialog<String> wd = null;

	public ProfilePage(String name) {
		super(name);
		createContentPane();
	}

	@Override
	public void createContentPane() {

		Student student = UIManager.getInstance().getStudent();
		StudentCourse studentCourse = UIManager.getInstance().getStudentCourse();

		GridPane grid = new GridPane();
		grid.setHgap(25);
		grid.setVgap(15);

		Label name = new Label(student.getName().toUpperCase());
		name.setWrapText(true);
		name.getStyleClass().add("name");
		
		HBox nameHbox = new HBox();
		nameHbox.setAlignment(Pos.TOP_LEFT);
		nameHbox.getChildren().add(name);
		
		Label regNoLabel = new Label("REGISTRATION NO");
		regNoLabel.getStyleClass().add("dob");

		Label dob = new Label("DATE OF BIRTH");
		dob.getStyleClass().add("dob");

		Label email = new Label("EMAIL ID");
		email.getStyleClass().add("email");

		Label contact = new Label("CONTACT NUMBER");
		contact.getStyleClass().add("contact");

		Label address = new Label("ADDRESS");
		address.getStyleClass().add("address");

		Label courseDetails = new Label("COURSE DETAILS");
		courseDetails.getStyleClass().add("courseDetails");

		Label batch = new Label("BATCH NAME");
		batch.getStyleClass().add("batch");

		Label course = new Label("COURSE NAME");
		course.getStyleClass().add("course");

		Label centre = new Label("CENTRE NAME");
		centre.getStyleClass().add("centre");
		
		grid.add(regNoLabel, 0, 1);
		grid.add(dob, 0, 2);
		grid.add(email, 0, 3);
		grid.add(contact, 0, 4);
		grid.add(address, 0, 5);
		grid.add(courseDetails, 0, 7);
		grid.add(batch, 0, 8);
		grid.add(course, 0, 9);
		grid.add(centre, 0, 10);
		
		Text textRegNo = new Text(student.getRegsitrationno());
		textRegNo.getStyleClass().add("textDob");

		Text textDob = new Text(student.getDob());
		textDob.getStyleClass().add("textDob");

		Text textEmail = new Text(student.getEmail());
		textEmail.getStyleClass().add("textEmail");

		Text textContact = new Text(student.getContactNumber());
		textContact.getStyleClass().add("textContact");

		Text textAddress = new Text(student.getAddress().toUpperCase());
		textAddress.setWrappingWidth(200);
		textAddress.getStyleClass().add("textAddress");

		Text textBatch = new Text(studentCourse.getBatch_name().toUpperCase());
		textBatch.getStyleClass().add("textBatch");

		Text textCourse = new Text(studentCourse.getCourse_name().toUpperCase());
		textCourse.getStyleClass().add("textCourse");

		Text textCentre = new Text(studentCourse.getCenter_name().toUpperCase());
		textCentre.getStyleClass().add("textCentre");

		grid.add(textRegNo, 3, 1);
		grid.add(textDob, 3, 2);
		grid.add(textEmail, 3, 3);
		grid.add(textContact, 3, 4);
		grid.add(textAddress, 3, 5);
		grid.add(textBatch, 3, 8);
		grid.add(textCourse, 3, 9);
		grid.add(textCentre, 3, 10);

		profileimage = new ImageView();

		String appFolder = AppUtils.getUserFolder();
		int index = student.getStudentImage().lastIndexOf(".");
		String fileExt = student.getStudentImage().substring(index + 1);
		String outFilePath = appFolder + File.separator + student.getId() + "." + fileExt;
		try {
			FileInputStream inputstream = new FileInputStream(outFilePath);
			Image image = new Image(inputstream);
			profileimage = new ImageView(image);
		} catch (Exception ex) {

		}
		profileimage.setFitWidth(150);
		profileimage.setFitHeight(150);
		profileimage.setSmooth(true);
		profileimage.setPreserveRatio(true);
		vbox = new VBox();
		vbox.setAlignment(Pos.TOP_RIGHT);
		vbox.setPadding(new Insets(0,100, 0, 0));
		vbox.getChildren().add(profileimage);

		Region midRegion = new Region();

		wd = new WorkIndicatorDialog<String>(UIManager.getInstance().getPrimaryStage(), "Please wait...");
		wd.addTaskEndNotification(result -> {
			wd = null; // don't keep the object, cleanup
		});

		long student_id = UIManager.getInstance().getStudentCourse().getId();
		long batch_id = UIManager.getInstance().getStudentCourse().getBatch_id();

		Button viewAnswerButton = new Button("View Result");
		viewAnswerButton.setPrefSize(150, 20);
		viewAnswerButton.setPadding(new Insets(5, 0, 5, 0));
		viewAnswerButton.getStyleClass().add("view-result-btn");
		viewAnswerButton.setOnAction(e -> {
			wd.exec("123", inputParam -> {
				List<AnswerSheet> answerSheets = ExamController.getExamAnswerSheet(student_id, batch_id);
				Platform.runLater(() -> {
					AnswerExamListPage examPage = new AnswerExamListPage("EXAM RESULT", answerSheets, false);
					MainWindow.getInstance().setCenter(examPage);
				});
				return Integer.valueOf(1);
			});
		});

		Button viewMockResult = new Button("View Mock Result");
		viewMockResult.setPrefSize(150, 20);
		viewMockResult.setPadding(new Insets(5, 0, 5, 0));
		viewMockResult.getStyleClass().add("view-result-btn");
		viewMockResult.setOnAction(e -> {
			wd.exec("123", inputParam -> {
				List<AnswerSheet> answerSheets = ExamController.getMockExamAnswerSheet(student_id, batch_id);
				Platform.runLater(() -> {
					AnswerExamListPage examPage = new AnswerExamListPage("MOCK EXAM RESULT", answerSheets, true);
					MainWindow.getInstance().setCenter(examPage);
				});
				return Integer.valueOf(1);
			});
		});

		HBox buttonPanel = new HBox();
		buttonPanel.setAlignment(Pos.CENTER);
		buttonPanel.setSpacing(10);
		buttonPanel.getChildren().addAll(viewAnswerButton, viewMockResult);

		HBox hbox = new HBox();
		hbox.setAlignment(Pos.TOP_LEFT);
		HBox.setHgrow(midRegion, Priority.ALWAYS);
		hbox.getStyleClass().add("hbox");
		hbox.getChildren().addAll(grid, midRegion, vbox);
		BorderPane contentPane = new BorderPane();
		contentPane.setTop(nameHbox);
		BorderPane.setAlignment(nameHbox, Pos.TOP_LEFT);
		contentPane.setCenter(hbox);
		BorderPane.setAlignment(hbox, Pos.CENTER_LEFT);
		contentPane.setBottom(buttonPanel);
		BorderPane.setAlignment(buttonPanel, Pos.CENTER);
		setCenter(contentPane);

	}

}
