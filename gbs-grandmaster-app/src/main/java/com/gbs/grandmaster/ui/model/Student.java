package com.gbs.grandmaster.ui.model;


public class Student{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String name = "";
	private String gender = "";
	private String address = "";
	private String street = "";
	private String district = "";
	private String state = "";
	private String contactNumber = "";
	private String whatsupNumber = "";
	private String mobileNumber = "";
	private String email = "";
	private String dob = "";
	private String studentImage = "";
	private String regsitrationno = "";

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getWhatsupNumber() {
		return whatsupNumber;
	}
	public void setWhatsupNumber(String whatsupNumber) {
		this.whatsupNumber = whatsupNumber;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getStudentImage() {
		return studentImage;
	}
	public void setStudentImage(String studentImage) {
		this.studentImage = studentImage;
	}
	public String getRegsitrationno() {
		return regsitrationno;
	}
	public void setRegsitrationno(String regsitrationno) {
		this.regsitrationno = regsitrationno;
	}

	

	
	

}
