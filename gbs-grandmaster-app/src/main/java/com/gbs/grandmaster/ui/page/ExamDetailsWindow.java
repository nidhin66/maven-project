package com.gbs.grandmaster.ui.page;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.gbs.grandmaster.ui.AppUtils;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.common.GrandMasterAlert;
import com.gbs.grandmaster.ui.common.GrandMasterServiceException;
import com.gbs.grandmaster.ui.common.MainWindow;
import com.gbs.grandmaster.ui.common.WorkIndicatorDialog;
import com.gbs.grandmaster.ui.model.ATTENDANCE_STATUS;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.ExamAttendance;
import com.gbs.grandmaster.ui.model.ExamDefinition;
import com.gbs.grandmaster.ui.model.ExamInfoWrapper;
import com.gbs.grandmaster.ui.model.ExamLog;
import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.model.ExamSectionConfig;
import com.gbs.grandmaster.ui.model.QuestionPaper;
import com.gbs.grandmaster.ui.page.controller.AppController;
import com.gbs.grandmaster.ui.page.controller.ExamController;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ExamDetailsWindow extends Stage {

	private ExamInfoWrapper examInfo;

	private QuestionPaper questionPaper = null;
	private ExamAttendance examAttendance = null;

	private WorkIndicatorDialog wd = null;
	private boolean mockExam;
	

    static Logger logger = Logger.getLogger(ExamDetailsWindow.class);


	public ExamDetailsWindow(ExamInfoWrapper examInfo,QuestionPaper questionPaper, boolean mockExam) {
		this.examInfo = examInfo;
		this.mockExam = mockExam;
		this.questionPaper =questionPaper; 
		Scene scene = new Scene(createCotent());

		scene.getStylesheets().add("/styles/Styles.css");
		initStyle(StageStyle.UNDECORATED);
		setScene(scene);
		setResizable(false);

//		setWidth(getWidthValue());
//		setHeight(getHeightValue());
	}

	private Parent createCotent() {
		BorderPane contentPane = new BorderPane();
		contentPane.setStyle("-fx-background-color: #ffffff;");

		HBox topPane = new HBox();
		topPane.setSpacing(20);
		topPane.setPadding(new Insets(20,20,20,20));
		topPane.setPrefHeight(50);
		topPane.setAlignment(Pos.CENTER_LEFT);
		topPane.setStyle("-fx-background-color: #ededed;");
		Label nameLabel = new Label(examInfo.getExamSchedule().getName());
		nameLabel.getStyleClass().add("nameLabel");
		topPane.getChildren().add(nameLabel);

		Image image = new Image("/images/close.png");
		ImageView imageView = new ImageView(image);
		imageView.setPreserveRatio(true);
		imageView.setFitWidth(20);
		imageView.setFitHeight(20);
		Button closeBtn = new Button();
		closeBtn.setGraphic(imageView);
		closeBtn.getStyleClass().add("close-btn");
		closeBtn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				UIManager.removeStageBlurEffect();
				close();
			}
		});

		Region midRegion = new Region();
		HBox.setHgrow(midRegion, Priority.ALWAYS);
		topPane.getChildren().add(midRegion);
		topPane.getChildren().add(closeBtn);
		BorderPane.setAlignment(topPane, Pos.CENTER);
		contentPane.setTop(topPane);



		GridPane infoGird = new GridPane();
		infoGird.setAlignment(Pos.CENTER);
		infoGird.setPadding(new Insets(20, 20, 20, 20));
		infoGird.setHgap(20);
		infoGird.setVgap(20);
		
		Label durationLbl = new Label("DURATION OF EXAMINATION");
		durationLbl.setAlignment(Pos.CENTER_LEFT);
		durationLbl.getStyleClass().add("durationLbl");
		
		Text dirationDiv = new Text();
		dirationDiv.setText("-");
		dirationDiv.getStyleClass().add("durationLbl");

		Label duration = new Label(examInfo.getExamSchedule().getExamDefinition().getDurationInMin() + " Min");
		duration.getStyleClass().add("durationLbl");
		
		infoGird.add(durationLbl, 0, 0);
		infoGird.add(dirationDiv, 1, 0);
		infoGird.add(duration, 2, 0);
		

		Label sectionLabel = new Label("SECTIONS");
		sectionLabel.getStyleClass().add("sectionLabel");
		HBox sectionLblBox = new HBox();
		sectionLblBox.setPadding(new Insets(0,0,0,10));
		sectionLblBox.setStyle("-fx-background-color:#383e4f");
		sectionLblBox.setAlignment(Pos.CENTER_LEFT);
		sectionLblBox.getChildren().add(sectionLabel);
		infoGird.add(sectionLblBox, 0, 1,3,1);

		
		ExamDefinition examDefinition = examInfo.getExamSchedule().getExamDefinition();
		int i = 2;
		List<ExamSectionConfig> sectionConfigs = new ArrayList<ExamSectionConfig>();
		sectionConfigs.addAll(examDefinition.getExamSections());		
		Collections.sort(sectionConfigs);
		for (ExamSectionConfig sectionConfig : sectionConfigs) {

			Label sectionName = new Label(sectionConfig.getSectionName().toUpperCase());
			sectionName.getStyleClass().add("durationLbl");
			infoGird.add(sectionName, 0, i);

			Text div = new Text();
			div.setText("-");
			div.getStyleClass().add("durationLbl");
			infoGird.add(div, 1, i);

			Label sectionDuration = new Label(sectionConfig.getDurationInMin() + " Min");
			sectionDuration.getStyleClass().add("durationLbl");
			infoGird.add(sectionDuration, 2, i);

			i++;

		}

		BorderPane.setAlignment(infoGird, Pos.CENTER);
		contentPane.setCenter(infoGird);


		Button startButton = new Button("START EXAMINATION");
		startButton.getStyleClass().add("startButton");
		startButton.setAlignment(Pos.CENTER);
		BorderPane.setMargin(startButton, new Insets(0,0,20,0));
		BorderPane.setAlignment(startButton, Pos.CENTER);
		contentPane.setBottom(startButton);

		startButton.setOnAction(e -> {
			wd = new WorkIndicatorDialog(UIManager.getInstance().getPrimaryStage(), "Please wait...");
			wd.addTaskEndNotification(result -> {
				wd = null; // don't keep the object, cleanup
			});
			String examPageName = "FINAL EXAM";
			if (mockExam) {
				examPageName = "MOCK EXAM";
			}
			ExamPage examPage = new ExamPage(examPageName, examInfo.getExamSchedule(), questionPaper,mockExam);
			checkForExamRecovery(examPage);			
			wd.exec("123", inputParam -> {
				try {
					ExamAttendance previousAttempt = examInfo.getExamAttendance();
					if (null != previousAttempt && previousAttempt.getStatus() == ATTENDANCE_STATUS.FINISHED) {
						Platform.runLater(() -> {
							
								UIManager.removeStageBlurEffect();
								MainWindow.getInstance().setCenter(examPage);
								this.close();
								examPage.finishExam(previousAttempt);
							
						});
						
					}else {
						examAttendance = ExamController.startExam(UIManager.getInstance().getStudent().getId(),
								examInfo.getExamSchedule().getId());
						Platform.runLater(() -> {
							if (null != examAttendance) {
								UIManager.removeStageBlurEffect();
								MainWindow.getInstance().setCenter(examPage);
								this.close();
								examPage.startExam(examAttendance);
							} else {
								GrandMasterAlert alert = new GrandMasterAlert(AlertType.ERROR);
								alert.setTitle("Exam Start Failure");
								alert.setHeaderText("Exam Start Failure");
								alert.setContentText("Failed to start exam");
								alert.showAndWait();
							}
						});
					}					
				} catch (GrandMasterServiceException e1) {
					Platform.runLater(() -> {
						GrandMasterAlert alert = new GrandMasterAlert(AlertType.ERROR);
						alert.setTitle("Exam Start Failure");
						alert.setHeaderText("Exam Start Failure");
						alert.setContentText(e1.getMessage());
						alert.showAndWait();
					});
					
				}				
				return Integer.valueOf(1);
			});

		});
		return contentPane;
	}


	private void checkForExamRecovery(ExamPage examPage) {
		ExamAttendance previousAttempt = examInfo.getExamAttendance();
		List<Answer> localAnswers = null;
		List<Answer> submittedAnswers = null;
		ExamLog previousAttemptLog = null;
		if (null != previousAttempt) {
			if (previousAttempt.getStatus() != ATTENDANCE_STATUS.SUBMITTED) {
				previousAttemptLog = examInfo.getExamLog();
				localAnswers = AppUtils.retrieveAnswersFromLocal(previousAttempt);
				
				try {
					submittedAnswers = ExamController.getAnswers(UIManager.getInstance().getStudent().getId(),
							examInfo.getExamSchedule().getId(), previousAttempt.getKey().getAttempt());
				} catch (GrandMasterServiceException e) {
					logger.error("Exception" + "inside checkForExamRecovery method:" + e);
					
				}
				if(null != localAnswers && null != submittedAnswers) {
					if(localAnswers.size() >= submittedAnswers.size()) {
						submittedAnswers = localAnswers;
					}
				}else if(null != localAnswers && null == submittedAnswers) {
					submittedAnswers = localAnswers;
				}
				if(null != submittedAnswers) {
					examPage.recoverExam(previousAttempt, submittedAnswers, previousAttemptLog);
				}				
			}
		}
	}

}
