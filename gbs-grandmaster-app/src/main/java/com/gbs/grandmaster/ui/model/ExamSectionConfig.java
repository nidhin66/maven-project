package com.gbs.grandmaster.ui.model;

import java.util.Set;




public class ExamSectionConfig implements Comparable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private Long id;
	

	private int version;
		
	private String sectionName;
	
	private int durationInMin;
	
	private int sequnce;
	

	private Set<ExamSectionDetails> details ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public int getDurationInMin() {
		return durationInMin;
	}

	public void setDurationInMin(int durationInMin) {
		this.durationInMin = durationInMin;
	}

	public int getSequnce() {
		return sequnce;
	}

	public void setSequnce(int sequnce) {
		this.sequnce = sequnce;
	}


	
	public Set<ExamSectionDetails> getDetails() {
		return details;
	}

	public void setDetails(Set<ExamSectionDetails> details) {
		this.details = details;
	}

	public int getQuestionCount() {
		int count = 0;
		for(ExamSectionDetails detail : details) {
			count = count + detail.getNoOfQuestions();
		}
		return count;
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return (this.getSequnce() < ((ExamSectionConfig) o).getSequnce() ? -1 : (this.getSequnce() == ((ExamSectionConfig) o).getSequnce() ? 0 : 1));
	}

}
