package com.gbs.grandmaster.ui.page;

import javafx.scene.control.Label;

public class ListItemBand extends Label{
	
	private String[] colorCodes = new String[5];
	{
		colorCodes[0] = "#E53935";
		colorCodes[1] = "#66BB6A";
		colorCodes[2] = "#FFA000";
		colorCodes[3] = "#1976D2";
		colorCodes[4] = "#E0E0E0";
	}
	public ListItemBand(int index) {
		setPrefSize(10, 60);
		int mod = index%5;
		setStyle("-fx-background-color: "+colorCodes[mod]+";");
	}

}
