package com.gbs.grandmaster.ui.page.action;

import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.common.GrandMasterAlert;
import com.gbs.grandmaster.ui.common.GrandMasterServiceException;
import com.gbs.grandmaster.ui.common.MainWindow;
import com.gbs.grandmaster.ui.model.ATTENDANCE_STATUS;
import com.gbs.grandmaster.ui.model.EXAM_STATUS;
import com.gbs.grandmaster.ui.model.Exam;
import com.gbs.grandmaster.ui.model.ExamAttendance;
import com.gbs.grandmaster.ui.model.ExamInfoWrapper;
import com.gbs.grandmaster.ui.model.QuestionPaper;
import com.gbs.grandmaster.ui.page.ExamDetailsWindow;
import com.gbs.grandmaster.ui.page.ExamFinishPage;
import com.gbs.grandmaster.ui.page.controller.ExamController;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;

public class ExamItemButtonActionListner implements EventHandler<ActionEvent> {
	
	private ExamInfoWrapper examInfo = null;
	private boolean mockExam;
	private long student_id;
	private long examScheduleId;
	
	public ExamItemButtonActionListner(ExamInfoWrapper examInfo,boolean mockExam) {
		this.examInfo = examInfo;
		this.mockExam = mockExam;
	}

	@Override
	public void handle(ActionEvent event) {
		Exam exam = examInfo.getExamSchedule();
		ExamAttendance attendance = examInfo.getExamAttendance();
	
		try {	
		
		if(exam.getStatus() == EXAM_STATUS.FINISHED) {		
			if(null != attendance && attendance.getStatus() != ATTENDANCE_STATUS.SUBMITTED) {							
				ExamController.finishExam(attendance);				
				MainWindow.getInstance().setCenter(new ExamFinishPage("UPLOAD ANSWERSHEET",attendance,mockExam,true));	
				UIManager.getInstance().setCurrentExamPage(null);	
			}			
		}else if(null != attendance && attendance.getStatus() == ATTENDANCE_STATUS.FINISHED) {			
			ExamController.finishExam(attendance);
			MainWindow.getInstance().setCenter(new ExamFinishPage("UPLOAD ANSWERSHEET",attendance,mockExam,true));			
			UIManager.getInstance().setCurrentExamPage(null);	
		}else {
			QuestionPaper questionPaper = ExamController.getQuestionPaper(UIManager.getInstance().getStudent().getId(),
					examInfo.getExamSchedule().getId());
			if(null != questionPaper) {
				ExamDetailsWindow examDetailsWindow = new ExamDetailsWindow(examInfo,questionPaper,mockExam);
				examDetailsWindow.initOwner(UIManager.getInstance().getPrimaryStage());
				examDetailsWindow.initModality(Modality.APPLICATION_MODAL); 
				UIManager.setStageBlurEffect();
				examDetailsWindow.showAndWait();
			}else {
				GrandMasterAlert alert = new GrandMasterAlert(AlertType.ERROR);
				alert.setTitle("Question Paper Error");
				alert.setHeaderText("Question Paper Error");
				alert.setContentText("Question Paper not found for this exam.");
				alert.showAndWait();
			}
			
		}
		
		} catch (GrandMasterServiceException e) {
			GrandMasterAlert alert = new GrandMasterAlert(AlertType.ERROR);
			alert.setTitle("Exam Start Error");
			alert.setHeaderText("Exam Start Error");
			alert.setContentText(e.getMessage());
			alert.showAndWait();
		}
		
	}

}
