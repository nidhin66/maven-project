package com.gbs.grandmaster.ui.page;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.common.MainWindow;
import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.model.Question;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class QuestionListWindow extends Stage {

	private ExamSectionPane examSectionPane;
	private Map<Integer, QuestionNAvigationBtn> btnMap = null;

	private Label paneLabel;
	private Button closeButton;
	private VBox questionNavigationVBox;

	public QuestionListWindow(ExamSectionPane sectionPane) {
		initOwner(UIManager.getInstance().getPrimaryStage());
		initModality(Modality.APPLICATION_MODAL);
		initStyle(StageStyle.UNDECORATED);
		this.examSectionPane = sectionPane;
		btnMap = new TreeMap<>();
		Scene scene = new Scene(createCotent());
		scene.getStylesheets().add("/styles/Styles.css");
		setScene(scene);
		setResizable(false);

	}

	private Parent createCotent() {
		BorderPane contentPane = new BorderPane();

		ExamSection examSection = examSectionPane.getExamSection();
		List<Question> questions = examSection.getQuestions();
		int i = 0;
		for (Question question : questions) {
			QuestionPane questionPane = examSectionPane.getQuestionPane(question.getId());
			QuestionNAvigationBtn navBtn = new QuestionNAvigationBtn(i, questionPane);
			btnMap.put(i, navBtn);
			i++;
		}

		HBox topPane = new HBox();
		topPane.setPadding(new Insets(20, 10, 20, 10));
		topPane.setAlignment(Pos.CENTER);
		topPane.getStyleClass().add("topPane");
		topPane.setSpacing(10);

		Circle ansLgd = new Circle(0, 0, 10);
		// ansLgd.setStyle("-fx-fill:#87c431;");
		ansLgd.getStyleClass().add("ansLgd");
		topPane.getChildren().add(ansLgd);
		Label ansLbl = new Label("Answered");
		topPane.getChildren().add(ansLbl);

		Circle unansLgd = new Circle(0, 0, 10);
		// unansLgd.setStyle("-fx-fill:#ef5050;");
		unansLgd.getStyleClass().add("unansLgd");
		topPane.getChildren().add(unansLgd);
		Label unAnsLbl = new Label("Skipped");
		topPane.getChildren().add(unAnsLbl);

		Circle markedLgd = new Circle(0, 0, 10);
		// markedLgd.setStyle("-fx-fill:#e2bd23;");
		markedLgd.getStyleClass().add("markedLgd");
		topPane.getChildren().add(markedLgd);
		Label markedLbl = new Label("Marked");
		topPane.getChildren().add(markedLbl);

		Circle markedAndAnsweredLgd = new Circle(0, 0, 10);
		markedAndAnsweredLgd.getStyleClass().add("markedAndAnsweredLgd");
		topPane.getChildren().add(markedAndAnsweredLgd);
		Label markedAndAnsweredLbl = new Label("Marked And Answered");
		topPane.getChildren().add(markedAndAnsweredLbl);

		Circle notVisited = new Circle(0, 0, 10);
		// notVisited.setStyle("-fx-fill:#bdbdbd;");
		notVisited.getStyleClass().add("notVisited");
		topPane.getChildren().add(notVisited);
		Label notVisitedLbl = new Label("Not Visited");
		topPane.getChildren().add(notVisitedLbl);
		// -fx-background-color: #E0E0E0;
		// topPane.getChildren().add(examSectionPane.getExamClock());

		Region midRegion = new Region();
		HBox.setHgrow(midRegion, Priority.ALWAYS);

		topPane.getChildren().add(midRegion);

		Image image = new Image("/images/close.png");
		ImageView imageView = new ImageView(image);
		imageView.setPreserveRatio(true);
		imageView.setFitWidth(20);
		imageView.setFitHeight(20);
		closeButton = new Button();
		closeButton.setGraphic(imageView);
		closeButton.getStyleClass().add("close-btn");
		closeButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				UIManager.removeStageBlurEffect();
				close();
				UIManager.getInstance().getPrimaryStage().toFront();
			}
		});

		topPane.getChildren().add(closeButton);

		contentPane.setTop(topPane);

		int k = 0;
		questionNavigationVBox = new VBox();
		questionNavigationVBox.setSpacing(5);
		questionNavigationVBox.setAlignment(Pos.CENTER_LEFT);
		// questionNavigationVBox.setStyle("-fx-background-color: white;");
		questionNavigationVBox.getStyleClass().add("questionNavigationBox");
		while (k < btnMap.size()) {
			int j = 0;
			HBox questionNavigationHBox = new HBox();
			questionNavigationHBox.setPadding(new Insets(10, 10, 10, 10));
			questionNavigationHBox.setSpacing(15);
			questionNavigationHBox.setAlignment(Pos.CENTER_LEFT);
			// questionNavigationHBox.setStyle("-fx-background-color: white;");
			questionNavigationHBox.getStyleClass().add("questionNavigationBox");
			while (k < btnMap.size() && j < 10) {
				QuestionNAvigationBtn navigationBtn = btnMap.get(k);
				navigationBtn.setOnAction(e -> {
					UIManager.removeStageBlurEffect();
					close();
					examSectionPane.showQuestion(navigationBtn.getIndex());
					UIManager.getInstance().getPrimaryStage().toFront();
				});
				questionNavigationHBox.getChildren().add(navigationBtn);
				j++;
				k++;
			}
			questionNavigationVBox.getChildren().add(questionNavigationHBox);
		}
		contentPane.setCenter(questionNavigationVBox);

		return contentPane;
	}

}
