package com.gbs.grandmaster.ui;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;

import com.gbs.grandmaster.ui.common.GrandMasterServiceException;
import com.gbs.grandmaster.ui.model.ApplicationConfig;
import com.gbs.grandmaster.ui.model.EXAM_STATUS;
import com.gbs.grandmaster.ui.model.ExamInfoWrapper;
import com.gbs.grandmaster.ui.model.Student;
import com.gbs.grandmaster.ui.oauth.OAuthUtils;
import com.gbs.grandmaster.ui.page.controller.ExamController;

public class UserInfoSynchWorker implements Runnable {
	
	private long student_id;
	private long batch_id;
	
    static Logger logger = Logger.getLogger(UserInfoSynchWorker.class);

	
	public UserInfoSynchWorker(long student_id,long batch_id) {
		this.student_id = student_id;
		this.batch_id = batch_id;
	}

	@Override
	public void run() {
		
		logger.debug("Downloading Profile image");
		ApplicationConfig imageConfig=  UIManager.getInstance().getApplictionConfig(Constants.IMAGE_BASE_URL);
		if(null != imageConfig) {
			Student student = UIManager.getInstance().getStudent();
			String resourceURL = imageConfig.getValue() +  student.getStudentImage();
			String appFolder = AppUtils.getUserFolder();
			int index = student.getStudentImage().lastIndexOf(".");
			String fileExt= student.getStudentImage().substring(index + 1);
			String outFilePath = appFolder + File.separator + student.getId() + "." + fileExt;
			 try
			 {
				 OAuthUtils.downloadFile(resourceURL, outFilePath);			  
		     } 
			 catch (Exception ex)
			 {
				 logger.error("Failed to download profile image." ,ex);
		     }	
		}	
		logger.debug("Fetching exam information to synch question paper");
		List<ExamInfoWrapper> examInfos = null;;
		try {
			examInfos = ExamController.listStudentExams(student_id, batch_id);
		} catch (GrandMasterServiceException e) {
			logger.error("Failed to fetch exam information.",e);
		}
		if(null != examInfos) {
			logger.debug("Donwloading question papers");
			for(ExamInfoWrapper examInfo : examInfos) {				
				if(examInfo.getExamSchedule().getStatus() == EXAM_STATUS.ENABLED || examInfo.getExamSchedule().getStatus()==EXAM_STATUS.SCHEDULED ) {
					try {
						ExamController.getQuestionPaper(student_id, examInfo.getExamSchedule().getId());
					} catch (GrandMasterServiceException e) {
						logger.error("Failed to download question paper",e);
					}
				}				
			}
		}else {
			logger.debug("No examinations configured for the student with id :  " + student_id );
		}
	}

}
