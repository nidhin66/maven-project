package com.gbs.grandmaster.ui.page;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.gbs.grandmaster.ui.AppUtils;
import com.gbs.grandmaster.ui.Constants;
import com.gbs.grandmaster.ui.LabelAndMessageUtil;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.common.GrandMasterAlert;
import com.gbs.grandmaster.ui.common.GrandMasterServiceException;
import com.gbs.grandmaster.ui.common.GrandmasterPage;
import com.gbs.grandmaster.ui.model.Exam;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.ExamAttendance;
import com.gbs.grandmaster.ui.model.ExamSectionConfig;
import com.gbs.grandmaster.ui.page.action.ExamTimerListner;
import com.gbs.grandmaster.ui.page.controller.ExamController;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.geometry.Insets;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.stage.Stage;
import javafx.scene.Scene;

public class ExamFinishPage extends GrandmasterPage {

	private ExamSectionConfig sectionConfig;
	private Exam exam;
	private boolean mockExam;
	Date date;
	private ExamAttendance examAttendance;

	public ExamFinishPage(String name, ExamAttendance examAttendance, boolean mockExam, boolean enableBackHomeBtn) {
		super(name, enableBackHomeBtn);
		this.examAttendance = examAttendance;
		this.mockExam = mockExam;
		createContentPane();
	}

	@Override
	public void createContentPane() {

		ScrollPane sp = new ScrollPane();
		sp.setHbarPolicy(ScrollBarPolicy.NEVER);
		sp.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		sp.getStyleClass().add("scrollpane");

		VBox vBox = new VBox();
		vBox.getStyleClass().add("examListPane");
		vBox.setSpacing(10);

		String megString = "Thank you for attending final exam";
		if (mockExam) {
			megString = "Thank you for attending mock exam";
		}
		Label thanksLabel = new Label(megString);
		thanksLabel.getStyleClass().add("thanksLabel");
		vBox.getChildren().add(thanksLabel);

		//date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		Date completionTime = examAttendance.getEndDateTime();
		if(null == completionTime) {
			completionTime = new Date();
		}
		String strDate = formatter.format(completionTime);


		Label dateLabel = new Label("You have finished exam on " + strDate);
		dateLabel.getStyleClass().add("dateLabel");
		vBox.getChildren().add(dateLabel);

		Label wishLabel = new Label("We wish you a great future");
		wishLabel.getStyleClass().add("wishLabel");
		vBox.getChildren().add(wishLabel);
		Button uploadAnswer = new Button();
		uploadAnswer.setText("UPLOAD YOUR ANSWER SHEET");
		uploadAnswer.getStyleClass().add("button-normal");
		uploadAnswer.setOnAction(e -> {
			try {
				List<Answer> answers = AppUtils.retrieveAnswersFromLocal(examAttendance);
				if (null != answers && answers.size() > 0) {
					ExamController.saveAnswers(answers);

					ExamController.submitExam(examAttendance);

					AppUtils.clearExamLocalData(examAttendance);
					GrandMasterAlert alert = new GrandMasterAlert(AlertType.INFORMATION);
					alert.setTitle(LabelAndMessageUtil.getLabel("Upload AnswerSheet"));
					alert.setContentText("Answersheet Uploaded Successfully!!");
					alert.showAndWait();
					UIManager.getInstance().showHomePage();
				} else {
//				GrandMasterAlert alert = new GrandMasterAlert(AlertType.ERROR);
//				alert.setTitle(LabelAndMessageUtil.getLabel("Upload AnswerSheet"));
//				alert.setContentText("Answers not found to upload.");
//				alert.showAndWait();
					ExamController.submitExam(examAttendance);

					AppUtils.clearExamLocalData(examAttendance);
					GrandMasterAlert alert = new GrandMasterAlert(AlertType.INFORMATION);
					alert.setTitle(LabelAndMessageUtil.getLabel("Upload AnswerSheet"));
					alert.setContentText("Answersheet Uploaded Successfully!!");
					alert.showAndWait();
					UIManager.getInstance().showHomePage();
				}
			} catch (GrandMasterServiceException e1) {
				GrandMasterAlert alert = new GrandMasterAlert(AlertType.ERROR);
				alert.setTitle(LabelAndMessageUtil.getLabel("Error Upload AnswerSheet"));
				alert.setContentText("Failed to uplaod answersheet");
				alert.showAndWait();
			}
		});
		vBox.getChildren().add(uploadAnswer);

		sp.setContent(vBox);
		vBox.setPadding(new Insets(20, 20, 20, 20));
		vBox.setAlignment(Pos.CENTER);
		setCenter(vBox);

	}

}
