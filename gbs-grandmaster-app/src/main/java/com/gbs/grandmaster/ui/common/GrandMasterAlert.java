package com.gbs.grandmaster.ui.common;

import javafx.scene.control.Alert;
import javafx.scene.control.DialogPane;

public class GrandMasterAlert extends Alert{

	public GrandMasterAlert(AlertType alertType) {
		super(alertType);
		DialogPane  dialogPane = getDialogPane();
		dialogPane.getStylesheets().add(
		   getClass().getResource("/styles/Styles.css").toExternalForm());
		dialogPane.getStyleClass().add("gmDialog");
	}

}
