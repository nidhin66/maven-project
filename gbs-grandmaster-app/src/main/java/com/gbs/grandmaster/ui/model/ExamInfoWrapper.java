package com.gbs.grandmaster.ui.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExamInfoWrapper {
	
	private Exam examSchedule;
	private ExamAttendance examAttendance;
	private ExamLog examLog;
	
	public Exam getExamSchedule() {
		return examSchedule;
	}
	public void setExamSchedule(Exam examSchedule) {
		this.examSchedule = examSchedule;
	}
	public ExamAttendance getExamAttendance() {
		return examAttendance;
	}
	public void setExamAttendance(ExamAttendance examAttendance) {
		this.examAttendance = examAttendance;
	}
	public ExamLog getExamLog() {
		return examLog;
	}
	public void setExamLog(ExamLog examLog) {
		this.examLog = examLog;
	}
	
	

}
