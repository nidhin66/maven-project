
package com.gbs.grandmaster.ui;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.scene.control.Tooltip;

public class UIUtilities {

	private static Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

	public static void positionAndSizeWindow(Stage stage, double widthPer, double heightPer) {

		primaryScreenBounds = Screen.getPrimary().getVisualBounds();
		heightPer = heightPer / 100;
		widthPer = widthPer / 100;

		double height = primaryScreenBounds.getHeight() * heightPer;
		double width = primaryScreenBounds.getWidth() * widthPer;
		double xPos = (primaryScreenBounds.getWidth() - width) * .5;
		double yPos = (primaryScreenBounds.getHeight() - height) * .5;

		stage.setX(xPos);
		stage.setY(yPos);
		stage.setWidth(width);
		stage.setHeight(height);
	}

	public static double getScreenWidth() {
		return primaryScreenBounds.getWidth();
	}

	public static double getScreenHeight() {
		return primaryScreenBounds.getHeight();
	}

	public static Scene createScene(Parent node) {
		// StackPane layout = new StackPane();
		// layout.setStyle("-fx-background-color: transparent;");
		// layout.getChildren().setAll(
		// createGlassPane()
		// );
		// if(null != node){
		// layout.getChildren().add(node);
		// }
		Scene scene = new Scene(node, Color.TRANSPARENT);
		scene.getStylesheets().add("/styles/Styles.css");
		return scene;

	}

	private static Pane createGlassPane() {
		final Pane glassPane = new Pane();
		glassPane.setPrefSize(getScreenWidth(), getScreenHeight());
		glassPane.getStyleClass().add("modal-dialog-glass");
		return glassPane;
	}

	public static Tooltip createToolTip(String toolTip) {
		Tooltip tt = new Tooltip();
		tt.setText(toolTip);
		tt.getStyleClass().add("tooltip");
		return tt;

	}

}
