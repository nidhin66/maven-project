package com.gbs.grandmaster.ui.page;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.gbs.grandmaster.ui.model.Choice;
import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.model.Question;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class QuestionPage extends BorderPane{
	
	private Question question;
	private ExamSection examSection;
	private Map<Integer, Choice> choiceMap;
	private int startAscii = 64;
	private Set<Choice> choices;
	private int index;



	
	public QuestionPage(ExamSection examSection,Question question,int index) {
		this.question = question;
		this.examSection = examSection;
		this.index = index;
		choiceMap = new TreeMap<>();
		this.choices = question.getChoices();
		for (Choice choice : choices) {
			choiceMap.put(choice.getSequence(), choice);
		}
		this.choices = question.getChoices();
		createContentPane();
	}
	
	private void createContentPane() {
		
		HBox topPane = new HBox();
		topPane.setSpacing(10);
		topPane.setAlignment(Pos.CENTER_LEFT);
		topPane.getStyleClass().add("hbox");

		Label questionNumberTxt = new Label("Question Number");
		questionNumberTxt.getStyleClass().add("questionNumberTxt");
		Label questionNumberLabel = new Label();
		questionNumberLabel.setMinWidth(50);
		questionNumberLabel.setMinHeight(25);
		questionNumberLabel.setAlignment(Pos.CENTER);
		questionNumberLabel.getStyleClass().add("questionNumberLabel");
		questionNumberLabel.setText((index + 1) + " - " + examSection.getQuestions().size());
		topPane.getChildren().addAll(questionNumberTxt, questionNumberLabel);
		
		ScrollPane scrollPane = new ScrollPane();
		VBox contentPane = new VBox();
		contentPane.setAlignment(Pos.CENTER);
		contentPane.setStyle("-fx-background-color: white;");
		contentPane.setPadding(new Insets(5,5,10,20));
		contentPane.setSpacing(1);
		
        if(question.getParagraphId() != 0) {
        	byte[] paragraphContent = examSection.getParagraphContentById(question.getParagraphId()); 
        	WebViewFitContent paragraphWebView = new WebViewFitContent(new String(paragraphContent));        
        	contentPane.getChildren().add(paragraphWebView);
        }
        
        contentPane.getChildren().add(topPane);
    	WebView questionTextwView = new WebView();
		WebEngine questionTextwViewEngine = questionTextwView.getEngine();
		questionTextwViewEngine.loadContent(question.getDefaultQuestionText());
		contentPane.getChildren().add(new WebViewFitContent(new String(question.getDefaultQuestionText())));
		for (Iterator<Integer> it = choiceMap.keySet().iterator(); it.hasNext();) {
			int key = it.next();
			Choice choice = choiceMap.get(key);
			contentPane.getChildren().add(createChoiceBox(choice));
		}	
		scrollPane.setContent(contentPane);
		BorderPane.setAlignment(scrollPane, Pos.CENTER);
		setCenter(scrollPane);
	}
	
	private HBox createChoiceBox(Choice choice) {
		HBox choiceBox = new HBox();
		choiceBox.setSpacing(5);
//		choiceBox.setMaxHeight(100);
		choiceBox.setAlignment(Pos.CENTER_LEFT);
		String optionLblTxt = Character.toString((char) (startAscii + choice.getSequence()));
		Label optionLabel = new Label(optionLblTxt);
		optionLabel.setStyle("-fx-font: bold 10pt \"Bebas Neue Bold\"");
		choiceBox.getChildren().addAll(optionLabel, new WebViewFitContent(new String(choice.getDefaultChoiceText())));
		return choiceBox;

	}

}
