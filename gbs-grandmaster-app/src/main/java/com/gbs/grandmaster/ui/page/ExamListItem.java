package com.gbs.grandmaster.ui.page;

import java.util.List;

import com.gbs.grandmaster.ui.AppUtils;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.model.ATTENDANCE_STATUS;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.EXAM_STATUS;
import com.gbs.grandmaster.ui.model.Exam;
import com.gbs.grandmaster.ui.model.ExamAttendance;
import com.gbs.grandmaster.ui.model.ExamInfoWrapper;
import com.gbs.grandmaster.ui.model.ExamLog;
import com.gbs.grandmaster.ui.page.action.ExamItemButtonActionListner;
import com.gbs.grandmaster.ui.page.action.ExamTimerListner;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public class ExamListItem extends HBox implements ExamTimerListner {

	private ExamInfoWrapper examInfo;
	private boolean mockExam;
	private Button statusButton;
	private String currentStyleClass;

	public ExamListItem(ExamInfoWrapper examInfo, int index, boolean mockExam) {
		this.examInfo = examInfo;
		this.mockExam = mockExam;
		setSpacing(10);
		setAlignment(Pos.CENTER);
		getStyleClass().add("exam-list-item");

		getChildren().add(new ListItemBand(index));
		
		getChildren().add(createExamLabel());		
		

		Region midRegion = new Region();
		HBox.setHgrow(midRegion, Priority.ALWAYS);
		getChildren().add(midRegion);
		Exam exam = examInfo.getExamSchedule();
		if (exam.getStatus() == EXAM_STATUS.SCHEDULED) {
			getChildren().add(new ExamStartTimer(examInfo.getExamSchedule().getStartDateTime(),this));
		}
		statusButton = createExamButton();
		statusButton.setPrefSize(40, 45);
		statusButton.setOnAction(new ExamItemButtonActionListner(examInfo, mockExam));
		getChildren().add(statusButton);
		setPrefWidth(getItemWidth());
	}

	private Label createExamLabel() {
		Label examLabel = new Label();
		examLabel.setText(examInfo.getExamSchedule().getName());
		return examLabel;
	}

	private double getItemWidth() {
		return UIUtilities.getScreenWidth() * .85;
	}

	private Button createExamButton() {
		Button examButton = new Button();
		ExamAttendance attendance = examInfo.getExamAttendance();
		ExamLog examLog = examInfo.getExamLog();
		Exam exam = examInfo.getExamSchedule();
	
		if (exam.getStatus() == EXAM_STATUS.SCHEDULED) {
			currentStyleClass = "scheduled-exam-btn";
			examButton.getStyleClass().add(currentStyleClass);
			examButton.setDisable(true);
		} else if (exam.getStatus() == EXAM_STATUS.ENABLED) {
			if (null == attendance) {
				currentStyleClass = "start-exam-btn";
				examButton.getStyleClass().add(currentStyleClass);
				examButton.setTooltip(UIUtilities.createToolTip("Start Exam"));
			} else {
				if (exam.getNoOfAttempts() == attendance.getKey().getAttempt()) {
					if (attendance.getStatus() == ATTENDANCE_STATUS.SUBMITTED) {
						currentStyleClass = "exam-attended-btn";
						examButton.getStyleClass().add(currentStyleClass);
						examButton.setDisable(true);
					} else if (attendance.getStatus() == ATTENDANCE_STATUS.FINISHED) {
						currentStyleClass = "upload-pending-btn";
						examButton.getStyleClass().add(currentStyleClass);
						examButton.setTooltip(UIUtilities.createToolTip("Upload Answer"));
					} else {						
						currentStyleClass = "start-exam-btn";
						examButton.getStyleClass().add(currentStyleClass);
						examButton.setTooltip(UIUtilities.createToolTip("Start Exam"));
					}
				} else if (exam.getNoOfAttempts() == 0 || exam.getNoOfAttempts() > attendance.getKey().getAttempt()) {
					if (attendance.getStatus() == ATTENDANCE_STATUS.FINISHED) {
						currentStyleClass = "upload-pending-btn";
						examButton.getStyleClass().add(currentStyleClass);
						examButton.setTooltip(UIUtilities.createToolTip("Upload Answer"));
					} else {
						currentStyleClass = "start-exam-btn";
						examButton.getStyleClass().add(currentStyleClass);
						examButton.setTooltip(UIUtilities.createToolTip("Start Exam"));
					}
				}
			}
		} else if (exam.getStatus() == EXAM_STATUS.FINISHED) {
			if (null == attendance) {
				examButton.setDisable(true);
				currentStyleClass = "exam-not-attended-btn";
				examButton.getStyleClass().add(currentStyleClass);
			} else {
				if (attendance.getStatus() != ATTENDANCE_STATUS.SUBMITTED) {
					List<Answer> answers = AppUtils.retrieveAnswersFromLocal(attendance);
					if (null != answers && answers.size() > 0) {
						currentStyleClass = "upload-pending-btn";
						examButton.getStyleClass().add(currentStyleClass);
						examButton.setTooltip(UIUtilities.createToolTip("Upload Answer"));
					} else {
						examButton.setDisable(true);
						currentStyleClass = "exam-not-attended-btn";
						examButton.getStyleClass().add(currentStyleClass);						
					}
				} else {					
					currentStyleClass = "exam-attended-btn";
					examButton.getStyleClass().add(currentStyleClass);		
					examButton.setDisable(true);
		
				}
			}
		}
		// statusButton.getStyleClass().add("exam-attended-btn");
		return examButton;

	}

	@Override
	public void timerFinished() {
		statusButton.getStyleClass().removeAll(currentStyleClass);
		currentStyleClass = "start-exam-btn";
		statusButton.getStyleClass().add(currentStyleClass);	
		statusButton.setDisable(false);
		statusButton.setTooltip(UIUtilities.createToolTip("Start Exam"));
	}

}
