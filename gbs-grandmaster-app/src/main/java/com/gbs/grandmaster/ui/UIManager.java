package com.gbs.grandmaster.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.gbs.grandmaster.ui.common.GrandMasterAlert;
import com.gbs.grandmaster.ui.common.MainWindow;
import com.gbs.grandmaster.ui.model.ApplicationConfig;
import com.gbs.grandmaster.ui.model.Student;
import com.gbs.grandmaster.ui.model.StudentCourse;
import com.gbs.grandmaster.ui.page.AnswerSheetPage;
import com.gbs.grandmaster.ui.page.ExamPage;
import com.gbs.grandmaster.ui.page.HomePage;

import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.effect.BoxBlur;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

public class UIManager {
	
	private Stage primaryStage = null;
	
	private ExamPage currentExamPage;
	
	private AnswerSheetPage currentAnswerSheet;
	
	private String currentUserName;
	
	private String password;
	
	private Student student;
	
	private StudentCourse studentCourse;
	
	private UserStatusLogWorker statusLogWorker;
	
	private Map<String,ApplicationConfig> applicationConifgs = new HashMap<>();
	
	private static class UIManagerHelper {
		private static final UIManager INSTANCE = new UIManager();
	}
	public static UIManager getInstance() {
		return UIManagerHelper.INSTANCE;
	}
	
	private UIManager() {

	}
	
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	public void setWindow(Parent root) {
		Scene scene = new Scene(root);
		scene.getStylesheets().add("/styles/Styles.css");      
		this.primaryStage.setScene(scene);
	}
	
	public void setApplicationTitle(String title) {
		this.primaryStage.setTitle(title);
	}
	
	public void showHomePage() {
		MainWindow.getInstance().setCenter(new HomePage());
	}
	
	public static void exitApplication(boolean confirm) {
		if(confirm) {
			GrandMasterAlert alert = new GrandMasterAlert(AlertType.CONFIRMATION);
		    alert.setTitle(LabelAndMessageUtil.getLabel(Constants.LBL_APPLICATION_EXIT));
		    alert.setHeaderText(LabelAndMessageUtil.getLabel(Constants.LBL_APPLICATION_EXIT));
		    alert.setContentText(LabelAndMessageUtil.getMessage(Constants.MSG_APPLICATION_EXIT));
		    alert.getButtonTypes().setAll(ButtonType.YES,ButtonType.NO);
		    Optional<ButtonType> result = alert.showAndWait();
		    if (result.get() == ButtonType.YES){
		    	if(null != getInstance().getCurrentExamPage()) {
		    		getInstance().getCurrentExamPage().getLogWorker().stop();
		    	}
		    	if(null != getInstance().getStatusLogWorker()) {
		    		getInstance().getStatusLogWorker().stop();
		    	}
		    	Platform.exit();
		    }
		}else {
			if(null != getInstance().getCurrentExamPage()) {
	    		getInstance().getCurrentExamPage().getLogWorker().stop();
	    	}
			if(null != getInstance().getStatusLogWorker()) {
	    		getInstance().getStatusLogWorker().stop();
	    	}
	    	Platform.exit();
		}
		
		
	}

	public ExamPage getCurrentExamPage() {
		return currentExamPage;
	}

	public void setCurrentExamPage(ExamPage currentExamPage) {
		this.currentExamPage = currentExamPage;
	}

	public String getCurrentUserName() {
		return currentUserName;
	}

	public void setCurrentUserName(String currentUserName) {
		this.currentUserName = currentUserName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public StudentCourse getStudentCourse() {
		return studentCourse;
	}

	public void setStudentCourse(StudentCourse studentCourse) {
		this.studentCourse = studentCourse;
	}

	public AnswerSheetPage getCurrentAnswerSheet() {
		return currentAnswerSheet;
	}

	public void setCurrentAnswerSheet(AnswerSheetPage currentAnswerSheet) {
		this.currentAnswerSheet = currentAnswerSheet;
	}
	
	public static void setStageBlurEffect() {
		getInstance().getPrimaryStage().getScene().getRoot().setEffect(new BoxBlur());
	}
	
	public static void removeStageBlurEffect() {
		getInstance().getPrimaryStage().getScene().getRoot().setEffect(null);
	}



	public void setApplicationConifgs(List<ApplicationConfig> applicationConifgs) {
		if(null != applicationConifgs) {
			for(ApplicationConfig applicationConfig : applicationConifgs) {
				this.applicationConifgs.put(applicationConfig.getProperty(),applicationConfig);
			}
		}
		
	}
	
	public ApplicationConfig getApplictionConfig(String property) {
		return this.applicationConifgs.get(property);
	}

	public UserStatusLogWorker getStatusLogWorker() {
		return statusLogWorker;
	}

	public void setStatusLogWorker(UserStatusLogWorker logWorker) {
		this.statusLogWorker = logWorker;
	}

}
