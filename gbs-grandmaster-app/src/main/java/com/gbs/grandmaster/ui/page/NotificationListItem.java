package com.gbs.grandmaster.ui.page;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Optional;

import com.gbs.grandmaster.ui.AppUtils;
import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.Constants;
import com.gbs.grandmaster.ui.LabelAndMessageUtil;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.common.GrandMasterAlert;
import com.gbs.grandmaster.ui.model.Notification;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class NotificationListItem extends HBox {

	private Notification notification;
	private NotificationPage notificationPage;

	public NotificationListItem(NotificationPage notificationPage, Notification notification, int index) {
		this.notification = notification;
		this.notificationPage = notificationPage;
		setSpacing(20);
		setAlignment(Pos.CENTER);
		getStyleClass().add("notification-list-item");

		getChildren().add(new ListItemBand(index));
		getChildren().add(createNotificationLabel());
		Region midRegion = new Region();
		HBox.setHgrow(midRegion, Priority.ALWAYS);
		getChildren().add(midRegion);

		if (null != notification.getCreationDate()) {

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			String strDate = formatter.format(notification.getCreationDate());
			Label dateLabel = new Label("Date : " + strDate);
			dateLabel.getStyleClass().add("date_Label");
			getChildren().add(dateLabel);

		}

		Button viewButton = new Button("VIEW");
		viewButton.setPrefSize(85, 30);
		viewButton.getStyleClass().add("viewButton");
		setPrefWidth(getItemWidth());
		viewButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {

				WebView wView = new WebView();
				WebEngine engine = wView.getEngine();
				engine.loadContent(notification.getContentAsString());

				ScrollPane viewScroll = new ScrollPane();
				viewScroll.setContent(wView);
				viewScroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
				viewScroll.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
				viewScroll.setPrefWidth(getItemWidth());
				viewScroll.setPrefHeight(getItemHeight());

				Scene viewScene = new Scene(viewScroll);

				Stage newWindow = new Stage();
				newWindow.setTitle(notification.getHeading());
				newWindow.setScene(viewScene);
				newWindow.initModality(Modality.WINDOW_MODAL);
				newWindow.resizableProperty().setValue(Boolean.FALSE);
				newWindow.initOwner(UIManager.getInstance().getPrimaryStage());
				newWindow.show();
			}

			private double getItemHeight() {

				return UIUtilities.getScreenHeight() * .75;
			}

			private double getItemWidth() {
				return UIUtilities.getScreenWidth() * .85;
			}

		});

		Button deleteButton = new Button("DELETE");
		deleteButton.setPrefSize(85, 30);
		deleteButton.getStyleClass().add("deleteButton");
		deleteButton.setOnAction(e -> {

			GrandMasterAlert alert = new GrandMasterAlert(AlertType.CONFIRMATION);
			alert.setTitle(LabelAndMessageUtil.getLabel(Constants.LBL_DELETE_NOTIFICATION));
			alert.setContentText(LabelAndMessageUtil.getMessage(Constants.MSG_DELETE_NOTIFICATION));
			alert.getButtonTypes().setAll(ButtonType.OK, ButtonType.CANCEL);
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				deleteNotification();
				notificationPage.reloadNotifications();
			}
		});
		setPrefWidth(getItemWidth());

		HBox hbox = new HBox();
		hbox.getChildren().addAll(viewButton, deleteButton);
		hbox.setSpacing(10);
		hbox.setAlignment(Pos.CENTER);
		hbox.setPadding(new Insets(0,10,0,10));
		getChildren().add(hbox);

	}

	private double getItemWidth() {

		return UIUtilities.getScreenWidth() * .85;
	}

	private Label createNotificationLabel() {
		Label notificationLabel = new Label();
		
		notificationLabel.setText(notification.getHeading());
		notificationLabel.setWrapText(true);
		notificationLabel.setMaxWidth(500);
		notificationLabel.getStyleClass().add("notificationLabel");
		return notificationLabel;
	}

	private void deleteNotification() {
		String appFolder = AppUtils.getUserFolder();
		String notificationFilePath = appFolder + File.separator + "Notifications";
		String notificationStr = CommonUtilitties.readFile(notificationFilePath);
		if (CommonUtilitties.isNullorEmpty(notificationStr)) {
			notificationStr = String.valueOf(notification.getId());
		} else {
			notificationStr = notificationStr.trim() + Constants.RECOD_DELIMITER + String.valueOf(notification.getId());
		}
		CommonUtilitties.writeFile(notificationFilePath, notificationStr);
	}

}
