package com.gbs.grandmaster.ui.model;



public class AnswerSheet {
	
	private Exam exam;
	private ExamAttendance attendance;
	public Exam getExam() {
		return exam;
	}
	public void setExam(Exam exam) {
		this.exam = exam;
	}
	public ExamAttendance getAttendance() {
		return attendance;
	}
	public void setAttendance(ExamAttendance attendance) {
		this.attendance = attendance;
	}
	
	

}
