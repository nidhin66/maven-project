package com.gbs.grandmaster.ui;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;

import javafx.application.Platform;

public class LabelAndMessageUtil {
	
//	final static Logger logger = Logger.getLogger(LabelAndMessageUtil.class);


	private static class LabelAndMessageUtilHelper {
		private static final LabelAndMessageUtil INSTANCE = new LabelAndMessageUtil();
		private static Properties msgProperties = new Properties();	   
		private static Properties lblProperties = new Properties();	   
		static {try {
			InputStream msgStream = ClassLoader.getSystemResourceAsStream("config/messages.properties");
			msgProperties.load(msgStream);
//			logger.debug("Messages Loaded..");
			InputStream lblStream = ClassLoader.getSystemResourceAsStream("config/labels.properties");
			lblProperties.load(lblStream);
//			logger.debug("Labels Loaded..");
		} catch (Exception e) {
//			logger.error("Failed to load labels and messages",e);
			Platform.exit();
		}}  

	}
	
	private LabelAndMessageUtil() {
		
	}

	public static LabelAndMessageUtil getInstance() {
		return LabelAndMessageUtilHelper.INSTANCE;
	}
	
	public static String getLabel(String labelId) {
		String label = LabelAndMessageUtilHelper.lblProperties.getProperty(labelId);
		if(!CommonUtilitties.isNullorEmpty(label)) {
			return label;
		}else {
			return "";
		}
	}
	
	public static String getMessage(String msgId) {
		String label = LabelAndMessageUtilHelper.msgProperties.getProperty(msgId);
		if(!CommonUtilitties.isNullorEmpty(label)) {
			return label;
		}else {
			return "";
		}
	}

}
