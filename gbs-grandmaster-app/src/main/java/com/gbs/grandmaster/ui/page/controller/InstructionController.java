package com.gbs.grandmaster.ui.page.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;

import com.gbs.grandmaster.ui.common.GrandMasterServiceException;
import com.gbs.grandmaster.ui.model.Instruction;
import com.gbs.grandmaster.ui.oauth.OAuthUtils;

public class InstructionController {

	private static String LIST_INSTRUCTION_URL = "/listInstruction";
	static Logger logger = Logger.getLogger(NotificationController.class);

	public static Instruction listInstructions() {
		Instruction instruction = null;
		Map<String, Object> response;
		try {
			response = OAuthUtils.getProtectedResource(LIST_INSTRUCTION_URL);
			JSONObject dataStr = (JSONObject) response.get("data");
			ObjectMapper mapper = new ObjectMapper();
			instruction = mapper.readValue(dataStr.toJSONString(), (Instruction.class));
		} catch (Exception e1) {
			logger.error("Exception" + "while getting list of instructions :" + e1);
		}		
		return instruction;
	}

}
