package com.gbs.grandmaster.ui.page;

import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.plaf.basic.BasicBorders.RadioButtonBorder;

import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.AnswerKey;
import com.gbs.grandmaster.ui.model.Choice;
import com.gbs.grandmaster.ui.model.ExamAttendance;
import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.model.Question;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class ChoicePane extends VBox {

	private Set<Choice> choices;
	private Question question;
	private ExamSection examSection;
	private int startAscii = 64;
	private Map<Integer, ButtonBase> optionBtnMap;
	private boolean isMultiSelect = false;
	private Map<Integer, Choice> choiceMap;
	private int answerCount = 0;
	private boolean showAnswer = false;
	private ToggleGroup group;
	private Choice choice;
	private Answer answer;

	public ChoicePane(ExamSection examSection,Question question, boolean showAnswer) {
		this.question = question;
		this.examSection = examSection;
		this.showAnswer = showAnswer;
		this.choices = question.getChoices();
		optionBtnMap = new TreeMap<>();
		choiceMap = new TreeMap<>();
		setSpacing(5);
		setPadding(new Insets(0, 0, 0, 20));
		getChildren().clear();
		setAlignment(Pos.CENTER_LEFT);

		for (Choice choice : choices) {
			if (choice.isCorrect()) {
				answerCount++;
			}
			choiceMap.put(choice.getSequence(), choice);
		}
		if (answerCount > 1) {
			isMultiSelect = true;
		}

		HBox optionBox = new HBox();
		optionBox.setSpacing(5);
		group = new ToggleGroup();

		for (Iterator<Integer> it = choiceMap.keySet().iterator(); it.hasNext();) {
			int key = it.next();
			Choice choice = choiceMap.get(key);
			getChildren().add(createChoiceBox(choice));
			if (isMultiSelect) {
				CheckBox option = new CheckBox();
				option.setId(String.valueOf(choice.getId()));
				option.setText(Character.toString((char) (startAscii + choice.getSequence())));
				option.setStyle("-fx-font: bold 10pt \"Bebas Neue Bold\"");
				optionBtnMap.put(choice.getSequence(), option);
				optionBox.getChildren().add(option);
			} else {
				RadioButton option = new RadioButton();
				option.setId(String.valueOf(choice.getId()));
				option.setText(Character.toString((char) (startAscii + choice.getSequence())));
				option.setStyle("-fx-font: bold 10pt \"Bebas Neue Bold\"");
				option.setToggleGroup(group);
				optionBtnMap.put(choice.getSequence(), option);
				optionBox.getChildren().add(option);
				if (!showAnswer) {
					if (UIManager.getInstance().getCurrentExamPage().isRecovered()) {
						Answer answer = UIManager.getInstance().getCurrentExamPage()
								.getRecoveredAnswer(question.getId());
						if (null != answer) {
							if (answer.getContent().contains("" + choice.getSequence())) {
								option.setSelected(true);
							}
						}
					}
				}

			}

		}
		group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			public void changed(ObservableValue<? extends Toggle> ob, Toggle o, Toggle n) {
				RadioButton rb = (RadioButton) group.getSelectedToggle();
				// if (rb != null) {
				ExamPage examPage = UIManager.getInstance().getCurrentExamPage();
				examPage.logAnswer(getAnswer());
				examPage.refreshPage();
				// }
			}
		});
		if (!showAnswer) {
			getChildren().add(optionBox);
		}

	}

	public ChoicePane(ExamSection examSection,Question question) {
		this(examSection,question, false);
	}

	private HBox createChoiceBox(Choice choice) {
		HBox choiceBox = new HBox();
		choiceBox.setSpacing(5);
		choiceBox.setMaxHeight(100);
		choiceBox.setAlignment(Pos.CENTER_LEFT);
		String optionLblTxt = Character.toString((char) (startAscii + choice.getSequence()));
		Label optionLabel = new Label(optionLblTxt);
		optionLabel.setStyle("-fx-font: bold 10pt \"Bebas Neue Bold\"");
		if (showAnswer && choice.isCorrect()) {
			optionLabel.setStyle("-fx-text-fill: green; -fx-font: bgm_admin	gggnhold 10pt \"Bebas Neue Bold\"");
		}
		WebView choicewView = new WebView();
		WebEngine choicewViewEngine = choicewView.getEngine();
		choicewViewEngine.loadContent(choice.getDefaultChoiceText());
		ImageView ansTypeImg = new ImageView("images/ans-not-attended.png");
		ansTypeImg.setFitWidth(25);
		ansTypeImg.setFitHeight(25);
		ansTypeImg.setPreserveRatio(true);
		if (showAnswer) {
			Answer answer = UIManager.getInstance().getCurrentAnswerSheet().getAnswer(question.getId());
			if (null != answer) {
				if (!CommonUtilitties.isNullorEmpty(answer.getContent())) {
					if (answer.getContent().contains("" + choice.getSequence()) && !choice.isCorrect()) {
						ansTypeImg = new ImageView("images/ans-not-correct.png");
						ansTypeImg.setFitWidth(25);
						ansTypeImg.setFitHeight(25);
						ansTypeImg.setPreserveRatio(true);
					}
				}
			}
		}
		if (showAnswer && choice.isCorrect()) {
			ansTypeImg = new ImageView("images/ans-correct.png");
			ansTypeImg.setFitWidth(25);
			ansTypeImg.setFitHeight(25);
			ansTypeImg.setPreserveRatio(true);
		}
		choicewView.setMinHeight(UIUtilities.getScreenHeight() * .05);
		if (choice.getDefaultChoiceText().length() < 250) {
			choicewView.setPrefHeight(UIUtilities.getScreenHeight() * .05);
		} else {
			choicewView.setPrefHeight(250);
		}

		// choicewView.setMaxHeight(UIUtilities.getScreenHeight() * .25);
		if (showAnswer) {
			choiceBox.getChildren().add(ansTypeImg);
		}
		choiceBox.getChildren().addAll(optionLabel, choicewView);

		return choiceBox;

	}

	public String getSelectedChoices() {
		StringBuffer sb = new StringBuffer();
		for (Iterator<Integer> it = optionBtnMap.keySet().iterator(); it.hasNext();) {
			int key = it.next();
			if (isMultiSelect) {
				CheckBox option = (CheckBox) optionBtnMap.get(key);
				if (option.isSelected()) {
					if (CommonUtilitties.isNullorEmpty(sb.toString())) {
						sb.append(key);
					} else {
						sb.append(",");
						sb.append(key);
					}
				}
			} else {
				RadioButton option = (RadioButton) optionBtnMap.get(key);
				if (option.isSelected()) {
					sb.append(key);
				}
			}

		}
		return sb.toString();
	}

	public Answer getAnswer() {
		Answer answer = new Answer();
		StringBuffer sb = new StringBuffer();
		int correctAnswerCount = 0;
		for (Iterator<Integer> it = optionBtnMap.keySet().iterator(); it.hasNext();) {
			int key = it.next();
			if (isMultiSelect) {
				CheckBox option = (CheckBox) optionBtnMap.get(key);
				if (option.isSelected()) {
					Choice choice = choiceMap.get(key);
					if (choice.isCorrect()) {
						correctAnswerCount++;
					}
					if (CommonUtilitties.isNullorEmpty(sb.toString())) {
						sb.append(key);
					} else {
						sb.append(",");
						sb.append(key);
					}
				}
			} else {
				RadioButton option = (RadioButton) optionBtnMap.get(key);
				if (option.isSelected()) {
					Choice choice = choiceMap.get(key);
					if (choice.isCorrect()) {
						correctAnswerCount++;
					}
					sb.append(key);
				}
			}

		}
		if (answerCount == correctAnswerCount) {
			answer.setCorrect(true);
		} else {
			answer.setCorrect(false);
		}
		answer.setContent(sb.toString());
		ExamPage examPage = UIManager.getInstance().getCurrentExamPage();
		ExamAttendance attendance = examPage.getExamAttendance();
		AnswerKey answerKey = new AnswerKey();
		answerKey.setQuestionId(question.getId());
		answerKey.setAttempt(attendance.getKey().getAttempt());
		answerKey.setExamScheduleId(attendance.getKey().getExamScheduleId());
		answerKey.setStudentId(attendance.getKey().getStudentId());
		answerKey.setQuestionPaperId(examPage.getQuestionPaper().getId());
		answer.setKey(answerKey);
		answer.setSectionId(examSection.getId());
		if (answer.isCorrect()) {
			answer.setMark(question.getMark());
		} else {
			if (!CommonUtilitties.isNullorEmpty(answer.getContent())) {
				answer.setNegativeMark(question.getNegativeMark());
			}
		}
		return answer;
	}

	public void clearAnswer() {
		if (null != optionBtnMap) {
			for (Iterator<Integer> it = optionBtnMap.keySet().iterator(); it.hasNext();) {
				int key = it.next();
				if (isMultiSelect) {
					CheckBox option = (CheckBox) optionBtnMap.get(key);
					if (option.isSelected()) {
						option.setSelected(false);
					}
				} else {
					RadioButton option = (RadioButton) optionBtnMap.get(key);
					if (option.isSelected()) {
						option.setSelected(false);
					}
				}

			}
		}
	}
}
