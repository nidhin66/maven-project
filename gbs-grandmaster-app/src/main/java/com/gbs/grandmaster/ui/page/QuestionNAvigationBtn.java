package com.gbs.grandmaster.ui.page;

import com.gbs.grandmaster.ui.model.QUESTION_STATUS;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;

public class QuestionNAvigationBtn extends Button {

	private QuestionPane questionPane = null;
	private int index;

	public QuestionNAvigationBtn(int index, QuestionPane questionPane) {
		this.setDisabled(true);
		this.index = index;
		this.questionPane = questionPane;

		this.setText(index + 1 + "");
		// this.getStyleClass().add("navigate_ans_btn");
		this.setStyle("-fx-font: 10 arial; " + "-fx-background-radius: 5em; " + "-fx-min-width: 33px; "
				+ "-fx-min-height: 33px; " + "-fx-max-width: 33px; "
				+ "-fx-max-height: 33px; -fx-background-color: #bdbdbd;");
		if (null != questionPane) {
			// this.setOnAction(new EventHandler<ActionEvent>() {
			// public void handle(ActionEvent event) {
			// if(questionPane.getStatus() != QUESTION_STATUS.NOTANSWERED){
			//// AppController.getInstance().getExamScreenContainer().setQuestion(Long.valueOf(questionLog.getSeq()).intValue());
			//// navigationDialog.close();
			// }
			// }
			// });
			if (questionPane.getStatus() == QUESTION_STATUS.MARKED) {

				this.setStyle("-fx-font: 10 arial; " + "-fx-background-radius: 5em; " + "-fx-min-width: 33px; "
						+ "-fx-min-height: 33px; " + "-fx-max-width: 33px; " + "-fx-max-height: 33px;"
						+ "-fx-background-color: #e2bd23;");
			} else {
				if (questionPane.getStatus() == QUESTION_STATUS.ANSWERED) {
					this.setDisable(false);
					this.setStyle("-fx-font: 10 arial; " + "-fx-background-radius: 5em; " + "-fx-min-width: 33px; "
							+ "-fx-min-height: 33px; " + "-fx-max-width: 33px; "
							+ "-fx-max-height: 33px; -fx-background-color: #87c431;");
					this.setTextFill(Color.WHITE);
				} else if (questionPane.getStatus() == QUESTION_STATUS.NOTANSWERED) {
					this.setDisable(false);
					this.setStyle("-fx-font: 10 arial; " + "-fx-background-radius: 5em; " + "-fx-min-width: 33px; "
							+ "-fx-min-height: 33px; " + "-fx-max-width: 33px; "
							+ "-fx-max-height: 33px; -fx-background-color: #ef5050;");

				} else if (questionPane.getStatus() == QUESTION_STATUS.MARKED_ANSWERED) {
					this.setStyle("-fx-font: 10 arial; " + "-fx-background-radius: 5em; " + "-fx-min-width: 33px; "
							+ "-fx-min-height: 33px; " + "-fx-max-width: 33px; " + "-fx-max-height: 33px;"
							+ "-fx-background-color: #1976D2;");
					this.setTextFill(Color.WHITE);
				}

			}

		}

	}

	public int getIndex() {
		return index;
	}

}
