package com.gbs.grandmaster.ui.page.controller;

import java.util.List;

import org.apache.log4j.Logger;

import com.gbs.grandmaster.ui.AppUtils;
import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.Constants;
import com.gbs.grandmaster.ui.LabelAndMessageUtil;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.UserInfoSynchWorker;
import com.gbs.grandmaster.ui.UserStatusLogWorker;
import com.gbs.grandmaster.ui.common.GrandMasterAlert;
import com.gbs.grandmaster.ui.common.GrandMasterServiceException;
import com.gbs.grandmaster.ui.common.MainWindow;
import com.gbs.grandmaster.ui.common.WorkIndicatorDialog;
import com.gbs.grandmaster.ui.model.ApplicationConfig;
import com.gbs.grandmaster.ui.model.Student;
import com.gbs.grandmaster.ui.model.StudentCourse;
import com.gbs.grandmaster.ui.oauth.OAuthUtils;
import com.gbs.grandmaster.ui.page.LoginWindow;

import javafx.application.Platform;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;

public class LoginController {

	private LoginWindow window;

	private WorkIndicatorDialog<String> wd = null;

	static Logger logger = Logger.getLogger(LoginController.class);

	public LoginController(LoginWindow loginWindow) {
		this.window = loginWindow;
		Button submitButton = window.getSubmitButton();
		submitButton.setOnAction(e -> {
			wd = new WorkIndicatorDialog<String>(UIManager.getInstance().getPrimaryStage(), "Please wait...");
			wd.addTaskEndNotification(result -> {
				wd = null; // don't keep the object, cleanup
			});
			wd.exec("123", inputParam -> {
				String userName = window.getUserName();
				String password = window.getPassword();

				if (CommonUtilitties.isNullorEmpty(userName)) {
					Platform.runLater(() -> {
						GrandMasterAlert alert = new GrandMasterAlert(AlertType.WARNING);
						alert.setTitle(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_ERROR));
						alert.setHeaderText(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_ERROR));
						alert.setContentText(LabelAndMessageUtil.getMessage(Constants.MSG_LOGIN_BLK_USER));
						alert.showAndWait();
					});
				} else if (CommonUtilitties.isNullorEmpty(password)) {
					Platform.runLater(() -> {
						GrandMasterAlert alert = new GrandMasterAlert(AlertType.WARNING);
						alert.setTitle(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_ERROR));
						alert.setHeaderText(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_ERROR));
						alert.setContentText(LabelAndMessageUtil.getMessage(Constants.MSG_LOGIN_BLK_PWD));
						alert.showAndWait();
					});
				} else {
					String accessToken = null;
					try {
						accessToken = OAuthUtils.getAccessToken(userName, password);
						if(!CommonUtilitties.isNullorEmpty(accessToken)) {
							
						UIManager.getInstance().setCurrentUserName(userName);
						UIManager.getInstance().setPassword(password);
						logger.debug("User successfully authenticated : " + userName);
						List<ApplicationConfig> applicationConifgs = AppController.getApplicationConfigs();
						UIManager.getInstance().setApplicationConifgs(applicationConifgs);
						logger.debug("Application configurations received");
						ApplicationConfig versionConfig = UIManager.getInstance()
								.getApplictionConfig(Constants.APP_VERSION);
						if (null != versionConfig
								&& AppUtils.getAppVersion().equalsIgnoreCase(versionConfig.getValue())) {
							
							ApplicationConfig appconfig = UIManager.getInstance()
									.getApplictionConfig(Constants.MULTIPLE_LOGIN_ALLOWED);
							boolean loginAllowed =  false;
							if(null != appconfig) {
								if("No".equalsIgnoreCase(appconfig.getValue())) {
									logger.debug("Multiple login not allowed");
									loginAllowed = StudentController.isUserActive(userName);
									if(loginAllowed) {
										logger.debug("User Login allowed");
										UserStatusLogWorker logWorker = new UserStatusLogWorker();
										ApplicationConfig timeCheckConfig = UIManager.getInstance()
												.getApplictionConfig(Constants.ACTIVE_USER_CHECK_TIME);
										int timeInterval = 1;
										if(null != timeCheckConfig) {
											timeInterval = Integer.valueOf(timeCheckConfig.getValue());
										}
										logWorker.start(timeInterval);
										UIManager.getInstance().setStatusLogWorker(logWorker);										
									}
								}else {
									loginAllowed = true;
								}
							}
							if(loginAllowed) {
								Student student = StudentController.findStudentByUserName(userName);
								if (null != student && null != student.getId() && student.getId() > 0) {
									StudentCourse studentCourse = StudentController.getStudentBatchDetails(student.getId());
									Platform.runLater(() -> {
										if (null != studentCourse && null != studentCourse.getId()) {
											UIManager.getInstance().setStudent(student);
											UIManager.getInstance().setStudentCourse(studentCourse);
											UserInfoSynchWorker infoSynchWorker = new UserInfoSynchWorker(student.getId(),
													studentCourse.getBatch_id());
											Thread infoSynchThread = new Thread(infoSynchWorker);
											infoSynchThread.start();
											UIManager.getInstance().setWindow(MainWindow.getInstance());
											UIUtilities.positionAndSizeWindow(UIManager.getInstance().getPrimaryStage(),
													100, 100);
											UIManager.getInstance().getPrimaryStage().setTitle("GRAND MASTER");
											UIManager.getInstance().showHomePage();
										} else {
											GrandMasterAlert alert = new GrandMasterAlert(AlertType.WARNING);
											alert.setTitle(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_ERROR));
											alert.setHeaderText(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_ERROR));
											alert.setContentText("Student course details not found");
											alert.showAndWait();
										}
									});
								} else {
									Platform.runLater(() -> {
										GrandMasterAlert alert = new GrandMasterAlert(AlertType.WARNING);
										alert.setTitle(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_ERROR));
										alert.setHeaderText(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_ERROR));
										alert.setContentText("Student details not found");
										alert.showAndWait();
									});
								}
							}else {
								Platform.runLater(() -> {
								GrandMasterAlert alert = new GrandMasterAlert(AlertType.ERROR);
								alert.setTitle("Mutiple Use Login");
								alert.setHeaderText("Mutiple Use Login");
								alert.setContentText("User is already login from another computer.Please try after some time");
								alert.showAndWait();});
							}
						} else {
							logger.debug("Wrong application version");
							Platform.runLater(() -> {
								GrandMasterAlert alert = new GrandMasterAlert(AlertType.ERROR);
								alert.setTitle("Application Version Error");
								alert.setHeaderText("Application Version Error");
								alert.setContentText("Latest version of app is available.Please install the same.");
								alert.showAndWait();
							});
						}
					}else {
						Platform.runLater(() -> {
							GrandMasterAlert alert = new GrandMasterAlert(AlertType.WARNING);
							alert.setTitle(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_ERROR));
							alert.setHeaderText(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_ERROR));
							alert.setContentText(LabelAndMessageUtil.getMessage(Constants.MSG_LOGIN_INVALID));
							alert.showAndWait();
						});
					}
					} catch (GrandMasterServiceException ge) {
						Platform.runLater(() -> {
							GrandMasterAlert alert = new GrandMasterAlert(AlertType.WARNING);
							alert.setTitle(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_ERROR));
							alert.setHeaderText(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_ERROR));
							alert.setContentText(ge.getMessage());
							alert.showAndWait();
						});
					}

				}
				return Integer.valueOf(1);
			});

		});
	}

}
