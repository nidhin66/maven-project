package com.gbs.grandmaster.ui.model;

import java.util.List;


public class ExamStatusLog {
	
	private List<Answer> answers;
	private List<QuestionAttemptTime> questionAttemptTime;
	
	
	private ExamLog examLog;

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public ExamLog getExamLog() {
		return examLog;
	}

	public void setExamLog(ExamLog examLog) {
		this.examLog = examLog;
	}

	public List<QuestionAttemptTime> getQuestionAttemptTime() {
		return questionAttemptTime;
	}

	public void setQuestionAttemptTime(List<QuestionAttemptTime> questionAttemptTime) {
		this.questionAttemptTime = questionAttemptTime;
	}
	
	

}
