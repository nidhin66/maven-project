package com.gbs.grandmaster.ui.page.action;

import com.gbs.grandmaster.ui.page.ExamSectionBox;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ExamSectionBoxListner implements EventHandler<ActionEvent> {

	private ExamSectionBox examSectionBox;
	
	public ExamSectionBoxListner(ExamSectionBox examSectionBox) {
		this.examSectionBox = examSectionBox;
	}
	
	@Override
	public void handle(ActionEvent event) {
		examSectionBox.showExamSection();
	}

}
