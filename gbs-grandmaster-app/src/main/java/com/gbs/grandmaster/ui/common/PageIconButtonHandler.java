package com.gbs.grandmaster.ui.common;

import java.util.List;

import com.gbs.grandmaster.ui.Constants;
import com.gbs.grandmaster.ui.LabelAndMessageUtil;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.model.ExamInfoWrapper;
import com.gbs.grandmaster.ui.page.InstructionPage;
import com.gbs.grandmaster.ui.page.NotificationPage;
import com.gbs.grandmaster.ui.page.ExamListPage;
import com.gbs.grandmaster.ui.page.ProfilePage;
import com.gbs.grandmaster.ui.page.controller.ExamController;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert.AlertType;

public class PageIconButtonHandler implements EventHandler<ActionEvent> {

	private String pageId = null;
	private WorkIndicatorDialog wd = null;

	public PageIconButtonHandler(String pageId) {
		this.pageId = pageId;
	}

	@Override
	public void handle(ActionEvent event) {
		wd = new WorkIndicatorDialog(UIManager.getInstance().getPrimaryStage(), "Loading...");
		wd.addTaskEndNotification(result -> {
			wd = null; // don't keep the object, cleanup
		});
		wd.exec("123", inputParam -> {
			long student_id = UIManager.getInstance().getStudentCourse().getId();
			long batch_id = UIManager.getInstance().getStudentCourse().getBatch_id();
			switch (pageId) {
			case Constants.PAGE_PROFILE:
				Platform.runLater(() -> {
					ProfilePage profilePage = new ProfilePage(LabelAndMessageUtil.getLabel(Constants.PAGE_PROFILE));

					MainWindow.getInstance().setCenter(profilePage);
				});
				break;
			case Constants.PAGE_INSTRUCTION:
				Platform.runLater(() -> {
					InstructionPage instructionPage = new InstructionPage(
							LabelAndMessageUtil.getLabel(Constants.PAGE_INSTRUCTION));

					MainWindow.getInstance().setCenter(instructionPage);
				});
				break;
			case Constants.PAGE_EXAM:
				List<ExamInfoWrapper> examDetails = null;
				try {
					examDetails = ExamController.listStudentExams(student_id, batch_id);
				} catch (GrandMasterServiceException e) {
					GrandMasterAlert alert = new GrandMasterAlert(AlertType.ERROR);
					alert.setTitle("Failed to fetch Exam details");
					alert.setHeaderText(e.getMessage());
					alert.setContentText(e.getMessage());
					alert.showAndWait();
				}
				if (null != examDetails) {
					ExamListPage examPage = new ExamListPage(LabelAndMessageUtil.getLabel(Constants.PAGE_EXAM),
							examDetails, false);
					Platform.runLater(() -> {
						MainWindow.getInstance().setCenter(examPage);
					});
				}
				break;
			case Constants.PAGE_MOCKTEST:

				List<ExamInfoWrapper> mockExamDetails = null;
				try {
					mockExamDetails = ExamController.listStudentMockExams(student_id, batch_id);
				} catch (GrandMasterServiceException e) {
					GrandMasterAlert alert = new GrandMasterAlert(AlertType.ERROR);
					alert.setTitle("Failed to fetch Mock Exam details");
					alert.setHeaderText(e.getMessage());
					alert.setContentText(e.getMessage());
					alert.showAndWait();
				}
				if (null != mockExamDetails) {
					ExamListPage mockExamPage = new ExamListPage(LabelAndMessageUtil.getLabel(Constants.PAGE_MOCKTEST),
							mockExamDetails, true);
					Platform.runLater(() -> {
						MainWindow.getInstance().setCenter(mockExamPage);
					});
				}

				break;
			case Constants.PAGE_NOTIFICATION:
				Platform.runLater(() -> {
					NotificationPage notificationPage = new NotificationPage(
							LabelAndMessageUtil.getLabel(Constants.PAGE_NOTIFICATION));

					MainWindow.getInstance().setCenter(notificationPage);
				});

				break;

			default:
				// Statements
			}

			return Integer.valueOf(1);
		});

	}

}
