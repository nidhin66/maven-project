package com.gbs.grandmaster.ui.model;

import java.util.ArrayList;
import java.util.List;



//@Entity
//@Table(name = "GM_QUESTION_PAPER")
public class QuestionPaper  {
	
	private static final long serialVersionUID = 1L;
//	@Id
//	@Column(nullable = false, length = 25)
//	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	

	private int version;
	
	private long examScheduleId;
	
	private String name;
	
//	@OneToMany(mappedBy="id", fetch=FetchType.EAGER)
	private List<ExamSection> sections;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public long getExamScheduleId() {
		return examScheduleId;
	}

	public void setExamScheduleId(long examScheduleId) {
		this.examScheduleId = examScheduleId;
	}

	public List<ExamSection> getSections() {
		return sections;
	}

	public void setSections(List<ExamSection> sections) {
		this.sections = sections;
	}
	
	public void addSection(ExamSection section) {
		if(null == sections) {
			sections = new ArrayList<>();
		}
		sections.add(section);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
    public int getMarks() {
    	double marks = 0.0;
    	for(ExamSection section : sections) {
    		marks = marks + section.getMarks();
    	}
		return Double.valueOf(marks).intValue();
    }
	
}
