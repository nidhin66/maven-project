package com.gbs.grandmaster.ui.model;



//@Entity
//@Table(name = "GM_QUESTION_TEXT")
public class QuestionText {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		
	private Long id;
	
	
	private int version;
	
	private Language language;
	
	
	private  byte[] content;
	
	private  byte[] solution;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public byte[] getSolution() {
		return solution;
	}

	public void setSolution(byte[] solution) {
		this.solution = solution;
	}

	
	
	
}
