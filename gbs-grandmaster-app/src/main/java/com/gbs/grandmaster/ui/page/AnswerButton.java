package com.gbs.grandmaster.ui.page;

import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.model.Question;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class AnswerButton extends Button{
	
	private Question question;
	private Answer answer;
	private ExamSection examSection;
	private int index;
	
	public AnswerButton(ExamSection examSection, Question question,Answer answer,int index) {
		this.answer = answer ;
		this.question = question;
		this.examSection = examSection;
		this.index = index;
		setText(String.valueOf(index+1));
		setOnAction(e -> showResult());		
		if(null != answer) {
			if(answer.isCorrect()) {
				getStyleClass().add("correct-ans-btn");
			}else if(!CommonUtilitties.isNullorEmpty(answer.getContent())) {
				getStyleClass().add("wrong-ans-btn");
			}else {
				getStyleClass().add("unattended-ans-btn");
			}
		}else {
			getStyleClass().add("unattended-ans-btn");
		}		
	}
	
	private void showResult() {
		
		String title = examSection.getSectionName() + " - Answer - " + String.valueOf(index+1);

		
		QuestionPane questionPane = new QuestionPane(examSection, question, 0,true);
		
		
		Stage dialog = new Stage(StageStyle.TRANSPARENT);
		dialog.setTitle(title);
		dialog.initStyle(StageStyle.UNDECORATED);
		
		BorderPane contentPane = new BorderPane();
		BorderPane.setAlignment(questionPane, Pos.CENTER);
		contentPane.setCenter(questionPane);	
		
		HBox topPane = new HBox();
		topPane.setAlignment(Pos.CENTER);
		topPane.setPadding(new Insets(10,20,10,20));
		topPane.setStyle("-fx-background-color: #E0E0E0;");
		Region midRegion = new Region();
		HBox.setHgrow(midRegion, Priority.ALWAYS);
		Label titileLabel = new Label(title);
		titileLabel.setStyle("-fx-font-size: 18px; -fx-font-style: normal;-fx-font-weight: bold;");
		
		Image image = new Image("/images/close.png");
		ImageView imageView = new ImageView(image);
		imageView.setPreserveRatio(true);
		imageView.setFitWidth(23);
		imageView.setFitHeight(23);
		Button closeButton = new Button();
		closeButton.setGraphic(imageView);
	    closeButton.getStyleClass().add("close-btn");
		closeButton.setOnAction(new EventHandler<ActionEvent>() {			 
            public void handle(ActionEvent event) {
            	UIManager.removeStageBlurEffect();
            	dialog.close();
            }
        });
		topPane.getChildren().addAll(titileLabel,midRegion,closeButton);		
		BorderPane.setAlignment(topPane, Pos.CENTER);
		contentPane.setTop(topPane);
		Scene scene = UIUtilities.createScene(contentPane); 
		
		dialog.setScene(scene);
		dialog.initOwner(UIManager.getInstance().getPrimaryStage());
		dialog.initModality(Modality.APPLICATION_MODAL); 
		UIManager.setStageBlurEffect();
		dialog.showAndWait();

	}
	


}
