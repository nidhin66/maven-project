package com.gbs.grandmaster.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.codehaus.jackson.map.ObjectMapper;

import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.ExamAttendance;
import com.gbs.grandmaster.ui.model.Student;

public class AppUtils {

	public static final String APP_FOLDER_NAME = ".GrandMaster";
	private static ObjectMapper mapper = new ObjectMapper();
	private static final String APP_VERSION = "1.0";
	private static String os = null;
	public static String getOsName()
	{
		os = System.getProperty("os.name");
		return os;
	}

	public static String getAppFolder() {
		String userFolder = System.getProperty("user.home");
		File appFodler = new File(userFolder + File.separator + APP_FOLDER_NAME);
		if (!appFodler.exists()) {
			appFodler.mkdir();	
			if(getOsName().startsWith("Windows"))
			{
				try {
					Process p = Runtime.getRuntime().exec("attrib +h " + appFodler.getPath());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return appFodler.getPath();
	}

	public static String getUserFolder() {
		String appFolder = getAppFolder();
		Student user = UIManager.getInstance().getStudent();
		File userFolder = new File(appFolder + File.separator + String.valueOf(user.getId()));
		if (!userFolder.exists()) {
			userFolder.mkdir();
		}
		return userFolder.getPath();
	}

	public static void clearExamLocalData(ExamAttendance previousAttempt) {
		String userFodler = AppUtils.getUserFolder();
		String answerFilePath = userFodler + File.separator + "ANS_" + previousAttempt.getKey().getExamScheduleId()
				+ "_" + previousAttempt.getKey().getAttempt();
		File answerFile = new File(answerFilePath);
		answerFile.delete();
		String logFilePath = userFodler + File.separator + "LOG_" + previousAttempt.getKey().getExamScheduleId() + "_"
				+ previousAttempt.getKey().getAttempt();
		File logFile = new File(logFilePath);
		logFile.delete();
	}

	public static List<Answer> retrieveAnswersFromLocal(ExamAttendance previousAttempt) {
		String userFodler = AppUtils.getUserFolder();
		String filePath = userFodler + File.separator + "ANS_" + previousAttempt.getKey().getExamScheduleId() + "_"
				+ previousAttempt.getKey().getAttempt();
		BufferedReader reader;
		Map<Long, Answer> answerMap = new TreeMap<>();
		try {
			reader = new BufferedReader(new FileReader(filePath));
			String line = reader.readLine();
			while (line != null) {
				Answer answer = mapper.readValue(line, Answer.class);
				if (null != answer) {
					answerMap.put(answer.getKey().getQuestionId(), answer);
				}
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {

		}
		List<Answer> answers = new ArrayList<Answer>(answerMap.values());
		return answers;
	}

	public static String getAppVersion() {
		return APP_VERSION;
	}

}
