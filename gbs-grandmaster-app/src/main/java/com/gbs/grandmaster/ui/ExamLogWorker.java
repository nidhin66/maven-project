package com.gbs.grandmaster.ui;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.ExamLog;
import com.gbs.grandmaster.ui.model.ExamStatusLog;
import com.gbs.grandmaster.ui.model.QuestionAttemptTime;
import com.gbs.grandmaster.ui.page.ExamPage;
import com.gbs.grandmaster.ui.page.controller.ExamController;

public class ExamLogWorker {
	ScheduledExecutorService executor = null;
	Runnable task = null;

	private ExamPage examPage;

	private BlockingQueue<Answer> answerQueue = null;
	private BlockingQueue<QuestionAttemptTime> questionAttemptTimeQueue = null;
	
	public ExamLogWorker(ExamPage examPage) {
		answerQueue = new LinkedBlockingQueue<>();
		questionAttemptTimeQueue = new LinkedBlockingQueue<>();
		this.examPage = examPage;
		executor = Executors.newScheduledThreadPool(1);
		task = () -> {
			saveLog();
		};
	}

	public void logAnswer(Answer answer) {
		try {
			answerQueue.put(answer);
		} catch (InterruptedException e) {
		}
	}
	public void logQuestionAttemptTime(QuestionAttemptTime questionAttemptTime)
	{
		try {
			questionAttemptTimeQueue.put(questionAttemptTime);
		} catch (InterruptedException e) {
			// TODO: handle exception
		}
	}

	private void saveLog() {
		ExamLog examLog = examPage.getExamLog();
		List<Answer> answers = new ArrayList<>();
		List<QuestionAttemptTime> questionAttemptTimes = new ArrayList<>();
		int answerCount = answerQueue.drainTo(answers);
		questionAttemptTimeQueue.drainTo(questionAttemptTimes);
		ExamStatusLog statusLog = new ExamStatusLog();
		statusLog.setAnswers(answers);
		statusLog.setQuestionAttemptTime(questionAttemptTimes);
		statusLog.setExamLog(examLog);
		ExamController.saveExamLog(statusLog);
		localPersisitExamLog(examLog);
		if (answerCount > 0) {
			localPersisitAnswers(examLog, answers);
		}	
	}
	public void start() {
		executor.scheduleWithFixedDelay(task, 5, 5, TimeUnit.SECONDS);
		executor.submit(task);
	}

	public void stop() {
		executor.shutdown();
		saveLog();
	}

	private void localPersisitExamLog(ExamLog examLog) {
		String userFodler = AppUtils.getUserFolder();
		String filePath = userFodler + File.separator + "LOG_" + examLog.getKey().getExamScheduleId() + "_"
				+ examLog.getKey().getAttempt();
		try {
			ObjectWriter ow = new ObjectMapper().writer().withType(ExamLog.class);

			String json = ow.writeValueAsString(examLog);
			CommonUtilitties.writeFile(filePath, json);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void localPersisitAnswers(ExamLog examLog, List<Answer> answers) {
		String userFodler = AppUtils.getUserFolder();
		String filePath = userFodler + File.separator + "ANS_" + examLog.getKey().getExamScheduleId() + "_"
				+ examLog.getKey().getAttempt();
		BufferedWriter out = null;
		ObjectWriter ow = new ObjectMapper().writer().withType(Answer.class);

		try {
			// Open given file in append mode.
			out = new BufferedWriter(new FileWriter(filePath, true));
			for (Answer answer : answers) {
				String jsonStr = ow.writeValueAsString(answer);
				out.write(jsonStr + "\n");
			}
			out.close();
		} catch (IOException e) {
			System.out.println("exception occoured" + e);
		} finally {
			if (null != out) {
				try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
