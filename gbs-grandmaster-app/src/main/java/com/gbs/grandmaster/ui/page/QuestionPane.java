package com.gbs.grandmaster.ui.page;

import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.AnswerKey;
import com.gbs.grandmaster.ui.model.ExamAttendance;
import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.model.QUESTION_STATUS;
import com.gbs.grandmaster.ui.model.Question;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class QuestionPane extends BorderPane{
	
	private Question question;
	private ExamSection examSection;
	private ChoicePane choicePane;
    private int index;
    private boolean marked = false;
    private boolean showAnswer = false;
    
    public QuestionPane(ExamSection examSection,Question question,int index,boolean showAnswer) {
    	this.question = question;
		this.examSection = examSection;
		this.index = index;
		this.showAnswer = showAnswer;
		createContentPane();
		setStyle("-fx-background-color: white;");
    }
	
	public QuestionPane(ExamSection examSection,Question question,int index) {
		this(examSection,question,index,false);
	}
	
	private void createContentPane() {

		
		VBox contentPane = new VBox();
		contentPane.setStyle("-fx-background-color: white;");
		contentPane.setPadding(new Insets(5,5,10,20));
		contentPane.setSpacing(1);
		WebView extraTextwView = new WebView();
		extraTextwView.setStyle("-fx-border-width: 0 0 2 0; -fx-border-color: black;");
//		extraTextwView.setMinHeight(UIUtilities.getScreenHeight() * .1);
//		extraTextwView.setPrefHeight(UIUtilities.getScreenHeight() * .1);
//		extraTextwView.setMaxHeight(UIUtilities.getScreenHeight() * .30);
		WebEngine extraTextwViewEngine = extraTextwView.getEngine();
        if(question.getParagraphId() != 0) {
        	byte[] paragraphContent = examSection.getParagraphContentById(question.getParagraphId()); 
        	extraTextwViewEngine.loadContent(new String(paragraphContent));
        	HBox extraParagraphBox = new HBox();
        	extraParagraphBox.setAlignment(Pos.CENTER);
        	extraParagraphBox.setPadding(new Insets(10,25,10,10));
        	extraParagraphBox.setStyle("-fx-background-color: #181E2B; ");
        	Region midRegion = new Region();
    		HBox.setHgrow(midRegion, Priority.ALWAYS);
    		String title = examSection.getSectionName() + " - Paragraph for question - " + (index+1); 		
            if(showAnswer) {
            	title = examSection.getSectionName() + " - Paragraph for question";
            }
        	Label extraParagraphLbl = new Label(title);        	
        	extraParagraphLbl.setStyle("-fx-text-fill: white; -fx-font-size: 16px;"
        			+ "-fx-font-style: normal;"
        			+ "	-fx-font-weight: bold;");
        	
        	Image image = new Image("/images/expand-info.png");
    		ImageView imageView = new ImageView(image);
    		imageView.setPreserveRatio(true);
    		imageView.setFitWidth(25);
    		imageView.setFitHeight(25);
        	
        	Button btn = new Button();
        	btn.setGraphic(imageView);
//        	btn.setOnAction(e->{showParagraph(extraTextwView);});
        	btn.setOnAction(e->{showQuestionInNewWindow();});
        	btn.setTooltip(UIUtilities.createToolTip("View Question"));
        	btn.getStyleClass().add("exapand-info-btn");
        	extraParagraphBox.getChildren().addAll(extraParagraphLbl,midRegion,btn);
        	contentPane.getChildren().add(extraParagraphBox);
//        	contentPane.getChildren().add(extraTextwView);
        }
		
		WebView questionTextwView = new WebView();
		WebEngine questionTextwViewEngine = questionTextwView.getEngine();
		questionTextwView.setMinHeight(UIUtilities.getScreenHeight() * .1);
		questionTextwView.setPrefHeight(UIUtilities.getScreenHeight() * .1);
		questionTextwView.setMaxHeight(UIUtilities.getScreenHeight() * .20);
		questionTextwViewEngine.loadContent(question.getDefaultQuestionText());
		
		
	
		contentPane.getChildren().add(questionTextwView);
		StringBuilder markStrBldr = new StringBuilder();
		markStrBldr.append("Mark : " + question.getMark());
		if(question.getNegativeMark() > 0) {
			markStrBldr.append(" Negative Mark : " + question.getNegativeMark());
		}
		Label markLabel = new Label(markStrBldr.toString());
		markLabel.setStyle("-fx-font-weight: bold;-fx-background-color: #E0E0E0;");
		contentPane.getChildren().add(markLabel);
		choicePane = new ChoicePane(examSection,question,showAnswer);
		contentPane.getChildren().addAll(choicePane);
		
		ScrollPane contentScrollPane = new ScrollPane();
		if(showAnswer) {
			TitledPane solutionPane = new TitledPane();
			solutionPane.getStyleClass().add("solution-pane");
			solutionPane.expandedProperty().addListener((obs, wasExpanded, isNowExpanded) -> {
			        if (isNowExpanded) {
			        	contentScrollPane.setVvalue(contentScrollPane.getVmax());   
			        }
			    });			
			solutionPane.setPadding(new Insets(10,20,20,10));
			solutionPane.setMaxHeight(350);
			solutionPane.setExpanded(false);
			solutionPane.setText("Solution");
			WebView soultionTextwView = new WebView();
			WebEngine solutionTextwViewEngine = soultionTextwView.getEngine();			
			solutionTextwViewEngine.loadContent(question.getDefaultSolutionText());
			solutionPane.setContent(soultionTextwView);
			contentPane.getChildren().addAll(solutionPane);
		}
		
		
		contentScrollPane.setStyle("-fx-background-color: transparent;");
		contentScrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		contentScrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		contentScrollPane.setContent(contentPane);
		
		setCenter(contentScrollPane);
		
	}
	
	public QUESTION_STATUS getStatus() {
		String ansStr = choicePane.getSelectedChoices();
		if(marked) {
			if(!CommonUtilitties.isNullorEmpty(ansStr)) {
				return QUESTION_STATUS.MARKED_ANSWERED;
			}else {
				return QUESTION_STATUS.MARKED;
			}
		
		}else {
			if(CommonUtilitties.isNullorEmpty(ansStr)) {
				return QUESTION_STATUS.NOTANSWERED;
			}else {
				return QUESTION_STATUS.ANSWERED;
			}
		}
		
	}

	public Question getQuestion() {
		return question;
	}

	public int getIndex() {
		return index;
	}

	public boolean isMarked() {
		return marked;
	}

	public void setMarked(boolean marked) {
		this.marked = marked;
		Answer answer = getAnswer();	
		answer.setMarked(marked);
		ExamPage examPage = UIManager.getInstance().getCurrentExamPage();
		examPage.logAnswer(answer);
	}
	
	public Answer getAnswer() {		
		Answer answer = choicePane.getAnswer();		
		answer.setMarked(marked);
		return answer;
	}
	
	
	private void showQuestionInNewWindow() {
		String title = examSection.getSectionName() + " - Paragraph for question - " + (index+1);
		Stage dialog = new Stage();
		dialog.setMaximized(true);

		dialog.setTitle(title);		
		Scene scene = new Scene(new QuestionPage(examSection, question,index));
		scene.getStylesheets().add("/styles/Styles.css"); 
		dialog.setScene(scene);
		dialog.showAndWait();
		
	}
	
	private void showParagraph(WebView extraTextwView) {
		
		String title = examSection.getSectionName() + " - Paragraph for question - " + (index+1);
		
        if(showAnswer) {
        	title = examSection.getSectionName() + " - Paragraph for question";
        }

		
		
		Stage dialog = new Stage();
		dialog.setTitle(title);
		
		BorderPane contentPane = new BorderPane();
		BorderPane.setAlignment(extraTextwView, Pos.TOP_LEFT);
		contentPane.setCenter(extraTextwView);	
		
		HBox topPane = new HBox();
		topPane.setPadding(new Insets(10,20,10,20));
		topPane.setStyle("-fx-background-color: #E0E0E0;");
		Region midRegion = new Region();
		HBox.setHgrow(midRegion, Priority.ALWAYS);
		
		Label titileLabel = new Label(title);
		titileLabel.setStyle("-fx-font-size: 18px; -fx-font-style: normal;-fx-font-weight: bold;");
		
		
		topPane.getChildren().addAll(titileLabel,midRegion);		
		
		
		contentPane.setTop(topPane);
		
		Scene scene = new Scene(contentPane);
		scene.getStylesheets().add("/styles/Styles.css"); 
		dialog.setScene(scene);
		dialog.showAndWait();

	}
	
public void	clearAnswer() {
		choicePane.clearAnswer();
	}
	
}
