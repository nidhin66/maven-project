package com.gbs.grandmaster.ui.page;

import java.util.ArrayList;
import java.util.List;

import com.gbs.grandmaster.ui.UIManager;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class GMImageSlider extends GridPane {

	List<String> imageList = new ArrayList<String>();
	Button lbutton, rButton;
	ImageView imageView;
	int j = 0;
	double orgCliskSceneX, orgReleaseSceneX;

	public GMImageSlider() {
		this.setMaxWidth(UIManager.getInstance().getPrimaryStage().getWidth()*.60);
		imageList.add("images/slider_1.jpg");
		imageList.add("images/slider_2.jpg");
		setAlignment(Pos.CENTER);
		lbutton = new Button("<");
		rButton = new Button(">");
		Image images[] = new Image[imageList.size()];
		for (int i = 0; i < imageList.size(); i++) {
			images[i] = new Image(imageList.get(i));
		}
		imageView = new ImageView(images[j]);
		imageView.setCursor(Cursor.CLOSED_HAND);
		imageView.setFitWidth(UIManager.getInstance().getPrimaryStage().getWidth()*.60);
		imageView.setOnMousePressed(circleOnMousePressedEventHandler);
		imageView.setPreserveRatio(true);
		imageView.setOnMouseReleased(e -> {
			orgReleaseSceneX = e.getSceneX();
			if (orgCliskSceneX > orgReleaseSceneX) {
				lbutton.fire();
			} else {
				rButton.fire();
			}
		});

		rButton.setOnAction(e -> {
			j = j + 1;
			if (j == imageList.size()) {
				j = 0;
			}
			imageView.setImage(images[j]);

		});
		lbutton.setOnAction(e -> {
			j = j - 1;
			if (j == 0 || j > imageList.size() + 1 || j == -1) {
				j = imageList.size() - 1;
			}
			imageView.setImage(images[j]);

		});

//	        imageView.setFitHeight(100);
//	        imageView.setFitWidth(300);

		HBox hBox = new HBox();
		hBox.setSpacing(0);
		hBox.setAlignment(Pos.TOP_CENTER);
		HBox.setHgrow(imageView, Priority.ALWAYS);
//		hBox.setPrefHeight(1000);
		// hBox.getChildren().addAll(lbutton, imageView, rButton);
		hBox.getChildren().addAll(imageView);

		add(hBox, 0,0);
	}

	EventHandler<MouseEvent> circleOnMousePressedEventHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent t) {
			orgCliskSceneX = t.getSceneX();
		}
	};
}
