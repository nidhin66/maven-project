package com.gbs.grandmaster.ui.page;

import java.util.List;

import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.common.GrandmasterPage;
import com.gbs.grandmaster.ui.model.AnswerSheet;
import com.gbs.grandmaster.ui.model.Exam;
import com.gbs.grandmaster.ui.model.ExamInfoWrapper;
import com.gbs.grandmaster.ui.page.controller.ExamController;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class AnswerExamListPage extends GrandmasterPage{
	
	private boolean mock = false;
	private List<AnswerSheet> answerSheets;

	public AnswerExamListPage(String name,List<AnswerSheet> answerSheets,boolean mock) {
		super(name);
		this.mock = mock;
		this.answerSheets = answerSheets;
		createContentPane();
	}

	@Override
	public void createContentPane() {
		
		
		
		ScrollPane sp = new ScrollPane();
		
		sp.setHbarPolicy(ScrollBarPolicy.NEVER);
		sp.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		sp.getStyleClass().add("scrollpane");
		
		VBox examListPane = new VBox();
		examListPane.setAlignment(Pos.CENTER);
		examListPane.getStyleClass().add("examListPane");
		examListPane.setSpacing(10);
		int i =0;
		if(null != answerSheets && answerSheets.size() > 0) {
			for(AnswerSheet answerSheet : answerSheets) {
				AnswerExamListItem item = new AnswerExamListItem(answerSheet,++i);
				examListPane.getChildren().add(item);
			}
			sp.setContent(examListPane);
			examListPane.setPadding(new Insets(20,20,20,20));
			BorderPane.setAlignment(sp, Pos.CENTER);
			setCenter(sp);
		}else {
			Label noExamLabel = new Label("No Results Published");
			noExamLabel.getStyleClass().add("noItemsLabel");
			BorderPane.setAlignment(noExamLabel, Pos.CENTER);
			setCenter(noExamLabel);
		}
		
		
		
		
	}

}
