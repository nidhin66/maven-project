package com.gbs.grandmaster.ui.page;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;

import javax.swing.text.Position;

import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.Constants;
import com.gbs.grandmaster.ui.ExamLogWorker;
import com.gbs.grandmaster.ui.LabelAndMessageUtil;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.common.GrandMasterAlert;
import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.model.Question;
import com.gbs.grandmaster.ui.model.QuestionAttemptKey;
import com.gbs.grandmaster.ui.model.QuestionAttemptTime;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.model.ATTENDANCE_STATUS;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.Choice;
import com.gbs.grandmaster.ui.model.ExamAttendance;
import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.model.Question;
import com.gbs.grandmaster.ui.page.action.ExamTimerListner;
import com.gbs.grandmaster.ui.page.controller.ExamController;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class ExamSectionPane extends BorderPane implements ExamTimerListner {

	private ExamSection examSection = null;
	private ExamTimer examClock;
	private Label questionNumberLabel;
	private int currentIndex = -1;
	private QuestionPane currentQuestionPane = null;;
	private Map<Long, QuestionPane> questionPaneMap = null;
	private QuestionListWindow questionListWindow;
	private CheckBox markQuestion;
	private Button nextBtn;
	private Button previousBtn;
	private Button finishExamBtn;
	private Button clearButton;
	private HBox buttonPanel;
	private ATTENDANCE_STATUS status;
	private boolean recovered = false;
	private ExamSectionBox sectionBox;
	private SectionQuestionPage examSectionQuestionPage = null;
	private Stage questionPaperWindow = null;
	private Choice choice;
	private Answer answer;
	private long questionStartTime;

	public ExamSectionPane(ExamSectionBox sectionBox, ExamSection examSection) {
		this.examSection = examSection;
		this.sectionBox = sectionBox;
		questionPaneMap = new HashMap<>();
		this.examSectionQuestionPage = new SectionQuestionPage(this);
		this.questionPaperWindow = createQuestionPaperWindow();
		setTopPane();
		setButtonPane();
	}

	private void setTopPane() {
		HBox topPane = new HBox();
		topPane.setSpacing(10);
		topPane.getStyleClass().add("hbox");

		Label sectionLbl = new Label(examSection.getSectionName().toUpperCase());
		sectionLbl.getStyleClass().add("sectionLbl");

		Label questionNumberTxt = new Label("Question Number");
		questionNumberTxt.getStyleClass().add("questionNumberTxt");

		Region midRegion = new Region();
		HBox.setHgrow(midRegion, Priority.ALWAYS);

		questionNumberLabel = new Label();
		questionNumberLabel.setMinWidth(50);
		questionNumberLabel.setMinHeight(25);
		questionNumberLabel.setAlignment(Pos.CENTER);
		questionNumberLabel.getStyleClass().add("questionNumberLabel");

		if (sectionBox.isFollowSequnce()) {
			examClock = new ExamTimer(examSection.getDurationInMin(), true, this);
		} else {
			examClock = new ExamTimer(UIManager.getInstance().getCurrentExamPage().getExamDuration(), true, null);
			examClock.startClock();
		}
		examClock.getStyleClass().add("examClock");
		Image image = new Image("/images/questions.png");
		ImageView imageView = new ImageView(image);
		imageView.setPreserveRatio(true);
		imageView.setFitWidth(20);
		imageView.setFitHeight(20);

		Button showQuestionBtn = new Button();
		showQuestionBtn.setGraphic(imageView);
		showQuestionBtn.setTooltip(UIUtilities.createToolTip("Show All Questions"));
		showQuestionBtn.getStyleClass().add("question-list-btn");
		showQuestionBtn.setOnAction(e -> showQuestionPaper());

		topPane.getChildren().addAll(questionNumberTxt, questionNumberLabel, examClock, showQuestionBtn, midRegion,
				sectionLbl);
		topPane.setPadding(new Insets(10, 10, 10, 10));
		setTop(topPane);
	}

	private Stage createQuestionPaperWindow() {
		String title = examSection.getSectionName() + " - Questions ";
		Stage dialog = new Stage();
		dialog.setMaximized(true);
		dialog.setTitle(title);
		Scene scene = new Scene(this.examSectionQuestionPage);
		scene.getStylesheets().add("/styles/Styles.css");
		dialog.setScene(scene);
		return dialog;
	}

	private void showQuestionPaper() {
		if (!questionPaperWindow.isShowing()) {
			questionPaperWindow.show();
			Question question = examSection.getQuestions().get(currentIndex);
			examSectionQuestionPage.setQuestion(question, currentIndex);
		}
		questionPaperWindow.toFront();
	}

	public void startSection() {		
		status = ATTENDANCE_STATUS.STARTED;
		if(sectionBox.isFollowSequnce()) {
			examClock.startClock();
		}else {
			examClock.startClock(UIManager.getInstance().getCurrentExamPage().getExamStartTime());
		}		
		showNextQuestion();
	}

	public void showNextQuestion() {
		if (currentIndex < examSection.getQuestions().size() - 1) {
			currentIndex = currentIndex + 1;
			showQuestion(currentIndex);
		}

	}

	public void showPreviousQuestion() {
		if (currentIndex > 0) {
			currentIndex = currentIndex - 1;
			showQuestion(currentIndex);
		}
	}

	public void showQuestionAttemptTime() {

		try {
			if (null != currentQuestionPane) {
				long currentTime = System.currentTimeMillis();
				long timeTaken = (currentTime - questionStartTime) / 1000;
				ExamPage examPage = UIManager.getInstance().getCurrentExamPage();
				ExamAttendance examAttendance = examPage.getExamAttendance();
				QuestionAttemptKey attemptKey = new QuestionAttemptKey();
				attemptKey.setStudentId(examAttendance.getKey().getStudentId());
				attemptKey.setExamScheduleId(examAttendance.getKey().getExamScheduleId());
				attemptKey.setAttempt(examAttendance.getKey().getAttempt());
				attemptKey.setQuestionPaperId(examPage.getQuestionPaper().getId());
				attemptKey.setQuestionId(currentQuestionPane.getQuestion().getId());
				QuestionAttemptTime attemptTime = new QuestionAttemptTime(attemptKey);
				attemptTime.setTimeTaken(timeTaken);
				examPage.getLogWorker().logQuestionAttemptTime(attemptTime);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		questionStartTime = System.currentTimeMillis();
	}

	public void showQuestion(int index) {

		showQuestionAttemptTime();
		currentIndex = index;
		int remainingSections = UIManager.getInstance().getCurrentExamPage().remainingSectionCount();
		if (index == 0) {
			previousBtn.setDisable(true);
		} else {
			previousBtn.setDisable(false);
		}
		
		if(sectionBox.isFollowSequnce()) {
			buttonPanel.getChildren().remove(clearButton);
			if (index == examSection.getQuestions().size() - 1) {
				nextBtn.setDisable(true);
				buttonPanel.getChildren().add(clearButton);
				if (remainingSections == 0) {
					if (buttonPanel.getChildren().contains(nextBtn)) {
						buttonPanel.getChildren().remove(nextBtn);
					}
					if (!buttonPanel.getChildren().contains(finishExamBtn)) {
						buttonPanel.getChildren().add(finishExamBtn);
					}
				}
			} else {
				nextBtn.setDisable(false);
				if (buttonPanel.getChildren().contains(finishExamBtn)) {
					buttonPanel.getChildren().remove(finishExamBtn);
				}
				if (!buttonPanel.getChildren().contains(nextBtn)) {
					buttonPanel.getChildren().add(nextBtn);
				}
				buttonPanel.getChildren().add(clearButton);
			}
		}else {
			if (index == examSection.getQuestions().size() - 1) {
				nextBtn.setDisable(true);
			}else {
				nextBtn.setDisable(false);
			}
		}
		
		
		Question question = examSection.getQuestions().get(index);
		QuestionPane questionPane = questionPaneMap.get(question.getId());
		if (null == questionPane) {
			questionPane = new QuestionPane(examSection, question, index);
			questionPaneMap.put(question.getId(), questionPane);
		}
		currentQuestionPane = questionPane;
		examSectionQuestionPage.setQuestion(question, index);

		if (questionPane.isMarked()) {
			markQuestion.setSelected(true);
		} else {
			markQuestion.setSelected(false);
		}
		setCenter(null);
		setCenter(questionPane);

		questionNumberLabel.setText((currentIndex + 1) + " - " + examSection.getQuestions().size());
		sectionBox.setAnswerCountLableTxt(getAnswerCount());
	}

	public void setButtonPane() {
		markQuestion = new CheckBox("Mark for Review");
		markQuestion.getStyleClass().add("markQuestion");
		markQuestion.selectedProperty().addListener(e -> {
			currentQuestionPane.setMarked(markQuestion.isSelected());
			refresh();
		});
		// markQuestion.selectedProperty().addListener(new ChangeListener<Boolean>() {
		// @Override
		// public void changed(ObservableValue<? extends Boolean> observable, Boolean
		// oldValue, Boolean newValue) {
		// if(null! ) {
		//
		// }
		// markQuestion.setSelected(!newValue);
		// }
		// });

		Button questionListBtn = new Button(LabelAndMessageUtil.getLabel(Constants.LBL_QUESTION_LIST));
		questionListBtn.setPrefSize(125, 25);
		questionListBtn.getStyleClass().add("questionListBtn");
		questionListBtn.setOnAction(e -> {
			questionListWindow = new QuestionListWindow(this);
			UIManager.setStageBlurEffect();
			questionListWindow.showAndWait();
		});

		previousBtn = new Button(LabelAndMessageUtil.getLabel(Constants.LBL_PREVIOUS));
		previousBtn.setOnAction(e -> {
			showPreviousQuestion();
		});
		previousBtn.getStyleClass().add("homeBtn");

		nextBtn = new Button(LabelAndMessageUtil.getLabel(Constants.LBL_NEXT));
		nextBtn.getStyleClass().add("homeBtn");
		nextBtn.setOnAction(e -> {
			showNextQuestion();
		});
		finishExamBtn = new Button("Finish Exam");
		finishExamBtn.getStyleClass().add("homeBtn");
		finishExamBtn.setOnAction(e -> {
			GrandMasterAlert alert = new GrandMasterAlert(AlertType.CONFIRMATION);
			alert.setTitle(LabelAndMessageUtil.getLabel("Exam Finish Confirmation"));
			alert.setContentText("Do you want to finish this exam ?");
			ButtonType buttonTypeYes = new ButtonType("Yes");
			ButtonType buttonTypeNo = new ButtonType("No", ButtonData.CANCEL_CLOSE);
			alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == buttonTypeYes) {
				if(sectionBox.isFollowSequnce()) {
					examClock.stopClock();
				}else {
					UIManager.getInstance().getCurrentExamPage().timerFinished();
				}
				
			}
		});

		clearButton = new Button("Clear Answer");
		clearButton.getStyleClass().add("homeBtn");
		clearButton.setOnAction(e -> {
			clearAnswerBtn();
		});
		buttonPanel = new HBox();
		buttonPanel.setSpacing(10);
		buttonPanel.getChildren().addAll(questionListBtn, markQuestion, previousBtn, nextBtn,
				clearButton,finishExamBtn);
		buttonPanel.setAlignment(Pos.CENTER_LEFT);
		buttonPanel.setPadding(new Insets(10, 0, 0, 10));
		setBottom(buttonPanel);
		BorderPane.setAlignment(buttonPanel, Pos.CENTER_LEFT);

	}

	public void clearAnswerBtn() {
		if (currentQuestionPane != null) {
			currentQuestionPane.clearAnswer();
		}

	}

	public void finishSection() {
		if (null != questionListWindow) {
			UIManager.removeStageBlurEffect();
			questionListWindow.close();
		}
		// setTop(null);
		// setCenter(null);
		// setBottom(null);
		// Button button = new Button("Submit Section");
		// button.setOnAction(e ->
		// UIManager.getInstance().getCurrentExamPage().startNextSection());
		// setCenter(button);
		setTop(null);
		setCenter(null);
		setBottom(null);
		questionPaperWindow.close();
		if (sectionBox.isFollowSequnce()) {
			UIManager.getInstance().getCurrentExamPage().startNextSection();
		}
	}

	@Override
	public void timerFinished() {
		finishSection();
	}

	public ExamSection getExamSection() {
		return examSection;
	}

	public QuestionPane getQuestionPane(long questionId) {
		return questionPaneMap.get(questionId);
	}

	public List<Answer> getAnswers() {
		List<Answer> answers = new ArrayList<>();
		for (Iterator<Long> it = questionPaneMap.keySet().iterator(); it.hasNext();) {
			long key = it.next();
			QuestionPane questionPane = questionPaneMap.get(key);
			Answer answer = questionPane.getAnswer();
			answers.add(answer);
		}
		return answers;
	}

	public int getAnswerCount() {
		int answerCount = 0;
		List<Answer> answers = getAnswers();
		for (Answer answer : answers) {
			if (!CommonUtilitties.isNullorEmpty(answer.getContent())) {
				answerCount++;
			}
		}
		return answerCount;
	}

	public ExamTimer getExamClock() {
		return examClock;
	}

	public void setStatus(ATTENDANCE_STATUS status) {
		this.status = status;
	}

	public String getStatusString() {

		return examSection.getId() + "$" + status;

	}

	public long getElapsedTime() {
		return examClock.elapsedTimeinSecond();
	}

	public void recoverSection(long elapsedTimeinSec, ATTENDANCE_STATUS status) {
		this.status = status;
		recovered = true;

		int i = 0;
		for (Question question : examSection.getQuestions()) {
			Answer answer = UIManager.getInstance().getCurrentExamPage().getRecoveredAnswer(question.getId());
			if (null != answer) {
				QuestionPane questionPane = new QuestionPane(examSection, question, i);
				questionPane.setMarked(answer.isMarked());
				questionPaneMap.put(question.getId(), questionPane);
			}
			i++;
		}
		examClock.setElapsedTime(elapsedTimeinSec);
	}

	public void refresh() {
		showQuestion(currentIndex);

	}

}
