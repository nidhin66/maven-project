package com.gbs.grandmaster.ui;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.gbs.grandmaster.ui.common.GrandMasterServiceException;
import com.gbs.grandmaster.ui.page.controller.StudentController;

public class UserStatusLogWorker {

	ScheduledExecutorService executor = null;
	Runnable task = null;
	
	 static Logger logger = Logger.getLogger(UserStatusLogWorker.class);
	
	public UserStatusLogWorker() {		
		executor = Executors.newScheduledThreadPool(1);
		task = () -> {
			saveLog();
		};
	}


	private void saveLog() {
		String userName = UIManager.getInstance().getCurrentUserName();
		try {
			StudentController.logActiveUserLog(userName);
		} catch (GrandMasterServiceException e) {
			logger.error("Failed to log user log",e);
		}
	}
	
	
	public void start(int interval) {
		executor.scheduleWithFixedDelay(task, 0, interval, TimeUnit.MINUTES);
		executor.submit(task);
	}

	public void stop() {
		executor.shutdown();
		saveLog();
	}

}
