package com.gbs.grandmaster.ui.model;

import java.util.UUID;




public class QuestionAttemptKey {
	
	  private long studentId;
	 
	  private long examScheduleId;
	  
	  private int attempt;
	  
	  private long questionPaperId;
	  
	  private long questionId;
	  
	  private UUID id;
	  
	  public QuestionAttemptKey() {
		  
	  }

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}

	public long getExamScheduleId() {
		return examScheduleId;
	}

	public void setExamScheduleId(long examScheduleId) {
		this.examScheduleId = examScheduleId;
	}

	public int getAttempt() {
		return attempt;
	}

	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}

	public long getQuestionPaperId() {
		return questionPaperId;
	}

	public void setQuestionPaperId(long questionPaperId) {
		this.questionPaperId = questionPaperId;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	  
	  

}
