package com.gbs.grandmaster.ui.page.controller;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.common.GrandMasterServiceException;
import com.gbs.grandmaster.ui.model.ActiveUserLog;
import com.gbs.grandmaster.ui.model.ExamAttendance;
import com.gbs.grandmaster.ui.model.Student;
import com.gbs.grandmaster.ui.model.StudentCourse;
import com.gbs.grandmaster.ui.oauth.OAuthUtils;

public class StudentController {
	
	private static String FIND_STUDENT_BY_NAME = "/findStudentByUserName";
	private static String GET_STUDENT_BATCH_DETAILS = "/getStudentBatchDetails";
	private static String IS_USER_ACTIVE = "/isUserActive";
	private static String LOG_ACTIVE_USER_LOG = "/logActiveUserLog";
	
    static Logger logger = Logger.getLogger(AppController.class);
    
    
    public static void logActiveUserLog(String userName) throws GrandMasterServiceException {
    	ActiveUserLog userLog = new ActiveUserLog(userName);
    	String ipAddress = CommonUtilitties.getIpAddress();
    	userLog.setIpAddress(ipAddress);
    	String macID = CommonUtilitties.getMacId();    	
    	userLog.setMacId(macID);
    	logger.debug("Logging user :" + userName + " IP : " + ipAddress + " mac Id : " + macID) ;
    	try {
    		Map<String, Object> response = OAuthUtils.postSecureData(LOG_ACTIVE_USER_LOG,userLog);
    		boolean success = (boolean) response.get("success");
    	}catch(Exception e) {
    		e.printStackTrace();
    	}		
	}    
    
    public static boolean isUserActive(String userName) throws GrandMasterServiceException {
    	ActiveUserLog userLog = new ActiveUserLog(userName);
    	String ipAddress = CommonUtilitties.getIpAddress();
    	userLog.setIpAddress(ipAddress);
    	String macID = CommonUtilitties.getMacId();    	
    	userLog.setMacId(macID);
    	logger.debug("Checking active user for the user :" + userName + " IP : " + ipAddress + " mac Id : " + macID) ;
    	Map<String, Object> response =	OAuthUtils.postSecureData(IS_USER_ACTIVE,userLog);
		boolean success = (boolean) response.get("success");
		boolean allowLogin = false;
		if(success) {
			boolean active = (boolean) response.get("data");
			if(!active) {
				allowLogin = true;
			}
		}
		return allowLogin;
    }
	
	public static Student findStudentByUserName(String userName) throws GrandMasterServiceException {
		Map<String, Object> response =	OAuthUtils.getProtectedResource(FIND_STUDENT_BY_NAME + "/" + userName);
		JSONObject dataStr =  (JSONObject) response.get("data");	
		ObjectMapper mapper = new ObjectMapper();
		Student student = null;
		try {
			student =mapper.readValue(dataStr.toJSONString(), Student.class);
		} catch (IOException e) {
			logger.error("Failed to retrieve student details");
			new GrandMasterServiceException("Failed to retrieve student details");
		}
		return student;		
	}
	
	
	public static StudentCourse getStudentBatchDetails(long studentId) throws GrandMasterServiceException {
		Map<String, Object> response =	OAuthUtils.getProtectedResource(GET_STUDENT_BATCH_DETAILS + "/" + studentId);
		JSONObject dataStr =  (JSONObject) response.get("data");	
		ObjectMapper mapper = new ObjectMapper();
		StudentCourse studentCourse = null;
		try {
			studentCourse =mapper.readValue(dataStr.toJSONString(), StudentCourse.class);
		} catch (IOException e) {
			logger.error("Failed to retrieve student course details");
			new GrandMasterServiceException("Failed to retrieve student course details");
		}
		return studentCourse;
		
	}

}
