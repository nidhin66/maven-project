package com.gbs.grandmaster.ui.page;

import com.gbs.grandmaster.ui.Constants;
import com.gbs.grandmaster.ui.LabelAndMessageUtil;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.common.PageIconButton;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public class HomePage extends BorderPane {

	public HomePage() {
		createTopBanner();
		createContentPane();
		setPadding(new Insets(50, 75, 20, 75));
		
	}

	private void createTopBanner() {
		HBox topBanner = new HBox();
		
		ImageView leftImage = new ImageView("images/grand.png");
		leftImage.setPreserveRatio(true);
		leftImage.setFitHeight(75);
		leftImage.setFitWidth(200);

		ImageView rightImage = new ImageView("images/client_logo.png");
		rightImage.setPreserveRatio(true);
		rightImage.setFitHeight(75);
		rightImage.setFitWidth(125);

		Region midRegion = new Region();
		HBox.setHgrow(midRegion, Priority.ALWAYS);
		
		topBanner.getChildren().addAll(leftImage,midRegion,rightImage);
		topBanner.setPadding(new Insets(0,0,10,0));
		setTop(topBanner);
	}
	private void createContentPane() {
		ScrollPane sp = new ScrollPane();
		sp.setHbarPolicy(ScrollBarPolicy.NEVER);
		sp.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);		
		
		GridPane gridPane = new GridPane();
		gridPane.setVgap(getGridVGap());
		gridPane.setHgap(getGridHGap());
		
		gridPane.add(new PageIconButton(LabelAndMessageUtil.getLabel(Constants.PAGE_PROFILE),Constants.PAGE_PROFILE), 0, 0);
		gridPane.add(new PageIconButton(LabelAndMessageUtil.getLabel(Constants.PAGE_INSTRUCTION),Constants.PAGE_INSTRUCTION), 1, 0);
		gridPane.add(new PageIconButton(LabelAndMessageUtil.getLabel(Constants.PAGE_CALENDAR),Constants.PAGE_CALENDAR), 2, 0);
		
		gridPane.add(new PageIconButton(LabelAndMessageUtil.getLabel(Constants.PAGE_EXAM),Constants.PAGE_EXAM), 0, 1);
		gridPane.add(new PageIconButton(LabelAndMessageUtil.getLabel(Constants.PAGE_NOTIFICATION),Constants.PAGE_NOTIFICATION), 1, 1);
		gridPane.add(new PageIconButton(LabelAndMessageUtil.getLabel(Constants.PAGE_MOCKTEST),Constants.PAGE_MOCKTEST), 2, 1);

		sp.setContent(gridPane);
		sp.setPadding(new Insets(getPaddingSize(), getPaddingSize(), getPaddingSize(), getPaddingSize()));
		
		setCenter(sp);
		
	}
	
	private double getPaddingSize() {
		return UIUtilities.getScreenHeight() * .075;
	}
	
	private double getGridVGap() {
		return UIUtilities.getScreenHeight() * .025;
	}
	
	private double getGridHGap() {
		return UIUtilities.getScreenWidth() * .025;
	}

}
