package com.gbs.grandmaster.ui.model;

import java.util.Date;

public class ActiveUserLog {

	private String userName;

	private String ipAddress;
	
	private String macId;

	private Date updateTime;
	
	public ActiveUserLog() {
		
	}

	public ActiveUserLog(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getMacId() {
		return macId;
	}

	public void setMacId(String macId) {
		this.macId = macId;
	}
	
	

}
