package com.gbs.grandmaster.ui.page;

import java.text.SimpleDateFormat;
import java.util.List;

import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.common.GrandMasterAlert;
import com.gbs.grandmaster.ui.common.GrandMasterServiceException;
import com.gbs.grandmaster.ui.common.MainWindow;
import com.gbs.grandmaster.ui.common.WorkIndicatorDialog;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.AnswerSheet;
import com.gbs.grandmaster.ui.model.EXAM_STATUS;
import com.gbs.grandmaster.ui.model.Exam;
import com.gbs.grandmaster.ui.model.ExamInfoWrapper;
import com.gbs.grandmaster.ui.model.ExamReport;
import com.gbs.grandmaster.ui.model.QuestionPaper;
import com.gbs.grandmaster.ui.page.action.ExamItemButtonActionListner;
import com.gbs.grandmaster.ui.page.controller.ExamController;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class AnswerExamListItem extends HBox {

	private AnswerSheet answerSheet;

	private WorkIndicatorDialog<String> wd = null;

	public AnswerExamListItem(AnswerSheet answerSheet, int index) {
		this.answerSheet = answerSheet;
		setSpacing(10);
		setAlignment(Pos.CENTER);
		getStyleClass().add("exam-list-item");

		getChildren().add(new ListItemBand(index));
		getChildren().add(createExamInfoBox());

		Region midRegion = new Region();
		HBox.setHgrow(midRegion, Priority.ALWAYS);
		getChildren().add(midRegion);

		Button viewButton = new Button("View Result");
		viewButton.getStyleClass().add("viewButton");
		viewButton.setPrefSize(100, 30);
		viewButton.setOnAction(e -> showResult());
		HBox.setMargin(viewButton, new Insets(0, 10, 0, 0));

		getChildren().add(viewButton);
		setPrefWidth(getItemWidth());
	}

	private VBox createExamInfoBox() {
		VBox examInfoBox = new VBox();
		examInfoBox.setSpacing(5);
		examInfoBox.setAlignment(Pos.CENTER_LEFT);
		Label examLabel = new Label();
		examLabel.setText(answerSheet.getExam().getName());
		examLabel.setStyle("-fx-font-weight: bold;-fx-font-size: 14.0px;-fx-font-family: \"Roboto\";");
		examInfoBox.getChildren().add(examLabel);
		HBox attemptInfoBox = new HBox();
		attemptInfoBox.setAlignment(Pos.CENTER_LEFT);
		attemptInfoBox.setSpacing(10);
		Label attemptLabel = new Label("Attempt");
		attemptLabel.setStyle("-fx-font-weight: bold;-fx-font-size: 12.0px;-fx-font-family: \"Roboto\";");
		Label attemptNoLabel = new Label(answerSheet.getAttendance().getKey().getAttempt() + "");
		attemptNoLabel.setPadding(new Insets(2, 10, 2, 10));
		attemptNoLabel.setStyle(
				"-fx-background-color: #E53935;	-fx-text-fill: white;-fx-font-weight: bold;-fx-font-size: 12.0px;-fx-font-family: \"Roboto\";");
		SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		String strDate = formatter.format(answerSheet.getAttendance().getStartedDateTime());
		Label timeLabel = new Label(strDate);
		timeLabel.setStyle("-fx-font-weight: bold;-fx-font-size: 12.0px;-fx-font-family: \"Roboto\";");
		attemptInfoBox.getChildren().addAll(attemptLabel, attemptNoLabel, timeLabel);
		examInfoBox.getChildren().add(attemptInfoBox);
		return examInfoBox;
	}

	private void showResult() {
		wd = new WorkIndicatorDialog<String>(UIManager.getInstance().getPrimaryStage(), "Please wait...");
		wd.addTaskEndNotification(result -> {
			wd = null; // don't keep the object, cleanup
		});
		wd.exec("123", inputParam -> {
			try {

				QuestionPaper questionPaper = ExamController
						.getQuestionPaper(UIManager.getInstance().getStudent().getId(), answerSheet.getExam().getId());
				List<Answer> answers = ExamController.getAnswers(UIManager.getInstance().getStudent().getId(),
						answerSheet.getExam().getId(), answerSheet.getAttendance().getKey().getAttempt());
				
				ExamReport examReports = ExamController
						.getExamReport(answerSheet.getAttendance());
			
				Platform.runLater(() -> {
					AnswerSheetPage sheetPage = new AnswerSheetPage("ANSWER SHEET", questionPaper, answerSheet,
							answers, examReports);
					UIManager.getInstance().setCurrentAnswerSheet(sheetPage);
					MainWindow.getInstance().setCenter(sheetPage);
				});
			} catch (GrandMasterServiceException e) {
				Platform.runLater(() -> {
					GrandMasterAlert alert = new GrandMasterAlert(AlertType.ERROR);
					alert.setTitle("Failed to show result");
					alert.setHeaderText("Failed to show result");
					alert.setContentText(e.getMessage());
					alert.showAndWait();
				});
			}
			return Integer.valueOf(1);
		});
	}

	private double getItemWidth() {
		return UIUtilities.getScreenWidth() * .85;
	}

}
