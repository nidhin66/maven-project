package com.gbs.grandmaster.ui.model;




public class ExamSectionDetails  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private Long id;
	

	private int version;
	
	private int noOfQuestions;
	
	private long subject_id;
	
	private QUESTION_TYPE questionType;
	
	private double markPerQuestion;
	
	private double negativeMarkPerQuestion;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getNoOfQuestions() {
		return noOfQuestions;
	}

	public void setNoOfQuestions(int noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}



	public long getSubject_id() {
		return subject_id;
	}

	public void setSubject_id(long subject_id) {
		this.subject_id = subject_id;
	}

	public QUESTION_TYPE getQuestionType() {
		return questionType;
	}

	public void setQuestionType(QUESTION_TYPE questionType) {
		this.questionType = questionType;
	}

	public double getMarkPerQuestion() {
		return markPerQuestion;
	}

	public void setMarkPerQuestion(double markPerQuestion) {
		this.markPerQuestion = markPerQuestion;
	}

	public double getNegativeMarkPerQuestion() {
		return negativeMarkPerQuestion;
	}

	public void setNegativeMarkPerQuestion(double negativeMarkPerQuestion) {
		this.negativeMarkPerQuestion = negativeMarkPerQuestion;
	}


}
