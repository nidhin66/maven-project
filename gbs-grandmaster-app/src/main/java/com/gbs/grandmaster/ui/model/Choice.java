package com.gbs.grandmaster.ui.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;




public class Choice  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private Long id;
	
	private int version;
	
	private int sequence;
	private boolean correct;
	
	private Set<ChoiceText> choiceTexts;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public Set<ChoiceText> getChoiceTexts() {
		return choiceTexts;
	}

	public void setChoiceTexts(Set<ChoiceText> choiceTexts) {
		this.choiceTexts = choiceTexts;
	}
	public void addChoiceText(ChoiceText choiseText) {
		if(null == choiceTexts) {
			choiceTexts = new HashSet<>();
		}
		choiceTexts.add(choiseText);
	}
	public String getDefaultChoiceText() {
		if(null != choiceTexts) {
			Iterator<ChoiceText> iter = choiceTexts.iterator();

			ChoiceText first = iter.next();
			byte[] cotent = first.getContent();
			return new String(cotent);
		}else {
			return "";
		}
	}

}
