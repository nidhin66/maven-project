package com.gbs.grandmaster.ui.page;

import java.util.List;

import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.common.GrandmasterPage;
import com.gbs.grandmaster.ui.model.Instruction;
import com.gbs.grandmaster.ui.page.controller.InstructionController;

import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.event.EventHandler;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Parent;

public class InstructionPage extends GrandmasterPage {
	public InstructionPage(String name) {
		super(name);
		createContentPane();

	}

	@Override
	
	public void createContentPane() {

		Instruction listInstruction = InstructionController.listInstructions();

		WebView wView = new WebView();
		
		WebEngine engine = wView.getEngine();
		engine.loadContent(listInstruction.getContentAsString());

		ScrollPane sp = new ScrollPane();
		sp.setHbarPolicy(ScrollBarPolicy.NEVER);
		sp.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		sp.getStyleClass().add("scrollpane");
		sp.setContent(wView);
		setCenter(sp);

	}
}
