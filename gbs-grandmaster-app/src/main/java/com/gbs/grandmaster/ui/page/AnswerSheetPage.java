package com.gbs.grandmaster.ui.page;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.common.GrandmasterPage;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.AnswerSheet;
import com.gbs.grandmaster.ui.model.Exam;
import com.gbs.grandmaster.ui.model.ExamReport;
import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.model.Question;
import com.gbs.grandmaster.ui.model.QuestionAttemptTime;
import com.gbs.grandmaster.ui.model.QuestionPaper;
import com.gbs.grandmaster.ui.page.controller.ExamController;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class AnswerSheetPage extends GrandmasterPage{
	private QuestionPaper questionPaper;
	private List<Answer> answers;
	private Map<Long, Answer> answerMap = null;
	private AnswerSheet answerSheet; 
	private double totalMarks = 0;
	private ExamReport examReport;

	public AnswerSheetPage(String name,QuestionPaper questionPaper,AnswerSheet answerSheet,List<Answer> answers, ExamReport examReports) {
		super(name);
		this.answerSheet = answerSheet;
		this.questionPaper = questionPaper;
		this.answers = answers;
		this.answerMap = new HashMap<>();
		this.examReport = examReports;
		for(Answer answer : answers) {
			totalMarks = totalMarks + answer.getMark();
			totalMarks = totalMarks - answer.getNegativeMark();
			answerMap.put(answer.getKey().getQuestionId(), answer);			
		}
		createContentPane();
	}

	@Override
	public void createContentPane() {
		
		long student_id = UIManager.getInstance().getStudentCourse().getId();
		long batch_id = UIManager.getInstance().getStudentCourse().getBatch_id();
		setCenter(createAnswerPane());
		
	}
	
	private BorderPane createAnswerPane() {
		BorderPane answerPane = new BorderPane();
		
		HBox examSummaryBox = new HBox();
		examSummaryBox.setSpacing(10);
		examSummaryBox.setPadding(new Insets(5,25,0,25));
		
		VBox examDetailsBox = new VBox();
		String examName = answerSheet.getExam().getName();
		Label examNameLbl = new Label(examName);
		examNameLbl.setPadding(new Insets(2,5,2,5));
		examNameLbl.getStyleClass().add("examNameLbl");
		examDetailsBox.getChildren().add(examNameLbl);
		
		HBox attemptInfoBox = new HBox();
		attemptInfoBox.setAlignment(Pos.CENTER_LEFT);
		attemptInfoBox.setSpacing(10);
		Label attemptLabel = new Label("Attempt");	
		attemptLabel.getStyleClass().add("attemptLabel");
		Label attemptNoLabel = new Label(answerSheet.getAttendance().getKey().getAttempt()+"");	
		attemptNoLabel.setPadding(new Insets(2,10,2,10));
		attemptNoLabel.getStyleClass().add("attemptNoLabel");
		SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		String strDate = formatter.format(answerSheet.getAttendance().getStartedDateTime());
		Label timeLabel = new Label(strDate);
		timeLabel.getStyleClass().add("attemptLabel");
		attemptInfoBox.getChildren().addAll(attemptLabel,attemptNoLabel,timeLabel);
		
		
		Region midRegion = new Region();
		HBox.setHgrow(midRegion, Priority.ALWAYS);
		Label resultLbl = new Label("TOTAL MARK");
		resultLbl.setPadding(new Insets(2,5,2,5));
		resultLbl.getStyleClass().add("resultLbl");
		Label markLbl = new Label(totalMarks + "/" + questionPaper.getMarks());
		markLbl.setPadding(new Insets(2,5,2,5));
		markLbl.getStyleClass().add("markLbl");
		
		examSummaryBox.getChildren().addAll(examDetailsBox,attemptInfoBox,midRegion,resultLbl,markLbl);
		answerPane.setTop(examSummaryBox);
		
		List<ExamSection> examSections = questionPaper.getSections();
		Collections.sort(examSections);
		TabPane tabpane = new TabPane(); 
		tabpane.setPadding(new Insets(10,0,0,0));
		tabpane.getStyleClass().add("answer-tab-pane");
		for(ExamSection examSection : examSections) {
			SectionAnswerTab answerTab = new SectionAnswerTab(this, examSection);
			tabpane.getTabs().add(answerTab); 
		}
		answerPane.setCenter(tabpane);
		
		return answerPane;
		
	}
		
	public Answer getAnswer(long questionId) {
		return answerMap.get(questionId);
	}
	
	public List<QuestionAttemptTime> getQuestionAttemptTime(long questionid){
		return examReport.getTimeDetails().get(questionid);
	}

}
