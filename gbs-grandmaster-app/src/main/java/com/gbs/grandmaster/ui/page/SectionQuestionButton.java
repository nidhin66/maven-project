package com.gbs.grandmaster.ui.page;

import com.gbs.grandmaster.ui.model.QUESTION_STATUS;
import com.gbs.grandmaster.ui.model.Question;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;

public class SectionQuestionButton extends Button {

	private SectionQuestionPage page;
	private Question question;
	private QuestionPane questionPane;

	public SectionQuestionButton(SectionQuestionPage page, Question question, int index) {
		this.page = page;
		this.question = question;
		ExamSectionPane sectionPane = page.getSectionPane();
		this.questionPane = sectionPane.getQuestionPane(question.getId());
		setText((index + 1) + "");
		setOnAction(e -> {
			page.setQuestion(question, index);
			sectionPane.showQuestion(index);
		});
		refreshButton();
	}

	public void refreshButton() {
		ExamSectionPane sectionPane = page.getSectionPane();
		this.questionPane = sectionPane.getQuestionPane(question.getId());
		this.setStyle("-fx-font: 10 arial; " + "-fx-background-radius: 5em; " + "-fx-min-width: 33px; "
				+ "-fx-min-height: 33px; " + "-fx-max-width: 33px; "
				+ "-fx-max-height: 33px; -fx-background-color: #bdbdbd;");
		if (null != questionPane) {
			// this.setOnAction(new EventHandler<ActionEvent>() {
			// public void handle(ActionEvent event) {
			// if(questionPane.getStatus() != QUESTION_STATUS.NOTANSWERED){
			//// AppController.getInstance().getExamScreenContainer().setQuestion(Long.valueOf(questionLog.getSeq()).intValue());
			//// navigationDialog.close();
			// }
			// }
			// });
			if (questionPane.getStatus() == QUESTION_STATUS.MARKED) {
				this.setStyle("-fx-font: 10 arial; " + "-fx-background-radius: 5em; " + "-fx-min-width: 33px; "
						+ "-fx-min-height: 33px; " + "-fx-max-width: 33px; " + "-fx-max-height: 33px;"
						+ "-fx-background-color: #e2bd23;");
			} else {
				if (questionPane.getStatus() == QUESTION_STATUS.ANSWERED) {
					this.setStyle("-fx-font: 10 arial; " + "-fx-background-radius: 5em; " + "-fx-min-width: 33px; "
							+ "-fx-min-height: 33px; " + "-fx-max-width: 33px; "
							+ "-fx-max-height: 33px; -fx-background-color: #87c431;");
					this.setTextFill(Color.WHITE);
				} else if (questionPane.getStatus() == QUESTION_STATUS.NOTANSWERED) {
					this.setStyle("-fx-font: 10 arial; " + "-fx-background-radius: 5em; " + "-fx-min-width: 33px; "
							+ "-fx-min-height: 33px; " + "-fx-max-width: 33px; "
							+ "-fx-max-height: 33px; -fx-background-color: #ef5050;");

				} else if (questionPane.getStatus() == QUESTION_STATUS.MARKED_ANSWERED) {
					this.setStyle("-fx-font: 10 arial; " + "-fx-background-radius: 5em; " + "-fx-min-width: 33px; "
							+ "-fx-min-height: 33px; " + "-fx-max-width: 33px; " + "-fx-max-height: 33px;"
							+ "-fx-background-color: #1976D2;");
					this.setTextFill(Color.WHITE);
				}

			}

		}
	}

}
