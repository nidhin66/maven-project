package com.gbs.grandmaster.ui.page;

import java.util.List;

import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.common.GrandmasterPage;
import com.gbs.grandmaster.ui.model.Exam;
import com.gbs.grandmaster.ui.model.ExamInfoWrapper;
import com.gbs.grandmaster.ui.page.controller.ExamController;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class ExamListPage extends GrandmasterPage {

	private boolean mockExam;

	private List<ExamInfoWrapper> examDetails;

	public ExamListPage(String name, List<ExamInfoWrapper> examDetails, boolean mockExam) {
		super(name);
		this.mockExam = mockExam;
		this.examDetails = examDetails;
		createContentPane();
	}

	@Override
	public void createContentPane() {

		ScrollPane sp = new ScrollPane();
		sp.setHbarPolicy(ScrollBarPolicy.NEVER);
		sp.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		sp.getStyleClass().add("scrollpane");

		VBox examListPane = new VBox();
		examListPane.getStyleClass().add("examListPane");
		examListPane.setSpacing(10);
		examListPane.setAlignment(Pos.CENTER);
		int i = 0;
		if (null != examDetails && examDetails.size() > 0) {
			for (ExamInfoWrapper examInfo : examDetails) {
				ExamListItem item = new ExamListItem(examInfo, ++i, mockExam);
				examListPane.getChildren().add(item);
			}
			sp.setContent(examListPane);
			examListPane.setPadding(new Insets(20, 20, 20, 20));
			setCenter(sp);
		} else {
			Label noExamLabel = new Label("No Exams Scheduled");
			noExamLabel.getStyleClass().add("noItemsLabel");
			BorderPane.setAlignment(noExamLabel, Pos.CENTER);
			setCenter(noExamLabel);

		}

	}

}
