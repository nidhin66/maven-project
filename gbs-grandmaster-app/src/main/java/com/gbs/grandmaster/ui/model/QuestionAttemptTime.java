package com.gbs.grandmaster.ui.model;

import java.util.Date;

public class QuestionAttemptTime {
	
	private QuestionAttemptKey key;
	 
	 private long timeTaken;
	 
	 private Date updateTime;
	 
	 public QuestionAttemptTime() {
		 
	 }
	 
	 public QuestionAttemptTime(QuestionAttemptKey key) {
		 this.key = key;
	 }

	public QuestionAttemptKey getKey() {
		return key;
	}

	public void setKey(QuestionAttemptKey key) {
		this.key = key;
	}

	public long getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(long timeTaken) {
		this.timeTaken = timeTaken;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	 
	 
	 

}
