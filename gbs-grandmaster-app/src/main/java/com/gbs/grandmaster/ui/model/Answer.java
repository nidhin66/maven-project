package com.gbs.grandmaster.ui.model;

import java.util.Date;

public class Answer {

	private AnswerKey key;
	
	private long sectionId;

	private String content;
	
	private boolean correct;

	private double mark;

	private double negativeMark;

	private Date updateTime;
	
	private boolean marked;

	public Answer() {

	}

	public Answer(AnswerKey key) {
		this.key = key;
	}

	public AnswerKey getKey() {
		return key;
	}

	public void setKey(AnswerKey key) {
		this.key = key;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}

	public double getNegativeMark() {
		return negativeMark;
	}

	public void setNegativeMark(double negativeMark) {
		this.negativeMark = negativeMark;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public boolean isMarked() {
		return marked;
	}

	public void setMarked(boolean marked) {
		this.marked = marked;
	}

	public long getSectionId() {
		return sectionId;
	}

	public void setSectionId(long sectionId) {
		this.sectionId = sectionId;
	}
	
	

}
