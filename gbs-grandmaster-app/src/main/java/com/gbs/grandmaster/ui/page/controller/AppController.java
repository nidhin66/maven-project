package com.gbs.grandmaster.ui.page.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;

import com.gbs.grandmaster.ui.common.GrandMasterServiceException;
import com.gbs.grandmaster.ui.model.ApplicationConfig;
import com.gbs.grandmaster.ui.oauth.OAuthUtils;

public class AppController {
	
	
	private static String GET_APP_CONFIGS = "/getApplicationConfigs";
	
	private static ObjectMapper mapper = new ObjectMapper();
	
    static Logger logger = Logger.getLogger(AppController.class);

	
	public static List<ApplicationConfig> getApplicationConfigs() throws GrandMasterServiceException{
		Map<String, Object> response =	OAuthUtils.getProtectedResource(GET_APP_CONFIGS);
		JSONArray dataStr =  (JSONArray) response.get("data");	
		List<ApplicationConfig> configs = new ArrayList<>();
		try {	
			configs = mapper.readValue(dataStr.toJSONString(), mapper.getTypeFactory().constructCollectionType(List.class, ApplicationConfig.class));
		} catch (IOException e) {
			logger.error("Failed to retrieve application configuration details");
			new GrandMasterServiceException("Failed to retrieve application configuration details");
		}
		return configs;
	}

}
