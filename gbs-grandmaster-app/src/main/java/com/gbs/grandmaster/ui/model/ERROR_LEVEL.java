package com.gbs.grandmaster.ui.model;

public enum ERROR_LEVEL {
	ERROR("ERROR"),WARN("WARN"),INFO("INFO");
	private final String level ;
	private ERROR_LEVEL(String level) {
		this.level = level;
	}
	public String getLevel() {
		return level;
	}
	
}
