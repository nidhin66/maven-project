package com.gbs.grandmaster.ui.page;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.gbs.grandmaster.ui.AppUtils;
import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.Constants;
import com.gbs.grandmaster.ui.common.GrandmasterPage;
import com.gbs.grandmaster.ui.model.Notification;
import com.gbs.grandmaster.ui.page.controller.NotificationController;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NotificationPage extends GrandmasterPage{
	
	private List<Notification> notificationList = null;
	
	public NotificationPage(String name){
		super(name);
		createContentPane();
	}
	
	@Override
	public void createContentPane() {
		
		setCenter(null);
		
		notificationList = NotificationController.listNotifications(); 
		
		reloadNotifications();
		
		
		
	}
	
	public void reloadNotifications() {

		String[] ids = readDeletedNotifications();	

		ScrollPane sp = new ScrollPane();
		sp.setHbarPolicy(ScrollBarPolicy.NEVER);
		sp.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		sp.getStyleClass().add("scrollpane");
		
		VBox notificationListPane = new VBox();
		notificationListPane.getStyleClass().add("notificationListPane");
		notificationListPane.setSpacing(10);
		
		Map<Long, Notification> notificationMap = new TreeMap<>();
		for(Notification notification : notificationList) {
			notificationMap.put(notification.getId(), notification);				
		}
		StringBuffer idsStrBfr = new StringBuffer();
		if(null != ids) {		
			for(String id : ids) {
				Notification notification = notificationMap.remove(Long.valueOf(id.trim()));
				if(null != notification) {
					if(CommonUtilitties.isNullorEmpty(idsStrBfr.toString())) {
						idsStrBfr.append(id);
					}else {
						idsStrBfr.append(Constants.RECOD_DELIMITER);
						idsStrBfr.append(id);
					}
				}				
			}
		}
		updateNotificationIds(idsStrBfr.toString());
		int i =0;
		for(Iterator<Long> it = notificationMap.keySet().iterator(); it.hasNext();) {
			Long key = it.next();
			Notification notification = notificationMap.get(key);
			if(null != notification) {
				NotificationListItem item = new NotificationListItem(this,notification,++i);
				notificationListPane.getChildren().add(item);	
			}
		}
		sp.setContent(notificationListPane);
		notificationListPane.setPadding(new Insets(20,20,20,20));
		setCenter(sp);
	
		
		if(notificationListPane.getChildren().size()==0)
		{
			
			Label emptyListLabel = new Label("No Contents");
			emptyListLabel.getStyleClass().add("noItemsLabel");
			BorderPane.setAlignment(emptyListLabel, Pos.CENTER);
			setCenter(emptyListLabel);
			
		}	
		
	}	
	
	
	
	private String[] readDeletedNotifications() {
		String appFolder = AppUtils.getUserFolder();
		String notificationFilePath = appFolder  + File.separator + "Notifications" ;
		String notificationStr = CommonUtilitties.readFile(notificationFilePath);
		if(!CommonUtilitties.isNullorEmpty(notificationStr)) {
			notificationStr = notificationStr.trim();
			String[] ids = notificationStr.split(Constants.RECOD_DELIMITER);
			return ids;
		}else {
			return null;
		}		
	}
	
	private void updateNotificationIds(String notificationStr) {
		String appFolder = AppUtils.getUserFolder();
		String notificationFilePath = appFolder  + File.separator + "Notifications" ;
		CommonUtilitties.writeFile(notificationFilePath, notificationStr);
	}

}
