package com.gbs.grandmaster.ui.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;





public class Exam {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private Long id;
	
	
	private int version;
	
	
	private ExamDefinition examDefinition;
	
    private long batch_id;
	
	private String name;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date startDateTime;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")	
	private Date endDateTime;
		
	private int noOfAttempts;
	
	private boolean resultImmeadiate;
	
//	private QuestionBank questionBank;
	
	private int noOfQuestionPapers;

	private EXAM_STATUS status;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public ExamDefinition getExamDefinition() {
		return examDefinition;
	}

	public void setExamDefinition(ExamDefinition examDefinition) {
		this.examDefinition = examDefinition;
	}

//	public Batch getBatch() {
//		return batch;
//	}
//
//	public void setBatch(Batch batch) {
//		this.batch = batch;
//	}

	public Date getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}

	public int getNoOfAttempts() {
		return noOfAttempts;
	}

	public void setNoOfAttempts(int noOfAttempts) {
		this.noOfAttempts = noOfAttempts;
	}

	public boolean isResultImmeadiate() {
		return resultImmeadiate;
	}

	public void setResultImmeadiate(boolean resultImmeadiate) {
		this.resultImmeadiate = resultImmeadiate;
	}



	public int getNoOfQuestionPapers() {
		return noOfQuestionPapers;
	}

	public void setNoOfQuestionPapers(int noOfQuestionPapers) {
		this.noOfQuestionPapers = noOfQuestionPapers;
	}

	public EXAM_STATUS getStatus() {
		return status;
	}

	public void setStatus(EXAM_STATUS status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getBatch_id() {
		return batch_id;
	}

	public void setBatch_id(long batch_id) {
		this.batch_id = batch_id;
	}
	
	

}
