package com.gbs.grandmaster.ui.common;

import com.gbs.grandmaster.ui.Constants;
import com.gbs.grandmaster.ui.LabelAndMessageUtil;
import com.gbs.grandmaster.ui.UIManager;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public abstract class GrandmasterPage extends BorderPane {

	private String name = "";	
	public GrandmasterPage(String name,boolean enableBackHomeBtn) {		
		this.name = name;
		createHeader();		
		if(enableBackHomeBtn) {
			createFooter();			
		}				
		setPadding(new Insets(25, 75, 20, 75));
	}

	public GrandmasterPage(String name) {
		this(name, true);	
	}
	
	public abstract void createContentPane();

	private void createHeader() {
		HBox topBanner = new HBox();

		ImageView leftImage = new ImageView("images/slogan.png");
		leftImage.setPreserveRatio(true);
		leftImage.setFitHeight(45);
		leftImage.setFitWidth(350);
		
		Label pageHeading = new Label(name.toUpperCase());
		pageHeading.getStyleClass().add("page_heading");
		
		VBox leftBox = new VBox();
		leftBox.getChildren().addAll(leftImage,pageHeading);
		leftBox.setSpacing(15);
		leftBox.setAlignment(Pos.TOP_LEFT);

		ImageView rightImage = new ImageView("images/client_logo.png");
		rightImage.setPreserveRatio(true);
		rightImage.setFitHeight(70);
		rightImage.setFitWidth(125);

		Region midRegion = new Region();
		HBox.setHgrow(midRegion, Priority.ALWAYS);

		topBanner.getChildren().addAll(leftBox, midRegion, rightImage);
		topBanner.setPadding(new Insets(0, 0, 10, 0));
		setTop(topBanner);
	}

	private void createFooter() {
		Button homeBtn = new Button(LabelAndMessageUtil.getLabel(Constants.LBL_BACK_HOME));
		homeBtn.getStyleClass().add("homeBtn");
		HBox buttonPanel = new HBox();
 		
		buttonPanel.getChildren().addAll(homeBtn);
		buttonPanel.setAlignment(Pos.CENTER_RIGHT);
		setBottom(buttonPanel);
		buttonPanel.setPadding(new Insets(10, 0, 0, 0));

		BorderPane.setAlignment(buttonPanel, Pos.CENTER_RIGHT);
		homeBtn.setOnAction(e->{
			UIManager.getInstance().showHomePage();
		});
	}

}
