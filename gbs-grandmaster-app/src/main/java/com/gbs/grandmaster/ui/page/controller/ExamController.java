package com.gbs.grandmaster.ui.page.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.gbs.grandmaster.ui.AppUtils;
import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.common.GrandMasterServiceException;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.AnswerSheet;
import com.gbs.grandmaster.ui.model.ApplicationError;
import com.gbs.grandmaster.ui.model.AttendanceKey;
import com.gbs.grandmaster.ui.model.ExamAttendance;
import com.gbs.grandmaster.ui.model.ExamInfoWrapper;
import com.gbs.grandmaster.ui.model.ExamReport;
import com.gbs.grandmaster.ui.model.ExamStatusLog;
import com.gbs.grandmaster.ui.model.QuestionAttemptTime;
import com.gbs.grandmaster.ui.model.QuestionPaper;
import com.gbs.grandmaster.ui.oauth.OAuthUtils;

public class ExamController {

	private static String LIST_STUDENT_EXAM_URL = "/listStudentExams";
	private static String LIST_STUDENT_MOCK_EXAM_URL = "/listStudentMockExams";

	private static String GET_QUESTION_PAPER = "/getQuestionPaper";
	private static String START_EXAM = "/startExam";
	private static String FINISH_EXAM = "/finishExam";
	private static String SUBMIT_EXAM = "/submitExam";
	private static String SAVE_ANSWERS = "/saveAnswers";
	private static String GET_ANSWERS = "/getAnswers";
	private static String SAVE_EXAM_LOG = "/saveExamLog";
	private static String GET_EXAM_ANS_SHEET = "/getExamAnswerSheet";
	private static String GET_MOCK_EXAM_ANS_SHEET = "/getMockExamAnswerSheet";
	private static String GET_EXAM_REPORT = "/getExamReport";

	static Logger logger = Logger.getLogger(ExamController.class);

	private static ObjectMapper mapper = new ObjectMapper();

	public static boolean saveExamLog(ExamStatusLog statusLog) {
		boolean success = false;
		try {
			Map<String, Object> response = OAuthUtils.postSecureData(SAVE_EXAM_LOG, statusLog);
			success = (boolean) response.get("success");
		} catch (Exception e) {
			logger.error("Exception inside saveExamLog method" + e);
		}
		return success;
	}

	public static List<Answer> getAnswers(long student_id, long examScheduleId, int attempt)
			throws GrandMasterServiceException {
		Map<String, Object> response = OAuthUtils.getProtectedResource(GET_ANSWERS + "?student_id=" + student_id
				+ "&exam_schedule_id=" + examScheduleId + "&attempt=" + attempt);
		JSONArray dataStr = (JSONArray) response.get("data");
		List<Answer> answers = new ArrayList<>();

		try {
			answers = mapper.readValue(dataStr.toJSONString(),
					mapper.getTypeFactory().constructCollectionType(List.class, Answer.class));
		} catch (IOException e) {
			logger.error("Failed to retrieve answers");
			new GrandMasterServiceException("Failed to retrieve answers");
		}
		return answers;
	}   

	public	 static ExamReport getExamReport(ExamAttendance examAttendance)
			throws GrandMasterServiceException {
		Map<String, Object> response = OAuthUtils.postSecureData(GET_EXAM_REPORT,examAttendance);
		JSONObject dataStr = (JSONObject) response.get("data");
		ExamReport examReport = null;
		try {
			examReport = mapper.readValue(dataStr.toJSONString(), ExamReport.class);
		} catch (IOException e) {
			logger.error("Failed to retrieve exam report");
			new GrandMasterServiceException("Failed to retrieve exam report");
		}
 		return examReport;
	}

	public static List<ExamInfoWrapper> listStudentExams(long student_id, long batch_id)
			throws GrandMasterServiceException {
		mapper.setSerializationInclusion(Inclusion.NON_EMPTY);
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		Map<String, Object> response = OAuthUtils
				.getProtectedResource(LIST_STUDENT_EXAM_URL + "?student_id=" + student_id + "&batch_id=" + batch_id);
		JSONArray dataStr = (JSONArray) response.get("data");
		List<ExamInfoWrapper> examDetails = new ArrayList<>();
		try {
			examDetails = mapper.readValue(dataStr.toJSONString(),
					mapper.getTypeFactory().constructCollectionType(List.class, ExamInfoWrapper.class));
		} catch (IOException e) {
			throw new GrandMasterServiceException("Failed to retrieve exam details");
		}
		return examDetails;
	}

	public static List<ExamInfoWrapper> listStudentMockExams(long student_id, long batch_id)
			throws GrandMasterServiceException {
		mapper.setSerializationInclusion(Inclusion.NON_EMPTY);
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		Map<String, Object> response = OAuthUtils.getProtectedResource(
				LIST_STUDENT_MOCK_EXAM_URL + "?student_id=" + student_id + "&batch_id=" + batch_id);
		JSONArray dataStr = (JSONArray) response.get("data");
		List<ExamInfoWrapper> examDetails = new ArrayList<>();
		try {
			examDetails = mapper.readValue(dataStr.toJSONString(),
					mapper.getTypeFactory().constructCollectionType(List.class, ExamInfoWrapper.class));
		} catch (IOException e) {
			throw new GrandMasterServiceException("Failed to retrieve exam details");
		}
		return examDetails;
	}

	public static ExamAttendance startExam(long student_id, long examScheduleId) throws GrandMasterServiceException {
		AttendanceKey key = new AttendanceKey(student_id, examScheduleId, 1);
		ExamAttendance attendance = new ExamAttendance(key);
		ExamAttendance updatedAttendance = null;
		Map<String, Object> response = OAuthUtils.postSecureData(START_EXAM, attendance);
		boolean success = (boolean) response.get("success");

		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!success) {
				JSONArray errorString = (JSONArray) response.get("errors");
				if (null != errorString) {
					List<ApplicationError> errorList = mapper.readValue(errorString.toJSONString(),
							mapper.getTypeFactory().constructCollectionType(List.class, ApplicationError.class));
					if (null != errorList && errorList.size() > 0) {
						ApplicationError applicationError = errorList.get(0);
						if (null != applicationError) {
							throw new GrandMasterServiceException(applicationError.getErrorMessage());
						}
					}
				}
			} else {
				JSONObject dataStr = (JSONObject) response.get("data");
				updatedAttendance = mapper.readValue(dataStr.toJSONString(), ExamAttendance.class);
			}
		} catch (IOException e) {
			throw new GrandMasterServiceException(e.getMessage());
		}
		return updatedAttendance;
	}

	public static ExamAttendance finishExam(ExamAttendance attendance) throws GrandMasterServiceException {
		ExamAttendance examattended = null;
		Map<String, Object> response = OAuthUtils.postSecureData(FINISH_EXAM, attendance);
		boolean success = (boolean) response.get("success");
		try {
			JSONObject dataStr = (JSONObject) response.get("data");
			examattended = mapper.readValue(dataStr.toJSONString(), ExamAttendance.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return examattended;
	}

	public static boolean submitExam(ExamAttendance attendance) throws GrandMasterServiceException {
		Map<String, Object> response = OAuthUtils.postSecureData(SUBMIT_EXAM, attendance);
		boolean success = (boolean) response.get("success");
		return success;
	}

	public static boolean saveAnswers(List<Answer> answers) {
		Map<String, Object> response = null;
		;
		try {
			response = OAuthUtils.postSecureData(SAVE_ANSWERS, answers);
		} catch (Exception e) {
			logger.error("Failed to save answers");
		}
		if (null != response) {
			boolean success = (boolean) response.get("success");
			return success;
		} else {
			return false;

		}

	}

	public static QuestionPaper getQuestionPaper(long student_id, long exam_schedule_id)
			throws GrandMasterServiceException {
		String appFolder = AppUtils.getUserFolder();
		String questionPaperFile = appFolder + File.separator + "QP_" + student_id + "_" + exam_schedule_id;
		String qpJsonContent = CommonUtilitties.readFile(questionPaperFile);
		QuestionPaper questionPaper = null;
		if (!CommonUtilitties.isNullorEmpty(qpJsonContent)) {
			try {
				questionPaper = mapper.readValue(qpJsonContent, QuestionPaper.class);
			} catch (IOException e) {
				logger.error("Failed to retrieve question paper from local file", e);
				new GrandMasterServiceException("Failed to retrieve question paper");
			}
		}
		if (null == questionPaper) {
			try {
				Map<String, Object> response = OAuthUtils.getProtectedResource(
						GET_QUESTION_PAPER + "?student_id=" + student_id + "&exam_schedule_id=" + exam_schedule_id);
				JSONObject dataStr = (JSONObject) response.get("data");
				ObjectMapper mapper = new ObjectMapper();
				if(null != dataStr) {
					questionPaper = mapper.readValue(dataStr.toJSONString(), QuestionPaper.class);
					CommonUtilitties.writeFile(questionPaperFile, dataStr.toJSONString());
				}else {
					logger.error("Failed to retrieve question paper from server for exam id : " + exam_schedule_id);
				}
			} catch (IOException e) {
				logger.error("Failed to retrieve question paper from server", e);
				new GrandMasterServiceException("Failed to retrieve question paper");
			}
		}
		return questionPaper;
	}

	public static List<AnswerSheet> getExamAnswerSheet(long student_id, long batch_id) {
		List<AnswerSheet> answerSheets = new ArrayList<>();
		try {
			Map<String, Object> response = OAuthUtils
					.getProtectedResource(GET_EXAM_ANS_SHEET + "?student_id=" + student_id + "&batch_id=" + batch_id);
			JSONArray dataStr = (JSONArray) response.get("data");

			answerSheets = mapper.readValue(dataStr.toJSONString(),
					mapper.getTypeFactory().constructCollectionType(List.class, AnswerSheet.class));
		} catch (Exception e) {
			logger.error("Failed to retrieve exam answer sheet");
		}
		return answerSheets;
	}

	public static List<AnswerSheet> getMockExamAnswerSheet(long student_id, long batch_id) {
		List<AnswerSheet> answerSheets = new ArrayList<>();
		try {
			Map<String, Object> response = OAuthUtils.getProtectedResource(
					GET_MOCK_EXAM_ANS_SHEET + "?student_id=" + student_id + "&batch_id=" + batch_id);
			JSONArray dataStr = (JSONArray) response.get("data");

			answerSheets = mapper.readValue(dataStr.toJSONString(),
					mapper.getTypeFactory().constructCollectionType(List.class, AnswerSheet.class));
		} catch (Exception e) {
			logger.error("Failed to retrieve mock exam answer sheet");
		}
		return answerSheets;
	}

}
