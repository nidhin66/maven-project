package com.gbs.grandmaster.ui.page;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.model.Question;

import javafx.geometry.Insets;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class SectionQuestionPage extends BorderPane{
	
	private ExamSection examSection;
	private Map<Integer, SectionQuestionButton> buttonMap = null;
	private ExamSectionPane sectionPane;
	
	public SectionQuestionPage(ExamSectionPane sectionPane) {
		this.sectionPane = sectionPane;
		this.examSection = sectionPane.getExamSection();
		buttonMap = new TreeMap<>();
		createButtonPane();
	}
	
	
	private void createButtonPane() {
		int i =0;
		for(Question question : examSection.getQuestions()) {
			buttonMap.put(i, new SectionQuestionButton(this, question,i));
			i++;
		}		
		ScrollPane scrolPane = new ScrollPane();
		GridPane buttonGrid = new GridPane();
		buttonGrid.setPadding(new Insets(20,20,20,20));
		buttonGrid.setStyle("-fx-background-color: white;");
		buttonGrid.setHgap(15);
		buttonGrid.setVgap(20);
		
		int row = -1;
		int col = 0;
		int j =0;
		for(Iterator<Integer> it = buttonMap.keySet().iterator(); it.hasNext();) {
			SectionQuestionButton btn  = buttonMap.get(it.next());
			if(j%3 == 0) {
				row++;
				col=0;
			}
			buttonGrid.add(btn, col,row );
			j++;
			col++;
		}
		scrolPane.setContent(buttonGrid);
		setLeft(scrolPane);
//		buttonMap.get(1).fire();
		
	}
	
	public void setQuestion(Question question,int index) {
		setCenter(null);
		QuestionPage questionPage =	new QuestionPage(examSection, question,index);
		setCenter(questionPage);
		for(Iterator<Integer> it = buttonMap.keySet().iterator(); it.hasNext();) {
			SectionQuestionButton questionBtn = buttonMap.get(it.next());
			questionBtn.refreshButton();
		}
		
	}


	public ExamSectionPane getSectionPane() {
		return sectionPane;
	}

}
