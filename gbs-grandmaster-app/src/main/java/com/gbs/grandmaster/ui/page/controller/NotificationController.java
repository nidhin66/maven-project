package com.gbs.grandmaster.ui.page.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;

import com.gbs.grandmaster.ui.model.Notification;
import com.gbs.grandmaster.ui.oauth.OAuthUtils;


public class NotificationController {
	
	private static String LIST_NOTFICATION_URL = "/listnotification";
	static Logger logger = Logger.getLogger(NotificationController.class);
	
	public static List<Notification> listNotifications() {
		List<Notification> notifications = new ArrayList<>();
		try {
		Map<String, Object> response =	OAuthUtils.getProtectedResource(LIST_NOTFICATION_URL);
		JSONArray dataStr =  (JSONArray) response.get("data");	
		ObjectMapper mapper = new ObjectMapper();	
		notifications = mapper.readValue(dataStr.toJSONString(), mapper.getTypeFactory().constructCollectionType(List.class, Notification.class));
		} catch (Exception e) {
			logger.error("Exception" + "while getting list of notifications :" + e);
		}
		return notifications;
		
	}

}
