package com.gbs.grandmaster.ui.page;

import java.util.List;

import com.gbs.grandmaster.ui.CommonUtilitties;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.model.ATTENDANCE_STATUS;
import com.gbs.grandmaster.ui.model.Answer;
import com.gbs.grandmaster.ui.model.ExamSection;
import com.gbs.grandmaster.ui.page.action.ExamSectionBoxListner;
import com.gbs.grandmaster.ui.page.controller.ExamController;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * @author gbs
 *
 */
public class ExamSectionBox extends StackPane{
	
	private ExamSection section;
	private ExamSectionPane examSectionPane ;
	private VBox infoBox;
	private Label nameLabel;
	private Label questionLabel;
	private Label timeLabel;
	private Label seqLabel;
	private HBox sectionBox;
	private Button sectionBtn;
	private boolean recovered = false;
	private boolean followSequnce = true;
	
	public ExamSectionBox(ExamSection section,boolean followSequnce) {
		this.section = section;
		this.followSequnce = followSequnce;
		
		infoBox = new VBox();
		
		nameLabel = new  Label(section.getSectionName());
		nameLabel.getStyleClass().add("section-box-name-lbl");
		
		questionLabel = new Label("QTN - "+section.getQuestionCount()+" | ANS - 0");
		questionLabel.getStyleClass().add("section-box-info-lbl");
		
		timeLabel = new Label(section.getDurationInMin() + " Min");
		timeLabel.getStyleClass().add("section-box-info-lbl");
		
		nameLabel.setMouseTransparent(true);
		questionLabel.setMouseTransparent(true);
		timeLabel.setMouseTransparent(true);
		
		infoBox.getChildren().addAll(nameLabel,questionLabel,timeLabel);
		infoBox.setPadding(new Insets(0,0,5,15));
		infoBox.setMouseTransparent(true);
		infoBox.getStyleClass().add("section-box-info-box");
		
		
		seqLabel = new Label(String.format("%02d",section.getSequnce()));
		seqLabel.getStyleClass().add("section-box-seq-lbl");
		seqLabel.setPrefSize(50, 60);
		seqLabel.setMinSize(50, 60);
		seqLabel.setPadding(new Insets(0, 0, 0, 15));
		seqLabel.setMouseTransparent(true);

		
		sectionBox = new HBox();
		sectionBox.setSpacing(1);
		sectionBox.setAlignment(Pos.TOP_LEFT);
		sectionBox.getChildren().addAll(seqLabel,infoBox);
		sectionBox.getStyleClass().add("inactive-section-box");


		sectionBox.setMouseTransparent(true);
		
		sectionBtn = new Button();
		sectionBtn.getStyleClass().add("section-btn");
		sectionBtn.setOnAction(new ExamSectionBoxListner(this));
		if(followSequnce) {
			sectionBtn.setDisable(true);
		}		
		sectionBtn.setPrefSize(300, sectionBox.getHeight());
		
		
		
		getChildren().addAll(sectionBtn,sectionBox);
		setMaxHeight(100);
		
	}
	
	
	
	
	public void showExamSection() {
//		sectionBtn.setDisable(false);
		if(null == examSectionPane) {
			examSectionPane = new ExamSectionPane(this,section);
		}		
		UIManager.getInstance().getCurrentExamPage().setCenter(null);
		UIManager.getInstance().getCurrentExamPage().setCenter(examSectionPane);
		if(!followSequnce) {
			UIManager.getInstance().getCurrentExamPage().setAllSectionInactive();
		}
		sectionBox.getStyleClass().remove("inactive-section-box");						
		sectionBox.getStyleClass().add("active-section-box");
		examSectionPane.startSection();
	}
	
	public void finishExamSection() {		
		examSectionPane.setStatus(ATTENDANCE_STATUS.FINISHED);		
		int answerCount = 0;
		List<Answer> answers = examSectionPane.getAnswers();
		for(Answer answer : answers) {
			if(!CommonUtilitties.isNullorEmpty(answer.getContent())) {
				answerCount++;
			}
		}		
		ExamController.saveAnswers(answers);
		examSectionPane.setStatus(ATTENDANCE_STATUS.SUBMITTED);
		setAnswerCountLableTxt(answerCount);
		sectionBox.getStyleClass().remove("active-section-box");
		sectionBox.getStyleClass().add("completed-section-box");

	}
	
	public void setAnswerCountLableTxt(int answerCount) {
		questionLabel.setText("QTN - "+section.getQuestionCount()+" | ANS - "+answerCount);
	}
	
	public String getStatusString() {
		if(null!=examSectionPane) {
			return examSectionPane.getStatusString();
		}else {
			return "";
		}
	}
	
	public long getElapsedTime() {
		return examSectionPane.getElapsedTime();
	}
	
	public ExamSection getSection() {
		return section;
	}

  


	public void recoverSection(long timeElapsed,ATTENDANCE_STATUS status) {
		recovered = true;
		if(null == examSectionPane) {
			examSectionPane = new ExamSectionPane(this,section);
		}	
		examSectionPane.recoverSection(timeElapsed,status);
		int answerCount = 0;
		List<Answer> answers = examSectionPane.getAnswers();
		for(Answer answer : answers) {
			if(!CommonUtilitties.isNullorEmpty(answer.getContent())) {
				answerCount++;
			}
		}
		if(status == ATTENDANCE_STATUS.SUBMITTED) {
			questionLabel.setText("QTN - "+section.getQuestionCount()+" | ANS - "+answerCount);
			sectionBox.getStyleClass().remove("inactive-section-box");
			sectionBox.getStyleClass().add("completed-section-box");
		}
	}
	
	public void refresh() {
		examSectionPane.refresh();
	}




	public void setFollowSequnce(boolean followSequnce) {
		this.followSequnce = followSequnce;
	}




	public boolean isFollowSequnce() {
		return followSequnce;
	}
	
	public void setAsInative() {
		sectionBox.getStyleClass().clear();
		sectionBox.getStyleClass().add("inactive-section-box");
	}
	
	

}
