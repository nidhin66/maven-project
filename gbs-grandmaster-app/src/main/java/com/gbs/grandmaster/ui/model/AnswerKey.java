package com.gbs.grandmaster.ui.model;

public class AnswerKey {

	private long studentId;

	private long examScheduleId;

	private int attempt;

	private long questionPaperId;

	private long questionId;

	public AnswerKey() {

	}

	public AnswerKey(long studentId, long examScheduleId, int attempt, long questionPaperId, long questionId) {
		this.studentId = studentId;
		this.examScheduleId = examScheduleId;
		this.attempt = attempt;
		this.questionPaperId = questionPaperId;
		this.questionId = questionId;
	}

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}

	public long getExamScheduleId() {
		return examScheduleId;
	}

	public void setExamScheduleId(long examScheduleId) {
		this.examScheduleId = examScheduleId;
	}

	public int getAttempt() {
		return attempt;
	}

	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}

	public long getQuestionPaperId() {
		return questionPaperId;
	}

	public void setQuestionPaperId(long questionPaperId) {
		this.questionPaperId = questionPaperId;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

}
