package com.gbs.grandmaster.ui.model;

public enum QUESTION_STATUS {
	NOTANSWERED("N"), ANSWERED("A"), MARKED("M"), MARKED_ANSWERED("MA");
	private final String code;

	private QUESTION_STATUS(String code) {
		this.code = code;
	}

}
