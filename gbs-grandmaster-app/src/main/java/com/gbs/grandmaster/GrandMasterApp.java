package com.gbs.grandmaster;


import com.gbs.grandmaster.ui.Constants;
import com.gbs.grandmaster.ui.LabelAndMessageUtil;
import com.gbs.grandmaster.ui.UIManager;
import com.gbs.grandmaster.ui.UIUtilities;
import com.gbs.grandmaster.ui.page.LoginWindow;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import org.apache.log4j.Logger;



public class GrandMasterApp extends Application {
	

    static Logger logger = Logger.getLogger(GrandMasterApp.class);


    @Override
    public void start(Stage stage) throws Exception {
    	
    	   	
    	Font.loadFont(ClassLoader.getSystemResourceAsStream("styles/fonts/BebasNeueBold.ttf"), 10);
    	
    	UIManager.getInstance().setPrimaryStage(stage);

        //set Stage boundaries to visible bounds of the main screen
       
    	UIUtilities.positionAndSizeWindow(stage,70,80);
    	
    	LoginWindow loginWindow = new LoginWindow();
       
        Scene scene = new Scene(loginWindow);
        scene.getStylesheets().add("/styles/Styles.css");       
        UIManager.getInstance().setApplicationTitle(LabelAndMessageUtil.getLabel(Constants.LBL_LOGIN_TITLE));
        stage.setScene(scene);
        stage.initStyle(StageStyle.DECORATED);
        stage.setResizable(false);
        stage.setOnHiding(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        UIManager.exitApplication(false);
                    }
                });
            }
        });
        stage.show();
        logger.info("GrandMaster Application Started");
    }

    
    public static void main(String[] args) {
        launch(args);
    }

}
