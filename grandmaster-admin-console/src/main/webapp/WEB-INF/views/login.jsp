<!--

Document Owned by GBS-PLUS Pvt Ltd
-----------------------------------------------------------
Document created by -   ID No: 20172017
Created Date        -   22 September 2017
Modified Date       -   22 September 2017

-->

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Directions</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/custom_ver2.0.css">


</head>


<body>


    <!--Content Area-->
    <section class="content_area">
        <div class="login_section">
            <div class="logo_wrap">
                <img src="images/logo.png" class="img-fluid" />
            </div>

            <div class="login-form ">
                <form method="post">
                    <div class="form-group">
                        <label>
                                Username
                            </label>
                        <input type="text" name="name" class="form-control" />

                    </div>
                    <div class="form-group">
                        <label>
                                Password
                            </label>
                        <input type="password"  name="password"  class="form-control" />
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" text="Login"/>
                        <button class="btn btn-warning">Cancel</button>
                    </div>

                </form>
            </div>
            <div class="copyright">
                2017 &copy;Direction. All rights reserved.
                <br> Powered by GBS - PLUS
            </div>

        </div>


        <div class="wall_section" style="background-image: url(images/login.jpg)">


        </div>
    </section>
    <!--Content Area-->

    <script src="js/jquery-1.11.3.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/all.js" type="text/javascript"></script>
</body>

</html>
