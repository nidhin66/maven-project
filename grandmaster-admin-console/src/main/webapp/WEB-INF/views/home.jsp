<!DOCTYPE html>
<html lang="en">

<head>
    <title>Direction</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/custom_ver2.0.css" />
</head>

<body>
    <!-- Top header section starts here -->
    <div class="container-fluid header_wrapper">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="row">
                    <button class="btn btn-default ham_navigation">
                            <i class="fa fa-bars"></i>
                        </button>
                    <div class="logo_wrapper">
                        <!--                        <img src="images/logo_inner.png" class="img-fluid" />-->
                        Direction
                    </div>
                </div>
            </div>

            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 pr-0">

                <button class="btn btn-info logout_btn float-right r_separation">
                        <i class="fa fa-power-off"></i>
                    </button>

                <div class="login_details float-right ">
                    Hello, Nitheesh Chandran
                </div>


            </div>
        </div>
    </div>

    <!-- Top header section ends here -->

    <!-- Navigation Section Starts here -->
    <div class="container-fluid main_nav">

        <button class="btn btn-default nav_close">
            <i class="fa fa-times"></i>
        </button>
        <h3 class="main_nav_h3">
            Direction
            <small>
                The Website Management
            </small>
        </h3>
        <ul class="main_nav_ul flex-wrap">

            <li class="col-3">
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Dashboard</a>
                <a href="">
                    <i class="fa fa-book"></i>
                    Subject</a>
                <a href="">
                    <i class="fa fa-users"></i>
                    Hostel</a>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Fees Management</a>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Notification</a>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Admission</a>
                <ul>
                    <li>
                        List/Add</li>
                    <li>Reports</li>
                </ul>
            </li>
            <li class="col-3">

                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Parents</a>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    RFID</a>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Biometric Reader</a>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Extra Curricular Activities</a>
                <a href="">
                    <i class="fa fa-sitemap"></i>
                    Class</a>
                <ul>
                    <li>Academic Syllabus
                    </li>
                    <li>Manage Classes
                    </li>
                </ul>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Daily Attendance</a>
                <ul>
                    <li>Daily Attendance</li>
                    <li>Attendance Report</li>
                </ul>
            </li>

            <li class="col-3">
                <a href="">
                    <i class="fa fa-users"></i>Teacher</a>
                <ul>
                    <li>Teacher
                    </li>
                    <li>Daily Attendance
                    </li>
                    <li>Leave Request
                    </li>
                    <li>Report
                    </li>
                    <li>Attendance Report
                    </li>
                </ul>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Conveyance</a>
                <ul>
                    <li>Driver</li>
                    <li>Bus</li>
                    <li>Conveyance</li>
                </ul>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Student</a>
                <ul>
                    <li>
                        Admit Student</li>
                    <li>Admint Bulk Student</li>
                    <li>Student Information</li>
                    <li>Student Promotion</li>
                    <li>Report</li>
                </ul>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Time Table Management</a>
                <ul>
                    <li>Add Period</li>
                    <li>Subject Plan</li>
                    <li>Assign Period</li>
                </ul>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Certificate</a>
                <ul>
                    <li>Certificate Types</li>
                    <li>Certificate management</li>
                    <li>Generate Certificate</li>
                </ul>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Library</a>
                <ul>
                    <li>Book</li>
                    <li>Report</li>
                </ul>

            </li>

            <li class="col-3">
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Payments</a>
                <ul>
                    <li>Create Students Payment</li>
                    <li>Students Payments</li>
                </ul>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Accounts</a>
                <ul>
                    <li>List All Journal</li>
                    <li>Add new journal</li>
                    <li>A/C Head List</li>
                    <li>Add new A/C head</li>
                    <li>Money Reciept list</li>
                    <li>Add new money reciept</li>
                    <li>payment reciept list</li>
                    <li>add new payment reciept</li>
                </ul>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Inventory</a>
                <ul>
                    <li>List sales</li>
                    <li>List sales return</li>
                    <li>List purchase</li>
                    <li>List purchase return</li>
                    <li>List customers</li>
                    <li>List suplier</li>
                    <li>List item</li>
                </ul>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Exam</a>
                <ul>
                    <li>Exam List</li>
                    <li>Exam Grades</li>
                    <li>Manage Marks</li>
                    <li>Send Marks By SMS</li>
                    <li>Question Paper</li>
                </ul>
                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    User Management</a>
                <ul>
                    <li>User Role</li>
                    <li>User Management</li>
                    <li>User Permission</li>
                </ul>

                <a href="">
                    <i class="fa fa-chart-pie"></i>
                    Settings</a>
                <ul>
                    <li>General Settings</li>
                    <li>Configuration
                    </li>
                    <li>Id Card Creator</li>
                    <li>Add Year</li>
                    <li>Add State</li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- Navigation Section Starts here -->

    <!-- Tab section Starts here -->

    <div class="container-fluid">
        <div class="row header_tab ">
            <div class="col">
                <img src="images/student.svg" class="img-responsive" />
                <span>Student Info</span>
            </div>
            <div class="col active">
                <img src="images/teacher.svg" class="img-responsive" />
                <span>Teachers Info</span>
            </div>
            <div class="col">
                <img src="images/payment.svg" class="img-responsive" />
                <span>Payment Info</span>
            </div>
            <div class="col">
                <img src="images/calendar.svg" class="img-responsive" />
                <span>Calendar</span></div>
            <div class="col">
                <img src="images/events.svg" class="img-responsive" />
                <span>Events</span></div>
            <div class="col">
                <img src="images/timetable.svg" class="img-responsive" />
                <span>Time Table</span></div>
            <div class="col">
                <img src="images/class.svg" class="img-responsive" />
                <span>Class Info</span></div>
            <div class="col">
                <img src="images/leave.svg" class="img-responsive" />
                <span>Leave Info</span></div>
        </div>
    </div>
    <!-- Tab section Ends here -->

    <div class="base">
        <!-- breadcrumbs section starts here -->
        <div class="container-fluid">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Dashboard</a>
                </li>

                <li class="breadcrumb-item active">Teachers Info</li>
            </ol>
        </div>
        <!-- breadcrumbs section Ends here -->

        <!-- Content Section starts here -->
        <div class="container-fluid">
            <div class="row">
                <div class="sub_nav_wrapper relative col-lg-2 col-md-3 col-sm-3 col-xs-12">
                    <div class=" card_wrapper ">
                        <ul class="sub_navigation ">
                            <li class="active ">
                                <a href="# ">Teachers</a>
                            </li>
                            <li>
                                <a href="# ">Daily Attendance</a>
                            </li>
                            <li>
                                <a href="# ">Leave Request</a>
                            </li>
                            <li>
                                <a href="# ">Report</a>
                            </li>
                            <li>
                                <a href="# ">Attendance report</a>
                            </li>
                        </ul>

                    </div>
                </div>
                <div class="content_wrapper relative col-lg-10 col-md-9 col-sm-9 col-xs-12 ">
                    <div class="transparent_card ">
                        <!-- Data Table Plugin Section Starts Here -->
                        <div class="row ">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                <button class="btn btn-default pagination_nav ">
                                    <i class="fa fa-caret-left "></i>
                                </button>
                                <button class="btn btn-default pagination_nav ">
                                    <i class="fa fa-caret-right "></i>
                                </button>
                                <span class="number_records ">
                                    Showing 1-10 of 103 records.
                                </span>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right flex-row-reverse ">
                                <button class="btn btn-default option_btn list_option ">
                                    <i class="fa fa-th-list "></i>
                                </button>
                                <button class="btn btn-default option_btn grid_option ">
                                    <i class="fa fa-th-large "></i>
                                </button>
                                <button class="btn btn-default option_btn filter_btn">
                                    <i class="fa fa-sliders"></i>
                                </button>
                                <button class="btn btn-default add_row add_new_btn">
                                    <i class="fa fa-plus "></i>
                                </button>
                                <div class="form_box add_new">
                                    <div class="form_head">
                                        Add New
                                    </div>
                                    <div class="form_body">
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="form-group"><label>Service Name</label><input class="form-control" type="text"></div>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="form-group"><label>Short Name</label><input class="form-control" type="text"></div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">

                                                <button class="btn btn-info">Save</button>


                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">

                                                <button class="btn btn-default">Cancel</button>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="form_box filter_new">
                                    <div class="form_head">
                                        Filter Option
                                    </div>
                                    <div class="form_body">
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="form-group"><label>Service Name</label><input class="form-control" type="text" /></div>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="form-group"><label>Short Name</label><input class="form-control" type="text" /></div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">

                                                <button class="btn btn-info">Filter</button>


                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">

                                                <button class="btn btn-default">Cancel</button>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="data_table ">
                            <li class="data_table_head ">
                                <div class="col sl_no ">Sl. No.
                                    <div class="sort_option ">
                                        <button class="btn btn-default sort_up ">
                                            <i class="fa fa-caret-left "></i>
                                        </button>
                                        <button class="btn btn-default sort_down ">
                                            <i class="fa fa-caret-right "></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col avatar ">Avatar</div>
                                <div class="col ">Name of Teacher
                                    <div class="sort_option ">
                                        <button class="btn btn-default sort_up ">
                                            <i class="fa fa-caret-left "></i>
                                        </button>
                                        <button class="btn btn-default sort_down ">
                                            <i class="fa fa-caret-right "></i>
                                        </button>
                                    </div>

                                </div>
                                <div class="col ">Email Address
                                    <div class="sort_option ">
                                        <button class="btn btn-default sort_up ">
                                            <i class="fa fa-caret-left "></i>
                                        </button>
                                        <button class="btn btn-default sort_down ">
                                            <i class="fa fa-caret-right "></i>
                                        </button>
                                    </div>

                                </div>
                                <div class="col ">Today's Attendance
                                    <div class="sort_option ">
                                        <button class="btn btn-default sort_up ">
                                            <i class="fa fa-caret-left "></i>
                                        </button>
                                        <button class="btn btn-default sort_down ">
                                            <i class="fa fa-caret-right "></i>
                                        </button>
                                    </div>

                                </div>
                                <div class="col actions ">Actions</div>
                            </li>
                            <li>
                                <div class="col sl_no ">1</div>
                                <div class="col avatar "><img src="images/avatar.svg " class="img-responsive " /></div>
                                <div class="col ">Daliya Susan George</div>
                                <div class="col ">daliyasusan007@gmail.com</div>
                                <div class="col ">
                                    <i class="far present-circle fa-check-circle "></i> Present
                                </div>
                                <div class="col actions ">
                                    <button class="btn btn-default view_btn ">VIEW</button>
                                    <div class="dropdown ">
                                        <button class="action_btn dropdown-toggle " data-toggle="dropdown ">
                                            <i class="fa fa-ellipsis-h "></i>
                                        </button>

                                        <div class="dropdown-menu ">
                                            <a class="dropdown-item " href="# ">
                                                <i class="fa fa-exclamation-circle "></i>
                                                Leave Taken</a>
                                            <a class="dropdown-item " href="# ">
                                                <i class="fa fa-pen-square "></i>
                                                Edit</a>
                                            <a class="dropdown-item " href="# ">
                                                <i class="fa fa-trash "></i>
                                                Delete</a>
                                        </div>
                                    </div>

                                </div>
                            </li>
                            <li>
                                <div class="col sl_no ">2</div>
                                <div class="col avatar "><img src="images/avatar.svg " class="img-responsive " /></div>
                                <div class="col ">Daliya Susan George</div>
                                <div class="col ">daliyasusan007@gmail.com</div>
                                <div class="col ">Present</div>
                                <div class="col actions ">--</div>
                            </li>
                            <li>
                                <div class="col sl_no ">3</div>
                                <div class="col avatar "><img src="images/avatar.svg " class="img-responsive " /></div>
                                <div class="col ">Daliya Susan George</div>
                                <div class="col ">daliyasusan007@gmail.com</div>
                                <div class="col ">Present</div>
                                <div class="col actions ">--</div>
                            </li>
                            <li>
                                <div class="col sl_no ">4</div>
                                <div class="col avatar "><img src="images/avatar.svg " class="img-responsive " /></div>
                                <div class="col ">Daliya Susan George</div>
                                <div class="col ">daliyasusan007@gmail.com</div>
                                <div class="col ">Present</div>
                                <div class="col actions ">--</div>
                            </li>
                            <li>
                                <div class="col sl_no ">5</div>
                                <div class="col avatar "><img src="images/avatar.svg " class="img-responsive " /></div>
                                <div class="col ">Daliya Susan George</div>
                                <div class="col ">daliyasusan007@gmail.com</div>
                                <div class="col ">Present</div>
                                <div class="col actions ">--</div>
                            </li>
                            <li>
                                <div class="col sl_no ">6</div>
                                <div class="col avatar "><img src="images/avatar.svg " class="img-responsive " /></div>
                                <div class="col ">Daliya Susan George</div>
                                <div class="col ">daliyasusan007@gmail.com</div>
                                <div class="col ">Present</div>
                                <div class="col actions ">--</div>
                            </li>
                            <li>
                                <div class="col sl_no ">7</div>
                                <div class="col avatar "><img src="images/avatar.svg " class="img-responsive " /></div>
                                <div class="col ">Daliya Susan George</div>
                                <div class="col ">daliyasusan007@gmail.com</div>
                                <div class="col ">Present</div>
                                <div class="col actions ">--</div>
                            </li>
                            <li>
                                <div class="col sl_no ">8</div>
                                <div class="col avatar "><img src="images/avatar.svg " class="img-responsive " /></div>
                                <div class="col ">Daliya Susan George</div>
                                <div class="col ">daliyasusan007@gmail.com</div>
                                <div class="col ">Present</div>
                                <div class="col actions ">--</div>
                            </li>
                            <li>
                                <div class="col sl_no ">9</div>
                                <div class="col avatar "><img src="images/avatar.svg " class="img-responsive " /></div>
                                <div class="col ">Daliya Susan George</div>
                                <div class="col ">daliyasusan007@gmail.com</div>
                                <div class="col ">Present</div>
                                <div class="col actions ">--</div>
                            </li>
                            <li>
                                <div class="col sl_no ">10</div>
                                <div class="col avatar "><img src="images/avatar.svg " class="img-responsive " /></div>
                                <div class="col ">Daliya Susan George</div>
                                <div class="col ">daliyasusan007@gmail.com</div>
                                <div class="col ">Present</div>
                                <div class="col actions ">--</div>
                            </li>
                        </ul>
                        <!-- Data Table Plugin Section Starts Here -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Content Section Ends here -->
    </div>

    <!-- Script Section Starts here -->
    <script src="js/jquery-3.2.1.slim.min.js "></script>
    <script src="js/popper.min.js "></script>
    <script src="js/bootstrap.min.js "></script>
    <script src="js/custom.js "></script>
    <!-- Script Section Ends here -->

</body>

</html>
