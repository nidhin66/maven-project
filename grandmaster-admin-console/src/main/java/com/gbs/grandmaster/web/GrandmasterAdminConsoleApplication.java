package com.gbs.grandmaster.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.gbs.grandmaster.web")
public class GrandmasterAdminConsoleApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrandmasterAdminConsoleApplication.class, args);
	}
}
