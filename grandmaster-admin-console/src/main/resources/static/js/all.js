$(document).ready(function () {
    $('#owl-carousel1').owlCarousel({
        loop: false,
        margin: 10,
        responsiveClass: true,
        autoplay: true,
        responsive: {
            0: {
                items: 1,
                nav: false,
                dots: false
            }
        }
    })
    $('#owl-carousel2').owlCarousel({
        animateOut: true,
        animateIn: true,
        loop: false,
        margin: 10,
        responsiveClass: true,
        autoplay: true,


        responsive: {
            0: {
                items: 1,
                nav: false,
                dots: false
            }
        }
    })
    $('#mycalendar').monthly({
        mode: 'event',
        dataType: 'json',
        events: sampleEvents
    });


})


var sampleEvents = {
    "monthly": [
        {
            "id": 1,
            "name": "Whole month event",
            "startdate": "2018-04-01",
            //            "enddate": "2018-04-05",
            //            "starttime": "12:00",
            //            "endtime": "2:00",

            "url": ""
		},
        {
            "id": 2,
            "name": "Test encompasses month",
            "startdate": "2018-04-29",
            //            "enddate": "2018-05-02",
            //            "starttime": "12:00",
            //            "endtime": "2:00",
            //            "color": "#CC99CC",
            //            "url": ""
		},
        {
            "id": 3,
            "name": "Test single day",
            "startdate": "2018-04-04",
            "enddate": "",
            "starttime": "",
            "endtime": "",
            //            "color": "#666699",
            "url": "https://www.google.com/"
		},
        {
            "id": 8,
            "name": "Test single day",
            "startdate": "2018-05-05",
            "enddate": "",
            "starttime": "",
            "endtime": "",
            //            "color": "#666699",
            "url": "https://www.google.com/"
		},
        {
            "id": 4,
            "name": "Test single day with time",
            "startdate": "2018-05-07",
            "enddate": "",
            "starttime": "12:00",
            "endtime": "02:00",
            //            "color": "#996666",
            "url": ""
		},
        {
            "id": 5,
            "name": "Test splits month",
            "startdate": "2018-05-25",
            "enddate": "2018-05-04",
            "starttime": "",
            "endtime": "",
            //            "color": "#999999",
            "url": ""
		},
        {
            "id": 6,
            "name": "Test events on same day",
            "startdate": "2018-05-25",
            "enddate": "",
            "starttime": "",
            "endtime": "",
            //            "color": "#99CC99",
            "url": ""
		},
        {
            "id": 7,
            "name": "Test events on same day",
            "startdate": "2018-05-25",
            "enddate": "",
            "starttime": "",
            "endtime": "",
            //            "color": "#669966",
            "url": ""
		},
        {
            "id": 9,
            "name": "Test events on same day",
            "startdate": "2018-05-25",
            "enddate": "",
            "starttime": "",
            "endtime": "",
            //            "color": "#999966",
            "url": ""
		}
	]
};
