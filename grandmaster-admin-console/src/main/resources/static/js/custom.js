$(document).ready(function () {
    $(".add_new_btn").click(function () {

        $(".add_new").toggle();
    });
    $(".filter_btn").click(function () {

        $(".filter_new").toggle();
    });
    $(".ham_navigation").click(function () {
        $(".main_nav").addClass("show");
    });
    $(".nav_close").click(function () {
        $(".main_nav").removeClass("show");
    });
    $(".grid_option").click(function () {
        $(".data_table").css("display", "flex")
        $(".data_table").addClass("grid flex-wrap");
    });
    $(".list_option").click(function () {
        $(".data_table").css("display", "block")
        $(".data_table").removeClass("grid flex-wrap");
    });

});
