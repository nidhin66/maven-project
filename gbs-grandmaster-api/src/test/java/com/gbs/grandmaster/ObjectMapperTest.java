package com.gbs.grandmaster;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gbs.grandmaster.entity.Institute;

public class ObjectMapperTest {
	
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		String jsonString = "{\"success\":true,\"data\":{\"id\":1,\"version\":1,\"name\":\"Global Business Solutions\"},\"errors\":null}";
		ObjectMapper objectMapper  = new ObjectMapper();
		objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		JsonNode actualObj = objectMapper.readTree(jsonString);
//		Response response = objectMapper.readValue(jsonString, Response.class); 
		
		Institute institute = objectMapper.readValue(actualObj.get("data").toString(), Institute.class); 
		System.out.println(institute.getName());
	}

}
