package com.gbs.grandmaster;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gbs.grandmaster.api.ExamSectionConfigRestController;
import com.gbs.grandmaster.entity.ExamSectionConfig;
import com.gbs.grandmaster.entity.ExamSectionDetails;

@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = GrandmasterApiApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@FixMethodOrder(MethodSorters.DEFAULT)
public class ExamSectionConfigRestControllerTest {


	@Autowired
	WebApplicationContext context;

	@Autowired
	private FilterChainProxy springSecurityFilterChain;

	private MockMvc mvc;

	@InjectMocks
	ExamSectionConfigRestController controller;
	String accessToken = null;
	String contentType = MediaType.APPLICATION_JSON + ";charset=UTF-8";
	@Autowired
	ObjectMapper objectMapper;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mvc = MockMvcBuilders.webAppContextSetup(context).addFilter(springSecurityFilterChain).build();
		accessToken = getAccessToken("Alex123", "password");
	}

	

	private String getAccessToken(String username, String password) throws Exception {
		String authorization = "Basic "
				+ new String(Base64Utils.encode("grandmaster-client:grandmaster-secret".getBytes()));

		// @formatter:off
		String content = mvc
				.perform(post("/oauth/token").header("Authorization", authorization)
						.contentType(MediaType.APPLICATION_FORM_URLENCODED).param("username", username)
						.param("password", password).param("grant_type", "password")
						// .param("scope", "read write trust")
						.param("client_id", "grandmaster-client").param("client_secret", "grandmaster-secret"))
				.andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.access_token", is(notNullValue())))
				.andExpect(jsonPath("$.token_type", is(equalTo("bearer"))))
				.andExpect(jsonPath("$.refresh_token", is(notNullValue())))
				.andExpect(jsonPath("$.expires_in", is(greaterThan(3000))))
				.andExpect(jsonPath("$.scope", is(equalTo("read write trust")))).andReturn().getResponse()
				.getContentAsString();

		// @formatter:on

		return content.substring(17, 53);
	}


	
	@Test
	public void testSaveExamSectionConfigAuthorized() throws Exception {
		ExamSectionConfig examSectionConfig = new ExamSectionConfig();
		ExamSectionDetails examSectionDetails=new ExamSectionDetails();
		examSectionDetails .setId((long) 1);
		Set<ExamSectionDetails> examSectionDetails2 =  new HashSet<>();
		examSectionDetails2.add(examSectionDetails);
		examSectionConfig.setDetails(examSectionDetails2);
		examSectionConfig.setDurationInMin(2);
		examSectionConfig.setSectionName("English");
		examSectionConfig.setSequnce(1);
		mvc.perform(post("/saveExamSectionConfig").header("Authorization", "Bearer " + accessToken)
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(examSectionConfig)))
				.andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.id", is(greaterThan(0))));
	}
	
	@Test
	public void testSaveExamSectionConfigUnAuthorized() throws Exception {
		ExamSectionConfig examSectionConfig = new ExamSectionConfig();
		ExamSectionDetails examSectionDetails=new ExamSectionDetails();
		examSectionDetails .setId((long) 1);
		Set<ExamSectionDetails> examSectionDetails2 = new HashSet<>();
		examSectionDetails2.add(examSectionDetails);
		examSectionConfig.setDetails(examSectionDetails2);
		examSectionConfig.setDurationInMin(2);
		examSectionConfig.setSectionName("English");
		examSectionConfig.setSequnce(1);
		mvc.perform(post("/saveExamSectionConfig").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
		.andExpect(jsonPath("$.error", is("unauthorized")));
	}
	
	@Test
	public void testEditExamSectionConfigAuthorized() throws Exception {

		mvc.perform(get("/editExamSectionConfig/1").header("Authorization", "Bearer " + accessToken)).andExpect(status().isOk())
				.andExpect(content().contentType(contentType));
	}
	
	@Test
	public void testEditExamSectionConfigUnAuthorized() throws Exception {

		mvc.perform(get("/editExamSectionConfig/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
		.andExpect(jsonPath("$.error", is("unauthorized")));
	}
	@Test
	public void testUpdateExamSectionConfigAuthorized() throws Exception {
		ExamSectionConfig examSectionConfig = new ExamSectionConfig();
		examSectionConfig.setId((long) 1);
		examSectionConfig.setVersion(0);
		ExamSectionDetails examSectionDetails=new ExamSectionDetails();
		examSectionDetails .setId((long) 1);
		Set<ExamSectionDetails> examSectionDetails2 = new HashSet<>();
		examSectionDetails2.add(examSectionDetails);
		examSectionConfig.setDetails(examSectionDetails2);
		examSectionConfig.setDurationInMin(5);
		examSectionConfig.setSectionName("English Section");
		examSectionConfig.setSequnce(1);
		mvc.perform(put("/updateExamSectionConfig").header("Authorization", "Bearer " + accessToken)
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(examSectionConfig)))
				.andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.id", is(greaterThan(0))));
	}

	@Test
	public void testUpdateExamSectionConfigUnAuthorized() throws Exception {
		ExamSectionConfig examSectionConfig = new ExamSectionConfig();
		examSectionConfig.setId((long) 1);
		examSectionConfig.setVersion(0);
		ExamSectionDetails examSectionDetails=new ExamSectionDetails();
		examSectionDetails .setId((long) 1);
		Set<ExamSectionDetails> examSectionDetails2 = new HashSet<>();
		examSectionDetails2.add(examSectionDetails);
		examSectionConfig.setDetails(examSectionDetails2);
		examSectionConfig.setDurationInMin(5);
		examSectionConfig.setSectionName("English Section");
		examSectionConfig.setSequnce(1);
		mvc.perform(put("/updateExamSectionConfig").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
		.andExpect(jsonPath("$.error", is("unauthorized")));
	}

	
	/*@Test
	public void testDeleteExamSectionConfigAuthorized() throws Exception {

		mvc.perform(get("/deleteExamSectionConfig/1").header("Authorization", "Bearer " + accessToken)).andExpect(status().isOk())
				.andExpect(content().contentType(contentType));
	}
	
	@Test
	public void testDeleteExamSectionConfigUnAuthorized() throws Exception {

		mvc.perform(get("/deleteExamSectionConfig/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
		.andExpect(jsonPath("$.error", is("unauthorized")));
	}*/
}
