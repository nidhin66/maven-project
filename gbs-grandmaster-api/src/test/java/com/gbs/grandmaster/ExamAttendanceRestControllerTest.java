package com.gbs.grandmaster;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;
import java.util.UUID;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

//import com.datastax.driver.core.utils.UUIDs;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gbs.grandmaster.api.ExamAttendanceRestController;
import com.gbs.grandmaster.common.entity.ATTENDANCE_STATUS;
import com.gbs.grandmaster.entity.cassandra.AttendanceKey;
import com.gbs.grandmaster.entity.cassandra.ExamAttendance;

@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = GrandmasterApiApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@FixMethodOrder(MethodSorters.DEFAULT)
public class ExamAttendanceRestControllerTest {

	@Autowired
	WebApplicationContext context;

	@Autowired
	private FilterChainProxy springSecurityFilterChain;

	private MockMvc mvc;

	@InjectMocks
	ExamAttendanceRestController controller;
	String accessToken = null;
	String contentType = MediaType.APPLICATION_JSON + ";charset=UTF-8";
	@Autowired
	ObjectMapper objectMapper;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mvc = MockMvcBuilders.webAppContextSetup(context).addFilter(springSecurityFilterChain).build();
		accessToken = getAccessToken("Alex123", "password");
	}


	private String getAccessToken(String username, String password) throws Exception {
		String authorization = "Basic "
				+ new String(Base64Utils.encode("grandmaster-client:grandmaster-secret".getBytes()));

		// @formatter:off
		String content = mvc
				.perform(post("/oauth/token").header("Authorization", authorization)
						.contentType(MediaType.APPLICATION_FORM_URLENCODED).param("username", username)
						.param("password", password).param("grant_type", "password")
						// .param("scope", "read write trust")
						.param("client_id", "grandmaster-client").param("client_secret", "grandmaster-secret"))
				.andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.access_token", is(notNullValue())))
				.andExpect(jsonPath("$.token_type", is(equalTo("bearer"))))
				.andExpect(jsonPath("$.refresh_token", is(notNullValue())))
				.andExpect(jsonPath("$.expires_in", is(greaterThan(3000))))
				.andExpect(jsonPath("$.scope", is(equalTo("read write trust")))).andReturn().getResponse()
				.getContentAsString();

		// @formatter:on

		return content.substring(17, 53);
	}

	
	@Test
	public void testSaveExamAttendanceAuthorized() throws Exception {
		ExamAttendance examAttendance = new ExamAttendance(new AttendanceKey(1, 1, 1));
//		examAttendance.setId(UUID.randomUUID());
//		examAttendance.setAttempt(0);
		@SuppressWarnings("deprecation")
		Date startdate=new Date("06/11/2018 05:20:30");
		@SuppressWarnings("deprecation")
		Date enddate=new Date("08/11/2018 05:20:30");
		examAttendance.setEndDateTime(enddate);
//		examAttendance.setexa
		examAttendance.setStartedDateTime(startdate);
		examAttendance.setStatus(ATTENDANCE_STATUS.STARTED);
//		examAttendance.setStudentId(1);
		mvc.perform(post("/saveExamAttendance").header("Authorization", "Bearer " + accessToken)
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(examAttendance)))
				.andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.id", is(greaterThan(0))));
	}
	
//	@Test
//	public void testSaveExamAttendanceUnAuthorized() throws Exception {
//		ExamAttendance examAttendance = new ExamAttendance();
//		examAttendance.setAttempt(0);
//		@SuppressWarnings("deprecation")
//		Date startdate=new Date("06/11/2018 05:20:30");
//		@SuppressWarnings("deprecation")
//		Date enddate=new Date("08/11/2018 05:20:30");
//		examAttendance.setEndDateTime(enddate);
//		examAttendance.setExamScheduleId(1);
//		examAttendance.setStartedDateTime(startdate);
//		examAttendance.setStatus(ATTENDANCE_STATUS.STARTED);
//		examAttendance.setStudentId(1);
//		mvc.perform(post("/saveExamAttendance").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
//		.andExpect(jsonPath("$.error", is("unauthorized")));
//	}
//	
//	@Test
//	public void testEditExamAttendanceAuthorized() throws Exception {
//
//		mvc.perform(get("/editExamAttendance/1").header("Authorization", "Bearer " + accessToken)).andExpect(status().isOk())
//				.andExpect(content().contentType(contentType));
//	}
//	
//	@Test
//	public void testEditExamAttendanceUnAuthorized() throws Exception {
//
//		mvc.perform(get("/editExamAttendance/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
//		.andExpect(jsonPath("$.error", is("unauthorized")));
//	}
//	@Test
//	public void testUpdateExamAttendanceAuthorized() throws Exception {
//		ExamAttendance examAttendance = new ExamAttendance();
//		examAttendance.setId(UUIDs.timeBased());
//		examAttendance.setVersion(0);
//		examAttendance.setAttempt(1);
//		
//		@SuppressWarnings("deprecation")
//		Date startdate=new Date("10/11/2018 05:20:30");
//		@SuppressWarnings("deprecation")
//		Date enddate=new Date("12/11/2018 05:20:30");
//		examAttendance.setEndDateTime(enddate);
//		examAttendance.setExamScheduleId(2);
//		examAttendance.setStartedDateTime(startdate);
//		examAttendance.setStatus(ATTENDANCE_STATUS.STARTED);
//		examAttendance.setStudentId(1);
//		mvc.perform(put("/updateExamAttendance").header("Authorization", "Bearer " + accessToken)
//				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(examAttendance)))
//				.andExpect(status().isOk()).andExpect(content().contentType(contentType))
//				.andExpect(jsonPath("$.id", is(greaterThan(0))));
//	}
//	@Test
//	public void testUpdateExamAttendanceUnAuthorized() throws Exception {
//		ExamAttendance examAttendance = new ExamAttendance();
//		examAttendance.setId(UUIDs.timeBased());
//		examAttendance.setVersion(0);
//		examAttendance.setAttempt(1);
//		
//		@SuppressWarnings("deprecation")
//		Date startdate=new Date("10/11/2018 05:20:30");
//		@SuppressWarnings("deprecation")
//		Date enddate=new Date("12/11/2018 05:20:30");
//		examAttendance.setEndDateTime(enddate);
//		examAttendance.setExamScheduleId(2);
//		examAttendance.setStartedDateTime(startdate);
//		examAttendance.setStatus(ATTENDANCE_STATUS.STARTED);
//		examAttendance.setStudentId(1);
//		mvc.perform(put("/updateExamAttendance").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
//		.andExpect(jsonPath("$.error", is("unauthorized")));
//	}
	/*@Test
	public void testExamAttendanceInstituteAuthorized() throws Exception {

		mvc.perform(get("/deleteExamAttendance/1").header("Authorization", "Bearer " + accessToken)).andExpect(status().isOk())
				.andExpect(content().contentType(contentType));
	}
	
	@Test
	public void testDeleteExamAttendanceUnAuthorized() throws Exception {

		mvc.perform(get("/deleteExamAttendance/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
		.andExpect(jsonPath("$.error", is("unauthorized")));
	}*/
}
