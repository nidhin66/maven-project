package com.gbs.grandmaster;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gbs.grandmaster.api.ChoiceTextRestController;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.ChoiceText;
import com.gbs.grandmaster.entity.Institute;
import com.gbs.grandmaster.entity.Language;
import com.gbs.grandmaster.service.LanguageService;

@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = GrandmasterApiApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ChoiceTextRestControllerTest {


	@Autowired
	WebApplicationContext context;

	@Autowired
	LanguageService languageService;
	
	@Autowired
	private FilterChainProxy springSecurityFilterChain;

	private MockMvc mvc;

	@InjectMocks
	ChoiceTextRestController controller;
	String accessToken = null;
	String contentType = MediaType.APPLICATION_JSON + ";charset=UTF-8";
	@Autowired
	ObjectMapper objectMapper;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mvc = MockMvcBuilders.webAppContextSetup(context).addFilter(springSecurityFilterChain).build();
		accessToken = getAccessToken("Alex123", "password");
	}



	private String getAccessToken(String username, String password) throws Exception {
		String authorization = "Basic "
				+ new String(Base64Utils.encode("grandmaster-client:grandmaster-secret".getBytes()));

		// @formatter:off
		String content = mvc
				.perform(post("/oauth/token").header("Authorization", authorization)
						.contentType(MediaType.APPLICATION_FORM_URLENCODED).param("username", username)
						.param("password", password).param("grant_type", "password")
						// .param("scope", "read write trust")
						.param("client_id", "grandmaster-client").param("client_secret", "grandmaster-secret"))
				.andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.access_token", is(notNullValue())))
				.andExpect(jsonPath("$.token_type", is(equalTo("bearer"))))
				.andExpect(jsonPath("$.refresh_token", is(notNullValue())))
				.andExpect(jsonPath("$.expires_in", is(greaterThan(3000))))
				.andExpect(jsonPath("$.scope", is(equalTo("read write trust")))).andReturn().getResponse()
				.getContentAsString();

		// @formatter:on

		return content.substring(17, 53);
	}

	
	
	@Test
	public void AtestSaveChoiceTextAuthorized() throws Exception {
		ChoiceText choiseText = new ChoiceText();
		choiseText.setContent("ChoiseText".getBytes());
		Language language=new Language();
		Language languageid=new Language();
		Response response=languageService.FindByID((long) 1);
		language=(Language) response.getData();
		languageid.setId(language.getId());
		choiseText.setLanguage(languageid);
		mvc.perform(post("/saveChoiseText").header("Authorization", "Bearer " + accessToken)
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(choiseText)))
				.andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.id", is(greaterThan(0))));
	}
	@Test
	public void BtestSaveChoiceTextUnAuthorized() throws Exception {
		ChoiceText choiseText = new ChoiceText();
		choiseText.setContent("ChoiseText".getBytes());
		Language language=new Language();
		Language languageid=new Language();
		Response response=languageService.FindByID((long) 1);
		language=(Language) response.getData();
		languageid.setId(language.getId());
		choiseText.setLanguage(languageid);
		mvc.perform(post("/saveChoiseText").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
		.andExpect(jsonPath("$.error", is("unauthorized")));
	}
	
	@Test
	public void CtestEditChoiseTextAuthorized() throws Exception {

		mvc.perform(get("/editChoiseText/1").header("Authorization", "Bearer " + accessToken)).andExpect(status().isOk())
				.andExpect(content().contentType(contentType));
	}
	
	@Test
	public void DtestEditChoiseTextUnAuthorized() throws Exception {

		mvc.perform(get("/editChoiseText/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
		.andExpect(jsonPath("$.error", is("unauthorized")));
	}
	@Test
	public void EtestUpdateChoiceTextAuthorized() throws Exception {
		ChoiceText choiseText = new ChoiceText();
		choiseText.setId((long) 1);
		choiseText.setVersion(0);
		choiseText.setContent("ChoiseTextUpdated".getBytes());
		Language language=new Language();
		Language languageid=new Language();
		Response response=languageService.FindByID((long) 1);
		language=(Language) response.getData();
		languageid.setId(language.getId());
		choiseText.setLanguage(languageid);
		mvc.perform(put("/updateChoiseText").header("Authorization", "Bearer " + accessToken)
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(choiseText)))
				.andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.id", is(greaterThan(0))));
	}
	@Test
	public void FtestUpdateChoiceTextUnAuthorized() throws Exception {
		ChoiceText choiseText = new ChoiceText();
		choiseText.setId((long) 1);
		choiseText.setVersion(0);
		choiseText.setContent("ChoiseTextUpdated".getBytes());
		Language language=new Language();
		Language languageid=new Language();
		Response response=languageService.FindByID((long) 1);
		language=(Language) response.getData();
		languageid.setId(language.getId());
		choiseText.setLanguage(languageid);
		mvc.perform(put("/updateChoiseText").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
		.andExpect(jsonPath("$.error", is("unauthorized")));
	}
	/*@Test
	public void GtestChoiseTextInstituteAuthorized() throws Exception {

		mvc.perform(get("/deleteChoiseText/1").header("Authorization", "Bearer " + accessToken)).andExpect(status().isOk())
				.andExpect(content().contentType(contentType));
	}
	
	@Test
	public void HtestChoiseTextInstituteUnAuthorized() throws Exception {

		mvc.perform(get("/deleteChoiseText/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
		.andExpect(jsonPath("$.error", is("unauthorized")));
	}*/
}
