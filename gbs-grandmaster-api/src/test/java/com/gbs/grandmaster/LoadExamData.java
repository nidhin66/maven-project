package com.gbs.grandmaster;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gbs.grandmaster.common.entity.EXAM_STATUS;
import com.gbs.grandmaster.common.entity.QUESTION_TYPE;
import com.gbs.grandmaster.entity.ExamDefinition;
import com.gbs.grandmaster.entity.ExamSchedule;
import com.gbs.grandmaster.entity.ExamSectionConfig;
import com.gbs.grandmaster.entity.ExamSectionDetails;
import com.gbs.grandmaster.entity.Institute;
import com.gbs.grandmaster.entity.Subject;

@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = GrandmasterApiApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@FixMethodOrder(MethodSorters.DEFAULT)
public class LoadExamData {
	
	@Autowired
	WebApplicationContext context;

	@Autowired
	private FilterChainProxy springSecurityFilterChain;

	private MockMvc mvc;

	
	String accessToken = null;
	String contentType = MediaType.APPLICATION_JSON + ";charset=UTF-8";
	@Autowired
	ObjectMapper objectMapper;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mvc = MockMvcBuilders.webAppContextSetup(context).addFilter(springSecurityFilterChain).build();
		accessToken = getAccessToken("gm_admin", "password");
		clearData();
	}
	
	private void clearData() throws Exception {
		
		mvc.perform(get("/deleteExamSchedules").header("Authorization", "Bearer " + accessToken));

		mvc.perform(get("/deleteexamDefinitions").header("Authorization", "Bearer " + accessToken)).andExpect(status().isOk())
		
		.andExpect(content().contentType(contentType));
//		mvc.perform(get("/deleteSubjects").header("Authorization", "Bearer " + accessToken)).andExpect(status().isOk())
//		
//		.andExpect(content().contentType(contentType));
	}
	
	private String getAccessToken(String username, String password) throws Exception {
		String authorization = "Basic "
				+ new String(Base64Utils.encode("grandmaster-client:grandmaster-secret".getBytes()));

		// @formatter:off
		String content = mvc
				.perform(post("/oauth/token").header("Authorization", authorization)
						.contentType(MediaType.APPLICATION_FORM_URLENCODED).param("username", username)
						.param("password", password).param("grant_type", "password")
						// .param("scope", "read write trust")
						.param("client_id", "grandmaster-client").param("client_secret", "grandmaster-secret"))
				.andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.access_token", is(notNullValue())))
				.andExpect(jsonPath("$.token_type", is(equalTo("bearer"))))
				.andExpect(jsonPath("$.refresh_token", is(notNullValue())))
				.andExpect(jsonPath("$.expires_in", is(greaterThan(3000))))
				.andExpect(jsonPath("$.scope", is(equalTo("read write trust")))).andReturn().getResponse()
				.getContentAsString();

		// @formatter:on

		return content.substring(17, 53);
	}
	
	@Test
	public void saveExamData() throws Exception {
		saveSubject();
		saveExamDefinition();
	}
	
	private void saveExamDefinition() throws Exception {
		
		
		String resposneStr = mvc.perform(get("/findSubjectByCode/PHY").header("Authorization", "Bearer " + accessToken)).andReturn().getResponse().getContentAsString();
		objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		JsonNode responseNode = objectMapper.readTree(resposneStr);			
		Subject physics = objectMapper.readValue(responseNode.get("data").toString(), Subject.class);
		
		resposneStr = mvc.perform(get("/findSubjectByCode/CHE").header("Authorization", "Bearer " + accessToken)).andReturn().getResponse().getContentAsString();
		objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		responseNode = objectMapper.readTree(resposneStr);			
		Subject chemistry = objectMapper.readValue(responseNode.get("data").toString(), Subject.class);
		
		resposneStr = mvc.perform(get("/findSubjectByCode/MAT").header("Authorization", "Bearer " + accessToken)).andReturn().getResponse().getContentAsString();
		objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		responseNode = objectMapper.readTree(resposneStr);			
		Subject maths = objectMapper.readValue(responseNode.get("data").toString(), Subject.class);
		
		ExamDefinition examDefinition = new ExamDefinition();
		examDefinition.setDurationInMin(3);
		examDefinition.setFollowSectionSequnce(true);
		
		ExamSectionConfig examSectionConfig1 = new ExamSectionConfig();
		examSectionConfig1.setDurationInMin(1);
		examSectionConfig1.setSectionName("Physics");
		examSectionConfig1.setSequnce(1);
		ExamSectionDetails detail1 = new ExamSectionDetails();
		detail1.setMarkPerQuestion(1);
		detail1.setNegativeMarkPerQuestion(.25);
		detail1.setNoOfQuestions(5);
		detail1.setQuestionType(QUESTION_TYPE.OBJECTIVE);
		detail1.setSubject_id(physics.getId());
		
		
		ExamSectionDetails detail11 = new ExamSectionDetails();
		detail11.setMarkPerQuestion(1);
		detail11.setNegativeMarkPerQuestion(.25);
		detail11.setNoOfQuestions(15);
		detail11.setQuestionType(QUESTION_TYPE.OBJECTIVE);
		detail11.setSubject_id(physics.getId());
		Set<ExamSectionDetails> details1 =   new HashSet<>();
		details1.add(detail1);
		details1.add(detail11);
		examSectionConfig1.setDetails(details1);
		
		ExamSectionConfig examSectionConfig2 = new ExamSectionConfig();
		examSectionConfig2.setDurationInMin(1);
		examSectionConfig2.setSectionName("Chemistry");
		examSectionConfig2.setSequnce(2);
		ExamSectionDetails detail2 = new ExamSectionDetails();
		detail2.setMarkPerQuestion(1);
		detail2.setNegativeMarkPerQuestion(.25);
		detail2.setNoOfQuestions(5);
		detail2.setQuestionType(QUESTION_TYPE.OBJECTIVE);
		detail2.setSubject_id(chemistry.getId());
		
		ExamSectionDetails detail21 = new ExamSectionDetails();
		detail21.setMarkPerQuestion(1);
		detail21.setNegativeMarkPerQuestion(.25);
		detail21.setNoOfQuestions(15);
		detail21.setQuestionType(QUESTION_TYPE.OBJECTIVE);
		detail21.setSubject_id(chemistry.getId());
		
		Set<ExamSectionDetails> details2 = new HashSet<>();
		details2.add(detail2);
		details2.add(detail21);
		examSectionConfig2.setDetails(details2);
		
		ExamSectionConfig examSectionConfig3 = new ExamSectionConfig();
		examSectionConfig3.setDurationInMin(1);
		examSectionConfig3.setSectionName("Maths");
		examSectionConfig3.setSequnce(3);
		ExamSectionDetails detail3 = new ExamSectionDetails();
		detail3.setMarkPerQuestion(1);
		detail3.setNegativeMarkPerQuestion(.25);
		detail3.setNoOfQuestions(5);
		detail3.setQuestionType(QUESTION_TYPE.OBJECTIVE);
		detail3.setSubject_id(maths.getId());
		
		ExamSectionDetails detail31 = new ExamSectionDetails();
		detail31.setMarkPerQuestion(1);
		detail31.setNegativeMarkPerQuestion(.25);
		detail31.setNoOfQuestions(15);
		detail31.setQuestionType(QUESTION_TYPE.OBJECTIVE);
		detail31.setSubject_id(maths.getId());
		
		Set<ExamSectionDetails> details3 = new HashSet<>();
		details3.add(detail3);
		details3.add(detail31);

		examSectionConfig3.setDetails(details3);
		
		
		Set<ExamSectionConfig> sectionConfigs = new HashSet<>();
		sectionConfigs.add(examSectionConfig1);
		sectionConfigs.add(examSectionConfig2);
		sectionConfigs.add(examSectionConfig3);		
		
		examDefinition.setExamSections(sectionConfigs);
		
		resposneStr = mvc.perform(post("/saveExamDefinition").header("Authorization", "Bearer " + accessToken)
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(examDefinition))).andReturn().getResponse().getContentAsString();
		objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		 responseNode = objectMapper.readTree(resposneStr);			
		 ExamDefinition exaDefinition = objectMapper.readValue(responseNode.get("data").toString(), ExamDefinition.class);
		 
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");		
		 
		 ExamSchedule examSchedule = new ExamSchedule();
		 examSchedule.setName("Sample Test");
		 examSchedule.setExamDefinition(exaDefinition);
		 examSchedule.setStartDateTime( sdf.parse("2018-12-10T00:00:00.000+0000"));
		 examSchedule.setEndDateTime( sdf.parse("2018-12-12T23:59:59.000+0000"));
		 examSchedule.setNoOfAttempts(1);
		 examSchedule.setNoOfQuestionPapers(1);
		 examSchedule.setResultImmeadiate(false);
		 examSchedule.setStatus(EXAM_STATUS.ENABLED);
		
		mvc.perform(post("/saveExamSchedule").header("Authorization", "Bearer " + accessToken)
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(examSchedule)))
				.andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.data.id", is(greaterThan(0))));
		
	
		
	}
	
	private void saveSubject() throws JsonProcessingException, Exception {
		Subject physics = new Subject();
		physics.setName("Physics");
		physics.setCode("PHY");
		
		
		Subject chemistry = new Subject();
		chemistry.setName("Chemistry");
		chemistry.setCode("CHE");
		
		Subject maths = new Subject();
		maths.setName("MATHS");
		maths.setCode("MAT");
		
		List<Subject> subjects = new ArrayList<Subject>();
		
		subjects.add(physics);
		subjects.add(chemistry);
		subjects.add(maths);

		mvc.perform(post("/saveSubjects").header("Authorization", "Bearer " + accessToken)
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(subjects)));
		
	
	}

}
