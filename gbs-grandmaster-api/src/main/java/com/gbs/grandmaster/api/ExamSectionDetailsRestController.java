package com.gbs.grandmaster.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.ExamSectionDetails;
import com.gbs.grandmaster.service.ExamSectionDetailsService;

@RestController
@RequestMapping("/")
public class ExamSectionDetailsRestController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ExamSectionDetailsService examSectionDetailsService;

	@RequestMapping(value = "/saveExamSectionDetails", method = RequestMethod.POST)
	public Response saveExamSectionDetails(@RequestBody ExamSectionDetails examSectionDetails) {

		Response response = new Response();
		try {
			if (null != examSectionDetails) {
				response = examSectionDetailsService.Save(examSectionDetails);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while saving ExamSectionDetails :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/updateExamSectionDetails", method = RequestMethod.PUT)
	public Response updateExamSectionDetails(@RequestBody ExamSectionDetails examSectionDetails) {

		Response response = new Response();
		try {
			if (null != examSectionDetails) {
				response = examSectionDetailsService.Update(examSectionDetails);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while updating ExamSectionDetails :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/deleteExamSectionDetails/{id}", method = RequestMethod.GET)
	public Response deleteExamSectionDetails(@PathVariable Long id) {

		Response response = new Response();
		try {
			response = examSectionDetailsService.FindByID(id);
			if (!response.equals(null)) {
				examSectionDetailsService.Delete(id);

				return response;

			} else {
				response.setSuccess(false);
				return response;
			}
		} catch (Exception e) {
			logger.error("Exception " + "while deleting ExamSectionDetails :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/editExamSectionDetails/{id}", method = RequestMethod.GET)
	public Response editExamSectionDetails(@PathVariable Long id) {

		Response response = new Response();

		try {
			response = examSectionDetailsService.FindByID(id);

			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while finding ExamSectionDetails :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/listexamSectionDetails", method = RequestMethod.GET)
	public Response listExamSectionDetails(@PageableDefault(value = 10, page = 0) Pageable pageable) {

		Response response = new Response();
		try {
			Page<ExamSectionDetails> page = examSectionDetailsService.getAll(pageable);
			page.getContent();
			response.setData(page);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing ExamSectionDetails :" + e);
		}
		return response;

	}

}
