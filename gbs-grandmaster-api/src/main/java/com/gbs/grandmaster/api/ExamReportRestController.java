package com.gbs.grandmaster.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.cassandra.ExamAttendance;
import com.gbs.grandmaster.service.ExamReportService;

@RestController
@RequestMapping("/")
public class ExamReportRestController {
	
	@Autowired
	private ExamReportService examservice;
	
	
	@RequestMapping(value = "/getExamReport", method = RequestMethod.POST)
	public Response getExamReport(@RequestBody ExamAttendance attendance)
	{
		Response response = new Response();
		try {
			response = examservice.getExamReport(attendance);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
		}
		return response;
	}

}
