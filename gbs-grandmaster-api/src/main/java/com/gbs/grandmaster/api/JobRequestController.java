package com.gbs.grandmaster.api;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.JobRequest;
import com.gbs.grandmaster.entity.cassandra.ActiveUserLog;
import com.gbs.grandmaster.service.JobRequestService;

@RestController
@RequestMapping("/")
public class JobRequestController {

	@Autowired
	public JobRequestService jobRequestService;

	@RequestMapping(value = "/saveJobRequest", method = RequestMethod.POST)
	public JobRequest logActiveUserLog(@RequestBody JobRequest jobRequest) {
		if (null != jobRequest) {
			jobRequest = jobRequestService.save(jobRequest);
		}
		return jobRequest;
	}

}
