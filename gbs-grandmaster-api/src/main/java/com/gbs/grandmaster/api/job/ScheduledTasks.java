package com.gbs.grandmaster.api.job;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gbs.grandmaster.common.CommonUtilitties;
import com.gbs.grandmaster.common.entity.ATTENDANCE_STATUS;
import com.gbs.grandmaster.common.entity.JOB_STATUS;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.ExamResult;
import com.gbs.grandmaster.entity.ExamResultID;
import com.gbs.grandmaster.entity.ExamResultSummary;
import com.gbs.grandmaster.entity.ExamResultSummaryID;
import com.gbs.grandmaster.entity.ExamSchedule;
import com.gbs.grandmaster.entity.JobRequest;
import com.gbs.grandmaster.entity.cassandra.Answer;
import com.gbs.grandmaster.entity.cassandra.ExamAttendance;
import com.gbs.grandmaster.entity.cassandra.QuestionAttemptTime;
import com.gbs.grandmaster.service.AnswerService;
import com.gbs.grandmaster.service.ExamAttendanceService;
import com.gbs.grandmaster.service.ExamLogService;
import com.gbs.grandmaster.service.ExamResultService;
import com.gbs.grandmaster.service.ExamResultSummaryService;
import com.gbs.grandmaster.service.ExamScheduleService;
import com.gbs.grandmaster.service.JobRequestService;
import com.gbs.grandmaster.service.StudentService;

@Component
public class ScheduledTasks {

	private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

	@Autowired
	public JobRequestService jobRequestService;

	@Autowired
	public StudentService studentService;

	@Autowired
	public ExamScheduleService examScheduleService;

	@Autowired
	public ExamAttendanceService examAttendanceService;

	@Autowired
	public AnswerService answerService;

	@Autowired
	public ExamResultSummaryService examResultSummaryService;

	@Autowired
	public ExamResultService examResultService;

	@Autowired
	public ExamLogService logService;

	@Scheduled(cron = "${exam-job.cron}")
	public void examResultPublisingJob() {
		logger.debug("Result publishing job started");
		List<JobRequest> jobRequests = jobRequestService.findJobByStatus(JOB_STATUS.SCHEDULED);
		if (null != jobRequests) {
			for (JobRequest jobRequest : jobRequests) {
				jobRequest.setStartedTime(new Date());
				try {
					/**
					 * Find Batch by Exam schedule id
					 */
					Long examScheduleId = jobRequest.getExamId();
					ExamSchedule examSchedule = examScheduleService.findById(examScheduleId);
					if (null != examSchedule) {
						/**
						 * List students by batch
						 */
						List<Long> studentIds = studentService.getStudentIdsByBatch(examSchedule.getBatch_id());
						for (Long studentId : studentIds) {
							/**
							 * Get student attendance from Cassandra
							 */
							List<ExamAttendance> attendances = examAttendanceService.getAttendance(examScheduleId,
									studentId);
							if (null != attendances && attendances.size() > 0) {
								for (ExamAttendance examAttendance : attendances) {
									ExamResultSummary summary = new ExamResultSummary();
									ExamResultSummaryID resultSumamryId = new ExamResultSummaryID();
									resultSumamryId.setAttempt(examAttendance.getKey().getAttempt());
									resultSumamryId.setExamId(examScheduleId);
									resultSumamryId.setStudentId(studentId);
									summary.setId(resultSumamryId);
									summary.setAttendanceStatus(examAttendance.getStatus());
									/**
									 * Get answers
									 */
									Response response = answerService.getAnswers(studentId, examScheduleId,
											examAttendance.getKey().getAttempt());
									int correctAnsCount = 0;
									int worongAnsCount = 0;
									double markObtained = 0.0;
									double negativeMark = 0.0;
									double totolMark = 0.0;
									if (null != response) {
										List<Answer> answers = (List<Answer>) response.getData();
										List<ExamResult> resultList = new ArrayList<>();
										for (Answer answer : answers) {
											if (!CommonUtilitties.isNullorEmpty(answer.getContent())) {
												ExamResult examResult = new ExamResult();
												ExamResultID resultId = new ExamResultID();
												resultId.setAttempt(examAttendance.getKey().getAttempt());
												resultId.setExamId(examScheduleId);
												resultId.setStudentId(studentId);
												resultId.setQuestionId(answer.getKey().getQuestionId());
												examResult.setId(resultId);
												examResult.setSelectedChoices(answer.getContent());
												if (answer.isCorrect()) {
													correctAnsCount++;
													markObtained = markObtained + answer.getMark();
												} else {
													worongAnsCount++;
													negativeMark = negativeMark + answer.getNegativeMark();
												}
												List<QuestionAttemptTime> questionAttempts = logService
														.findQuestionAttemptTimeByQuestionId(studentId, examScheduleId,
																examAttendance.getKey().getAttempt(),
																answer.getKey().getQuestionId());
												long timeTaken = 0;
												if (null != questionAttempts && questionAttempts.size() > 0) {
													for (QuestionAttemptTime questionAttemptTime : questionAttempts) {
														timeTaken = timeTaken + questionAttemptTime.getTimeTaken();
													}
												}
												examResult.setCorrect(answer.isCorrect());
												examResult.setMark(answer.getMark());
												examResult.setNegativeMark(answer.getNegativeMark());

												examResult.setSectonId(answer.getSectionId());
												examResult.setTimeTaken(timeTaken);
												resultList.add(examResult);
											}

										}
										/**
										 * Save Exam Result
										 */
										examResultService.save(resultList);
									}
									totolMark = markObtained - negativeMark;
									summary.setCorrectAnswers(correctAnsCount);
									summary.setMarkObtained(markObtained);
									summary.setNegativeMark(negativeMark);
									summary.setTotalMark(totolMark);
									summary.setWrongAnswers(worongAnsCount);
									examResultSummaryService.save(summary);
								}
							} else {
								ExamResultSummary summary = new ExamResultSummary();
								ExamResultSummaryID resultId = new ExamResultSummaryID();
								resultId.setAttempt(1);
								resultId.setExamId(examScheduleId);
								resultId.setStudentId(studentId);
								summary.setId(resultId);
								summary.setAttendanceStatus(ATTENDANCE_STATUS.NOT_ATTEMPTED);
								summary.setCorrectAnswers(0);
								summary.setMarkObtained(0);
								summary.setNegativeMark(0);
								summary.setTotalMark(0);
								summary.setWrongAnswers(0);
								examResultSummaryService.save(summary);
							}
						}
					} else {
						logger.error("Exam Schedule not found for the exam id : " + examScheduleId);
					}
					jobRequest.setJobStatus(JOB_STATUS.COMPLETED);
					jobRequest.setFinishedTime(new Date());
				} catch (Exception e) {
					jobRequest.setJobStatus(JOB_STATUS.FAILED);
					jobRequest.setFinishedTime(new Date());
					logger.error("Failed to execute the job for the job request id : " + jobRequest.getId(), e);
				}
				jobRequestService.save(jobRequest);
			}
		}
		logger.debug("Result publishing job finished");
	}

}
