package com.gbs.grandmaster.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.cassandra.Answer;
import com.gbs.grandmaster.service.AnswerService;

@RestController
@RequestMapping("/") 
public class AnswerRestController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private AnswerService answerService;
	//ans
	
	@RequestMapping(value = "/saveAnswers", method = RequestMethod.POST)
	public Response saveAnswers(@RequestBody List<Answer> answers) {

    	Response response=new Response();
    	try {
    	if(null != answers) {
    		response = answerService.saveAswers(answers);
    		response.setSuccess(true);
    	}
    	}
    	catch(Exception e)
    	{
    		response.setSuccess(false);
    		logger.error("Exception " + "while saving ExamAttendance :" + e);
    	}
    	return response;


	}
	@RequestMapping(value = "/getAnswers", method = RequestMethod.GET,params = {"exam_schedule_id","student_id","attempt"})
	public Response getAnswers(@RequestParam(value = "exam_schedule_id") long exam_schedule_id,@RequestParam(value = "student_id") long student_id,@RequestParam(value = "attempt") int attempt) {
		return answerService.getAnswers(student_id, exam_schedule_id, attempt);
	}
	
	
	@RequestMapping(value = "/getExamAnswerSheet", method = RequestMethod.GET,params = {"student_id","batch_id"})
	public Response getExamAnswerSheet(@RequestParam(value = "student_id") long student_id,@RequestParam(value = "batch_id") long batch_id) {
		return answerService.getExamAnswerSheet(student_id, batch_id);
	}
	
	
	@RequestMapping(value = "/getMockExamAnswerSheet", method = RequestMethod.GET,params = {"student_id","batch_id"})
	public Response getMockExamAnswerSheet(@RequestParam(value = "student_id") long student_id,@RequestParam(value = "batch_id") long batch_id) {
		return answerService.getMockExamAnswerSheet(student_id, batch_id);
	}
	
	

}
