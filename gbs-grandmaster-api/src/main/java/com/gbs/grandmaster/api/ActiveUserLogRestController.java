package com.gbs.grandmaster.api;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.cassandra.ActiveUserLog;
import com.gbs.grandmaster.service.ActiveUserLogService;

@RestController
@RequestMapping("/")
public class ActiveUserLogRestController {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ActiveUserLogService activeUserLogService;
	
	
	@RequestMapping(value = "/logActiveUserLog", method = RequestMethod.POST)
	public Response logActiveUserLog(@RequestBody ActiveUserLog activeUserLog) {
		if(null != activeUserLog) {
			activeUserLog.setUpdateTime(new Date());
		}
		return activeUserLogService.logActiveUserLog(activeUserLog);
	}
	
	
	@RequestMapping(value = "/isUserActive", method = RequestMethod.POST)
	public Response isUserActive(@RequestBody ActiveUserLog activeUserLog) {
		if(null != activeUserLog) {
			activeUserLog.setUpdateTime(new Date());
		}
		return activeUserLogService.isUserActive(activeUserLog);
	}

}
