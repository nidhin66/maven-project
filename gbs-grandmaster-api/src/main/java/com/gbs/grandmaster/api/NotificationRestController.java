package com.gbs.grandmaster.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.Notification;
import com.gbs.grandmaster.service.NotificationService;

@RestController
@RequestMapping("/")
public class NotificationRestController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private NotificationService notificationService;

	@RequestMapping(value = "/saveNotification", method = RequestMethod.POST)
	public Response saveNotification(@RequestBody Notification notification) {

		Response response = new Response();
		try {
			if (null != notification) {
				response = notificationService.Save(notification);
				response.setSuccess(true);

			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while saving notification :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/updateNotification", method = RequestMethod.PUT)
	public Response updateNotification(@RequestBody Notification notification) {

		Response response = new Response();
		try {
			if (null != notification) {
				response = notificationService.Update(notification);
				response.setSuccess(true);

			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while updating notification :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/deleteNotification/{id}", method = RequestMethod.GET)
	public Response deleteNotification(@PathVariable Long id) {

		Response response = new Response();
		try {
			response = notificationService.FindByID(id);
			if (!response.equals(null)) {
				notificationService.Delete(id);

				return response;

			} else {
				response.setSuccess(false);
				return response;
			}
		} catch (Exception e) {
			logger.error("Exception " + "while deleting Notification :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/editNotification/{id}", method = RequestMethod.GET)
	public Response editNotification(@PathVariable Long id) {

		Response response = new Response();

		try {
			response = notificationService.FindByID(id);

			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while finding Notification :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/listnotification", method = RequestMethod.GET)
	public Response listNotification() {

		Response response = new Response();
		try {
			List<Notification> notificationList = notificationService.getAll();
			response.setData(notificationList);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing Notification :" + e);
		}
		return response;

	}

}
