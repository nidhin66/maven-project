package com.gbs.grandmaster.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Identifiable;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.Instruction;
import com.gbs.grandmaster.service.InstructionService;

@RestController
@RequestMapping("/")
public class InstructionRestController {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private InstructionService instructionService;

	@RequestMapping(value = "/saveInstruction", method = RequestMethod.POST)
	public Response saveInstruction(@RequestBody Instruction instruction) {
		Response response = new Response();
		try {
			if (null != instruction) {
				response = instructionService.Save(instruction);
				response.setSuccess(true);

			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while saving instruction :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/updateInstruction", method = RequestMethod.PUT)
	public Response updateInstruction(@RequestBody Instruction instruction) {

		Response response = new Response();
		try {
			if (null != instruction) {
				response = instructionService.Update(instruction);
				response.setSuccess(true);

			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while updating instruction :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/deleteInstruction/{id}", method = RequestMethod.GET)
	public Response deleteInstruction(@PathVariable Long id) {

		Response response = new Response();
		try {
			response = instructionService.FindByID(id);
			if (!response.equals(null)) {
				instructionService.Delete(id);

				return response;

			} else {
				response.setSuccess(false);
				return response;
			}
		} catch (Exception e) {
			logger.error("Exception " + "while deleting Instruction :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/editInstruction/{id}", method = RequestMethod.GET)
	public Response editInstruction(@PathVariable Long id) {

		Response response = new Response();

		try {
			response = instructionService.FindByID(id);

			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while finding Instruction :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/listInstruction", method = RequestMethod.GET)
	public Response listInstruction() {

		Response response = new Response();
		try {
			Instruction instruction = instructionService.getInstruction();
			response.setData(instruction);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing Instruction :" + e);
		}
		return response;

	}

}
