package com.gbs.grandmaster.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.ExamSectionConfig;
import com.gbs.grandmaster.service.ExamSectionConfigService;

@RestController
@RequestMapping("/")
public class ExamSectionConfigRestController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ExamSectionConfigService examSectionConfigService;

	@RequestMapping(value = "/saveExamSectionConfig", method = RequestMethod.POST)
	public Response saveExamSectionConfig(@RequestBody ExamSectionConfig examSectionConfig) {

		Response response = new Response();
		try {
			if (null != examSectionConfig) {
				response = examSectionConfigService.Save(examSectionConfig);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception" + "while saving ExamSectionConfig :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/updateExamSectionConfig", method = RequestMethod.PUT)
	public Response updateExamSectionConfig(@RequestBody ExamSectionConfig examSectionConfig) {

		Response response = new Response();
		try {
			if (null != examSectionConfig) {
				response = examSectionConfigService.Update(examSectionConfig);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception" + "while updating ExamSectionConfig :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/deleteExamSectionConfig/{id}", method = RequestMethod.GET)
	public Response deleteExamSectionConfig(@PathVariable Long id) {

		Response response = new Response();
		try {
			response = examSectionConfigService.FindByID(id);
			if (!response.equals(null)) {
				examSectionConfigService.Delete(id);
				response.setSuccess(true);
				return response;

			} else {
				response.setSuccess(false);
				return response;
			}
		} catch (Exception e) {
			logger.error("Exception " + "while deleting ExamSectionConfig :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/editExamSectionConfig/{id}", method = RequestMethod.GET)
	public Response editExamSectionConfig(@PathVariable Long id) {

		Response response = new Response();

		try {
			response = examSectionConfigService.FindByID(id);
			response.setSuccess(true);
			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while finding ExamSectionConfig :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/listexamSectionConfig", method = RequestMethod.GET)
	public Response listExamSectionConfig(@PageableDefault(value = 10, page = 0) Pageable pageable) {

		Response response = new Response();
		try {
			Page<ExamSectionConfig> page = examSectionConfigService.getAll(pageable);
			page.getContent();
			response.setData(page);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing ExamSectionConfig :" + e);
		}
		return response;

	}

}
