package com.gbs.grandmaster.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.ExamSchedule;
import com.gbs.grandmaster.service.ExamScheduleService;

@RestController
@RequestMapping("/")
public class ExamScheduleRestController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ExamScheduleService examScheduleService;

	@RequestMapping(value = "/saveExamSchedule", method = RequestMethod.POST)
	public Response saveExamSchedule(@RequestBody ExamSchedule examSchedule) {

		Response response = new Response();
		try {
			if (null != examSchedule) {
				response = examScheduleService.Save(examSchedule);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while saving ExamSchedule :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/updateExamSchedule", method = RequestMethod.PUT)
	public Response updateExamSchedule(@RequestBody ExamSchedule examSchedule) {

		Response response = new Response();
		try {
			if (null != examSchedule) {
				response = examScheduleService.Update(examSchedule);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while updating ExamSchedule :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/deleteExamSchedule/{id}", method = RequestMethod.GET)
	public Response deleteExamSchedule(@PathVariable Long id) {

		Response response = new Response();
		try {
			response = examScheduleService.FindByID(id);
			if (!response.equals(null)) {
				examScheduleService.Delete(id);
				response.setSuccess(true);

				return response;

			} else {
				response.setSuccess(false);
				return response;
			}
		} catch (Exception e) {
			logger.error("Exception " + "while deleting ExamSchedule :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/editExamSchedule/{id}", method = RequestMethod.GET)
	public Response editExamSchedule(@PathVariable Long id) {

		Response response = new Response();

		try {
			response = examScheduleService.FindByID(id);
			response.setSuccess(true);

			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while finding ExamSchedule :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/listexamSchedule", method = RequestMethod.GET)
	public Response listExamSchedule(@PageableDefault(value = 10, page = 0) Pageable pageable) {

		Response response = new Response();
		try {
			Page<ExamSchedule> page = examScheduleService.getAll(pageable);
			page.getContent();
			response.setData(page);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing ExamSchedule :" + e);
		}
		return response;
	}

	@RequestMapping(value = "/deleteExamSchedules", method = RequestMethod.GET)
	public Response deleteExamSchedules() {
		Response response = new Response();
		try {
			examScheduleService.deleteAllSchedules();
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while deleting ExamSchedule :" + e);
		}
		return response;
	}

	@RequestMapping(value = "/listStudentExams", method = RequestMethod.GET, params = { "student_id", "batch_id" })
	public Response listStudentExams(@RequestParam(value = "student_id") long student_id,
			@RequestParam(value = "batch_id") long batch_id) {
		Response response = new Response();
		try {
			response = examScheduleService.listStudentExams(student_id, batch_id);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing ExamSchedule :" + e);
		}
		return response;
	}

	@RequestMapping(value = "/listStudentMockExams", method = RequestMethod.GET, params = { "student_id", "batch_id" })
	public Response listStudentMockExams(@RequestParam(value = "student_id") long student_id,
			@RequestParam(value = "batch_id") long batch_id) {
		Response response = new Response();
		try {
			response = examScheduleService.listStudentMockExams(student_id, batch_id);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing ExamSchedule :" + e);
		}
		return response;
	}

	@RequestMapping(value = "/getQuestionPaper", method = RequestMethod.GET, params = { "exam_schedule_id",
			"student_id" })
	public Response getQuestionPaper(@RequestParam(value = "exam_schedule_id") long exam_schedule_id,
			@RequestParam(value = "student_id") long student_id) {
		return examScheduleService.getQuestionPaper(exam_schedule_id, student_id);
	}

}
