package com.gbs.grandmaster.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.service.AppConfigService;

@RestController
@RequestMapping("/") 
public class AppConfigController {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private AppConfigService appConfigService;
	
	@RequestMapping(value = "/getApplicationConfigs", method = RequestMethod.GET)
	public Response getApplicationConfigs(){
		return appConfigService.getApplicationConfigs();
	}

}
