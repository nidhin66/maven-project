package com.gbs.grandmaster.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.StudentDevice;
import com.gbs.grandmaster.service.StudentDeviceService;

@RestController
@RequestMapping("/")
public class StudentDeviceRestController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private StudentDeviceService studentDeviceService;

	@RequestMapping(value = "/saveStudentDevice", method = RequestMethod.POST)
	public Response saveStudentDevice(@RequestBody StudentDevice studentDevice) {

		Response response = new Response();
		try {
			if (null != studentDevice) {
				response = studentDeviceService.Save(studentDevice);
				response.setSuccess(true);

			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while saving StudentDevice :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/updateStudentDevice", method = RequestMethod.PUT)
	public Response updateStudentDevice(@RequestBody StudentDevice studentDevice) {

		Response response = new Response();
		try {
			if (null != studentDevice) {
				response = studentDeviceService.Update(studentDevice);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while updating StudentDevice :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/deleteStudentDevice/{id}", method = RequestMethod.GET)
	public Response deleteStudentDevice(@PathVariable Long id) {

		Response response = new Response();
		try {
			response = studentDeviceService.FindByID(id);
			if (!response.equals(null)) {
				studentDeviceService.Delete(id);

				return response;

			} else {
				response.setSuccess(false);
				return response;
			}
		} catch (Exception e) {
			logger.error("Exception " + "while deleting StudentDevice :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/editStudentDevice/{id}", method = RequestMethod.GET)
	public Response editStudentDevice(@PathVariable Long id) {

		Response response = new Response();

		try {
			response = studentDeviceService.FindByID(id);

			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while finding StudentDevice :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/liststudentDevice", method = RequestMethod.GET)
	public Response listStudentDevice(@PageableDefault(value = 10, page = 0) Pageable pageable) {

		Response response = new Response();
		try {
			Page<StudentDevice> page = studentDeviceService.getAll(pageable);
			page.getContent();
			response.setData(page);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing StudentDevice :" + e);
		}
		return response;

	}

}
