package com.gbs.grandmaster.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.Language;
import com.gbs.grandmaster.service.LanguageService;

@RestController
@RequestMapping("/")
public class LanguageRestController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private LanguageService languageService;

	@RequestMapping(value = "/saveLanguage", method = RequestMethod.POST)
	public Response saveLanguage(@RequestBody Language language) {

		Response response = new Response();
		try {
			if (null != language) {
				response = languageService.Save(language);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while saving language :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/updateLanguage", method = RequestMethod.PUT)
	public Response updateLanguage(@RequestBody Language language) {

		Response response = new Response();
		try {
			if (null != language) {
				response = languageService.Update(language);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while updating language :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/deleteLanguage/{id}", method = RequestMethod.GET)
	public Response deleteLanguage(@PathVariable Long id) {

		Response response = new Response();
		try {
			response = languageService.FindByID(id);
			if (!response.equals(null)) {
				languageService.Delete(id);

				return response;

			} else {
				response.setSuccess(false);
				return response;
			}
		} catch (Exception e) {
			logger.error("Exception " + "while deleting Language :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/editLanguage/{id}", method = RequestMethod.GET)
	public Response editLanguage(@PathVariable Long id) {

		Response response = new Response();

		try {
			response = languageService.FindByID(id);

			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while finding language :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/listlanguage", method = RequestMethod.GET)
	public Response listLanguage(@PageableDefault(value = 10, page = 0) Pageable pageable) {

		Response response = new Response();
		try {
			Page<Language> page = languageService.getAll(pageable);
			page.getContent();
			response.setData(page);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing language :" + e);
		}
		return response;

	}

}
