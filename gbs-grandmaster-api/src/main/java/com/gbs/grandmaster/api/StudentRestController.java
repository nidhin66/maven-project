package com.gbs.grandmaster.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.common.entity.UserAbstract;
import com.gbs.grandmaster.service.StudentService;
import com.gbs.grandmaster.service.UserService;

@RestController
@RequestMapping("/") 
public class StudentRestController {
	
	@Autowired
	private StudentService studentService;
	@Autowired
	private UserService userService;
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@RequestMapping(value = "/findStudentByUserName/{userName}", method = RequestMethod.GET)
	public Response findStudentByUserName(@PathVariable String userName) {
		return studentService.findStudentByUserName(userName);
	}
	
	@RequestMapping(value = "/getStudentBatchDetails/{studentId}", method = RequestMethod.GET)
	public Response getStudentBatchDetails(@PathVariable long studentId) {
		return studentService.getStudentBatchDetails(studentId);
	}
	
	@RequestMapping(value = "/createOrUpdateUser", method = RequestMethod.POST)
	public Response createOrUpdateUser(@RequestBody UserAbstract userAbstract) {
		return userService.createOrUpdateUser(userAbstract);
	}
	
	
	
	

}
