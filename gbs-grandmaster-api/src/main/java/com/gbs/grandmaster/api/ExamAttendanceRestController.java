package com.gbs.grandmaster.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.cassandra.ExamAttendance;
import com.gbs.grandmaster.service.ExamAttendanceService;

@RestController
@RequestMapping("/")
public class ExamAttendanceRestController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ExamAttendanceService examAttendanceService;

	@RequestMapping(value = "/startExam", method = RequestMethod.POST)
	public Response startExam(@RequestBody ExamAttendance examAttendance) {

		Response response = new Response();
		try {
			if (null != examAttendance) {
				response = examAttendanceService.startExam(examAttendance);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while saving ExamAttendance :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/finishExam", method = RequestMethod.POST)
	public Response finishExam(@RequestBody ExamAttendance examAttendance) {

		Response response = new Response();
		try {
			if (null != examAttendance) {
				response = examAttendanceService.finishExam(examAttendance);
				response.setSuccess(true);

			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while updating ExamAttendance :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/submitExam", method = RequestMethod.POST)
	public Response submitExam(@RequestBody ExamAttendance examAttendance) {

		Response response = new Response();
		try {
			if (null != examAttendance) {
				response = examAttendanceService.submitExam(examAttendance);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while updating ExamAttendance :" + e);
		}
		return response;

	}

}
