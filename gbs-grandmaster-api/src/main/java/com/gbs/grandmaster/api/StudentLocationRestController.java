package com.gbs.grandmaster.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.StudentLocation;
import com.gbs.grandmaster.service.StudentLocationService;

@RestController
@RequestMapping("/")
public class StudentLocationRestController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private StudentLocationService studentLocationService;

	@RequestMapping(value = "/saveStudentLocation", method = RequestMethod.POST)
	public Response saveStudentLocation(@RequestBody StudentLocation studentLocation) {

		Response response = new Response();
		try {
			if (null != studentLocation) {
				response = studentLocationService.Save(studentLocation);
				response.setSuccess(true);

			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while saving studentLocation :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/updateStudentLocation", method = RequestMethod.PUT)
	public Response updateStudentLocation(@RequestBody StudentLocation studentLocation) {

		Response response = new Response();
		try {
			if (null != studentLocation) {
				response = studentLocationService.Update(studentLocation);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while updating studentLocation :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/deleteStudentLocation/{id}", method = RequestMethod.GET)
	public Response deleteStudentLocation(@PathVariable Long id) {

		Response response = new Response();
		try {
			response = studentLocationService.FindByID(id);
			if (!response.equals(null)) {
				studentLocationService.Delete(id);
				response.setSuccess(true);
				return response;

			} else {
				response.setSuccess(false);
				return response;
			}
		} catch (Exception e) {
			logger.error("Exception " + "while deleting StudentLocation :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/editStudentLocation/{id}", method = RequestMethod.GET)
	public Response editStudentLocation(@PathVariable Long id) {

		Response response = new Response();

		try {
			response = studentLocationService.FindByID(id);
			response.setSuccess(true);
			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while finding StudentLocation :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/liststudentLocation", method = RequestMethod.GET)
	public Response listStudentLocation(@PageableDefault(value = 10, page = 0) Pageable pageable) {

		Response response = new Response();
		try {
			Page<StudentLocation> page = studentLocationService.getAll(pageable);
			page.getContent();
			response.setData(page);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing StudentLocation :" + e);
		}
		return response;

	}

}
