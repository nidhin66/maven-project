package com.gbs.grandmaster.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.ChoiceText;
import com.gbs.grandmaster.service.ChoiceTextService;

@RestController
@RequestMapping("/")
public class ChoiceTextRestController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ChoiceTextService choiceTextService;

	@RequestMapping(value = "/saveChoiseText", method = RequestMethod.POST)
	public Response saveChoiseText(@RequestBody ChoiceText choiseText) {

		Response response = new Response();
		try {
			if (null != choiseText) {
				response = choiceTextService.Save(choiseText);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while saving choiceText :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/updateChoiseText", method = RequestMethod.PUT)
	public Response updateChoiseText(@RequestBody ChoiceText choiseText) {

		Response response = new Response();
		try {
			if (null != choiseText) {
				response = choiceTextService.Update(choiseText);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while updating choiceText :" + e);
		}
		return response;
	}

	@RequestMapping(value = "/deleteChoiseText/{id}", method = RequestMethod.GET)
	public Response deleteChoiseText(@PathVariable Long id) {

		Response response = new Response();
		try {
			response = choiceTextService.FindByID(id);
			if (!response.equals(null)) {
				choiceTextService.Delete(id);
				return response;

			} else {
				response.setSuccess(false);
				return response;
			}
		} catch (Exception e) {
			logger.error("Exception " + "while deleting ChoiceText :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/editChoiseText/{id}", method = RequestMethod.GET)
	public Response editChoiseText(@PathVariable Long id) {

		Response response = new Response();

		try {
			response = choiceTextService.FindByID(id);

			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while finding ChoiceText :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/listchoiseText", method = RequestMethod.GET)
	public Response listChoiseText(@PageableDefault(value = 10, page = 0) Pageable pageable) {

		Response response = new Response();
		try {
			Page<ChoiceText> page = choiceTextService.getAll(pageable);
			page.getContent();
			response.setData(page);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing ChoiceText :" + e);
		}
		return response;

	}

}
