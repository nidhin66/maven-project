package com.gbs.grandmaster.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.ExamDefinition;
import com.gbs.grandmaster.service.ExamDefinitionService;

@RestController
@RequestMapping("/")
public class ExamDefinitionRestController {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ExamDefinitionService examDefinitionService;

	@RequestMapping(value = "/saveExamDefinition", method = RequestMethod.POST)
	public Response saveExamDefinition(@RequestBody ExamDefinition examDefinition) {

		Response response = new Response();
		try {
			if (null != examDefinition) {
				response = examDefinitionService.Save(examDefinition);
				response.setSuccess(true);

			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while saving ExamDefinition :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/updateExamDefinition", method = RequestMethod.PUT)
	public Response updateExamDefinition(@RequestBody ExamDefinition examDefinition) {

		Response response = new Response();
		try {
			if (null != examDefinition) {
				response = examDefinitionService.Update(examDefinition);
				response.setSuccess(true);

			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while updating ExamDefinition :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/deleteExamDefinition/{id}", method = RequestMethod.GET)
	public Response deleteExamDefinition(@PathVariable Long id) {

		Response response = new Response();
		try {
			response = examDefinitionService.FindByID(id);
			if (!response.equals(null)) {
				examDefinitionService.Delete(id);

				return response;

			} else {
				response.setSuccess(false);
				return response;
			}
		} catch (Exception e) {
			logger.error("Exception" + "while deleting ExamDefinition :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/editExamDefinition/{id}", method = RequestMethod.GET)
	public Response editExamDefinition(@PathVariable Long id) {

		Response response = new Response();

		try {
			response = examDefinitionService.FindByID(id);

			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while finding ExamDefinition :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/deleteexamDefinitions", method = RequestMethod.GET)
	public Response deleteexamDefinitions() {
		Response response = new Response();
		try {
			if (!response.equals(null)) {
				examDefinitionService.deleteExamDefinitions();
				response.setSuccess(true);
				return response;

			} else {
				response.setSuccess(false);
				return response;
			}
		} catch (Exception e) {
			logger.error("Exception" + "while deleting ExamDefinition :" + e);
		}
		return response;
	}

	@RequestMapping(value = "/listexamDefinition", method = RequestMethod.GET)
	public Response listExamDefinition(@PageableDefault(value = 10, page = 0) Pageable pageable) {

		Response response = new Response();
		try {
			Page<ExamDefinition> page = examDefinitionService.getAll(pageable);
			page.getContent();
			response.setData(page);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing ExamDefinition :" + e);
		}
		return response;

	}

}
