package com.gbs.grandmaster.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.entity.Choice;
import com.gbs.grandmaster.service.ChoiceService;

@RestController
@RequestMapping("/")
public class ChoiceRestController {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ChoiceService choiceService;

	@RequestMapping(value = "/saveChoise", method = RequestMethod.POST)
	public Response saveChoise(@RequestBody Choice choise) {

		Response response = new Response();
		try {
			if (null != choise) {
				response = choiceService.Save(choise);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while saving choice :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/updateChoise", method = RequestMethod.PUT)
	public Response updateChoise(@RequestBody Choice choise) {

		Response response = new Response();
		try {
			if (null != choise) {
				response = choiceService.Update(choise);
				response.setSuccess(true);
			}
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while updating choice :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/deleteChoise/{id}", method = RequestMethod.GET)
	public Response deleteChoise(@PathVariable Long id) {

		Response response = new Response();
		try {
			response = choiceService.FindByID(id);
			if (!response.equals(null)) {
				choiceService.Delete(id);
				return response;

			} else {
				response.setSuccess(false);
				return response;
			}
		} catch (Exception e) {
			logger.error("Exception " + "while deleting choice :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/editChoise/{id}", method = RequestMethod.GET)
	public Response editChoise(@PathVariable Long id) {

		Response response = new Response();

		try {
			response = choiceService.FindByID(id);
			return response;
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while finding choice :" + e);
		}
		return response;

	}

	@RequestMapping(value = "/listchoise", method = RequestMethod.GET)
	public Response listChoise(@PageableDefault(value = 10, page = 0) Pageable pageable) {

		Response response = new Response();
		try {
			Page<Choice> page = choiceService.getAll(pageable);
			page.getContent();
			response.setData(page);
			response.setSuccess(true);
		} catch (Exception e) {
			response.setSuccess(false);
			logger.error("Exception " + "while listing choice :" + e);
		}
		return response;

	}

}
