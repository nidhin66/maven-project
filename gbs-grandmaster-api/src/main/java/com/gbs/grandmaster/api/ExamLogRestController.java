package com.gbs.grandmaster.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.grandmaster.common.entity.ExamStatusLog;
import com.gbs.grandmaster.common.entity.Response;
import com.gbs.grandmaster.service.ExamLogService;

@RestController
@RequestMapping("/") 
public class ExamLogRestController {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ExamLogService examLogService;
	
	@RequestMapping(value = "/saveExamLog", method = RequestMethod.POST)
	public Response saveExamLog(@RequestBody ExamStatusLog statusLog) {

    	Response response=new Response();
    	try {
    	if(null != statusLog) {
    		response = examLogService.saveExamLog(statusLog);
    		response.setSuccess(true);
    	}
    	}
    	catch(Exception e)
    	{
    		response.setSuccess(false);
    		logger.error("Exception " + "while saving ExamAttendance :" + e);
    	}
    	return response;
	}
}
